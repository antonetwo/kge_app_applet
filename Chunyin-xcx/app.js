//app.js
var utils = require('utils/util.js');
App({
    onLaunch: function() {
        // 展示本地存储能力
        var logs = wx.getStorageSync('logs') || []
        logs.unshift(Date.now())
        wx.setStorageSync('logs', logs)
        wx.setEnableDebug({
            enableDebug: true
        })
        this.globalData.cyUser = wx.getStorageSync("cyUser");
        console.log("weqweq" + this.globalData.cyUser)
        var that = this
            // 登录
        wx.setKeepScreenOn({
            keepScreenOn: true
        })
        wx.login({
            success: res => {
                // 发送 res.code 到后台换取 openId, sessionKey, 没有unionId
                console.log()
                wx.request({
                    url: this.globalData.siteUrl + 'xcx/getSessionKey',
                    data: {
                        code: res.code,
                    },
                    header: {
                        'content-type': 'application/json'
                    },
                    success: function(res) {
                        console.log("loginres")
                        console.log(res)
                        if (res.data.code != 0) {
                            console.log(获取openid失败);
                        } else {
                            that.globalData.loginres = res.data.msg
                        }
                        if (that.globalData.cyUser == null || that.globalData.cyUser.id == null) {
                            wx.navigateTo({
                                url: '/pages/index/index'
                            })
                            return;
                        }
                        wx.getSetting({
                            success: res => {
                                if (res.authSetting['scope.userInfo']) {
                                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                                    wx.getUserInfo({
                                        success: res => {
                                            that.globalData.userInfo = res.userInfo
                                            if (that.globalData.unionInfo != null && that.globalData.unionInfo.attrs.unionId != null && that.globalData.unionInfo.attrs.unionId != "") {
                                                that.createUserByWXQQ()
                                            } else {
                                                utils.getUserInfo(that, function(res) {
                                                    if (res != null) {
                                                        that.globalData.cyUser = res;
                                                        console.log(that.globalData.cyUser)
                                                    }
                                                })
                                            }
                                        }
                                    })
                                } else {
                                    wx.navigateTo({
                                        url: '../index/index'
                                    })
                                }
                            }
                        })
                    },
                    fail: function(res) {
                        console.log(res.errMsg)
                    }
                })
            }
        })
    },
    createUserByWXQQ: function() {
        wx.request({
            url: app.globalData.siteUrl + 'xcx/createUserByWXQQ',
            data: {
                "platform": "xcx",
                "openId": app.globalData.unionInfo.attrs.openId,
                'cyType': 0,
                'unionId': app.globalData.unionInfo.attrs.unionId,
                'cyUserName': app.globalData.unionInfo.attrs.nickName,
                'cyImg': app.globalData.unionInfo.attrs.avatarUrl,
                'cySex': app.globalData.unionInfo.attrs.gender,
                'cyCounty': app.globalData.unionInfo.attrs.county,
                'cyProvince': app.globalData.unionInfo.attrs.province,
                'cyCity': app.globalData.unionInfo.attrs.city
            },
            header: {
                'content-type': 'application/json'
            },
            success: function(res) {
                console.log(res)
                if (res.data.code == 0) {
                    console.log("登录成功")
                    console.log(res)
                    app.globalData.cyUser = res.data.msg
                    console.log("app.globalData.cyUser")
                    console.log(app.globalData.cyUser)
                } else {
                    console.log("登录失败")
                }
            },
            fail: function(res) {
                console.log(res.errMsg)
                wx.showToast({
                    title: '网络请求失败',
                    image: 'components/resources/pic/fail.png',
                    duration: 2000
                })
            }
        })
    },
    // getAuthorize: function (callback) {
    //   let that = this
    //   wx.getSetting({
    //     success: res => {
    //       if (res.authSetting['scope.userInfo']) {
    //         that.setData({
    //           authorizeShow: false
    //         })
    //         wx.getUserInfo({
    //           success: res => {
    //           }
    //         })
    //       } else {
    //         console.log(22222)
    //         wx.navigateTo({
    //           url: '../index/index'
    //         })
    //       }
    //     }
    //   })
    // },
    globalData: {
        userInfo: null,
        cyUser: null,
        loginres: null,
        unionInfo: null,
        curplay: { id: 1, mp3Url: "https://jfinal-cms-shop.oss-cn-beijing.aliyuncs.com/icon/%E4%BD%A0%E7%9A%84%E9%85%92%E9%A6%86%E4%B8%BA%E6%88%91%E6%89%93%E4%BA%86%E7%82%80%20%E4%BC%B4%E5%A5%8F.mp3" },
        playStatus: null,
        // siteUrl: "http://localhost:8080/chunyin/",
        siteUrl: "https://18kcy.com/chunyin/",
        // siteUrl: "http://18kcy.com/chunyin/",
        currentPosition: 0
    }
})