//index.js
//获取应用实例
var app = getApp()
var utils = require('../../utils/util.js');
Page({
  data: {
    motto: '',
    userInfo: {},
    unionInfo:null,
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  onLoad: function () {
  },
  getUserInfo: function(e) {
    var that = this
    console.log(e)
    console.log(e.detail)
    console.log(e.detail.userInfo)
    console.log("app.globalData.siteUrl")
    console.log(app)

    wx.getSetting({
      success: res => {
        console.log("wx.getSetting3")
        console.log(res)
        if (res.authSetting['scope.userInfo']) {
          console.log(33333)
          app.globalData.userInfo = e.detail.userInfo

          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              console.log("res3")
              console.log(res)
              // res.encryptedData
              // res.iv
              // res.rawData
              // res.signature

              app.globalData.userInfo = res.userInfo
              console.log("app.globalData.userInfo")
              console.log(app.globalData.userInfo)
              that.getOpenIdandUnionId(res.rawData, res.signature, res.encryptedData, res.iv)
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
          wx.switchTab({
            url: '../ksong/index'
          })
        } else {
          console.log(44444)
          wx.showToast({
            title: '请授权登录',
            icon: 'fail',
            duration: 2000
          })
          wx.navigateTo({
            url: '../index/index'
          })
        }
      }
    }) 
  },
  getOpenIdandUnionId: function (rawData, signature, encryptedData, iv) {
    var that = this 
    wx.login({
      success: res => {
        let path = app.globalData.siteUrl + 'xcx/getSessionKey';
        let data={
          code: res.code
        };
        utils.sendRequest(path, data,res=>{
          console.log("getSessionKey")
          console.log(res)
          if (res.data.code != 0) {
            console.log(res);
          } else {
            app.globalData.loginres = res.data.msg
          }
          wx.request({
            url: app.globalData.siteUrl + 'xcx/getOpenIdandUnionId',
            data: {
              "rawData": rawData,
              "signature": signature,
              'encryptedData': encryptedData,
              'iv': iv,
              'session_key': app.globalData.loginres.session_key
            },
            header: {
              'content-type': 'application/json'
            },
            success: function (res) {
              console.log(res)
              if (res.data.code == 0) {
                console.log("登录成功")
                that.unionInfo = res.data.msg
                app.globalData.unionInfo = res.data.msg
                that.createUserByWXQQ()
              } else {
                console.log("登录失败")
              }
            },
            fail: function (res) {
              console.log(res.errMsg)
              wx.showToast({
                title: '网络请求失败s',
                image: 'components/resources/pic/fail.png',
                duration: 2000
              })
            }
          })
          })
      }})
  },
  createUserByWXQQ: function () {
    wx.request({
      url: app.globalData.siteUrl + 'xcx/createUserByWXQQ',
      data: {
        "platform": "xcx",
        "openId": app.globalData.unionInfo.attrs.openId,
        'cyType': 0,
        'unionId': app.globalData.unionInfo.attrs.unionId,
        'cyUserName': app.globalData.unionInfo.attrs.nickName,
        'cyImg': app.globalData.unionInfo.attrs.avatarUrl,
        'cySex': app.globalData.unionInfo.attrs.gender,
        'cyCounty': app.globalData.unionInfo.attrs.country,
        'cyProvince': app.globalData.unionInfo.attrs.province,
        'cyCity': app.globalData.unionInfo.attrs.city
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          console.log("登录成功")
          app.globalData.cyUser = res.data.msg
          wx.setStorageSync("cyUser", res.data.msg);
          console.log("app.globalData.cyUser")
          console.log(app.globalData.cyUser)
        } else {
          console.log("登录失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败s',
          image: 'components/resources/pic/fail.png',
          duration: 2000
        })
      }
    })
  }
})
