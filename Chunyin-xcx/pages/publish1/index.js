// pages/publish1/index.js
var utils = require('../../utils/util.js');
import Toast from '../../libs/vant/toast/toast';
var record = wx.createInnerAudioContext();
var banzou = wx.createInnerAudioContext();
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    music: {
      kProgress: 60,
      banzou: "",
      banzouV: 0.5,
      record: "",
      recordV: 0.5,
      currentTime: 0,
      rate: 0,
      currentTimeStr: "00:00",
      duration: "00:00",
      status: "ready"
    }
  },
  onSeek: function (e) {
    let rate = e.detail;
    let music = this.data.music;
    let time= music.duration*rate*0.01;
    if (this.data.status=="play"){
       record.seek(time);
       banzou.seek(time);
    }
  },
  goK: function () {
    record.stop();
    banzou.stop();
    wx.redirectTo({
      url: '/pages/k-room/index?id=' + this.data.id
    })
  },
  goPublish2: function () {
    wx.redirectTo({
      url: '/pages/publish2/index',
    })
  },
  doSplicRecord: function () {
    let that = this;
    let data = {
      mp3SentenceKey: that.data.mp3SentenceKey,
      cyUserId: app.globalData.cyUser.id,
      cySongId: that.data.id,
      cyRenShengTime: parseInt(that.data.time*0.001)


      
    }
    console.log(data);
    let path = app.globalData.siteUrl + 'xcx/doSplicRecord';
    utils.sendRequest(path, data, function (res) {
      console.log(res)
      if (res.data.code == 0) {
        that.setData({
          record: res.data.msg
        })
        that.init();
      } else {
        wx.showModal({
          content: '录音合成失败，请返回重唱？',
          showCancel: true,
          cancelText: "取消",
          success(res) {
            if (res.confirm) {
              that.goK();
            } else if (res.cancel) {
              wx.switchTab({
                url: '/pages/ksong/index'
              })
            }
          }
        })
      }
    })
  },
  onPlayChange: function (e) {
    if (this.data.status != "play") {
      this.setData({ status: "play" })
      banzou.play();
      record.play();
    } else {
      this.setData({ status: "pause" })
      banzou.pause();
      record.pause();
    }
  },
  onBanzouV: function (event) {
    let music = this.data.music;
    music.banzouV = event.detail / 100;
    this.setData({
      music: music
    })
    banzou.volume = music.banzouV;
  },
  onRecordV: function (event) {
    let music = this.data.music;
    music.recordV = event.detail / 100;
    this.setData({
      music: music
    })
    record.volume = music.recordV;
  },
  playMusic() {
    let that = this;
    record.volume = this.data.music.recordV;
    console.log(this.data.music.record)
    record.src = this.data.music.record;
    record.play();
    banzou.volume = this.data.music.recordV;
    banzou.src = this.data.music.banzou;
    console.log(banzou.duration)
    let music = that.data.music;

    banzou.autoplay = true;
    banzou.onTimeUpdate(() => {
      let music = that.data.music;
      console.log(banzou.currentTime)
      music.currentTime = banzou.currentTime;
      music.duration = banzou.duration;
      music.currentTimeStr = utils.timeToSec(banzou.currentTime);
      music.durationStr = utils.timeToSec(banzou.duration);
      that.setData({
        music: music
      })
    })
    banzou.onStop(() => {
      record.stop();
      this.setData({ status: "ready" })
    })
    banzou.onEnded(() => {
      this.setData({ status: "ready" });
      record.stop();
    })
    that.setData({
      music: music
    })

  },
  getSong: function () {
    wx.showLoading({
      title: '歌曲合成中···'
    })
    let path = "doComposeSongLive2";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': app.globalData.cyUser.id,
      'cyRecordId': that.data.record.id,
      'cyRenShengValue': that.data.music.recordV * 100,
      'cyBanzhouValue': that.data.music.banzouV * 100,
      'cyBanzhouId': that.data.record.cyBanzhouId
    }
    console.log(data)
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.code == 0) {
          wx.hideLoading();
          let song=that.data.song;
          song.songLiveFileId = res.data.msg.id;
          song.songUrl = res.data.msg.fileUrl
          wx.setStorageSync("song", song);
          wx.redirectTo({
            url: '/pages/publish2/index'
          })
        } else {
          wx.showModal({
            content: '歌曲合成失败，请返回重唱？',
            showCancel: true,
            cancelText: "取消",
            success(res) {
              if (res.confirm) {
                that.goK();
              } else if (res.cancel) {
                wx.switchTab({
                  url: '/pages/ksong/index'
                })
              }
            }
          })
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  init: function () {
    let flag = 0;
    let music = this.data.music;
    let that = this;
    utils.downloadFile(this.data.record.fileUrl).then(record => {
      flag = flag + 1;
      music.record = record;
      if (flag == 2) {
        wx.hideLoading()
        that.setData({
          music: music,
          status: "play"
        })
        wx.hideLoading();
        that.playMusic();
      }
    })
    utils.downloadFile(this.data.record.banzouFileUrl).then(banzou => {
      music.banzou = banzou;
      flag = flag + 1;
      if (flag == 2) {
        wx.hideLoading()
        that.setData({
          music: music,
          status: "play"
        })
        wx.hideLoading();
        that.playMusic();
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let record = wx.getStorageSync("record")
    wx.setNavigationBarTitle({
      title: record.name
    })
    console.log(record)
    this.setData({
      id: record.id,
      mp3SentenceKey: record.mp3SentenceKey,
      cyScore: record.score,
      time: record.time,
      cyUser: app.globalData.cyUser,
      song: record,
      songImg: record.music.data.cyImg
    })
    wx.showLoading({
      title: '录音合成中···',
    })
    let that=this
    setTimeout(function () {
      that.doSplicRecord();
    }, 1500)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    banzou.pause();
    record.pause();
    banzou.stop();
    record.stop();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    banzou.pause();
    record.pause();
    banzou.stop();
    record.stop();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})