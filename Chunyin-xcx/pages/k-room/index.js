// pages/k-room/index.js
var utils = require('../../utils/util.js');
import Toast from '../../libs/vant/toast/toast';
var app = getApp();
var InnerAudioContext = wx.createInnerAudioContext();
var canvas = wx.createCanvasContext('krc')
var timeId=null;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    music: {
      status: "ready",
      url: "https://jfinal-cms-shop.oss-cn-beijing.aliyuncs.com/icon/%E4%BD%A0%E7%9A%84%E9%85%92%E9%A6%86%E5%AF%B9%E6%88%91%E6%89%93%E4%BA%86%E7%83%8A%20-%20%E9%99%88%E9%9B%AA%E5%87%9D.mp3",
      name: "你的酒馆为我打了炀",
      singer: "陈凝雪",
      coverImgUrl: "http://img1.imgtn.bdimg.com/it/u=453587121,3309618994&fm=26&gp=0.jpg",
      currentTime: "00:00",
      durationStr: "03:34",
      time: 0,
      rate: 0,
      dlProgress: 0,
      kProgress: 60,
      lastIndex: -1,
      record: {
        cyNum: 0,
        uploadNum: 0
      },
      lrc: {
        url: "https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/lyric/%E9%99%88%E9%9B%AA%E5%87%9D%20-%20%E4%BD%A0%E7%9A%84%E9%85%92%E9%A6%86%E5%AF%B9%E6%88%91%E6%89%93%E4%BA%86%E7%83%8A.lrc",
        index: 0,
        now_lrc: []
      }
    },
    isBanzou: 1,
    score: 0
  },
  switchOriginal: function () {
    if (this.data.music.data.cyOriginalSongUrl == null || this.data.music.data.cyOriginalSongUrl == ""){
      wx.showToast({
        icon: "none",
        title: '此歌曲暂无原唱',
        duration: 1000
      })
      return;
    }

    let isBanzou = this.data.isBanzou;
    let that = this;
    if (this.data.music.cyOriginalPath) {
      let platform = this.data.platform;
      if (platform =="android"){
        InnerAudioContext.startTime = that.data.music.time2;
      }
      if (isBanzou == 1) {
        InnerAudioContext.src = that.data.music.cyOriginalPath;
        this.setData({
          isBanzou: 0
        })
      }
      else {
        InnerAudioContext.src = this.data.music.path;
        this.setData({
          isBanzou: 1
        })
      }
      if (platform == "ios") {
        setTimeout(function () {
          InnerAudioContext.seek(that.data.music.time2 + 0.5);
          InnerAudioContext.play();
        }, 100)
      }
    } else {
      wx.showLoading({
        title: '原唱正在加载中',
      })
      setTimeout(function () {
        wx.hideLoading()
      }, 1500)
    }
  },
  goPublish: function () {
    let record = {
      "id": this.data.music.data.id,
      "mp3SentenceKey": this.data.music.data.Mp3SentenceKey,
      "name": this.data.music.name,
      "score": this.data.score,
      "time": this.data.music.time,
      "music": this.data.music
    }
    wx.setStorage({
      key: 'record',
      data: record,
    })
    wx.redirectTo({
      url: '/pages/publish1/index'
    })
  },
  getBanzou: function (id, callback) {
    let path = "getBanzou";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': app.globalData.cyUser.id,
      'songId': id
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          let music = that.data.music;
          music.data = res.data.msg;
          music.lrc.url = music.data.cyLyricUrl;
          music.url = music.data.cySongUrl;
          music.name = music.data.cySongName;
          music.singer = music.data.cySinger;
          that.setData({
            "music": music
          });
          callback();
          wx.setNavigationBarTitle({
            title: res.data.msg.cySongName
          })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  init: function () {
    this.setData({
      app: app,
      canvas: canvas
    })
    wx.setNavigationBarTitle({
      title: this.data.music.name
    })
    let key = "banzou-id-" + this.data.music.data.id;
    utils.loadLrc(this);
    utils.parseKrc(this);
    let that = this
    utils.downloadMusic(this, key).then(musicPath => {
      utils.playAlrc(that, InnerAudioContext)
    });
    utils.downloadFile(this.data.music.data.cyOriginalSongUrl).then(path => {
      let music = that.data.music;
      music.cyOriginalPath = path;
      that.setData({
        music: music
      })
    })
    InnerAudioContext.onEnded(() => {
      that.onFinish();
    })
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          platform: res.platform
        })
      }
    })
    that.startOndraw();
  },

  startOndraw(){
    if(this.data.krc==null){
      utils.onDrawNoLrc(this);
      return
    };
    let that=this
   timeId = setInterval(function () {
      utils.onDraw(that, InnerAudioContext)
    }, 20)
  },
  stopOndraw(){
    if (timeId!=null){
      console.log(timeId)
      clearInterval(timeId);
      timeId=null;
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "ctxH": wx.getSystemInfoSync().windowHeight - 150,
      "ctxW": wx.getSystemInfoSync().windowWidth - 40,
      user: app.globalData.cyUser
    });
    this.getBanzou(options.id, () => this.init());
    try {
      wx.removeStorageSync('song');
      wx.removeStorageSync('record');
    }
    catch (err) {
      console.log(err)
    }
  },
  onPlayChange: function () {
    console.log("点击")
    if (this.data.music.status != "play") {
      this.onContinue();
    } else {
      this.onPause();
    }
  },
  reSet: function () {
    let that = this
    wx.showModal({
      content: '录音未保存确定要重唱？',
      showCancel: true,
      cancelText: "取消",
      success(res) {
        if (res.confirm) {
          that.onStop();
          let music = that.data.music;
          let lrc = music.lrc;
          lrc.index = 0;
          music.lrc = lrc;
          music.status = "play";
          music.nosave = true;
          music.currentTime = "00:00";
          music.time = 0,
          music.rate = 0;
          music.record = {
            cyNum: 0,
            uploadNum: 0
          };
          InnerAudioContext.destroy();
          InnerAudioContext = wx.createInnerAudioContext();
          utils.updateMusicK(that, InnerAudioContext);
          that.startOndraw()
          InnerAudioContext.onEnded(() => {
            that.onFinish();
          })
        } else if (res.cancel) {

        }
      }
    })

  },
  onContinue() {
    InnerAudioContext.play();
    let music = this.data.music;
    music.status = "play";
    this.setData({
      music: music
    })
    utils.recordSong(this);
  },
  onPause() {
    InnerAudioContext.pause();
    wx.getRecorderManager().stop();
    let music = this.data.music;
    music.status = "pause";
    this.setData({
      music: music
    })
    this.stopOndraw()
  },
  onStop() {
    let music = this.data.music;
    music.status = "stop";
    this.setData({
      music: music
    })
    InnerAudioContext.stop(); 
    wx.getRecorderManager().stop();
    this.stopOndraw();
  },
  onFinish() {
    this.stopOndraw();
    if (this.data.music.record == null || this.data.music.record.cyNum < 2) {
      wx.showToast({
        icon: "none",
        title: '录音时间太短,再唱一会儿吧',
        duration: 1500
      })
    }
    else {
      this.onStop();
      this.goPublish();
    }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let music = this.data.music;
    let lrc = music.lrc;
    lrc.index = 0;
    music.lrc = lrc;
    music.status = "play";
    music.currentTime = "00:00";
    music.time = 0,
      music.rate = 0;
    music.record = {
      cyNum: 0,
      uploadNum: 0
    };
    this.setData({
      "score": 0,
      music: music
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.onStop();
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.onStop();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})