// pages/song-cover/index.js
var app = getApp();
var utils = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:""
  },
  goCard: function () {
    let that = this;
    if (app.globalData.cyUser.cyCode != "") {
      wx.openCard({
        cardList: [
          {
            cardId: "pVpLc5kVUnsiB9nXWgxriSs9V9_0",
            code: app.globalData.cyUser.cyCode
          }
        ],
        success(res) {
          console.log(res.cardList) // 卡券添加结果
        },
        fail(res) {
          console.log(res)
        }
      })
      return;
    }

    wx.request({
      url: app.globalData.siteUrl + 'xcx/getCardExt',
      data: {},
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          console.log(res.data.msg.cardExt)
          wx.addCard({
            cardList: [
              {
                cardId: res.data.msg.cardId,
                cardExt: res.data.msg.cardExt
              }
            ],
            success(res) {
              let code = res.cardList[0].code;
              let cardId = res.cardList[0].cardId;
              that.getCode(cardId, code)
            },
            fail(res) {
              console.log(res)
            }
          })
        }
      }
    })
  },
  getCode: function (cardId, encrypt_code) {
    let that = this;
    wx.request({
      url: app.globalData.siteUrl + 'xcx/getCode',
      data: {
        "encrypt_code": encrypt_code,
        'cyUserId': app.globalData.cyUser.id,
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          app.globalData.cyUser.cyCode = res.data.msg;
          wx.openCard({
            cardList: [
              {
                cardId: cardId,
                code: res.data.msg
              }
            ],
            success(res) {
              console.log(res.cardList) // 卡券添加结果
            },
            fail(res) {
              console.log(res)
            }
          })
        }
      }
    })
  },
  goKSong:function(){
    let that=this;
    wx.showModal({
      content: '使用App K歌效果更佳哦',
      showCancel: true,
      cancelText: "继续K歌",
      confirmText:"去APP",
      success(res) {
        if (res.confirm) {
          that.goCard();
        } else if (res.cancel) {
          wx.authorize({
            scope: 'scope.record',
            success() {
              wx.redirectTo({
                url: '/pages/k-room/index?id=' + that.data.music.id
              })
            },
            fail() {
              console.log("第一次录音授权失败");
              wx.showModal({
                title: '提示',
                content: '您未授权录音，功能将无法使用',
                showCancel: true,
                confirmText: "授权",
                success: function (res) {
                  if (res.confirm) {
                    //确认则打开设置页面（重点）
                    wx.openSetting({
                      success: (res) => {
                        console.log(res.authSetting);
                        if (!res.authSetting['scope.record']) {
                          //未设置录音授权
                          wx.showModal({
                            title: '提示',
                            content: '您未授权录音，功能将无法使用',
                            showCancel: false,
                            success: function (res) {

                            },
                          })
                        } else {
                          wx.redirectTo({
                            url: '/pages/k-room/index?id=' + that.data.music.id
                          })
                        }
                      },
                      fail: function () {
                        console.log("授权设置录音失败");
                      }
                    })
                  } else if (res.cancel) {
                    wx.switchTab({
                      url: '/pages/ksong/index',
                    })
                  }
                },
                fail: function () {
                  console.log("openfail");
                }
              })
            }
          })
        }
      }
    })
  }, 
  getBanzou: function (id) {
    let path = "getBanzou";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': app.globalData.cyUser.id,
      'songId':id
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          let music = res.data.msg;
          music.duration = utils.timeToSec(music.cySongTime);
          that.setData({ "music": music })
          wx.setNavigationBarTitle({
            title: res.data.msg.cySongName
          })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getBanzou(options.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})