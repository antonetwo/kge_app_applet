// pages/ksong/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    keywords: "",
    isShowCancel: false,
    slidePicture: [],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    content: "song",
    dianchang: { data: [], pageNumber: 1 },
    biaosheng: { "data": [], pageNumber: 1 },
    xinge: { "data": [], pageNumber: 1 },
    musics1:{}
  },
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
    this.freshPage(e.detail.current);
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.currentTarget.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.currentTarget.dataset.current
      })
      this.freshPage(that.data.currentTab);
    }
  },
  onShowCancel: function () {
    this.setData({ isShowCancel: true, content: "keywords" })
  },
  onCancel: function () {
    this.setData({ isShowCancel: false, content: "song" })
  },
  onViewResult: function (e) {
    this.setData({ content: "result" })
  },
  goSongCover: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/song-cover/index?id=' + id
    })
  },
  goListen: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/listen/index?id=' + id,
    })
  },
  goK: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/k-room/index?id=" + id,
    })
  },
  freshPage: function (e) {
    let cyType = this.data.currentTab;
    let key = "dianchang";
    if (cyType == 1) {
      key = "biaosheng";
    } else if (cyType == 2) {
      key = "xinge";
    }
    let musics = this.data[key];
    if (musics.end) {
      return;
    }
    let pageNumber = musics.pageNumber;
    this.getSongRank(cyType, pageNumber);
  },
  getSongRank: function (cyType, pageNumber) {
    let that = this;
    wx.request({
      url: app.globalData.siteUrl + 'xcx/getSongRank',
      data: {
        "platform": "xcx",
        'cyType': cyType,
        'pageNumber': pageNumber,
        'pageSize': 20,
        'cyUserId': app.globalData.cyUser.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          let key = "dianchang";
          if (cyType == 1) {
            key = "biaosheng";
          } else if (cyType == 2) {
            key = "xinge";
          }
          let musics = that.data[key];
          musics.pageNumber = pageNumber+1;
          if (res.data.msg.length == 0) {
            musics.end = true;
          }
          for (let o of res.data.msg) {
            musics.data.push(o);
          }
          if (cyType == 0) {
            that.setData({
              "dianchang": musics
            })
          } else if (cyType == 1) {
            that.setData({
              "biaosheng": musics
            })
          }
          else if (cyType == 2) {
            that.setData({
              "xinge": musics
            })
          }
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  getSlidePicture: function () {
    let that = this;
    wx.request({
      url: app.globalData.siteUrl + 'xcx/getSlidePicture',
      data: {
        "platform": "xcx",
        'cyType': 1,
        'cyUserId': app.globalData.cyUser.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          that.setData({ slidePicture: res.data.msg })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  getBanZouBySingerOrSong1: function () {
    let musics1 = this.data.musics1;
    if (musics1.pageNumber == null)
      return;
    musics1.pageNumber = musics1.pageNumber + 1;
    this.setData({ musics1: musics1 })
  },
  getBanZouBySingerOrSong: function (event) {
    let that = this;
    let pageNumber = 1;
    if (this.data.musics1.pageNumber){
      pageNumber = this.data.musics1.pageNumber;
    }
    if (event.detail){
      this.data.musics1={};
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/getBanZouBySingerOrSong',
      data: {
        "platform": "xcx",
        'singerOrSong': event.detail,
        'pageNumber': pageNumber,
        "pageSize": 100,
        'cyUserId': app.globalData.cyUser.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          let musics1 = that.data.musics1;
          if(musics1.list==null){
            musics1.list=[];
          }
          for (let o of res.data.msg.list) {
            musics1.list.push(o);
          }
          that.setData({ musics1: res.data.msg })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  getBanZouSpinner: function (e) {
    console.log(e)
    let that = this;
    wx.request({
      url: app.globalData.siteUrl + 'xcx/getBanZouSpinner',
      data: {
        "platform": "xcx",
        'singerOrSong': singerOrSong,
        'cyUserId': app.globalData.cyUser.id
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          that.setData({ musics2: res.data.msg })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ "wH1": wx.getSystemInfoSync().windowHeight - 54 });
    this.setData({ "wH": wx.getSystemInfoSync().windowHeight - 210 });
    this.getSlidePicture();
    this.getSongRank(0, 1)
    this.getSongRank(1, 1)
    this.getSongRank(2, 1)
    wx.setNavigationBarTitle({
      title: 'K歌',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})