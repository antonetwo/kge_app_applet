// pages/publish1/index.js
var app = getApp();
var InnerAudioContext = wx.createInnerAudioContext();
import Toast from '../../libs/vant/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    music: {
      kProgress: 60,
      currentTime: "02:23",
      duration: "03:34",
    },
    cyPhotoIds: "",
    images: [],
    status: "ready"
  },
  onPlayChange: function (e) {
    if (this.data.status != "play") {
      this.setData({ status: "play" })
      this.onPlay();
    } else {
      this.setData({ status: "pause" })
      InnerAudioContext.pause();
    }
  },
  onPlay: function () {
    if (InnerAudioContext.paused) {
      InnerAudioContext.src = this.data.song.songUrl;
      InnerAudioContext.play();
      InnerAudioContext.onEnded(()=>{
        this.setData({ status: "ready" })
      })
      InnerAudioContext.onError((res) => {
        console.log(res.errMsg)
        console.log(res.errCode)
      })
    }
    else {
      if (InnerAudioContext.paused)
        InnerAudioContext.play();
    }
    InnerAudioContext.onPlay(() => {
      this.setData({ status: "play" })
    })
    InnerAudioContext.onPause(() => {
      this.setData({ status: "pause" })
    })
  },
  chooseImage: function () {
    let that = this;
    let images = that.data.images;
    wx.chooseImage({
      count: 8 - images.length,
      sizeType: ['compressed'],
      sourceType: ['album'],
      success(res) {
        let images = that.data.images;
        for (let o of res.tempFilePaths) {
          images.push(o);
          that.uploadImg(o);
        }
        that.setData({ images: images })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let song =wx.getStorageSync("song");
    let record=wx.getStorageSync("record");
    this.setData({
      song:song,
      music: record.music
    })
    wx.setNavigationBarTitle({
      title: song.name,
    })
    this.getBanzou(song.id)
  },
  goPerson: function () {
    wx.switchTab({
      url: '/pages/person/index',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  
  chooseLocation: function () {
    let that = this
    wx.chooseLocation({
      success: function (res) {
        that.setData({ location: res })
      }
    })
  },
  getBanzou: function (id) {
    let path = "getBanzou";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': app.globalData.cyUser.id,
      'songId': id
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          that.setData({
            "music": res.data.msg
          });
          wx.setNavigationBarTitle({
            title: res.data.msg.cySongName
          })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },

  uploadImg: function (file) {
    let that = this;
    let params = "&userId=" + app.globalData.cyUser.id
      + "&cyType=0"
    console.log(file)
    const uploadTask = wx.uploadFile({
      url: app.globalData.siteUrl + 'client/uploadImg?' + params,
      filePath: file,
      name: 'img',
      success(res) {
        console.log(res)
        let cyPhotoIds = that.data.cyPhotoIds;
        let json = JSON.parse(res.data);
        if (cyPhotoIds != "") {
          cyPhotoIds += "_" + json.msg.id;
        } else {
          cyPhotoIds = json.msg.id;;
        }
        that.setData({ cyPhotoIds: cyPhotoIds })
      }
    })

    uploadTask.onProgressUpdate((res) => {
      console.log('上传进度', res.progress)
      console.log('已经上传的数据长度', res.totalBytesSent)
      console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
    })

  },
  doFabu: function () {
    if (this.data.song==null){
      Toast("音频合成中，请稍候···");
      return;
    }
    let path = "doFabu";
    let that = this;
    let cyLat = "";
    let cyLng = "";
    if (that.data.location != null) {
      cyLat = that.data.location.latitude;
      cyLng = that.data.location.longitude;
    }
    let data = {
      "platform": "xcx",
      'cyUserId': app.globalData.cyUser.id,
      'cyScore': that.data.song.cyScore || 0,
      "cyPhotoIds": that.data.cyPhotoIds,
      "songLiveFileId": that.data.song.songLiveFileId,
      'songId': that.data.song.id,
      "cyMood": that.data.cyMood || "",
      "cyLocationFlag": that.data.location != null ? 1 : 0,
      "cyLat": cyLat,
      "cyLng": cyLng
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          wx.redirectTo({
            url: '/pages/listen/index?id=' + res.data.msg
          })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  bindinput: function (event) {
    this.setData({ "cyMood": event.detail.value });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    InnerAudioContext.stop();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    InnerAudioContext.stop();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})