// pages/rank/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    allweb: { data: [], pageNumber: 1 },
    nearby: { data: [], pageNumber: 1 },
    location: null
  },
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
    this.freshPage(e)
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.currentTarget.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.currentTarget.dataset.current
      })
      this.freshPage(e);
    }
  },
  goListen: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/listen/index?id=' + id,
    })
  },
  goOther: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/other/index?id=' + id,
    })
  },
  goK: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/k-room/index?id=' + id,
    })
  },
  onTipsInfo: function (e) {
    let content = "全网排行榜发布歌曲的评分进行排序，等你来秀翻全场哦！每个用户仅显示最高得分作品哦"
    if (e.target.dataset.type == "nearby") {
      content = "附近热歌榜根据附近城市的作品获得的礼物价值进行排序！每个用户只选取成绩最好的作品进行打榜"
    }
    wx.showModal({
      content: content,
      showCancel: false,
      confirmText: "我知道了",
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  freshPage: function (e) {
    let cyType = this.data.currentTab;
    let location = this.data.location;
    let that = this;
    if (cyType == 1) {
      that.getSongRank(1);
    } else {
      that.getSongRank(0);
    }
  },
  getSongRank: function (cyType) {
    let key = "allweb";
    let path = "getTopHundred";
    let that = this;
    let data = {
      "platform": "xcx",
      'pageNumber': 1,
      'pageSize': 20,
      'cyUserId': app.globalData.cyUser.id
    }
    if (cyType == 1) {
      key = "nearby";
      path = "getNearHundred";
      if (location == null) {
        wx.getLocation({
          type: 'wgs84',
          success(res) {
            location = { latitude: res.latitude, longitude: res.longitude }
            that.setData({ location: location });
          },
          fail(res){
            location = { latitude:0, longitude: 0 }
            that.setData({ location: location });
          }
        })
      } else {
        data.lat = that.data.location.latitude;
        data.lng = that.data.location.longitude;
      }
    }
    let musics = that.data[key];
    if (musics.end) {
      return;
    }
    data.pageNumber = musics.pageNumber;
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          if (res.data.msg.list.length == 0) {
            musics.end = true;
          }
          musics.pageNumber = musics.pageNumber + 1;
          for (let o of res.data.msg.list) {
            musics.data.push(o);
          }
          if (cyType == 0) {
            that.setData({
              "allweb": musics
            })
          } else if (cyType == 1) {
            that.setData({
              "nearby": musics
            })
          }
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  goSongCover: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/song-cover/index?id=' + id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ "wH": wx.getSystemInfoSync().windowHeight - 88 });
    this.getSongRank(0);
    this.getSongRank(1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})