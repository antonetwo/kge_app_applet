// pages/listen/index.js
var utils = require('../../utils/util.js');
import Toast from '../../libs/vant/toast/toast';
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperData: {
      imgUrls: [
        'https://images.unsplash.com/photo-1551334787-21e6bd3ab135?w=640',
        'https://images.unsplash.com/photo-1551214012-84f95e060dee?w=640',
        'https://images.unsplash.com/photo-1551446591-142875a901a1?w=640'
      ],
      indicatorDots: false,
      autoplay: true,
      interval: 5000,
      duration: 2000
    },
    music: {
      data: {},
      status: "ready",
      url: "",
      currentTime: "00:00",
      duration: "00:00",
      rate: 0,
      lrc: {
        url: "",
        index: 0,
        now_lrc: []
      }
    },
    tabbar: {
      index: 3
    },
    commentShow: false
  },
  goOther: function (e) {
    let id = e.currentTarget.dataset.id;
    if (id == app.globalData.cyUser.id) {
      wx.switchTab({
        url: '/pages/person/index',
      })
    } else {
      wx.navigateTo({
        url: '/pages/other/index?id=' + id,
      })
    }
  },
  showComment: function (event) {
    this.setData({
      commentShow: true
    })
  },
  hiddenComment: function (e) {
    if (e.target.id == "comment") {
      this.setData({
        commentShow: false
      })
    }
  },
  getMusic: function (songLiveId, callback) {
    console.log("getSongLive")
    let path = "getSongLive";
    let that = this;
    let data = {
      "platform": "xcx",
      "songLiveId": songLiveId,
      'userId': app.globalData.cyUser.id
    }
    console.log(data)
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          let music = that.data.music;
          music.data = res.data.msg;
          music.lrc.url = music.data.cyLyricUrl;
          music.url = music.data.cySongLiveUrl;
          music.name = music.data.cySongName;
          music.singer = music.data.userName;
          that.setData({ "music": music });
          callback();
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  onPlay: function () {
    utils.playAlrc(this);
  },
  onPause: function () {
    wx.getBackgroundAudioManager().pause()
    let music = this.data.music;
    music.status = "pause"
    this.setData({
      music: music
    })
  },
  onPlayChange: function (e) {
    var curTime = e.timeStamp
    var lastTime = e.currentTarget.dataset.time
    if (curTime - lastTime > 0) {
      if (curTime - lastTime < 300) {
        console.log(222)
        if (this.data.music.status != "play") {
          this.onPlay();
        } else {
          this.onPause();
        }
      }

    }
    this.setData({
      lastTapTime: curTime
    })
  },
  onSeek: function (e) {
    let rate = e.detail;
    this.setData({ sliderRate: rate })
    utils.seek(this, rate);
  },
  showLrc: function () {

  },
  init: function () {
    this.getSongLiveComments();
    wx.setNavigationBarTitle({
      title: this.data.music.data.cySongName
    })
    wx.getBackgroundAudioManager().stop();
    utils.loadLrc(this);
    this.doListenSongLive();
  },
  goSongCover: function (e) {
    this.setData({
      tabbar: { index: 3 }
    })
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/song-cover/index?id=' + id
    })
  },
  getSongLiveComments: function () {
    let that = this;
    let path = "getSongLiveComments";
    let data = {
      "platform": "xcx",
      "songLiveId": that.data.music.data.id,
      'cyCommentUserId': app.globalData.cyUser.id
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          let comments = res.data.msg.songLiveComments;
          for (let o of comments) {
            o.timeStr = utils.formatTime(o.cyCreateTimeUTC * 1000, 2)
          }

          that.setData({ comments: comments })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  changeCommentValue: function (event) {
    this.setData({ cyContent: event.detail });
  },
  doSongLiveComment: function () {
    this.setData({
      tabbar: { index: 3 }
    })
    let that = this;
    let cyContent = that.data.cyContent;
    if (cyContent == null || cyContent == "") {
      return;
    }
    let path = "doSongLiveComment";
    let data = {
      "platform": "xcx",
      "cyContent": cyContent,
      "songLiveId": that.data.music.data.id,
      'cyCommentUserId': app.globalData.cyUser.id
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          that.setData({
            commentShow: false
          })
          Toast("评论成功")
          that.getSongLiveComments();
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  sendFlowers: function () {
    let that = this;
    if (that.data.music.data.cyUserId == app.globalData.cyUser.id) {
      Toast("不能送花给自己哦");
      return;
    }
    that.setData({
      tabbar: { index: 3 }
    })
    let path = "sendFlowers";
    let data = {
      "platform": "xcx",
      "cyFlowerNum": 1,
      "cyIncomeUserId": that.data.music.data.cyUserId,
      'cySendUserId': app.globalData.cyUser.id,
      'songLiveId': that.data.music.data.id,
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res.data)
        if (res.data.code == 0) {
          Toast("送花成功");
          let music = that.data.music;
          let data = music.data;
          data.cyFlowerNum = data.cyFlowerNum + 1;
          music.data = data;
          that.setData({
            music: music
          })
          app.globalData.cyUser.cyFlowerNum--;
        } else {
          Toast(res.data.error);
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  doListenSongLive: function () {
    let path = "doListenSongLive";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': app.globalData.cyUser.id,
      'songLiveId': that.data.music.data.id
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          let music = that.data.music;
          let data = music.data
          data.cyListenNum = data.cyListenNum + 1;
          music.data = data;
          that.setData({ "music": music });
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ "wh1": wx.getSystemInfoSync().windowHeight });
    this.setData({ "wh2": wx.getSystemInfoSync().windowHeight - 345 });
    this.getMusic(options.id, () => this.init());
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    wx.getBackgroundAudioManager().stop();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.getBackgroundAudioManager().stop();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var that = this
    var p = '/pages/listen/index?id=' + that.data.music.data.id;
    var imgs = that.data.music.data.userPhotos;
    let img = that.data.music.data.userImgUrl;
    if (imgs != null && imgs.length > 0) {
      img = imgs[0];
    }
    return {
      title: that.data.music.name,
      path: p,
      imageUrl: img,
      success: function (res) {
        if (yuyinId != null) {
          let path = app.globalData.siteUrl + 'xcx/shareUrl';
          let that = this;
          let data = {
            "platform": "xcx",
            'cyUserId': app.globalData.cyUser.id,
            'songLiveId': that.data.music.data.id
          }
          utils.sendRequest(path, data);
        }
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})