// pages/person/index.js
var app = getApp();
import Toast from '../../libs/vant/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  goCard: function () {
    let that = this;
    if (app.globalData.cyUser.cyCode != "") {
      wx.openCard({
        cardList: [
          {
            cardId: "pVpLc5kVUnsiB9nXWgxriSs9V9_0",
            code: app.globalData.cyUser.cyCode
          }
        ],
        success(res) {
          console.log(res.cardList) // 卡券添加结果
        },
        fail(res) {
          console.log(res)
        }
      })
      return;
    }

    wx.request({
      url: app.globalData.siteUrl + 'xcx/getCardExt',
      data: {},
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          console.log(res.data.msg.cardExt)
          wx.addCard({
            cardList: [
              {
                cardId: res.data.msg.cardId,
                cardExt: res.data.msg.cardExt
              }
            ],
            success(res) {
              let code = res.cardList[0].code;
              let cardId = res.cardList[0].cardId;
              that.getCode(cardId, code)
            },
            fail(res) {
              console.log(res)
            }
          })
        }
      }
    })
  },
  getCode: function (cardId, encrypt_code) {
    let that = this;
    wx.request({
      url: app.globalData.siteUrl + 'xcx/getCode',
      data: {
        "encrypt_code": encrypt_code,
        'cyUserId': app.globalData.cyUser.id,
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          app.globalData.cyUser.cyCode = res.data.msg;
          wx.openCard({
            cardList: [
              {
                cardId: cardId,
                code: res.data.msg
              }
            ],
            success(res) {
              console.log(res.cardList) // 卡券添加结果
            },
            fail(res) {
              console.log(res)
            }
          })
        }
      }
    })
  },
  goListen: function (e) {
    wx.navigateTo({
      url: '/pages/listen/index?id=' + e.currentTarget.dataset.id
    })
  },
  getMySongLives: function (pageNumber,id) {
    let path = "getMySongLives";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': id,
      "pageNumber": pageNumber,
      "pageSize": 20
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          let musics = that.data.musics;
          if (musics == null) {
            musics = [];
          }
          for (let o of res.data.msg.list) {
            musics.push(o);
          }
          that.setData({ musics: musics })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  getUserInfo: function (id) {
    let path = "getUserInfo";
    let that = this;
    let data = {
      "platform": "xcx",
      'cyUserId': id,
    }
    wx.request({
      url: app.globalData.siteUrl + 'xcx/' + path,
      data: data,
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.code == 0) {
          wx.setNavigationBarTitle({
            title: res.data.msg.cyUserName+"的个人主页"
          })
          that.setData({ "cyUser": res.data.msg })
        } else {
          console.log("请求失败")
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
        wx.showToast({
          title: '网络请求失败',
          image: 'images/fail.png',
          duration: 2000
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserInfo(options.id);
    this.getMySongLives(1, options.id);
    this.setData({
      "wH": wx.getSystemInfoSync().windowHeight - 140
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var that = this
    var p = '/pages/other/index?id=' + that.data.cyUser.id;
    return {
      title: that.data.cyUser.cyUserName + "的作品",
      imageUrl: that.data.cyUser.cyImg,
      path: p,
      success: function (res) {
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})