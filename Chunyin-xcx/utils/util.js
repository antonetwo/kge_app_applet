var bsurl = "https://127.0.0.1/";
if (typeof requestAnimationFrame == 'undefined') {
  var lastTime = 0;
  var requestAnimationFrame = function(callback) {
    var currTime = new Date().getTime();
    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
    var id = setTimeout(function() {
      callback();
    }, timeToCall);
    lastTime = currTime + timeToCall;
    return id;
  };
}
if (typeof cancelAnimationFrame == 'undefined') {
  var cancelAnimationFrame = function(id) {
    clearTimeout(id);
  };
}

function formatTime(date, type) {
  type = type || 1;
  //type 1,完成输出年月日时分秒，2对比当前时间输出日期，或时分;
  var d = new Date(date)
  var year = d.getFullYear()
  var month = d.getMonth() + 1
  var day = d.getDate()

  var hour = d.getHours()
  var minute = d.getMinutes()
  var second = d.getSeconds();
  if (type == 1) {
    return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':');
  } else {
    var current = new Date();
    var curtimes = current.getTime();
    if ((curtimes - date) < 24 * 3600000) {
      if (curtimes - date < 3600000) {
        return (new Date(curtimes - date)).getSeconds() + "分钟前";
      } else {
        return "今天：" + [hour, minute].map(formatNumber).join(':');
      }

    } else if ((curtimes - date) < 48 * 3600000) {
      return "昨天：" + [hour, minute].map(formatNumber).join(':');

    } else if (year != current.getFullYear()) {
      return year + "年" + month + "月" + day + "日"
    } else {
      return month + "月" + day + "日"
    }
  }
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function timeToSec(duration) {
  let sec = parseInt(duration);
  let m = parseInt(sec / 60);
  let s = parseInt(sec % 60);
  return formatNumber(m) + ":" + formatNumber(s)
}

function formatduration(duration) {
  if (duration == null)
    return "00:00";
  duration = new Date(duration);
  let mint = duration.getMinutes();
  let sec = duration.getSeconds();
  return formatNumber(mint) + ":" + formatNumber(sec);
}

function parse_lrc(filePath, lrc_content) {
  if (!lrc_content && !filePath) {
    return;
  }
  if (!lrc_content && filePath)
    lrc_content = wx.getFileSystemManager().readFileSync(filePath, "utf-8")
  if (lrc_content == null) return;
  let now_lrc = [];
  if (filePath == null)
    lrc_content = lrc_content.replace(/[[]/g, '\n[');
  let lrc_row = lrc_content.split("\n");
  let scroll = true;
  for (let i in lrc_row) {
    if ((lrc_row[i].indexOf(']') == -1) && lrc_row[i]) {
      now_lrc.push({
        lrc: lrc_row[i]
      });
    } else if (lrc_row[i] != "") {
      var tmp = lrc_row[i].split("]");
      for (let j in tmp) {
        scroll = false
        let tmp2 = tmp[j].substr(1, 8);
        tmp2 = tmp2.split(":");
        let lrc_sec = parseFloat(tmp2[0] * 60 + tmp2[1] * 1);
        if (lrc_sec && (lrc_sec > 0)) {
          let count = tmp.length;
          let lrc = trimStr(tmp[count - 1]);
          if (lrc != "") {
            now_lrc.push({
              lrc_sec: lrc_sec,
              lrc: lrc
            });
          }

        }
      }
    }
  }
  if (!scroll) {
    now_lrc.sort(function(a, b) {
      return a.lrc_sec - b.lrc_sec;
    });
  }
  return {
    now_lrc: now_lrc,
    scroll: scroll
  };
}

function trimStr(str) {
  return str.replace(/(^\s*)|(\s*$)/g, "");
}

//音乐播放
function playAlrc(that, InnerAudioContext) {
  let music = that.data.music;
  music.status = "play";
  if (that.data.music.status == "stop") {
    music.lrc.index = 0;
    music.currentTime = "00:00";
    music.rate = 0;
    if (music.record) {
      InnerAudioContext.play();
    } else {
      wx.getBackgroundAudioManager().play();
    }
  } else if (that.data.music.status == "pause") {
    if (music.record) {
      InnerAudioContext.play();
    } else {
      wx.getBackgroundAudioManager().play();
    }
  } else {
    if (music.record) {
      updateMusicK(that, InnerAudioContext)
    } else {
      updateMusicPlay(that)
    }
  }
  that.setData({
    music: music
  })
};

function loadRes(that) {
  downloadFile(that.data.music.lrc.url).then(lrcPath => {
    if (lrcPath != null && lrcPath != "") {
      let result = parse_lrc(lrcPath)
      let lrc = that.data.music.lrc;
      lrc.now_lrc = result.now_lrc;
      lrc.scroll = result.scroll;
      lrc.show = true;
      that.setData({
        lrc: lrc
      })
    }
  });
  // downloadFile(that.data.music.url).then(musicPath => {
  //   let music = that.data.music;
  //   console.log(musicPath)
  //   music.path = musicPath;
  //     that.setData({
  //       music: music
  //     })
  //   }
  // );
}

function loadLrc(that) {
  if (that.data.music.data.cyLyricStr==""){return;}
  let result = parse_lrc(null, that.data.music.data.cyLyricStr);
  if (!result)
    return;
  let lrc = that.data.music.lrc;
  lrc.now_lrc = result.now_lrc;
  lrc.scroll = result.scroll;
  lrc.show = true;
  that.setData({
    lrc: lrc
  })
}

function downloadFile(url) {
  console.log(url)
  return new Promise((resolve, reject) => {
    if (url == null || url == "") {
      resolve(null);
      return;
    }
    let path = wx.getStorageSync(url + "");
    if (path != null && path.length > 0) {
      resolve(path)
    } else {
      wx.downloadFile({
        url: url,
        success(res) {
          wx.setStorageSync(url + "", res.tempFilePath)
          resolve(res.tempFilePath)
        }
      })
    }
  })
}

function downloadMusic(that, key) {
  console.log(key);
  let music = that.data.music;
  return new Promise((resolve, reject) => {
    let path = wx.getStorageSync(key + "");

    console.log("path==========")
    console.log(path)
    let flag = false;
    if (path != null && path.length > 0) {
      wx.getFileInfo({
        filePath: path,
        success(res) {
          music.dlProgress = 100;
          music.path = path;
          that.setData({
            music: music
          })
          flag = true;
          resolve(path);
        },
        fail(res) {
          const downloadTask = wx.downloadFile({
            url: music.url,
            success(res) {
              music.path = res.tempFilePath;
              wx.setStorageSync(key, res.tempFilePath)
              that.setData({
                music: music
              })
              resolve(res.tempFilePath)
            }
          })
          downloadTask.onProgressUpdate((res) => {
            console.log('下载进度', res.progress)
            music.dlProgress = res.progress;
            that.setData({
              music: music
            })
          })
        }
      })
    } else {
      const downloadTask = wx.downloadFile({
        url: music.url,
        success(res) {
          music.path = res.tempFilePath;
          wx.setStorageSync(key, res.tempFilePath)
          that.setData({
            music: music
          })
          resolve(res.tempFilePath)
        }
      })
      downloadTask.onProgressUpdate((res) => {
        console.log('下载进度', res.progress)
        music.dlProgress = res.progress;
        that.setData({
          music: music
        })
      })
    }
  })
}


function updateMusicPlay(that) {
  const BackgroundAudioManager = wx.getBackgroundAudioManager();
  let music = that.data.music;
  BackgroundAudioManager.epname = music.name;
  BackgroundAudioManager.title = music.name;
  BackgroundAudioManager.singer = music.singer;
  music.duration = formatduration(BackgroundAudioManager.duration)
  if (music.rate != 0) {
    BackgroundAudioManager.startTime = BackgroundAudioManager.duration * music.rate / 100;
  }
  BackgroundAudioManager.src = music.path || music.url;
  let index = music.lrc.index;
  let length = music.lrc.now_lrc.length;
  BackgroundAudioManager.onTimeUpdate(() => {
    index = that.data.music.lrc.index;
    let flag = false;
    for (let i = music.lrc.now_lrc.length - 1; i >= 0; i--) {
      music.lrc.now_lrc[i].current = false;
      if (!flag && music.lrc.now_lrc[i].lrc_sec <= BackgroundAudioManager.currentTime) {
        music.lrc.index = i;
        music.lrc.now_lrc[i].current = true;
        flag = true;
      }
    }
    if (index == 0) {
      music.duration = timeToSec(BackgroundAudioManager.duration);
    }
    music.rate = (BackgroundAudioManager.currentTime / BackgroundAudioManager.duration) * 100;
    music.currentTime = timeToSec(BackgroundAudioManager.currentTime);
    if (music.rate == null) {
      music.music = "stop"
    }
    that.setData({
      music: music
    })
  })
  BackgroundAudioManager.onEnded(() => {
    console.log("Music is stop")
    music.rate = 0;
    music.status = "stop";
    music.lrc.index = 0;
    that.setData({
      music: music
    })
  })
  BackgroundAudioManager.onError(function callback(res) {
    console.log(res)
  })
  BackgroundAudioManager.onStop(function callback(res) {
    let recorderManager = wx.getRecorderManager();
    recorderManager.stop();
  })
}


function updateMusicK(that, InnerAudioContext) {
  let music = that.data.music;
  music.duration = formatduration(InnerAudioContext.duration)
  InnerAudioContext.startTime = music.time * 0.001;
  let isBanzou = that.data.isBanzou;
  if (isBanzou == 1) {
    InnerAudioContext.src = music.path || music.url;
  } else {
    InnerAudioContext.src = music.cyOriginalPath;
  }

  InnerAudioContext.autoplay = true;
  InnerAudioContext.play();
  if (music.record)
    recordSong(that)
  InnerAudioContext.onPlay(() => {});
  InnerAudioContext.onTimeUpdate(() => {
    music.time = InnerAudioContext.currentTime * 1000;
    music.time2 = InnerAudioContext.currentTime;
    music.durationStr = timeToSec(InnerAudioContext.duration);
    music.duration = timeToSec(InnerAudioContext.duration)
    music.rate = (InnerAudioContext.currentTime / InnerAudioContext.duration) * 100;
    music.currentTime = timeToSec(InnerAudioContext.currentTime);
    music.status = "play";
    that.setData({
      music: music
    })
  })
}

function recordSong(that) {
  let record = that.data.music.record;
  let music = that.data.music;
  if (!record) return;
  let index = record.cyNum;
  record.cyStartTime = index * 1000 * 10;
  record.cyEndTime = record.cyStartTime + 10000;
  record.cyNum = index;
  record.Mp3SentenceKey = that.data.music.data.Mp3SentenceKey;
  if (index == 0) {
    let data = music.data;
    data.Mp3SentenceKey = new Date().getTime();
    music.data = data;
    record.Mp3SentenceKey = data.Mp3SentenceKey;
  }
  music.record = record;
  that.setData({
    music: music
  })
  const options = {
    duration: 10000,
    sampleRate: 44100,
    numberOfChannels: 1,
    encodeBitRate: 192000,
    format: 'mp3',
    frameSize: 50
  }
  let recorderManager = wx.getRecorderManager();
  recorderManager.start(options);
  recorderManager.onStop((res) => {
    record = that.data.music.record;
    record.file = res.tempFilePath;
    recordStop(that, record);
  })
  recorderManager.onInterruptionBegin((res) => {

  })
  recorderManager.onError((res) => {

  })
}

function reSet(that) {
  wx.showModal({
    content: '录制发生错误',
    showCancel: true,
    cancelText: "返回",
    confirmText: "重唱",
    success(res) {
      if (res.confirm) {
        let music = that.data.music;
        music.status = "play";
        music.currentTime = "00:00";
        music.time = 0,
          music.rate = 0;
        music.record = {
          cyNum: 0,
          uploadNum: 0
        };
        that.setData({
          "score": 0,
          music: music
        })
        updateMusicK(that, InnerAudioContext);
      } else if (res.cancel) {
        wx.switchTab({
          url: '/pages/ksong/index'
        })
      }
    }
  })

}

function recordStop(that, record) {
  if (that.data.music.nosave) {} else {
    saveMp3Sentence(that, record)
  }
  if (that.data.music.status == "play") {
    let record = that.data.music.record;
    let music = that.data.music;
    record.cyNum++;
    music.record = record;
    music.nosave = false;
    that.setData({
      music: music
    })
    recordSong(that)
  }

}

function saveMp3Sentence(that, record) {
  let params = "cySongId=" + that.data.music.data.id +
    "&cyUserId=" + that.data.app.globalData.cyUser.id +
    "&mp3SentenceKey=" + record.Mp3SentenceKey +
    "&cyType=1&cyStartTime=" + record.cyStartTime +
    "&cyEndTime=" + record.cyEndTime +
    "&cySongTime=" + parseInt(that.data.music.duration) +
    "&cyNum=" + record.cyNum;


  console.log(params)
  wx.uploadFile({
    url: that.data.app.globalData.siteUrl + 'xcx/saveMp3Sentence?' + params,
    filePath: record.file,
    name: 'file',
    success(res) {
      console.log(res.data)
      try {
        let krc = that.data.krc;
        let txt = krc!=null?krc[that.data.music.index].words:"";
        if (txt==""||(that.data.music.status != "pause" && txt.indexOf("：") == -1 && txt.indexOf("-") == -1)) {
          console.log(txt)
          let uploadNum = that.data.record;
          that.setData({
            "score": that.data.score + JSON.parse(res.data).msg,
            uploadNum: uploadNum + 1
          })
        } else {
         
        }
      } catch (e) {
        console.log(e)
      }
    }
  })
}

function seek(that, rate) {
  let music = that.data.music;
  music.rate = rate;
  const BackgroundAudioManager = wx.getBackgroundAudioManager();
  let currentTime = BackgroundAudioManager.duration * rate / 100;
  console.log(currentTime);
  let flag = false;
  for (let i = music.lrc.now_lrc.length - 1; i >= 0; i--) {
    music.lrc.now_lrc[i].current = false;
    if (!flag && music.lrc.now_lrc[i].lrc_sec <= currentTime) {
      music.lrc.index = i;
      flag = true;
      music.lrc.now_lrc[i].current = true;
    }
  }
  that.setData({
    music: music
  })
  if (BackgroundAudioManager.paused) {
    playAlrc(that);
  } else {
    BackgroundAudioManager.seek(currentTime);
  }
}

function sendRequest(path, data, callback) {
  wx.request({
    url: path,
    data: data,
    header: {
      'content-type': 'application/json'
    },
    success: function(res) {
      if (res.data.code == 0) {
        callback(res)
      } else {
        console.log("请求失败")
      }
    },
    fail: function(res) {
      console.log(res.errMsg)
    }
  })
}

function parseKrc(that) {
  let lrcText = that.data.music.data.cyKrcLyricStr;
  if (!lrcText)
    return;
  //保存最终结果的数组    
  let result = [];
  let lines = lrcText.split('\r\n');
  let noLrc = /[(ar|ti|offset):(.+)]/
  let isLrc = /[(d{0,8}),(d{0,8})](.+)/
  //用于匹配时间的正则表达式，匹配的结果类似[xxx,xxx]    
  for (let o of lines) {
    if (!noLrc.test(o) && o != "") {
      let t = o.split(']')[0].replace("[", "");
      let lrc = o.split(']')[1];
      let words = [];
      let ws = [];
      let ss = [];
      let lens = [];
      let wStr = ""
      for (let w of lrc.split("<")) {
        if (w == "") continue;
        let lrct = w.split(">")[0];
        let word = w.split(">")[1];
        ws.push(word);
        ss.push(lrct.split(",")[0]);
        lens.push(lrct.split(",")[1])
        wStr += word;
      }
      let line = {
        start: t.split(",")[0],
        len: t.split(",")[1],
        ws: ws,
        ss: ss,
        lens: lens,
        words: wStr
      };
      result.push(line)
    }
  }
  that.setData({
    krc: result
  })
  //去掉不含时间的行    
  console.log(result);
}

function onDrawNoLrc(that){
  let ctx = that.data.canvas;
  ctx.setFillStyle('#323232')
  ctx.setTextAlign("center")
  ctx.font = 'normal bold 17px sans-serif';
  let w = that.data.ctxW;
  let h = that.data.ctxH;
  ctx.fillText("暂无歌词", w / 2, h/2-50);
  ctx.draw();
}

function onDraw(that, InnerAudioContext) {
  let krc = that.data.krc;
  let music = that.data.music;
  music.time = InnerAudioContext.currentTime * 1000;
  let time = music.time;
  for (let i = 0; i < krc.length; i++) {
    if (krc[i].start < time && (i + 1 == krc.length|| krc[i+1].start > time)) {
      music.index = i;
    }
  }
  let ctx = that.data.canvas;
  let lineindex = music.index;
  let lastIndex = music.lastIndex;
  let w = that.data.ctxW;
  let h = that.data.ctxH;
  ctx.setFillStyle('#323232')
  ctx.setTextAlign("center")
  if (!krc) return;
  if (lineindex == null) {
    lineindex = 0;
  }
  let lineH = (w - 30) / 10 > 30 ? 30 : (w - 30) / 10;
  let ln = krc.length - lineindex < 10 ? krc.length - lineindex : 10;
  for (let i = lineindex; i < krc.length && i < lineindex + 10; i++) {
    let top = (h - ln * lineH) / 2 + (i - lineindex) * lineH;
    if (i == lineindex) {
      ctx.font = 'normal bold 19px sans-serif';
    } else {
      ctx.font = 'normal bold 17px sans-serif';
    }
    let words = krc[i].words;
    ctx.fillText(words, w / 2, top);
  }

  music.lastIndex = lineindex;
  that.setData({
    music: music
  })
  if (time > krc[lineindex].start && (lineindex + 1 == krc.length || time <= krc[lineindex + 1].start)) {
    let dtime = time - krc[lineindex].start;
    let ssTime = krc[lineindex].ss;
    let wRect = 0;
    for (let j = 0; j < ssTime.length; j++) {
      if (ssTime[j] >= dtime && ssTime[j] + krc[lineindex].lens[j] > dtime) {
        let wws = krc[lineindex].words;
        ctx.font = 'normal bold 19px sans-serif';
        let maxLen = ctx.measureText(wws.substr(0, j + 1)).width;
        let minLen = ctx.measureText(wws.substr(0, j)).width;
        let len = ctx.measureText(wws).width;
        let width = that.data.ctxW;
        wRect = width * 0.5 - len * 0.5 + minLen + (maxLen - minLen) * (dtime - ssTime[j]) / dtime;
        break;
      } else if (j == ssTime.length-1){
        ctx.font = 'normal bold 19px sans-serif';
        wRect = that.data.ctxW;
      }
    }
    let tp = (h - ln * lineH) / 2;
    ctx.rect(0, tp - lineH, wRect, tp + lineH);
    ctx.clip();
    ctx.setFillStyle('#8238e3');
    ctx.fillText(krc[lineindex].words, w / 2, tp);
  }
  ctx.draw();
}

function getUserInfo(app, callback) {
  let data = {
    "platform": "xcx",
    'cyUserId': app.globalData.cyUser.id,
  }
  wx.request({
    url: app.globalData.siteUrl + 'xcx/getUserInfo',
    data: data,
    header: {
      'content-type': 'application/json'
    },
    success: function(res) {
      if (res.data.code == 0) {
        callback(res.data.msg)
      } else {
        callback()
      }
    },
    fail: function(res) {
      callback()
    }
  })
}

module.exports = {
  formatTime: formatTime,
  formatduration: formatduration,
  timeToSec: timeToSec,
  playAlrc: playAlrc,
  updateMusicPlay: updateMusicPlay,
  seek: seek,
  loadRes: loadRes,
  loadLrc: loadLrc,
  downloadFile: downloadFile,
  downloadMusic: downloadMusic,
  sendRequest: sendRequest,
  updateMusicK: updateMusicK,
  recordSong: recordSong,
  parseKrc: parseKrc,
  onDraw: onDraw,
  onDrawNoLrc: onDrawNoLrc,
  getUserInfo: getUserInfo,
  cancelAnimationFrame: cancelAnimationFrame,
  requestAnimFrame: requestAnimationFrame
}