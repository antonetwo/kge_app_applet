
Pod::Spec.new do |spec|
  spec.name         = 'TXIMSDK_iOS'
  spec.version      = '4.4.716'
  spec.platform     = :ios 
  spec.ios.deployment_target = '8.0'
  spec.license      = { :type => 'Proprietary',
      :text => <<-LICENSE
        copyright 2017 tencent Ltd. All rights reserved.
        LICENSE
       }
  spec.homepage     = 'https://cloud.tencent.com/document/product/269/3794'
  spec.documentation_url = 'https://cloud.tencent.com/document/product/269/9147'
  spec.authors      = 'tencent video cloud'
  spec.summary      = 'TXIMSDK_iOS'
  
  spec.requires_arc = true

  spec.source = { :git => 'https://github.com/tencentyun/TIMSDK.git', :tag => spec.version}

  spec.preserve_paths = '18kcy/18kcy/MLVB/IMSDK/ImSDK.framework'
  spec.source_files = '18kcy/18kcy/MLVB/IMSDK/ImSDK.framework/Headers/*.h'
  spec.public_header_files = '18kcy/18kcy/MLVB/IMSDK/ImSDK.framework/Headers/*.h'
  spec.vendored_frameworks = '18kcy/18kcy/MLVB/IMSDK/ImSDK.framework'
  spec.xcconfig = { 'HEADER_SEARCH_PATHS' => '${PODS_ROOT}/18kcy/MLVB/IMSDK/ImSDK/Framework/ImSDK.framework/Headers/'}
end