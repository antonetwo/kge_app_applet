//
//  UIImageView+K18ClickAvatar.m
//  18kcy
//
//  Created by beuady on 2019/8/21.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "UIImageView+K18ClickAvatar.h"
#import "K18LiveService.h"
#import <objc/runtime.h>

static char *kBB = "kBB";

@implementation UIImageView (K18ClickAvatar)

- (void)setUserId:(NSString *)userId
{
    objc_setAssociatedObject(self, &kBB,userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)userId
{
    return objc_getAssociatedObject(self, &kBB);
    
}


- (void) setupClickWithUserId:(NSString *)userId
{
    self.userId = userId;
    if(self.userInteractionEnabled==NO){
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAvatar)];
        [self addGestureRecognizer:tap];
        self.userInteractionEnabled = YES;
    }
}

- (void) clickAvatar {
    [K18LiveService gotoPersonDetail:self.userId parent:nil];
}

@end
