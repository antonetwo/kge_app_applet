//
//  UIImageView+K18ClickAvatar.h
//  18kcy
//
//  Created by beuady on 2019/8/21.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (K18ClickAvatar)

@property(nonatomic, strong)NSString *userId;

- (void) setupClickWithUserId:(NSString*)userId;

@end

NS_ASSUME_NONNULL_END
