//
//  K18TabBarController.m
//  18kcy
//
//  Created by beuady on 2019/6/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18TabBarController.h"
#import "K18NavigationController.h"
#import "K18MainController.h"
#import "K18SongController.h"
#import "K18MineController.h"
#import "MLVBLiveRoom.h"
#import "TCLiveListModel.h"
#import "TCLiveListViewController.h"
#import "User.h"
#import "K18ChatListController.h"
#import "LoginService.h"
#import "UITabBar+DLBadge.h"
#import "TCUserInfoModel.h"

@import TXLiteAVSDK_Smart;

@interface K18TabBarController ()<UITabBarControllerDelegate>
@property (nonatomic, strong) MLVBLiveRoom *liveRoom;
@property(strong, nonatomic) UIViewController *chatVC;
@end

@implementation K18TabBarController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    [self initLiveRoom];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beLahei:) name:k18_laheiAccountEvent object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMsgs:) name:k18_onNewMessageEvent object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTotalUnreadUpdate:) name:@"k18TotalUnreadEvent" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(synRoomInfo:) name:k18IMSyncUserInfoEvent object:nil];
    
}

- (void) synRoomInfo:(NSNotification *)notify {
    NSString *faceUrl;
    NSString *nickname;
    int cySex = 0;
    if (K18UserModel.shared.currentUserDetail) {
        faceUrl = K18UserModel.shared.currentUserDetail.cyImg;
        nickname = K18UserModel.shared.currentUserDetail.cyUserName;
        cySex = K18UserModel.shared.currentUserDetail.cySex.intValue;
    }else if(K18UserModel.shared.currentUser){
        faceUrl = K18UserModel.shared.currentUser.cyImg;
        nickname = K18UserModel.shared.currentUser.cyUserName;
        cySex = K18UserModel.shared.currentUser.cySex.intValue;
    }else{
        faceUrl = [TCUserInfoModel sharedInstance].getUserProfile.faceURL;
        nickname = [TCUserInfoModel sharedInstance].getUserProfile.nickName;
    }
    
    if(faceUrl.length){
        [[TCUserInfoModel sharedInstance] saveUserFace:faceUrl handler:^(int errCode, NSString *strMsg) {
            NSLog(@"saveUserFace %@",strMsg);
        }];
    }
    if(nickname.length){
        [[TCUserInfoModel sharedInstance] saveUserNickName:nickname handler:^(int errCode, NSString *strMsg) {
            NSLog(@"saveUserNickName %@",strMsg);
        }];

    }
    
    [TCUserInfoModel sharedInstance].getUserProfile.gender = cySex;
    
}

- (void) onTotalUnreadUpdate:(NSNotification*)notify {
    NSInteger unread = [notify.object integerValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(unread){
            self.tabBar.badgeSize = CGSizeMake(8, 8);
            [self.tabBar showBadgeOnItemIndex:3];
            
        }else{
            [self.tabBar hiddenRedPointOnIndex:3 animation:NO];
        }
    });
}

- (void) onNewMsgs:(NSNotification *)notify {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *list = notify.object;
        if(list){
            self.tabBar.badgeSize = CGSizeMake(8, 8);
            [self.tabBar showBadgeOnItemIndex:3];

        }else{
            [self.tabBar hiddenRedPointOnIndex:3 animation:NO];
        }
    });
}



- (void) beLahei:(NSNotification *)notify {
    NSString *tip = [NSString stringWithFormat:@"%@",notify.object];
    [HUDHelper alert:tip action:^{
        [LoginService logout];
    }];
}

- (void)initLiveRoom
{
    self.liveRoom = [MLVBLiveRoom sharedInstance];
    [TCLiveListMgr sharedMgr].liveRoom = self.liveRoom;
}

- (void) setupUI {
	
	UIViewController *vc1,*vc2,*vc3,*vc4,*vc5;
	
	vc1 = [K18MainController new];
	vc2 = [TCLiveListViewController new];
	vc3 = [K18SongController new];
	vc4 = [K18ChatListController new];
	vc5 = [K18MineController new];
    self.chatVC = vc4;
   
    
    self.viewControllers = @[vc1, vc2, vc3, vc4, vc5];
    
	[self addChildViewController:vc1 imageName:@"faxian_hui" selectedImageName:@"faxian_xuanze" title:@"发现"];
	[self addChildViewController:vc2 imageName:@"zhibo_hui" selectedImageName:@"zhibo_xuanze" title:@"直播"];
	[self addChildViewController:vc3 imageName:@"Kge_hui" selectedImageName:@"Kge_xuanze" title:@"K歌"];
	[self addChildViewController:vc4 imageName:@"xiaoxi_hui" selectedImageName:@"xiaoxi_xaunze" title:@"消息"];
	[self addChildViewController:vc5 imageName:@"wode_hui" selectedImageName:@"wode_xuanze" title:@"我的"];
    
    self.tabBar.tintColor = k18_TopicC;
    
    self.delegate = self; // this make tabBaController call
    [self setSelectedIndex:2];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    return YES;
}

#pragma mark - private

- (void)addChildViewController:(UIViewController *)childController imageName:(NSString *)normalImg selectedImageName:(NSString *)selectImg title:(NSString *)title {
    K18NavigationController *nav = [[K18NavigationController alloc] initWithRootViewController:childController];
    childController.tabBarItem.image = normalImg ? [[UIImage imageNamed:normalImg] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] : nil;
	
	UIImage *selectImage = [[UIImage imageNamed:selectImg] imageWithTintColor:k18_TopicC];
	
    childController.tabBarItem.selectedImage = selectImg ? [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] : nil;
    childController.title = title;
    
    [self addChildViewController:nav];
}




@end
