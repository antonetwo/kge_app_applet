//
//  K18BaseController.m
//  18kcy
//
//  Created by 唐 on 2019/6/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import <ZLPhotoBrowser/ZLPhotoActionSheet.h>


@interface K18BaseController ()<UIGestureRecognizerDelegate>

@end

@implementation K18BaseController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor whiteColor];
    
    
	[self initUI];
	[self initData];
}

- (void)dealloc
{
	NSLog(@"%@ dealloc",self.class);
}

- (void) initUI {
	//并没有所有的子类都调用父类方法
}

- (void) initData {
	//并没有所有的子类都调用父类方法
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if (self.forbidGuestureBack && [self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
		self.navigationController.interactivePopGestureRecognizer.delegate = self;
	}
    self.lastNaviHidden = self.navigationController.navigationBarHidden;
    
//    [self.navigationController.navigationBar.topItem.leftBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:0.1], NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateNormal];

}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	if (self.forbidGuestureBack && [self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
		self.navigationController.interactivePopGestureRecognizer.delegate = nil;
	}
}

- (UIBarButtonItem *)leftBarBackItem
{
	if (!_leftBarBackItem) {
		
		_leftBarBackItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back arrow"] style:(UIBarButtonItemStylePlain) target:self action:@selector(leftBarButtonAction)];
	}
	return _leftBarBackItem;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer*)gestureRecognizer
{
	
	return !self.forbidGuestureBack;
	
}

- (BOOL) checkPhone:(NSString*)phone {
    if (phone.length==0) {
        [[HUDHelper sharedInstance] tipMessage:@"请输入手机号"];
        return NO;
    }
    
    if (!phone.isMobilphone) {
        [[HUDHelper sharedInstance] tipMessage:@"请输入正确的手机号"];
        return NO;
    }
    return YES;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{

        [self.view endEditing:YES];

}

- (void) updateTimeLabel:(UILabel *)label curTime:(CGFloat)curTimeNum totalTime:(CGFloat)totalTime {
	NSUInteger curTime = ceil(curTimeNum);
	NSUInteger curSec = curTime%60;
	NSUInteger curMin = curTime/60;
	NSUInteger duration = ceil(totalTime);
	NSUInteger tSec = duration%60;
	NSUInteger tMin = duration/60;
	
	if (curMin == tMin) {
		curSec = MIN(curSec, tSec);
	}
	
	label.text = [NSString stringWithFormat:@"%02lu:%02lu/%02lu:%02lu",curMin,curSec,tMin,tSec];
}

- (void) updateTimeLabel:(UILabel *)label curTime:(CGFloat)curTimeNum {
    NSUInteger curTime = ceil(curTimeNum);
    NSUInteger curSec = curTime%60;
    NSUInteger curMin = curTime/60;
        
    label.text = [NSString stringWithFormat:@"%02lu:%02lu",curMin,curSec];
}


- (void) chooseImage:(void(^)(UIImage *image, PHAsset * asset)) callback {
	ZLPhotoActionSheet *ac = [[ZLPhotoActionSheet alloc] init];
	ac.configuration.navBarColor = k18_TopicC;
	ac.configuration.bottomBtnsNormalTitleColor = k18_TopicC;
	ac.configuration.maxSelectCount = 1;
	ac.configuration.maxPreviewCount = 10;
	ac.configuration.allowTakePhotoInLibrary = YES;
	ac.configuration.showCaptureImageOnTakePhotoBtn = YES;
	ac.configuration.allowEditImage = YES;
	ac.configuration.allowSelectVideo = NO;
	ac.configuration.allowSelectGif = NO;
	ac.configuration.allowSelectOriginal = NO;
	ac.configuration.showSelectBtn = NO;
	
	
	ac.sender = self;
	[ac showPhotoLibrary];
	
//	__weak typeof(self)wself = self;
	[ac setSelectImageBlock:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nonnull assets, BOOL isOriginal) {
		
		if (callback) {
			callback(images.firstObject, assets.firstObject);
		}
		
	}];
}


@end
