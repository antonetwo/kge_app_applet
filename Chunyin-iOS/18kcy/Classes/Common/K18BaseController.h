//
//  K18BaseController.h
//  18kcy
//
//  Created by 唐 on 2019/6/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Common.h"
#import "HUDHelper.h"
#import "LoginService.h"
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN

@protocol K18BaseProtocol <NSObject>

- (void) initUI;
- (void) initData;

@end

@interface K18BaseController : UIViewController<K18BaseProtocol>

@property(nonatomic) BOOL forbidGuestureBack;//Default=NO
@property(strong, nonatomic) UIBarButtonItem *leftBarBackItem;//需要实现leftBarButtonAction方法
@property(assign, nonatomic) BOOL lastNaviHidden;
// base
- (void) leftBarButtonAction;

//functions
- (BOOL) checkPhone:(NSString*)phone;
- (void) updateTimeLabel:(UILabel *)label curTime:(CGFloat)curTimeNum;
- (void) updateTimeLabel:(UILabel *)label curTime:(CGFloat)curTimeNum totalTime:(CGFloat)totalTime;
- (void) chooseImage:(void(^)(UIImage *image, PHAsset * asset)) callback;


@end

NS_ASSUME_NONNULL_END
