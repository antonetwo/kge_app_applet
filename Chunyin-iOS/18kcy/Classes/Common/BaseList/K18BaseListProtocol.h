//
//  K18BaseListProtocol.h
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol K18BaseListProtocol <NSObject>
@required

//- (int) listType;
- (NSString *) requestPath;
- (NSString *) cellNibName;
- (NSDictionary *)requestExtraParams;
- (Class) modelClass;

@optional
- (NSString *) noneDataTip;

@end

NS_ASSUME_NONNULL_END
