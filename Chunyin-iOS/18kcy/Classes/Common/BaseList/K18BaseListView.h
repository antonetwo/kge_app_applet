//
//  K18BaseListView.h
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPageListView.h"
#import "K18Common.h"
#import "K18BaseListProtocol.h"


NS_ASSUME_NONNULL_BEGIN

/**
 需要实现K18BaseListProtocol,才能使用
 */
@interface K18BaseListView : UIView< JXPageListViewListDelegate,UITableViewDataSource, UITableViewDelegate,K18BaseListProtocol>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) BOOL isNeedHeader;
@property (nonatomic, assign) BOOL isFirstLoaded;

@property(nonatomic,weak) UIViewController *rootVC;
@property(copy, nonatomic) void(^onRefreshData)(void);

@end

NS_ASSUME_NONNULL_END
