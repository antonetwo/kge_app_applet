//
//  K18BaseListView.m
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseListView.h"

#import <MJRefresh/MJRefresh.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "K18SongCell.h"
#import "K18SongService.h"
#import "K18BaseCell.h"

@interface K18BaseListView()
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) NSIndexPath *lastSelectedIndexPath;
@property(nonatomic) NSInteger pageNumber;
@property(nonatomic) BOOL isPageMax;
@property(strong, nonatomic) UILabel *noDataLabel;
@end

@implementation K18BaseListView

- (void)dealloc
{
    self.scrollCallback = nil;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isFirstLoaded = YES;
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.tableFooterView = [UIView new];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        [self.tableView registerNib:[UINib nibWithNibName:self.cellNibName bundle:nil] forCellReuseIdentifier:@"cell"];
        [self addSubview:self.tableView];
        
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
        
        _noDataLabel = [UILabel new];
        _noDataLabel.textAlignment = NSTextAlignmentCenter;
        _noDataLabel.textColor = RGBOF(0x323232);
        _noDataLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_noDataLabel];
        [_noDataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(100);
//            make.bottom.equalTo(self);
            make.height.mas_equalTo(20);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
    }
    return self;
}

- (void)setIsNeedHeader:(BOOL)isNeedHeader {
    _isNeedHeader = isNeedHeader;
    
    __weak typeof(self)weakSelf = self;
    if (self.isNeedHeader) {
        self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
			[weakSelf requestData:nil];
        }];
    }
}

- (void)loadMoreDataTest {
    //    [self.dataSource addObject:@"加载更多成功"];
    [self.tableView reloadData];
}

#pragma mark - Data service

- (void) requestData:(void(^)(void)) callback {
    self.isPageMax = NO;
    self.pageNumber = 0;
    __weak typeof(self)wself = self;
    NSDictionary *params = @{ @"pageNumber":@(self.pageNumber+1),@"pageSize":@(kPageSize)};
    NSMutableDictionary *ps = params.mutableCopy;
    if(self.requestExtraParams){
        [ps addEntriesFromDictionary:self.requestExtraParams];
    }
    [self sendWithPath:self.requestPath params:ps succ:^(id  _Nonnull data) {
        [wself.tableView.mj_header endRefreshing];
        wself.pageNumber ++;
        
        [wself dealDataSourceWithJson:data];
        [wself.tableView reloadData];
        [wself setRefreshFooter];
        
        if (self.dataSource.count==0 && [wself respondsToSelector:@selector(noneDataTip)]) {
            
            wself.noDataLabel.text = [wself noneDataTip];
            wself.noDataLabel.hidden = NO;
        }else{
            wself.noDataLabel.hidden = YES;
        }
        
		if(callback)callback();
    } failed:^(NSError * _Nonnull error) {
        [wself.tableView.mj_header endRefreshing];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
		if(callback)callback();
    }];
    
}

- (void) dealDataSourceWithJson:(id)json {
    NSArray *dataList;
    if([json isKindOfClass:[NSDictionary class]]){
        dataList = json[@"list"];
    }else{
        dataList = json;
    }
    NSArray *list = [NSArray yy_modelArrayWithClass:self.modelClass json:dataList];
    K18Song *m1 = list.lastObject;
    K18Song *m2 = self.dataSource.lastObject;
    if (!m1) {
        self.isPageMax = YES;
        [self setRefreshFooter];
        return;
    }
    
    if ([m1.id isEqualToString:m2.id]) {
        self.isPageMax = YES;
        [self setRefreshFooter];
        return;
    }
    
    if(m2==nil){//刚开始
        if(list.count<kPageSize){
            self.isPageMax = YES;
            [self setRefreshFooter];
        }
    }
    
    if (self.dataSource.count) {
        [self.dataSource addObjectsFromArray:list];
    }else{
        self.dataSource = list.mutableCopy;
    }
    
}

- (void)  setRefreshFooter {
    if (!self.isPageMax) {
        //模拟加载完数据之后添加footer
        __weak typeof(self)wself = self;
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            
            NSDictionary *params = @{ @"pageNumber":@(wself.pageNumber+1),@"pageSize":@(kPageSize)};
            NSMutableDictionary *ps = params.mutableCopy;
            if(self.requestExtraParams){
                [ps addEntriesFromDictionary:self.requestExtraParams];
            }
            
            [wself sendWithPath:self.requestPath params:ps succ:^(id  _Nonnull data) {
                [wself.tableView.mj_footer endRefreshing];
                wself.pageNumber ++;
                [wself dealDataSourceWithJson:data];
                [wself.tableView reloadData];
//                [wself setRefreshFooter];
            } failed:^(NSError * _Nonnull error) {
                [wself.tableView.mj_footer endRefreshing];
                [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
            }];
            
        }];
    }else {
        self.tableView.mj_footer = nil;        
    }
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id<K18BaseCell> cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ((UITableViewCell *)cell).selectionStyle = UITableViewCellSelectionStyleNone;

    K18Song *model = self.dataSource[indexPath.row];
    cell.model = model;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cclass = NSClassFromString(self.cellNibName);
    id<K18BaseCell> cell = cclass.class;
    return [cell.class cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback?:self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)listViewLoadDataIfNeeded {
	NSLog(@"listViewLoadDataIfNeeded");
    if (!self.isFirstLoaded) {
        return;
    }
    self.isFirstLoaded = NO;
    if (self.isNeedHeader) {
        [self.tableView.mj_header beginRefreshing];
    }else {
        
		[self requestData:nil];
        
    }
    
}

- (void)listViewRefreshData:(void (^)(void))callback
{
	[self requestData:callback];
    if(self.onRefreshData){
        self.onRefreshData();
    }
}


@end
