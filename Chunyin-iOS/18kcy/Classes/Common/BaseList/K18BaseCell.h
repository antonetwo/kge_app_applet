//
//  K18BaseCell.h
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol K18BaseCell <NSObject>
@property(strong, nonatomic) id model;
+ (CGFloat) cellHeight;
@end

NS_ASSUME_NONNULL_END
