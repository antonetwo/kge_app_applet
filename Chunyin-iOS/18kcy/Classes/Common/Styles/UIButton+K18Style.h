//
//  UIButton+K18Style.h
//  18kcy
//
//  Created by 唐 on 2019/6/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Common.h"
NS_ASSUME_NONNULL_BEGIN

@interface UIButton (K18Style)
- (void) k18_setStyle;//背景、字体、圆角

- (void) k18_setStyleC:(NSUInteger)corner;

- (void) k18_setCircleBG:(UIColor*)bgColor;

@end

NS_ASSUME_NONNULL_END
