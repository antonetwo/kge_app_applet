//
//  UIButton+K18Style.m
//  18kcy
//
//  Created by 唐 on 2019/6/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "UIButton+K18Style.h"

@implementation UIButton (K18Style)

- (void) k18_setStyle {
    self.backgroundColor = k18_TopicC;

    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
	self.layer.cornerRadius = self.height/2;
    self.layer.masksToBounds = YES;
}

- (void) k18_setStyleC:(NSUInteger)corner {
    self.backgroundColor = k18_TopicC;
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.layer.cornerRadius = corner;
    self.layer.masksToBounds = YES;
}

- (void)k18_setCircleBG:(UIColor *)bgColor
{
    self.backgroundColor = bgColor;
    self.layer.cornerRadius = self.width/2.0;
    self.layer.masksToBounds = YES;
}

@end
