//
//  K18Common.h
//  18kcy
//	全局通用宏
//  Created by 唐 on 2019/6/4.
//  Copyright © 2019 502papapa. All rights reserved.
//


#ifndef K18Common_h
#define K18Common_h

#import "TCConstants.h"
#import "ColorMacro.h"
#import "CommonMacro.h"
#import "RequestAPI.h"

//工具套件
#import "NSString+XTExtension.h"
#import "NSString+Common.h"
#import "UIImage+Additions.h"

//UI套件
#import "UIView+Additions.h"
#import "UIButton+K18Style.h"
#import "HUDHelper.h"
#import <YYModel/YYModel.h>
#import <UserDefaultsHelper/UserDefault.h>
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+K18ClickAvatar.h"

//业务公共
#import "K18UserModel.h"
#import "LoginService.h"

#define IsTestMode 0 //测试模式

//默认头像地址
#define kDefaultIconUrl @"https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/touxiangImg.png"

//1可切换平台0不可切换
#define ISTEST            1

//小直播日志开关, 注释了等于屏蔽，
//#define DebugLog 1
#import "TCUtil.h"

//每次都需要登录
#define NEED_LOGIN_EVERYTIMES 0
//每次都需要修改头像和昵称
#define NEED_EDIT_EVERYTIMES 0
//每次都需要出现主播认证页面
#define NEED_LIVE_AUTHORATION 1
//录制是否要10秒才允许结束
#define NEED_SONG_10 1   //发布时候应该为1


//平台类型（0测试平台，1正式平台)
#define PLANTFORM        0

//上传图片压缩比
#define kCompressRate 0.8

#define notNil(x) (x?x:@"")

// 统一分页数量
#define kPageSize 10


//UI
#define k18_DefaultIcon @"touxiang" //默认头像
#define k18_TopicC RGBOF(0x7f35e5) //0xF45A8D
#define k18_DefaultBG @"kgezhiwang"
#define k18_DefaultCover @"zhibo_fengmian" //直播封面
#define k18_DefaultSinger @"default_singer" //歌手默认头像


#define HUDShowDeveloping [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"]

#define k18IMSyncUserInfoEvent @"k18IMSyncUserInfoEvent"//同步自己的个人用户信息到im服务器和小直播服务器
#define K18Event_RESONG @"k18event_resong" //重唱
#define k18_LoginSuccessEvent @"k18_loginSuccesEvent"
#define k18_dashangEvent @"k18_dashangEvent" //通知打开打赏界面
#define k18_laheiEvent @"k18_laheiEvent" //通知直播间拉黑某人
#define k18_laheiAccountEvent @"k18_laheiAccountEvent" //通知当前账号被拉黑
#define k18_closeLiveRoomEvent @"k18_closeLiveRoomEvent" //通知关闭当前直播的直播间
#define k18_onNewMessageEvent @"k18_onNewMessageEvent" //通知接收新消息, 用于小红点
#define k18_RefreshMineEvent @"k18_RefreshMineEvent" //我的页面刷新
#endif /* K18Common_h */
