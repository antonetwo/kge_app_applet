//
//  K18AudioService.h
//  18kcy
//
//  Created by 唐 on 2019/6/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVPlayer.h"
NS_ASSUME_NONNULL_BEGIN


/**
 录音模块功能通知回调事件
 */
//#define K18Event_onAudioStop @"onAudioStop"
//#define K18Event_onAudioProgress @"onAudioProgress"
#define K18Event_onVolumnChange @"onVolumnChange"
#define K18Event_onRecordProgress @"onRecordProgress"
#define K18Event_onRecordStop @"onRecordStop"
#define K18Event_onCombineComplete @"onCombineComplete"

typedef void (^RCTPromiseResolveBlock)(id result);
typedef void (^RCTPromiseRejectBlock)(NSString *code, NSString *message, NSError *error);

@interface K18AudioService : NSObject

@property(readonly, nonatomic) VVPlayer *player;

+(instancetype)shared;

//- (void) startRecordWithURL:(NSURL*)bgmFileURL resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock) reject;
- (void) startOnlyRecordWithURL:(NSURL *)bgmFileURL resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject;
- (void) stopRecord:(BOOL)immediate;
- (void) stopRecord:(BOOL)immediate comletion:(RCTPromiseResolveBlock)resolve;
- (void) pauseRecord;
- (void) resumeRecord;
- (BOOL) isRecording;


/**
 合成

 @param recordFilePath
 @param bgmFileURL
 @param duration 录音时长
 @param srcVolume 0~1
 @param bgmVolume 0~1
 @param resolve
 */
- (void) combineRecordPath:(NSURL *)recordFileURL
					bgmUrl:(NSURL *) bgmFileURL
				  duration:(NSTimeInterval)duration
				 srcVolume:(float)srcVolume
				 bgmVolume:(float)bgmVolume
				   resolve:(RCTPromiseResolveBlock)resolve;

- (void) getAudioDurationWithURL:(NSURL *)audioFileURL  resolve:(RCTPromiseResolveBlock)resolve;

- (void) getAudioMetaInfoWithURL:(NSURL *)audioFileURL resolve:(RCTPromiseResolveBlock)resolve;


//- (void) playAudioWithURL:(NSURL *)audioFileURL resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock) reject;
//- (void) stopPlayAudio;
//- (void) pausePlayAudio;
//- (void) resumePlayAudio;

/**
 * 删除录音、合成录音、裁剪音频、编码解码音频
 */
- (void)clearAudios;

- (void) setVolumn:(float)volumn;


@end

NS_ASSUME_NONNULL_END
