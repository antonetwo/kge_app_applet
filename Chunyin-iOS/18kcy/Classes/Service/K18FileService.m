//
//  K18FileService.m
//  18kcy
//
//  Created by 唐 on 2019/6/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18FileService.h"
#import <AFNetworking/AFNetworking.h>

@implementation K18FileService


+ (void)downloadWithUrl:(NSString *)url destinationPath:(NSString *)destinationPath progress:(void (^)(NSProgress * _Nonnull))downloadProgressBlock completionHandler:(void (^)(NSURLResponse * _Nonnull, NSURL * _Nonnull, NSError * _Nonnull))completionHandler
{
	/* 创建网络下载对象 */
	AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
	
	/* 下载地址 */
	NSURL *urla = [NSURL URLWithString:url];
	NSURLRequest *request = [NSURLRequest requestWithURL:urla];
	/* 下载路径 */
	NSString *path = destinationPath;
	if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	}
	NSString *filePath = [path stringByAppendingPathComponent:url.lastPathComponent];
	
	/* 开始请求下载 */
	NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
		
		NSLog(@"下载进度：%.0f％", downloadProgress.fractionCompleted * 100);
		if (downloadProgressBlock) {
			downloadProgressBlock(downloadProgress);
		}
	} destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
//		dispatch_async(dispatch_get_main_queue(), ^{
			//如果需要进行UI操作，需要获取主线程进行操作
			
//		});
		/* 设定下载到的位置 */
		return [NSURL fileURLWithPath:filePath];
		
	} completionHandler:completionHandler];
	[downloadTask resume];
}

+ (NSString *)accompanyFilePath:(NSString *)url
{
	if(url.length==0){
		return @"";
	}
	NSString *path = [self accompanyPath];
	NSString *filePath = [path stringByAppendingPathComponent:url.lastPathComponent];
	return filePath;
}

+ (NSString *)accompanyPath
{
//	return [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Announcement"];
	NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
	return [path stringByAppendingPathComponent:@"Accompany"];
}

+ (BOOL)isExistFile:(NSString *)filePath
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		return YES;
	}
	return NO;
}

+ (NSString *)recordSongPath
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    return [path stringByAppendingPathComponent:@"Combines"];
}

+ (void)cacheRecordSong:(NSString *)fileName sourceURL:(NSURL *)sourceURL error:(NSError * _Nullable __autoreleasing * _Nullable)error
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self recordSongPath]]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self recordSongPath] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *filePath = [[self recordSongPath] stringByAppendingPathComponent:fileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    [[NSFileManager defaultManager] moveItemAtPath:sourceURL.relativePath toPath:filePath error:error];
    NSLog(@"%@",*error);
}

@end
