//
//  K18FileService.h
//  18kcy
//
//  Created by 唐 on 2019/6/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 
 */
@interface K18FileService : NSObject

//+ (instancetype) shared;

+ (void) downloadWithUrl:(NSString *)url
		 destinationPath:(NSString *)destinationPath
				progress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock
	   completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;

+ (void) removeFileWithUrl;

+ (void) uploadWithUrl;

+ (BOOL) isExistFile:(NSString *)filePath;

/**
 @param url 远程伴奏url
 @return 返回伴奏本地缓存文件路径
 */
+ (NSString *) accompanyFilePath:(NSString *)url;


/**

 @return 返回伴奏本地目录路径
 */
+ (NSString *) accompanyPath;//


/**
 

 @return 返回录制好的歌曲目录
 */
+ (NSString *) recordSongPath;


/**
 缓存录制好的歌曲

 @param fileName <#fileName description#>
 @param sourceURL <#sourceURL description#>
 */
+ (void) cacheRecordSong:(NSString *)fileName sourceURL:(NSURL*)sourceURL error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
