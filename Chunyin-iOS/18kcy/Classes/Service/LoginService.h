//
//  LoginService.h
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18TabBarController.h"



NS_ASSUME_NONNULL_BEGIN

@interface LoginService : NSObject

+ (void)enterMainUI;
+ (K18TabBarController *) mainTabController;

+ (void)enterLoginUI;

+ (void) openWebWithUrl:(NSString *)url;

+ (void)autoLogin:(void(^)(void)) callback;

+ (void)logout;

@end

NS_ASSUME_NONNULL_END
