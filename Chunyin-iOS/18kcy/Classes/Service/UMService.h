//
//  UMService.h
//  18kcy
//
//  Created by beuady on 2019/6/7.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UMShare/UMShare.h>

@class UMInfo;

NS_ASSUME_NONNULL_BEGIN

/**
 友盟业务
 */
@interface UMService : NSObject

+ (void) initUM;

+ (void)getUserInfoForPlatform:(UMSocialPlatformType)platformType completion:(UMSocialRequestCompletionHandler) completion;

+ (void)cancelAuthWithPlatform:(UMSocialPlatformType)platformType
                    completion:(UMSocialRequestCompletionHandler)completion;


+ (void) shareURL:(NSString*) shareUrl title:(NSString*)title image:(UIImage*)image comletion:(void(^)(NSError *error)) completion;

@end

NS_ASSUME_NONNULL_END
