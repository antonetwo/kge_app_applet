//
//  K18LiveService.m
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LiveService.h"
#import "K18SearchController.h"
#import "K18SearchMainController.h"
#import "TCPushPrepareViewController.h"
#import "K18NavigationController.h"
#import "K18UserDetailController.h"
#import "AppDelegate.h"

@implementation K18LiveService

+ (void) gotoSearchController:(id)model parent:(UIViewController *)parentVC
{
	K18SearchMainController *vc = [K18SearchMainController new];
	vc.hidesBottomBarWhenPushed = YES;
	[parentVC.navigationController pushViewController:vc animated:YES];
	
}

+ (void)showPushSettingView:(UIViewController *)parentVC
{
    TCPushPrepareViewController *publish = [TCPushPrepareViewController new];
//    K18NavigationController *nav = [[K18NavigationController alloc] initWithRootViewController:publish];
    //[parentVC presentViewController:nav animated:YES completion:nil];
    publish.hidesBottomBarWhenPushed = YES;
    [parentVC.navigationController pushViewController:publish animated:YES];
}

+ (void) gotoPersonDetail:(id)userId parent:(UIViewController *)parentVC
{
    K18UserDetailController *vc = [[K18UserDetailController alloc]initWithUserId:userId];
    vc.userId = userId;
    if(parentVC){
        [parentVC.navigationController pushViewController:vc animated:YES];
    }else{
        [[AppDelegate sharedAppDelegate] pushViewController:vc animated:YES];
    }
}

@end
