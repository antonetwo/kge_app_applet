//
//  LoginService.m
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "LoginService.h"
#import "K18MainController.h"
#import "AppDelegate.h"
#import "K18LoginController.h"
#import <SafariServices/SafariServices.h>
#import "K18UserModel.h"
#import "K18NavigationController.h"
#import "K18TabBarController.h"

//小直播登录
#import "TCLoginParam.h"
#import "TCLoginModel.h"

@implementation LoginService

+ (void)enterMainUI
{
//	K18MainController *vc = [[K18MainController alloc]init];	
	AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
//	K18NavigationController *nc = [[K18NavigationController alloc]initWithRootViewController:vc];
    UIViewController *vc = [[K18TabBarController alloc]init];
	del.window.rootViewController = vc;
	[del.window makeKeyAndVisible];
    del.mainTabController = vc;
}

+ (K18TabBarController *) mainTabController {
    AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return del.mainTabController;
}

+ (void)enterLoginUI
{
	AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
	del.window.rootViewController = [[K18NavigationController alloc] initWithRootViewController:[[K18LoginController alloc] initWithNibName:@"K18LoginController" bundle:nil]];
	[del.window makeKeyAndVisible];
}

+ (void) openWebWithUrl:(NSString *)url {
	SFSafariViewController *vc = [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:url]];
	AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[del.window.rootViewController presentViewController:vc animated:YES completion:nil];
}

+ (void)autoLogin:(void(^)(void)) callback
{
    if (K18UserModel.shared.userId.length==0) {
        return;
    }
    NSLog(@"正在自动登录...");
    if (K18UserModel.shared.cyType==2) {
        
        [K18UserModel loginWithPhone:K18UserModel.shared.account pwd:K18UserModel.shared.password completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            if(callback)callback();
            NSLog(@"自动登录成功");
            [self autoXiaozhiboLogin];
            
        }];
        
    }else{
        [K18UserModel loginWithThridOpenId:K18UserModel.shared.openId accessToken:nil refreshToken:nil cyType:K18UserModel.shared.cyType unionId:K18UserModel.shared.unionId
         userInfoDic:nil
                                completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            if(callback)callback();
			[self autoXiaozhiboLogin];
            NSLog(@"自动登录成功");
        }];
    }
}


/**
 自动登录小直播服务器
 */
+ (void)autoXiaozhiboLogin {
    
    TCLoginParam *_loginParam = [TCLoginParam loadFromLocal];
    if (![_loginParam isExpired] && _loginParam.identifier.length) {
        // 刷新票据
        dispatch_async(dispatch_get_main_queue(), ^{
            [[HUDHelper sharedInstance] syncLoading];
        });
        [[TCLoginModel sharedInstance] loginByToken:_loginParam.identifier hashPwd:_loginParam.hashedPwd succ:^(NSString *userName, NSString *md5pwd) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[HUDHelper sharedInstance] syncStopLoading];
//                [[HUDHelper sharedInstance] syncStopLoadingMessage:@"登录成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:k18_LoginSuccessEvent object:nil];
                NSLog(@"自动登录小直播成功");
            });
        } fail:^(int errCode, NSString *errMsg) {
             dispatch_async(dispatch_get_main_queue(), ^{
                [[HUDHelper sharedInstance] syncStopLoading];
                NSLog(@"自动登录小直播失败%s %d %@", __func__, errCode, errMsg);
                [self enterLoginUI];
            });
        }];
    }
    else {
        [self enterLoginUI];

    }
}

+ (void) logout {
    [[TCLoginModel sharedInstance] logout:^{
        
        [K18UserModel shared].autoLogin = NO;
        [LoginService enterLoginUI];
        DebugLog(@"退出登录成功");
    }];
}


@end
