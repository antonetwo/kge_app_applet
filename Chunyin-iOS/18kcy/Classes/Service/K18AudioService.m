//
//  K18AudioService.m
//  18kcy
//
//  Created by 唐 on 2019/6/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AudioService.h"
#import "VVAudioUtil.h"
#import "Recorder.h"
#import <AVFoundation/AVFoundation.h>
#import "AEncodePCMOperation.h"
#import <MediaPlayer/MPVolumeView.h>
#import "VVPlayer.h"

@interface K18AudioService()<AVAudioRecorderDelegate, RecorderDelegate, AVAudioPlayerDelegate, VVPlayerDelegate>
{
	BOOL _hasListeners;
}

@property(strong, nonatomic) NSString *bgmFilePath;
@property(strong, nonatomic) NSMutableDictionary *bgmMetaDic;

@property(strong, nonatomic) AVAudioPlayer *audioPlayer;
@property(strong, nonatomic) AEncodePCMOperation *operation;
@property(strong, nonatomic) MPVolumeView *volumeView;

@property(strong, nonatomic) VVPlayer *player;
@property(nonatomic) float volume;

@property(copy, nonatomic)  RCTPromiseResolveBlock startRecordResolve;
@property(copy, nonatomic)  RCTPromiseRejectBlock startRecordReject;
@property(copy, nonatomic)  RCTPromiseResolveBlock stopRecordResolve;

@property(copy, nonatomic)  RCTPromiseResolveBlock playAudioResolve;
@property(copy, nonatomic)  RCTPromiseRejectBlock playAudioReject;
@end

@implementation K18AudioService

+ (instancetype) shared
{
	static dispatch_once_t once;
	static id instance;
	dispatch_once(&once, ^{
		instance = [self new];
	});
	return instance;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		
		[[UIApplication sharedApplication] beginReceivingRemoteControlEvents];//
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChange:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
	}
	return self;
}

- (NSString *) filterBGMPath:(NSString *)path {
	if ([path.lastPathComponent isEqualToString:path]) {
		path = [[NSBundle mainBundle] pathForResource:path.stringByDeletingPathExtension ofType:path.pathExtension];
	}
	return path;
}


- (void) startRecordWithURL:(NSURL *)bgmFileURL resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject
{
	self.bgmFilePath = [self filterBGMPath:bgmFileURL.relativePath];
	self.startRecordResolve = resolve;
	self.startRecordReject = reject;
	
//	if ([[NSFileManager defaultManager] fileExistsAtPath:bgmFilePath]) {
//		self.bgmFilePath = bgmFilePath;
//	}
	
	[Recorder shareRecorder].recorderDelegate = self;
	
	AVAudioSession *session = [AVAudioSession sharedInstance];
	
	[session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
	//	[session setMode:AVAudioSessionModeVoiceChat error:nil];
	[session setActive:YES error:nil];
	
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		
		if (self.bgmFilePath) {
			[self.player pause];
			[self.audioPlayer stop];
			self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:bgmFileURL error:nil];
			[self.audioPlayer prepareToPlay];
			self.audioPlayer.delegate = self;
			[self.audioPlayer play];
			self.bgmMetaDic[@"duration"] = @(self.audioPlayer.duration);
			
			[self getAudioMetaInfoWithURL:bgmFileURL resolve:nil];//缓存音频元数据
		}
		
		[[Recorder shareRecorder] startRecord];
	
	});
}

- (void) startOnlyRecordWithURL:(NSURL *)bgmFileURL resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject
{
    self.bgmFilePath = [self filterBGMPath:bgmFileURL.relativePath];
    self.startRecordResolve = resolve;
    self.startRecordReject = reject;
    [Recorder shareRecorder].recorderDelegate = self;
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    //    [session setMode:AVAudioSessionModeVoiceChat error:nil];
    [session setActive:YES error:nil];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
	
	if (self.bgmFilePath) {
		self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:bgmFileURL error:nil];
		self.bgmMetaDic[@"duration"] = @(self.audioPlayer.duration);
		[self getAudioMetaInfoWithURL:bgmFileURL resolve:nil];//缓存音频元数据
	}
	
	[[Recorder shareRecorder] startRecord];
        
//    });
}

- (void) stopRecord:(BOOL)immediate comletion:(RCTPromiseResolveBlock)resolve
{
	self.stopRecordResolve = resolve;
	if (self.audioPlayer) {
		[self.audioPlayer stop];
		self.audioPlayer = nil;
	}
	if (immediate) {
		[[Recorder shareRecorder] stopRecord];
		[[Recorder shareRecorder] deleteRecord];
	}else{
		[[Recorder shareRecorder] stopRecord];
	}
}

- (void) stopRecord:(BOOL)immediate
{
	[self stopRecord:immediate comletion:nil];
}

- (void)pauseRecord
{
	[[Recorder shareRecorder] pauseRecord];
}

- (void)resumeRecord
{
	[[Recorder shareRecorder] resumeRecord];
}

- (BOOL)isRecording
{
	return [Recorder shareRecorder].isRecording;
}

/**
 * 获取音频时长
 */
- (void) getAudioDurationWithURL:(NSURL *)audioFileURL resolve:(RCTPromiseResolveBlock)resolve
{
	NSString *audioFilePath = [self filterBGMPath:audioFileURL.relativePath];
	dispatch_async(dispatch_get_main_queue(), ^{
		double duration = [[VVAudioUtil shareAudioUtil] getDurationWithPath:audioFilePath];
		resolve(@(duration));
	});
}

/**
 * 获取音频元数据
 */
- (void) getAudioMetaInfoWithURL:(NSURL *)audioFileURL resolve:(RCTPromiseResolveBlock)resolve
{
	NSString *audioFilePath = [self filterBGMPath:audioFileURL.relativePath];
	dispatch_async(dispatch_get_main_queue(), ^{
		
		__weak typeof(self)wself = self;
		[[VVAudioUtil shareAudioUtil] getMetaInfoWithPath:audioFilePath callback:^(NSDictionary * meta) {
			wself.bgmMetaDic = [NSMutableDictionary dictionaryWithDictionary:meta];
			wself.bgmMetaDic[@"filePath"] = audioFilePath;
			if(resolve)
				resolve(meta);
		}];
	});
}

- (void) playAudioWithURL:(NSURL *)audioFileURL resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock) reject
{
	self.playAudioResolve = resolve;
	self.playAudioReject = reject;
	NSString *audioFilePath = [self filterBGMPath:audioFileURL.relativePath];
	if (audioFileURL.isFileURL &&
		(!audioFilePath || [[NSFileManager defaultManager] fileExistsAtPath:audioFilePath]==NO)) {
		reject(nil,@"not exist file",nil);
		return;
	}
	dispatch_async(dispatch_get_main_queue(), ^{
		AVAudioSession *session = [AVAudioSession sharedInstance];
		if([session.category isEqualToString:AVAudioSessionCategoryPlayAndRecord] || [session.category isEqualToString:AVAudioSessionCategoryRecord]){
			
		}else{
			[session setCategory:AVAudioSessionCategoryAmbient error:nil];
			[[AVAudioSession sharedInstance] setActive:YES error:nil];
		}
		
		[session setCategory:AVAudioSessionCategoryPlayback error:nil];     // 播放音乐忽视ios设备静音按钮
		
		if (self.player) {
			[self.player pause];
		}

		self.player = [[VVPlayer alloc]initWithURL:audioFileURL];
		self.player.delegate = self;
		[self.player play];
	});
}

- (void)stopPlayAudio
{
	[self.player pause];
}

- (void)pausePlayAudio
{
	[self.player pause];
}

- (void)resumePlayAudio
{
	[self.player play];
}

/**
 * 删除录音、合成录音、裁剪音频、编码解码音频
 */
- (void)clearAudios
{
	[self _clearAudios];
}

- (void) _clearAudios {
	[[Recorder shareRecorder] deleteRecord];
	[[VVAudioUtil shareAudioUtil] deleteTransitAudioFiles];
}

- (void) setVolumn:(float)volumn
{
	self.volume = volumn;
	dispatch_async(dispatch_get_main_queue(), ^{
		if(!_volumeView){
			MPVolumeView *volumeView = [[MPVolumeView alloc]init];
			UIView *parent = [UIApplication sharedApplication].delegate.window.rootViewController.view;
			[parent addSubview:volumeView];
			volumeView.center = CGPointMake(- 50, - 50);
			_volumeView = volumeView;
		}
		UISlider* volumeViewSlider =nil;
		for(UIView*view in [_volumeView subviews]){
			if([view.class.description isEqualToString:@"MPVolumeSlider"]){
				volumeViewSlider = (UISlider*)view;
				break;
			}
		}
		[volumeViewSlider setValue:volumn animated:NO];
		[volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
	});
}

- (void)getVolumn:(RCTPromiseResolveBlock)resolve
{
	dispatch_async(dispatch_get_main_queue(), ^{
		resolve(@([AVAudioSession sharedInstance].outputVolume));
		return;
		if(!_volumeView){
			MPVolumeView *volumeView = [[MPVolumeView alloc]init];
			UIView *parent = [UIApplication sharedApplication].delegate.window.rootViewController.view;
			[parent addSubview:volumeView];
			volumeView.center = CGPointMake(- 50, - 50);
			_volumeView = volumeView;
		}
		UISlider* volumeViewSlider =nil;
		for(UIView*view in [_volumeView subviews]){
			if([view.class.description isEqualToString:@"MPVolumeSlider"]){
				volumeViewSlider = (UISlider*)view;
				break;
			}
		}
		resolve(@(volumeViewSlider.value));
		
	});
}

#pragma mark - RecorderDelegate

-(void)recorderCurrentTime:(NSTimeInterval)currentTime
{
	float peak = [[Recorder shareRecorder] getPeakVolume];
	//TODO 播放进度回调
	[self sendEventWithName:@"onRecordProgress" body:@{@"curTime":@(currentTime), @"curPeak":@(peak)}];
}

/**
 * 录音完成
 * filePath 录音文件保存路径
 * fileName 录音文件名
 * duration 录音时长
 **/
-(void)recorderStop:(NSString *)filePath voiceName:(NSString *)fileName duration:(NSTimeInterval)duration
{
	if (self.stopRecordResolve) {//如果是立即删除停止，不需要再合成
		self.stopRecordResolve(nil);
		return;
	}
	
	//对录音转码
    [[VVAudioUtil shareAudioUtil] encode:filePath completion:^(NSError *error, NSString *destPath) {
        destPath = destPath ? destPath : @"";
        [[Recorder shareRecorder] deleteRecord];
		NSURL *url = [NSURL fileURLWithPath:destPath];
		url = url ? url : @"";
        [self sendEventWithName:@"onRecordStop" body:@{@"recordFileURL":url,@"duration":@(duration)}];
        
    }];
	
	
}

- (void)combineRecordPath:(NSURL *)recordFileURL
				   bgmUrl:(NSURL *)bgmFileURL
				 duration:(NSTimeInterval)duration
				srcVolume:(float)srcVolume
				bgmVolume:(float)bgmVolume
				  resolve:(RCTPromiseResolveBlock)resolve
{
	__weak typeof(self)wself = self;
	
		//先裁剪bgm -> 合成
	[[VVAudioUtil shareAudioUtil] combine:recordFileURL srcStart:0 srcEnd:duration bgmPath:bgmFileURL bgmStart:0 bgmEnd:duration
								srcVolume:srcVolume
								bgmVolume:bgmVolume completion:^(NSError *error, NSString *destPath) {
									NSLog(@"combine, destPath=%@, error=%@",destPath, error);
																		
									[[Recorder shareRecorder] deleteRecord];
									destPath = destPath ? destPath : @"";
									
									if (error) {
										resolve(@{@"error":error});
										return;
									}
									
									[[VVAudioUtil shareAudioUtil] encode:destPath completion:^(NSError *error, NSString *adestPath) {
                                        
                                        adestPath = adestPath ? adestPath : @"";
                                        NSURL *aurl = [NSURL fileURLWithPath:adestPath];
                                        
										NSDictionary *body = @{@"exportURL":aurl,@"duration":@(duration)};
										[wself sendEventWithName:@"onCombineComplete" body:body];
										
										if (resolve) {
											resolve(body);
										}
										
									}];
									
								}];
	

}

-(void)recorderStart:(NSError *)error
{
	if (error) {
		if (self.startRecordReject) {
			self.startRecordReject(@(error.code).stringValue, error.localizedDescription,nil);
			self.startRecordReject =  nil;
		}
	}else{
		if (self.startRecordResolve) {
			self.startRecordResolve(nil);
			self.startRecordResolve =  nil;
		}
	}
}

#pragma mark - VVPlayerDelegate

- (void)vvplayer:(VVPlayer *)player onEnd:(NSError *)error
{
	if(error){
		[self sendEventWithName:@"onAudioStop" body:@{@"state":@-2}];
	}else{
		[self sendEventWithName:@"onAudioStop" body:@{@"state":@1}];
	}
}

- (void)vvplayer:(VVPlayer *)player onStart:(NSError *)error
{
	if (error) {
		self.playAudioReject(error.domain, error.localizedDescription, error);
	}else{
		self.playAudioResolve(nil);
	}
}

- (void)vvplayer:(VVPlayer *)player onCurrentTimeSec:(NSTimeInterval)currentTimeSec total:(NSTimeInterval)total
{
	[self sendEventWithName:@"onAudioProgress" body:@{@"curTime":@(currentTimeSec),@"total":@(total)}];
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	[self sendEventWithName:@"onAudioStop" body:@{@"state":@1}];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError * __nullable)error
{
	[self sendEventWithName:@"onAudioStop" body:@{@"state":@-1}];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
	[self sendEventWithName:@"onAudioStop" body:@{@"state":@-2}];
}

#pragma mark - 音量监听

- (void) volumeChange:(NSNotification*)notify {
	CGFloat value = [[notify.userInfo objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"] doubleValue];
	self.volume = value;
	if(_hasListeners){
		[self sendEventWithName:@"onVolumnChange" body:@{@"volumn":@(value)}];
	}

}

// 在添加第一个监听函数时触发
-(void)startObserving {
	_hasListeners = YES;
}

// Will be called when this module's last listener is removed, or on dealloc.
-(void)stopObserving {
	_hasListeners = NO;
}

#pragma mark -

- (void)sendEventWithName:(NSString *)eventName body:(id)body
{
	[[NSNotificationCenter defaultCenter] postNotificationName:eventName object:nil userInfo:body];
}


@end
