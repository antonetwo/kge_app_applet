//
//  K18SongService.m
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongService.h"
#import "K18PublishController1.h"
#import "K18SongDetailController.h"
#import "K18SongAuthorController.h"
#import "K18SongListController.h"
#import "AppDelegate.h"
#import "K18RecordingController.h"
#import "K18Singer.h"

@implementation K18SongService

+ (void) gotoSongDetailWithId:(NSString *) songId root:(nonnull UIViewController *)root
{
	K18SongDetailController *vc = [[K18SongDetailController alloc] init];
	vc.hidesBottomBarWhenPushed = YES;
    vc.songLiveId = songId;
    if(!root){
        [[AppDelegate currentPresentestController].navigationController pushViewController:vc animated:YES];
    }else if([root isKindOfClass:[UINavigationController class]]){
        UINavigationController *nc = (UINavigationController*)root;
        [nc pushViewController:vc animated:YES];
    }else{
        [root.navigationController pushViewController:vc animated:YES];
    }
}

+ (void) gotoRecordingWithModel:(K18Song*) model root:(nonnull UIViewController *)root
{
    K18RecordingController *vc = [[K18RecordingController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
	vc.songModel = model;
    [root.navigationController pushViewController:vc animated:YES];

}

+ (void)gotoPublishWithModel:(id)model root:(UIViewController *)root
{
    K18PublishController1 *vc = [[K18PublishController1 alloc] initWithNibName:@"K18PublishController1" bundle:nil];
    vc.hidesBottomBarWhenPushed = YES;
	vc.model = model;
    [root.navigationController pushViewController:vc animated:YES];
}

+ (void)gotoSingerList:(NSMutableArray *)dataSource title:(NSString *)title root:(UIViewController *)root
{
	K18SongAuthorController *vc = [[K18SongAuthorController alloc]init];
	vc.hidesBottomBarWhenPushed = YES;
	vc.title = @"热门歌手";
	vc.dataSource = dataSource;
	[root.navigationController pushViewController:vc animated:YES];
	
}

+ (void)gotoSongListWithData:(id)data category:(K18Category *)category root:(nonnull UIViewController *)root
{
	K18SongListController *listVC = [[K18SongListController alloc] initWithType:1];
	listVC.hidesBottomBarWhenPushed = YES;
	listVC.title = category.cyTypeName;
	listVC.category = category;
	listVC.dataSource = data;
	[root.navigationController pushViewController:listVC animated:YES];
	
}

+ (void)gotoSongListWithData:(id)data singer:(K18Singer *)singer root:(nonnull UIViewController *)root
{
	K18SongListController *listVC = [[K18SongListController alloc] initWithType:0];
	listVC.hidesBottomBarWhenPushed = YES;
	listVC.title = singer.cyName;
	listVC.singer = singer;
	listVC.dataSource = data;
	[root.navigationController pushViewController:listVC animated:YES];
	
}

@end
