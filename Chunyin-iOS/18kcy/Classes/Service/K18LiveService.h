//
//  K18LiveService.h
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18LiveService : NSObject
+ (void) gotoSearchController:(nullable id)model parent:(UIViewController *)parentVC;

+ (void)showPushSettingView:(UIViewController *)parentVC;

+ (void) gotoPersonDetail:(id)userId parent:(UIViewController *)parentVC;
@end

NS_ASSUME_NONNULL_END
