//
//  K18SongService.h
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18Common.h"
#import "K18Song.h"
#import "K18Singer.h"
#import "K18SongModel.h"

NS_ASSUME_NONNULL_BEGIN


/**
 K歌业务
 */
@interface K18SongService : NSObject

+ (void) gotoSongDetailWithId:(NSString *) songId root:(nonnull UIViewController *)root;

+ (void) gotoRecordingWithModel:(K18Song*) model root:(UIViewController *)root;

+ (void) gotoPublishWithModel:(id)model root:(UIViewController *)root;

+ (void) gotoSingerList:(NSMutableArray *)dataSource title:(NSString*)title root:(UIViewController *)root;

+ (void)gotoSongListWithData:(id)data category:(K18Category *)category root:(nonnull UIViewController *)root;
+ (void)gotoSongListWithData:(id)data singer:(K18Singer *)singer root:(nonnull UIViewController *)root;

@end

NS_ASSUME_NONNULL_END
