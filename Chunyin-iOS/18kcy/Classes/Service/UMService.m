//
//  UMService.m
//  18kcy
//
//  Created by beuady on 2019/6/7.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "UMService.h"

#import <UMCommon/UMCommon.h>
#import <UMAnalytics/MobClick.h>
#import <UMCommonLog/UMCommonLogManager.h>

#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>

#define UMKey @"5cfa7ad33fc19524660001ff"
//#define

@implementation UMService

+ (void)initUM {
//    return;//让xcode捕抓异常，先屏蔽Um
//    [MobClick setLogEnabled:YES];
	[UMCommonLogManager setUpUMCommonLogManager];
//    [[UMSocialManager defaultManager] setUmSocialAppkey:UMKey];
//    UMAnalyticsConfig *cconfig = UMConfigInstance;
//    cconfig.appKey = UMKey;
    [UMConfigure initWithAppkey:UMKey channel:@"App Store"];
#if DEBUG
	[UMConfigure setLogEnabled:YES];
#endif
    
    [UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
    
    [self configUSharePlatforms];
	
	//UMSocialPlatformType
	[UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_QQ),
											   @(UMSocialPlatformType_WechatSession),
											   @(UMSocialPlatformType_WechatTimeLine),
											   @(UMSocialPlatformType_Qzone),
											   ]];
}

+ (void) configUSharePlatforms {
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wxc68ea9a3e7dc6f33" appSecret:@"2793c34ca3b4289faebabc6f8afee93b" redirectURL:@"http://mobile.umeng.com/social"];
    
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     */
	[[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"101618170"/*设置QQ平台的appID*/  appSecret:@"aa52cb30ce7d074a59aaf43c2a9faee8" redirectURL:@"http://mobile.umeng.com/social"];
    
}

+ (void)getUserInfoForPlatform:(UMSocialPlatformType)platformType completion:(nonnull UMSocialRequestCompletionHandler)completion
{
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:platformType currentViewController:nil completion:^(id result, NSError *error) {
        
        UMSocialUserInfoResponse *resp = result;
        
        // 第三方登录数据(为空表示平台未提供)
        // 授权数据
//        NSLog(@" uid: %@", resp.uid);
//        NSLog(@" openid: %@", resp.openid);
//        NSLog(@" accessToken: %@", resp.accessToken); //如果accessToken为空，则为过期
//        NSLog(@" refreshToken: %@", resp.refreshToken);
//        NSLog(@" expiration: %@", resp.expiration);
        
        // 用户数据
//        NSLog(@" name: %@", resp.name);
//        NSLog(@" iconurl: %@", resp.iconurl);
//        NSLog(@" gender: %@", resp.unionGender);
        
        if (completion) {
            completion(result, error);
        }
        
        // 第三方平台SDK原始数据
        NSLog(@" originalResponse: %@", resp.originalResponse);
    }];
}

+ (void)cancelAuthWithPlatform:(UMSocialPlatformType)platformType
                      completion:(UMSocialRequestCompletionHandler)completion
{
    
}



+ (void) shareURL:(NSString *)shareUrl title:(id)title image:(UIImage *)image comletion:(void (^)(NSError * _Nonnull))completion {
	
	[UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
		//UMShareWebpageObject
		UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
		UMShareWebpageObject *shareObj = [UMShareWebpageObject shareObjectWithTitle:title descr:@"" thumImage:image];
		shareObj.webpageUrl = shareUrl;
		messageObject.shareObject = shareObj;
		[[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:nil completion:^(id result, NSError *error) {
			if (completion) {
				completion(error);
			}
		}];
		
	}];
	
}

@end
