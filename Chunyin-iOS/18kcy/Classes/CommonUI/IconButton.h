//
//  IconButton.h
//
//  Created by linjinzhu on 9/1/13.
//  Copyright (c) 2014 linjinzhu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define UIEdgeOffsetsZero UIEdgeOffsetsMake(0, 0, 0, 0)

typedef enum {
    IconButtonStyleIconTop,
    IconButtonStyleIconBottom,
    IconButtonStyleIconLeft,
    IconButtonStyleIconRight
} IconButtonStyle;

typedef struct UIEdgeOffsets {
    CGFloat left, top, width, height;
} UIEdgeOffsets;

UIKIT_STATIC_INLINE UIEdgeOffsets UIEdgeOffsetsMake(CGFloat left,
                                                    CGFloat top,
                                                    CGFloat width,
                                                    CGFloat height)
{
    UIEdgeOffsets offsets = {left, top, width, height};
    return offsets;
}

UIKIT_STATIC_INLINE CGRect UIEdgeOffsetsRect(CGRect rect, UIEdgeOffsets offsets)
{
    rect.origin.x    += offsets.left;
    rect.origin.y    += offsets.top;
    rect.size.width  += offsets.width;
    rect.size.height += offsets.height;
    return rect;
}

/**
 * IconButton is used to change the layout of UIButton.
 *
 * Icon is put on the top and label on the bottom for IconButtonStyleIconTop style.
 * +----------+
 * | +------+ |
 * | | Icon | |
 * | +------+ |
 * |   Text   |
 * +----------+
 */
@interface IconButton : UIButton

@property (nonatomic) IconButtonStyle   iconStyle; // default is IconButtonStyleIconTop
@property (nonatomic) CGSize            imageSize; // fixed image size
@property(nonatomic)  UIEdgeOffsets     imageEdgeOffsets; // default is UIEdgeOffsetsZero
@property(nonatomic)  UIEdgeOffsets     titleEdgeOffsets; // default is UIEdgeOffsetsZero

- (id)initWithFrame:(CGRect)frame;

@end
