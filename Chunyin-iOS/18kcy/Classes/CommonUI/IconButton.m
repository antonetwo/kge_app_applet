//
//  IconButton.m
//
//  Created by linjinzhu on 9/1/13.
//  Copyright (c) 2014 linjinzhu. All rights reserved.
//

#import "IconButton.h"

#define IBTopBottomMargin 4
#define IBLeftRightMargin 4

@implementation IconButton

- (void)commonInit
{
    _imageSize = CGSizeZero;
    _imageEdgeOffsets = UIEdgeOffsetsZero;
    _titleEdgeOffsets = UIEdgeOffsetsZero;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)setIconStyle:(IconButtonStyle)iconStyle
{
    if (_iconStyle != iconStyle) {
        _iconStyle = iconStyle;
        [self setNeedsLayout];
        [self sizeToFit];
    }
}

-(void)sizeToFit
{
    [self layoutSubviews];
//    CGSize szImage = _imageSize;
//    if (CGSizeEqualToSize(szImage, CGSizeZero)) {
//        szImage = self.imageView.image.size;
//    }
//    CGFloat w, h;
//    CGSize szText = [IconButton sizeText:self.titleLabel.text withFont:self.titleLabel.font constrainedToSize:NULL];
//    if (_iconStyle == IconButtonStyleIconLeft || _iconStyle == IconButtonStyleIconRight) {
//        w = IBLeftRightMargin+szImage.width + _imageEdgeOffsets.left + _imageEdgeOffsets.width;
//        w += szText.width + _titleEdgeOffsets.left + _titleEdgeOffsets.width;
//        h = IBTopBottomMargin;
//        h += MAX(szImage.height + _imageEdgeOffsets.top + _imageEdgeOffsets.height,
//                 szText.height + _titleEdgeOffsets.top + _titleEdgeOffsets.height);
//    } else {
//        w = IBLeftRightMargin;
//        w += MAX(szImage.width + _imageEdgeOffsets.left + _imageEdgeOffsets.width,
//                 szText.width + _titleEdgeOffsets.left + _titleEdgeOffsets.width);
//        h = IBTopBottomMargin + szImage.height + _imageEdgeOffsets.top + _imageEdgeOffsets.height;
//        h += szText.height + _titleEdgeOffsets.top + _titleEdgeOffsets.height;
//    }
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, w, h);
}

+ (CGSize)sizeText:(NSString*)text withFont:(UIFont *)font constrainedToSize:(CGSize*)size
{
    if (font == nil) {
        return CGSizeZero;
    }
    
    CGSize sz = CGSizeZero;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
#ifdef __IPHONE_7_0
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        if (size != NULL) {
            sz = [text boundingRectWithSize:*size
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:attributes
                                    context:nil].size;
        } else {
            sz = [text sizeWithAttributes:attributes];
        }
#else
        if (size != NULL) {
            sz = [text sizeWithFont:font constrainedToSize:*size];
        } else {
            sz = [text sizeWithFont:font];
        }
#endif
    } else if (size != NULL) {
        sz = [text boundingRectWithSize:*size options:NSStringDrawingUsesLineFragmentOrigin attributes:nil context:NULL].size;
    } else {
        sz = [text sizeWithAttributes:nil];
    }
    
    return sz;
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];
    [self sizeToFit];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (CGRectIsEmpty(self.bounds)) {
        return;
    }
    CGSize szImage = _imageSize;
    if (CGSizeEqualToSize(szImage, CGSizeZero)) {
        szImage = self.imageView.image.size;
    }
    CGSize szFrame = self.bounds.size;
    CGRect rcIconFrame, rcTextFrame;

    if (_iconStyle == IconButtonStyleIconLeft || _iconStyle == IconButtonStyleIconRight) {
        CGFloat y = (szFrame.height - szImage.height - IBTopBottomMargin) / 2;
        if (_iconStyle == IconButtonStyleIconLeft) {
            rcIconFrame = CGRectMake(IBLeftRightMargin / 2,
                                     IBTopBottomMargin / 2 + y,
                                     szImage.width,
                                     szImage.height);
            rcTextFrame = CGRectMake(IBLeftRightMargin + szImage.width,
                                     IBTopBottomMargin / 2,
                                     szFrame.width - (IBLeftRightMargin * 1.5 + szImage.width),
                                     szFrame.height - IBTopBottomMargin);
        } else {
            rcTextFrame = CGRectMake(IBLeftRightMargin / 2,
                                     IBTopBottomMargin / 2,
                                     szFrame.width - (IBLeftRightMargin * 1.5 + szImage.width),
                                     szFrame.height - IBTopBottomMargin);
            rcIconFrame = CGRectMake(IBLeftRightMargin + rcTextFrame.size.width,
                                     IBTopBottomMargin / 2 + y,
                                     szImage.width,
                                     szImage.height);
        }
    } else {
        CGFloat x = (szFrame.width - szImage.width - IBLeftRightMargin) / 2;
        if (_iconStyle == IconButtonStyleIconTop) {
            rcIconFrame = CGRectMake(IBLeftRightMargin / 2 + x,
                                     IBTopBottomMargin / 2,
                                     szImage.width,
                                     szImage.height);
            rcTextFrame = CGRectMake(IBLeftRightMargin / 2,
                                     IBTopBottomMargin + szImage.height,
                                     szFrame.width - IBLeftRightMargin,
                                     szFrame.height - (IBTopBottomMargin * 1.5 + szImage.height));
        } else {
            rcTextFrame = CGRectMake(IBLeftRightMargin / 2,
                                     IBTopBottomMargin / 2,
                                     szFrame.width - IBLeftRightMargin * 2,
                                     szFrame.height - (IBTopBottomMargin * 1.5 + szImage.height));
            rcIconFrame = CGRectMake(IBLeftRightMargin / 2 + x,
                                     IBTopBottomMargin + rcTextFrame.size.height,
                                     szImage.width,
                                     szImage.height);
        }
    }

    rcIconFrame = UIEdgeOffsetsRect(rcIconFrame, _imageEdgeOffsets);
    rcTextFrame = UIEdgeOffsetsRect(rcTextFrame, _titleEdgeOffsets);

    if (!CGRectIsEmpty(rcIconFrame)) {
        self.imageView.frame = rcIconFrame;
    }
    if (!CGRectIsEmpty(rcTextFrame)) {
        self.titleLabel.frame = rcTextFrame;
    }
}

@end
