//
//  NSObject+StoryAPI.h
//  ZhiTong
//
//  Created by 唐 on 2018/12/21.
//  Copyright © 2018 张地. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+ServerURL.h"

NS_ASSUME_NONNULL_BEGIN


@interface NSObject (K18API)

#define kAPI @"/client/"

#define kAPI_init kAPI @"init"

//=========================
//公共API
//=========================
//轮播图片
#define kAPI_getSlidePicture kAPI @"getSlidePicture"
//搜索
#define kAPI_searchforFlag kAPI @"searchforFlag"
//上传文件（cyType 0 演唱的歌曲 1 伴奏 3 歌词 4 原唱）
#define kAPath_doUploadFile kAPI @"doUploadFile"
//上传图片, 上传头像,上传相册,身份证,直播封面
#define kAPath_uploadImg kAPI @"uploadImg"
//获取省市
#define kAPI_getProvince kAPI @"getProvince"
//获取音乐列表
#define kAPI_getSongs kAPI @"getSongs"

//=========================
//注册登录模块
//=========================

//微信，qq注册（创建，更新新用户）
#define kAPI_createUserByWXQQ kAPI @"createUserByWXQQ"
//手机号注册（创建新用户）
#define kAPI_createUserByPhone kAPI @"createUserByPhone"
//发送验证码(手机号注册或者重置密码)
#define kAPI_sendCode kAPI @"sendCode"
//手机号登录
#define kAPI_phoneLogin kAPI @"phoneLogin"
//重置密码
#define kAPI_resetPassword kAPI @"resetPassword"
//个人信息修改
#define kAPI_doUserInfo kAPI @"doUserInfo"
//头像/相册上传
#define kAPI_uploadImg kAPI @"uploadImg"

//绑定手机号
#define kAPI_doBangPhone kAPI @"doBangPhone"

//=========================
//K歌模块
//=========================
//    歌曲分类
#define kAPI_getSongTypes kAPI @"getSongTypes"
// 每个类别下面的歌曲（歌手的歌曲在另外一个接口）
#define kAPI_getSongByType kAPI @"getSongByType"
//每个歌手的歌曲列表
#define kAPI_getSongBySinger kAPI @"getSongBySinger"
//歌曲排行榜
#define kAPI_getSongRank kAPI @"getSongRank"
//录制时需要的伴奏信息
#define kAPI_getBanzou kAPI @"getBanzou"
//发布歌曲
#define kAPI_doFabu kAPI @"doFabu"
//歌曲详情
#define kAPI_getSongLive kAPI @"getSongLive"
//歌曲评论列表
#define kAPI_getSongLiveComments kAPI @"getSongLiveComments"
//送鲜花
#define kAPI_sendFlowers kAPI @"sendFlowers"
//    分享链接
#define kAPI_shareUrl kAPI @"shareUrl"
//评论歌曲
#define kAPI_doSongLiveComment kAPI @"doSongLiveComment"
//收藏或取消收藏歌曲
#define kAPI_doCollectSong kAPI @"doCollectSong"
//关注或取消关注用户
#define kAPI_doFollowUser kAPI @"doFollowUser"
//A用户是否关注B用户
#define kAPI_getFollow kAPI @"getFollow"
//K歌排行榜
#define kAPI_kGeRank kAPI @"kGeRank"

// 删除歌曲
#define kAPI_doDeleteSongLive kAPI @"doDeleteSongLive"
//=========================
//直播模块
//=========================

//主播认证
#define kAPI_doAnchorAuthentication kAPI @"doAnchorAuthentication"
//主播是否认证
#define kAPI_anchorAuthentication kAPI @"anchorAuthentication"
//创建直播间
#define kAPI_createLiveRoom kAPI @"createLiveRoom"
//结束直播
#define kAPI_closeLiveRoom kAPI @"closeLiveRoom"
//直播间列表
#define kAPI_liveRooms kAPI @"liveRooms"
//收听用户演唱的歌曲（收听+1）
#define kAPI_doListenSongLive kAPI @"doListenSongLive"
//礼物列表
#define kAPI_liveRoomFlowers kAPI @"liveRoomFlowers"
//用户A给用户B送鲜花(直播间)
#define kAPI_liveRoomSendFlowers kAPI @"liveRoomSendFlowers"
//用户A拉黑用户B
#define kAPI_doLahei kAPI @"doLahei"
//检查用户A是否拉黑用户B
#define kAPI_getLahei kAPI @"getLahei"
//投诉
#define kAPI_doComplaint kAPI @"doComplaint"
//实时更新直播间人数
#define kAPI_doUpdateLiveRoomVisitorNum kAPI @"doUpdateLiveRoomVisitorNum"
//直播排行榜
#define kAPI_liveRank kAPI @"liveRank"


//
//#define kAPI

//=========================
//发现模块
//=========================

//关注列表
#define kAPI_userFollow kAPI @"userFollow"
//直播列表
#define kAPI_discoverLiveRooms kAPI @"discoverLiveRooms"
//推荐列表
#define kAPI_getTuijianSongLives kAPI @"getTuijianSongLives"

//=========================
//我的模块
//=========================
//我的作品列表
#define kAPI_getMySongLives kAPI @"getMySongLives"
//我的相册列表
#define kAPI_getMyPhotos kAPI @"getMyPhotos"
//获取个人详情
#define kAPI_getUserInfo kAPI @"getUserInfo"
//我的粉丝
#define kAPI_myFans kAPI @"myFans"
//我的关注
#define kAPI_myFollow kAPI @"myFollow"
//我的收藏
#define kAPI_myCollects kAPI @"myCollects"
//我的收发礼物
#define kAPI_myFlowerRecord kAPI @"myFlowerRecord"
//我的播放记录
#define kAPI_myBroadRecord kAPI @"myBroadRecord"
//用户A给用户B送鲜花(直播间)
//#define kAPI_getMySongLives kAPI @"getMySongLives"

/*==========*/


- (void) sendWithPath:(NSString *)path params:(NSDictionary *_Nullable)params succ:(void (^)(id data))successBlock failed:(void (^)(NSError * _Nonnull error))failedBlock;


/**
 上传头像/相册
 
 @param data
 @param fileName
 @param userId
 @param ctType 0,相册照片，1，头像照片,5，身份证 6直播封面
 @param success
 @param failure
 */
- (void)uploadImage:(NSData *)data
              fileName:(NSString *)fileName
                userId:(NSString *)userId
                cyType:(int)ctType
               success:(void (^)(id response))success failure:(void (^)(NSError *err))failure;



/**
 上传文件

 @param fileUrl
 @param cyType  演唱的歌曲 1 伴奏 3 歌词 4 原唱
 @param cyUserId
 @param success
 @param failure
 @param progress
 */
- (void)uploadFile:(NSURL *)fileUrl
             cyType:(int)cyType
           cyUserId:(NSString *)cyUserId
			success:(void(^)(id response))success
			failure:(void(^)(NSError *err))failure
		   progress:(void(^)(NSProgress *progress)) progress;

@end

NS_ASSUME_NONNULL_END
