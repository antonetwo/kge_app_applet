//
//  NSObject+ServerURL.m
//  GoodInCome
//
//  Created by Sunyi on 15/5/4.
//  Copyright (c) 2015年 KMTeam. All rights reserved.
//

#import "NSObject+ServerURL.h"
#import "K18Common.h"

@implementation NSObject (ServerURL)


//平台类型（0正式平台，1测试平台)
- (NSString *)getSERVERURL{
    NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
    
    NSString *serverUrl = nil;
    
    NSArray *serverUrls = [self getSERVERURLArray];
    
//    if([userInfo getPlanform] != nil && ISTEST == 1){
//        NSInteger planform = [[userInfo getPlanform] integerValue];
//        serverUrl = [serverUrls objectAtIndex:planform];
//    }else{
        serverUrl = [serverUrls objectAtIndex:PLANTFORM];
//    }
	
    return serverUrl;
}

- (NSString *)getHTMLSERVERURL{
    NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
    
    NSString *serverUrl = nil;
    
    NSArray *serverUrls = [self getHTMLSERVERURLArray];
    
//    if([userInfo getPlanform] != nil && ISTEST == 1){
//        NSInteger planform = [[userInfo getPlanform] integerValue];
//        serverUrl = [serverUrls objectAtIndex:planform];
//    }else{
        serverUrl = [serverUrls objectAtIndex:PLANTFORM];
//    }
	
    return serverUrl;
}

- (NSArray *)getSERVERURLArray{
    NSArray *serverUrls = [NSArray arrayWithObjects:kK18ServerUrl,nil];
    return serverUrls;
}

- (NSArray *)getHTMLSERVERURLArray{
    NSArray *serverHTMLUrls = [NSArray arrayWithObjects:kK18ServerUrl,nil];
    return serverHTMLUrls;
}



- (NSString *)getUpLoadUrl{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *upLoadUrl = [userDefault objectForKey:@"uploadUrl"];
    
    return upLoadUrl;
}

- (NSString *)getDownLoadUrl{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *downLoadUrl = [userDefault objectForKey:@"downloadUrl"];
    
    return downLoadUrl;
}

- (void)setUpLoadUrl:(NSString *)content{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (content != nil) {
        [userDefault setObject:content forKey:@"uploadUrl"];
    }else{
        [userDefault removeObjectForKey:@"uploadUrl"];
    }
    [userDefault synchronize];
}
- (void)setDownLoadUrl:(NSString *)content{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (content != nil) {
        [userDefault setObject:content forKey:@"downloadUrl"];
    }else{
        [userDefault removeObjectForKey:@"downloadUrl"];
    }
    [userDefault synchronize];
}




@end
