//
//  NSString+HTTP.m
//  eather
//
//  Created by 青秀斌 on 14-6-12.
//  Copyright (c) 2014年 com.cdzlxt.iw. All rights reserved.
//

#import "NSString+HTTP.h"

@implementation NSString (HTTP)

- (NSString *)urlAction:(NSString *)action{
    NSString *url = [self stringByAppendingString:action];
    return url;
}

@end
