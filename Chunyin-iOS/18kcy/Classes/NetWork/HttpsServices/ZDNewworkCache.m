//
//  ZDNewworkCache.m
//  HeHuangClub
//
//  Created by 张地 on 16/10/13.
//  Copyright © 2016年 张地. All rights reserved.
//

#import "ZDNewworkCache.h"
#import <YYCache/YYCache.h>
@implementation ZDNewworkCache
static NSString *const NetworkResponseCache = @"NetworkResponseCache";
static YYCache *_dataCache;


+ (void)initialize
{
    _dataCache = [YYCache cacheWithName:NetworkResponseCache];
}

+ (void)saveHttpCache:(id)httpCache forKey:(NSString *)key
{
    //异步缓存,不会阻塞主线程
    [_dataCache setObject:httpCache forKey:key withBlock:nil];
}

+ (id)getHttpCacheForKey:(NSString *)key
{
    return [_dataCache objectForKey:key];
}

//+ (NSInteger)getAllHttpCacheSize
//{
////    return [_dataCache.diskCache totalCost];
//}
//
//+ (void)removeAllHttpCache
//{
////    [_dataCache.diskCache removeAllObjects];
//}
@end
