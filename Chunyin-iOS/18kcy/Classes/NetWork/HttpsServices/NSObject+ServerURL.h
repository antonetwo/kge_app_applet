//
//  NSObject+ServerURL.h
//  GoodInCome
//
//  Created by Sunyi on 15/5/4.
//  Copyright (c) 2015年 KMTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ServerURL)

- (NSString *)getSERVERURL;
- (NSString *)getHTMLSERVERURL;

- (NSArray *)getSERVERURLArray;
- (NSArray *)getHTMLSERVERURLArray;

- (NSString *)getUpLoadUrl;
- (NSString *)getDownLoadUrl;
- (void)setUpLoadUrl:(NSString *)content;
- (void)setDownLoadUrl:(NSString *)content;



@end
