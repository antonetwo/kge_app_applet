//
//  NSObject+StoryAPI.m
//  ZhiTong
//
//  Created by 唐 on 2018/12/21.
//  Copyright © 2018 张地. All rights reserved.
//

#import "NSObject+K18API.h"
#import "NSError+HTTP.h"
#import "NSObject+HTTP.h"
#import "XTSystem.h"
#import <AFNetworking.h>

#define OpenNetLog 0

@implementation NSObject (K18API)

- (void) sendWithPath:(NSString *)path params:(NSDictionary *_Nullable)params succ:(void (^)(id data))successBlock failed:(void (^)(NSError * _Nonnull))failedBlock
{
	if(!params){
		params = [NSMutableDictionary dictionary];
	}
    if(![params isMemberOfClass:[NSMutableDictionary class]]){
        params = [params mutableCopy];
    }
	NSString *url = [[self getSERVERURL] stringByAppendingString:path];
	
	NSMutableDictionary *allDic = [self commonParams];
	[allDic addEntriesFromDictionary:params];
#if OpenNetLog
	NSLog(@"%@, %@",url, allDic);
#endif
	[self GET:url parameters:allDic success:^(id responseObject) {
#if OpenNetLog
		NSLog(@"%@",responseObject);
#endif
		NSInteger code = [[responseObject objectForKey:@"code"] integerValue];
		if(code == 0){
			successBlock([responseObject objectForKey:@"msg"]);
		}else{
			if (failedBlock) {
				NSString *message = [responseObject objectForKey:@"error"];
				failedBlock([NSError bussinessError:code message:message]);
			}
		}
	} failure:^(NSError *error) {
		[error forFailedBlock:failedBlock];
	}];
}

- (NSMutableDictionary *) commonParams {
	return [NSMutableDictionary dictionaryWithDictionary:@{
														   @"deviceId":[XTSystem deviceUUID],
														   @"platform":@"iOS",
														   @"versionCode":[XTSystem appBuild],
														   @"versionName":[XTSystem appVersion],
														   }];
}


- (void)uploadImage:(NSData *)data
              fileName:(NSString *)fileName
                userId:(NSString *)userId
                cyType:(int)cyType
               success:(void (^)(id response))success failure:(void (^)(NSError *err))failure {
    
    NSURL *baseURL = [NSURL URLWithString:[self getSERVERURL]];
    AFHTTPSessionManager *mgr = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    
    mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    
    NSString *url = kAPath_uploadImg;
    url = [url stringByAppendingFormat:@"?userId=%@&cyType=%@",userId,@(cyType)];
    NSDictionary *params = @{@"userId":userId, @"cyType":@(cyType)};
    NSLog(@"%@,%@",url,params);
    [mgr POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:data name:@"img" fileName:fileName mimeType:@"image/jpeg"];
    } progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#if OpenNetLog
          NSLog(@"%@",responseObject);
#endif
          NSInteger code = [[responseObject objectForKey:@"code"] integerValue];
          if(code == 0){
              success([responseObject objectForKey:@"msg"]);
          }else{
              if (failure) {
                  NSString *message = [responseObject objectForKey:@"error"];
                  NSLog(@"上传图片失败,%@",message);
                  failure([NSError bussinessError:code message:message]);
              }
          }
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          NSLog(@"上传图片失败,%@",error);
          failure(error);
      }];
    
}

- (void)uploadFile:(NSURL *)fileUrl
            cyType:(int)cyType
          cyUserId:(NSString *)cyUserId
           success:(void (^)(id _Nonnull))success
           failure:(void (^)(NSError * _Nonnull))failure
          progress:(nonnull void (^)(NSProgress * _Nonnull))progress
{
    NSString *fileName = fileUrl.lastPathComponent;
    NSURL *baseURL = [NSURL URLWithString:[self getSERVERURL]];
    AFHTTPSessionManager *mgr = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    
    mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    
    NSString *url = kAPath_doUploadFile;
    url = [url stringByAppendingFormat:@"?cyUserId=%@&cyType=%@",cyUserId,@(cyType)];
//    NSDictionary *params = @{@"userId":cyUserId, @"cyType":@(cyType)};
    NSLog(@"%@",url);
    [mgr POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        @autoreleasepool {
            NSData *data = [NSData dataWithContentsOfURL:fileUrl];
            NSLog(@"上传文件字节:%lu",(unsigned long)data.length);
            [formData appendPartWithFileData:data name:@"file" fileName:fileName mimeType:@"audio/mpeg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%@",uploadProgress);
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#if OpenNetLog
        NSLog(@"%@",responseObject);
#endif
        NSInteger code = [[responseObject objectForKey:@"code"] integerValue];
        if(code == 0){
            success([responseObject objectForKey:@"msg"]);
        }else{
            if (failure) {
                NSString *message = [responseObject objectForKey:@"error"];
                NSLog(@"上传文件失败,%@",message);
                failure([NSError bussinessError:code message:message]);
            }
        }
        
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          NSLog(@"上传文件失败,%@",error);
          failure(error);
      }];
}

@end
