//
//  K18Song.h
//  18kcy
//
//  Created by 唐 on 2019/6/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class K18Accompany;
/**
 歌曲
 */
@interface K18Song : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cySinger;//歌手名字
@property(strong, nonatomic) NSString *cyImg;//封面
@property(strong, nonatomic) NSNumber *cySongNum;//演唱次数
@property(strong, nonatomic) NSString *cySongName;//歌名

@property(strong, nonatomic) K18Accompany *accompany;

+ (instancetype) testNew;

@end


/**
 歌曲分类项
 */
@interface K18Category : NSObject
@property(strong, nonatomic) NSString *cyImg;//分类背景图
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyTypeName;//分类名
@property(strong, nonatomic) NSString *cyZimu;//该分类对应的字母
@end


/**
 伴奏
 */
@interface K18Accompany : NSObject
@property(strong, nonatomic) NSString *cySongSize;//歌曲大小   /1024/1024  =多少M
@property(strong, nonatomic) NSString *cySongTime;
@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyLyricUrl;//歌词地址
@property(strong, nonatomic) NSString *cySongName;//歌名
@property(strong, nonatomic) NSString *cySongUrl;//伴奏地址
@property(strong, nonatomic) NSString *cyLyricStr;//歌词字符串
@property(strong, nonatomic) NSString *cyOriginalSongUrl;
@property(strong, nonatomic) NSString *cyStartTime;//毫秒
@property(strong, nonatomic) NSString *cyKrcLyricStr;//krc歌词
@property(strong, nonatomic) NSString *cySinger;
@end


/**
 服务器的录音文件信息
 */
@interface K18File : NSObject

@property(strong, nonatomic) NSNumber *fileSize;
@property(strong, nonatomic) NSString *fileUrl;
@property(strong, nonatomic) NSString *id;
@property(assign, nonatomic) int cyFileType;

@end


/**
 录制的歌曲详情信息
 */
@interface K18SongDetail : NSObject
@property(strong, nonatomic) NSString *id;

@property(strong, nonatomic) NSString *cyUserId;//用户id
@property(strong, nonatomic) NSString *userName;//用户名
@property(strong, nonatomic) NSString *cyLyricUrl;
@property(strong, nonatomic) NSString *cyLyricStr;//歌词字符串
@property(strong, nonatomic) NSString *cySongName;//歌名
@property(strong, nonatomic) NSString *cyMood;//心情
@property(strong, nonatomic) NSNumber *cyFlowerNum; //鲜花数
@property(strong, nonatomic) NSArray *userPhotos;//背景图片，数组
@property(strong, nonatomic) NSString *cySongLiveUrl;

@property(strong, nonatomic) NSString *userImgUrl;//用户头像
@property(strong, nonatomic) NSNumber *cyShareNum;//分享数
@property(strong, nonatomic) NSNumber *cyListenNum;//收听数
@property(strong, nonatomic) NSString *createTimeStr;//演唱创建时间
@property(strong, nonatomic) NSString *createTimeUTC;//演唱创建时间（秒）
@property(strong, nonatomic) NSNumber *cyScore;
@property(strong, nonatomic) NSString *cySongId;//伴奏id
@property(strong, nonatomic) NSNumber *collectFlag;//收藏/不收藏
@property(strong, nonatomic) NSNumber *userFollowFlag;//1，已关注，0，取消关注

@property(strong, nonatomic) NSString *cyShareUrl;//（分享的链接）

@end


/**
 录制的歌曲评论
 */
@interface K18SongComment : NSObject
@property(strong, nonatomic) NSNumber *cyCreateTimeUTC;
@property(strong, nonatomic) NSString *cyCommentUserId;//评论的用户id
@property(strong, nonatomic) NSString *cyContent;//评论的内容
@property(strong, nonatomic) NSString *cyImgUrl;//评论的用户的头像
@property(strong, nonatomic) NSString *cySongLiveId;//评论的歌曲id
@property(strong, nonatomic) NSString *cyCommentName;//评论的用户名

@end
