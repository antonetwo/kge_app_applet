//
//  K18Singer.h
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/**
 歌手类别
 */
@interface K18Singer : NSObject

@property(strong, nonatomic) NSString *cyImgUrl;//歌手头像
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyName;//歌手名字

+ (instancetype) testNew;

@end

NS_ASSUME_NONNULL_END
