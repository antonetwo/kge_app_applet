//
//  K18Img.h
//  18kcy
//
//  Created by beuady on 2019/6/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18Img : NSObject
@property(strong, nonatomic) NSString *cyImgUrl;
@property(assign, nonatomic) int cyType;
@property(strong, nonatomic) NSString *id;//图片在相册中的id
@end

NS_ASSUME_NONNULL_END
