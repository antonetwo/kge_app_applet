//
//  K18Rank.h
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/**
 K歌模块
 0,人气榜 1,纯音榜
 {
 "cyImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/img-0-1563786697467846.jpg,       用户头像图片
 "rankNum": 1,                            排名
 "cyLat": 0,
 "cySongLiveFileUrl": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/song/songlive-100000005-1563963607087219.mp3,
 "cyLng": 0,
 "cyCreateTime": "2019-07-29 18:20:08",
 "cyLocationFlag": 0,
 "cyExamine": 0,
 "cyUserId": 100000005,
 "cySongName": "你的酒馆对我打了烊",     歌名
 "cyLevel": "SS",
 "cyMood": "IOS reverb test",
 "cyFlowerNum": 300,
 "cyType": 0,
 "cySongLiveFileId": 825,
 "cyUserName": "vxvxcvxc",              作者名
 "cyPhotoIds": "",
 "cyShareNum": 0,
 "userPhotoImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/img-0-1563786697467846.jpg,    作品图片
 "id": 93,
 "cyListenNum": 2,
 "cySongId": 4,
 "cySongLiveSize": 0,
 "cyScore": 0
 },
 */
@interface K18Rank : NSObject
@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *rankNum;
@property(strong, nonatomic) NSString *cyLat;
@property(strong, nonatomic) NSString *cySongLiveFileUrl;
@property(strong, nonatomic) NSString *cyLng;
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyLocationFlag;
@property(strong, nonatomic) NSString *cyExamine;
@property(strong, nonatomic) NSString *cyUserId;
@property(strong, nonatomic) NSString *cySongName;
@property(strong, nonatomic) NSString *cyLevel;
@property(strong, nonatomic) NSString *cyMood;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *cyType;//0,人气榜 1,纯音榜
@property(strong, nonatomic) NSString *cySongLiveFileId;
@property(strong, nonatomic) NSString *cyUserName;
@property(strong, nonatomic) NSString *cyPhotoIds;
@property(strong, nonatomic) NSString *cyShareNum;
@property(strong, nonatomic) NSString *userPhotoImg;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyListenNum;
@property(strong, nonatomic) NSString *cySongLiveSize;
@property(strong, nonatomic) NSString *cyScore;
@property(strong, nonatomic) NSString *cySongId;

@end


/**
 K歌模块
 金主榜
 
 {
 "rankNum": 1,                            排名
 "cyFlowerNum": 300,
 "cyLiveRoomId": 0,
 "cyType": 0,
 "lastWeekRankStr": "上周第一",     上周第一
 "cyCreateTime": "2019-07-30 12:21:21",
 "cyImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/pipixiong.jpg,                  图片
 "cyUserName": "啤啤熊",           昵称
 "cyTotalFlowerNum": 700,          贡献
 "id": 2,
 "cySongLiveId": 1,
 "cyIncomeUserId": 100000005,
 "cySendUserId": 100000004          该昵称用户的id
 },
 */
@interface K18GoldRank : NSObject
@property(strong, nonatomic) NSString *rankNum;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *cyLiveRoomId;
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *lastWeekRankStr;
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *cyUserName;
@property(strong, nonatomic) NSString *cyTotalFlowerNum;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cySongLiveId;
@property(strong, nonatomic) NSString *cyIncomeUserId;
@property(strong, nonatomic) NSString *cySendUserId;

@end


/**
直播模块
 人气榜单
 
 {
 "cyCreateTime": "2019-08-05 20:55:35",
 "cyImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/img-100000036-1564738642759549.jpg,    头像图片
 "cyTotalFlowerNum": 104,                热度
 "cySongLiveId": 0,
 "cyFlowerNum": 1,
 "cyLiveRoomId": 98,
 "cyType": 1,
 "lastWeekRankStr": "上周第三",         上周第一
 "rankNum": 1,                            排名
 "cyUserName": "哦哦哦2",                昵称
 "id": 242,
 "cyIncomeUserId": 100000036            用户id（打榜送花）
 "cySendUserId": 100000036              用户id（打榜送花）
 "cyUserId": 100000036                   用户id（打榜送花）
 },
 */
@interface K18ManRank : NSObject
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *cyTotalFlowerNum;
@property(strong, nonatomic) NSString *cySongLiveId;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *cyLiveRoomId;
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *lastWeekRankStr;
@property(strong, nonatomic) NSString *rankNum;
@property(strong, nonatomic) NSString *cyUserName;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyIncomeUserId;
@property(strong, nonatomic) NSString *cySendUserId;
@property(strong, nonatomic) NSString *cyUserId;

@end


/**
 直播模块
 金主榜单
 
 {
 "rankNum": 1,                            排名
 "cyFlowerNum": 1,
 "cyLiveRoomId": 6,
 "cyType": 1,
 "lastWeekRankStr": "上周第二",           上周第二
 "cyCreateTime": "2019-07-30 12:21:21",
 "cyImg": http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKMX2wBYnRnBlulM74qo70ASLUmh3Xicn9hPMPdto5FRIrfWfWicNfolY91jcQV7kTibfHeiaH2CzQyFQ/132,                                    图片
 "cyUserName": "亮尾巴*",                 昵称
 "cyTotalFlowerNum": 184,                 贡献
 "id": 138,
 "cySongLiveId": 0,
 "cyIncomeUserId": 100000007,
 "cySendUserId": 100000017                 该昵称用户的id
 },
 */
@interface K18MusicRank : NSObject
@property(strong, nonatomic) NSString *rankNum;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *cyLiveRoomId;
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *lastWeekRankStr;
@property(strong, nonatomic) NSString *cyCreateTime;

@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *cyUserName;
@property(strong, nonatomic) NSString *cyTotalFlowerNum;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cySongLiveId;
@property(strong, nonatomic) NSString *cyIncomeUserId;
@property(strong, nonatomic) NSString *cySendUserId;
@end

NS_ASSUME_NONNULL_END
