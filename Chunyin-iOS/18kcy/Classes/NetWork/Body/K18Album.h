//
//  K18Album.h
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/**
 {
 "totalRow": 2,                             总行数
 "pageNumber": 1,                           第几页
 "firstPage": true,
 "lastPage": false,
 "totalPage": 2,                            页数
 "pageSize": 1,                             每页的数量
 "list": [
 {
 "cyType": 1,
 "cyCreateTime": "2019-06-23 19:30:12",
 "cyExamine": 0,
 "cyImgUrl": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/touxiangImg.png,            照片url
 "cyUserId": 100000004,
 "id": 10                       照片id
 */
@interface K18Album : NSObject
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyExamine;
@property(strong, nonatomic) NSString *cyImgUrl;
@property(strong, nonatomic) NSString *cyUserId;
@end

NS_ASSUME_NONNULL_END
