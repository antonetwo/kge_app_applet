//
//  K18SearchM.h
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 "cyPhone": "18814118084",
 "cyBirthday": "2009-02-19 00:00:00",
 "cyImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/pipixiong.jpg,                      图片
 "cyCreateTime": "2019-06-04 17:04:42",
 "songLivesSize": 2,                   作品
 "cyType": 2,
 "cyFlowerNum": 96,
 "cyPassword": "222222",
 "cyUnionid": "qwerttyuio",
 "cyImgId": 10,
 "userFansSize": 3,                    粉丝
 "cyRefreshToken": "qwerttyuio",
 "cyLaheiTime": "2019-06-06 09:38:34",
 "id": 100000004,
 "cyLaheiReason": "",
 "cyExpiresIn": 48000,
 "cyPersonalSign": "个人签名",
 "cyLevelJifen": 1000,
 "cyXcxOpenId": "qwerttyuio",
 "cyCounty": "",
 "cyFollow": 0,                       0未关注  1已关注
 "cyOpenid": "qwerttyuio",
 "cyUserName": "啤啤熊",              名字
 "cyAccessToken": "qwerttyuio",
 "cyCity": "广州",
 "cySex": 0,
 "cyProvince": "广东",
 "cyIsNew": 0,
 "cyLahei": 1
 */
@interface K18SearchM : NSObject
@property(strong, nonatomic) NSString *cyPhone;
@property(strong, nonatomic) NSString *cyBirthday;
@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *songLivesSize;
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *userFansSize;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyPersonalSign;
@property(strong, nonatomic) NSString *cyLevelJifen;
@property(strong, nonatomic) NSString *cyFollow;
@property(strong, nonatomic) NSString *cyUserName;
@property(strong, nonatomic) NSString *cyCity;
@property(strong, nonatomic) NSString *cySex;


@end

NS_ASSUME_NONNULL_END
