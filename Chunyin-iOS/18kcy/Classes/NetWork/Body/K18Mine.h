//
//  K18Mine.h
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18Mine : NSObject
@property(strong, nonatomic) NSString* cyImg;
@property(strong, nonatomic) NSString* cyUserName;
@property(strong, nonatomic) NSString* cyUserId;
@property(strong, nonatomic) NSString* id;
@property(strong, nonatomic) NSString* cyFollow;

@end


/**
 "cyImg": http://thirdwx.qlogo.cn/mmopen/vi_32/RMJpC9GSuJhGDtnAt1tTAH0EOsPYpSJnURwTDU98JSYFnMWPAhzsetHFDHWVK0I1rYibwfx4kia0W38SWgtC9aZg/132,                                      图片
 "cyUserName": "一个脱离了高级趣",        姓名
 "cyUserId": 100000007,                   粉丝id
 "id": 35,
 "cyFollow": 1
 */
@interface K18Fensi : K18Mine

@end


/**
 "cyImg": http://thirdwx.qlogo.cn/mmopen/vi_32/RMJpC9GSuJhGDtnAt1tTAH0EOsPYpSJnURwTDU98JSYFnMWPAhzsetHFDHWVK0I1rYibwfx4kia0W38SWgtC9aZg/132,                                       图片
 "cyUserName": "一个脱离了高级趣",         姓名
 "cyUserId": 100000008,                   我关注的id
 "id": 20,
 "cyFollow": 1
 */
@interface K18Concern : K18Mine

@end


/**
 {
 "songName": "K歌之王",                    歌名
 "photoUrl": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/img-0-1559987035138995.jpg,          图片
 "cyFlag": 1,
 "cyUserId": 100000005,
 "id": 2,
 "cyCollectSongId": 1,
 "userName": "啤啤熊"                       用户名
 }
 */
@interface K18Collect : NSObject
@property(strong, nonatomic) NSString* songName;
@property(strong, nonatomic) NSString* photoUrl;
@property(strong, nonatomic) NSString* cyFlag;
@property(strong, nonatomic) NSString* cyUserId;
@property(strong, nonatomic) NSString* id;
@property(strong, nonatomic) NSString* cyCollectSongId;
@property(strong, nonatomic) NSString* userName;

@end


/**
 {
 "cyFlowerNum": 50,               鲜花数
 "cyLiveRoomId": 5,
 "cyType": 1,
 "id": 174,
 "cySongLiveId": 0,
 "userName": "一个脱离了高级趣",   用户名
 "cyIncomeUserId": 100000007,
 "cySendUserId": 100000008
 },
 */
@interface K18GfList : NSObject
@property(strong, nonatomic) NSString* cyFlowerNum;
@property(strong, nonatomic) NSString* cyLiveRoomId;
@property(strong, nonatomic) NSString* cyType;
@property(strong, nonatomic) NSString* id;
@property(strong, nonatomic) NSString* cySongLiveId;
@property(strong, nonatomic) NSString* userName;
@property(strong, nonatomic) NSString* cyIncomeUserId;
@property(strong, nonatomic) NSString* cySendUserId;

@end

/*
{
 songLiveId
    "songName": "K歌之王",                歌名
    "cyBroadcastId": 1,
    "photoUrl": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/img-0-1559987035138995.jpg,        图片
    "cyCreateTime": "2019-07-29 19:52:35",
    "broadTimeUTC": 1564401155111,           播放时间，毫秒
    "cyUserId": 100000008,
    "id": 2,
    "userName": "啤啤熊"                 用户名（演唱者）
}*/
@interface K18Record : NSObject
@property(strong, nonatomic) NSString* songLiveId;
@property(strong, nonatomic) NSString* songName;
@property(strong, nonatomic) NSString* cyBroadcastId;
@property(strong, nonatomic) NSString* photoUrl;
@property(strong, nonatomic) NSString* cyCreateTime;
@property(strong, nonatomic) NSString* broadTimeUTC;
@property(strong, nonatomic) NSString* cyUserId;
@property(strong, nonatomic) NSString* id;
@property(strong, nonatomic) NSString* userName;

@end

NS_ASSUME_NONNULL_END
