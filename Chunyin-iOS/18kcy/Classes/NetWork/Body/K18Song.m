//
//  K18Song.m
//  18kcy
//
//  Created by 唐 on 2019/6/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18Song.h"

@implementation K18Song

+ (instancetype)testNew
{
	K18Song *m = [K18Song new];
	m.cySinger = @"陈奕迅";
	m.cyImg = @"https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/kgezhiwang.jpg";
	m.cySongNum = @2;
	m.id = @"1";
	m.cySongName = @"K歌之王";
	return m;
}

@end

@implementation K18Category
@end

@implementation K18Accompany
@end

@implementation K18File
@end

@implementation K18SongDetail
//+ (NSDictionary *)modelContainerPropertyGenericClass {
//    // value should be Class or Class name.
//    return @{@"shadows" : [Shadow class],
//             @"borders" : Border.class,
//             @"attachments" : @"Attachment" };
//}
@end

@implementation K18SongComment


@end
