//
//  User.h
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18Img.h"

NS_ASSUME_NONNULL_BEGIN

@interface User : NSObject
@property(strong, nonatomic) NSString *id;//用户id（userId）
@property(strong, nonatomic) NSString *cyExpiresIn;//过期时间
@property(strong, nonatomic) NSString *cyPersonalSign;//个人签名
@property(strong, nonatomic) NSString *cyPhone;
@property(strong, nonatomic) NSString *cyBirthday;//出生日期
@property(strong, nonatomic) NSString *birthdayStr;//生日
@property(strong, nonatomic) NSString *cyImg;//头像地址
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSNumber *cyLevelJifen;
@property(strong, nonatomic) NSString *cyXcxOpenId;//小程序openId
@property(strong, nonatomic) NSNumber *cyType;//0，微信注册，1，qq注册，2，手机号注册
@property(strong, nonatomic) NSNumber *cyFlowerNum;//鲜花数量
@property(strong, nonatomic) NSString *cyUnionid;//Unionid(微信，小程序)
@property(strong, nonatomic) NSString *cyOpenid;//opened(微信，qq)
@property(strong, nonatomic) NSString *cyRefreshToken;
@property(strong, nonatomic) NSString *cyUserName;//昵称
@property(strong, nonatomic) NSString *cyAccessToken;//AccessToken(微信，qq)
@property(strong, nonatomic) NSString *cyCity;//城市
@property(strong, nonatomic) NSNumber *cySex;//0,女  1,男
@property(strong, nonatomic) NSString *cyProvince;//省
@property(strong, nonatomic) NSNumber *cyIsNew;//1新用户 ，0旧用户

@property(strong, nonatomic) NSNumber *cyLahei;//0,正常用户， 1拉黑用户
@property(strong, nonatomic) NSString *cyLaheiTime;//拉黑时间
@property(strong, nonatomic) NSString *cyLaheiReason;//投诉

@property(strong, nonatomic) K18Img *headImg;


@end


/**
 查看他人的个人信息
 "flowerNum": 1500,                       //赠送的鲜花数
 "cyPhone": "18814118084",                //手机号
 "cyImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/touxiangImg.png,                                  //头像图片
 "birthdayUTC": 1234972800,               //生日时间戳秒
 "userPhotosSize": 1,                     //相册数
 "cyType": 2,                             //
 "cyFlowerNum": 97,                       //用户持有的鲜花数
 "cyUnionid": "qwerttyuio",               //
 "cyImgId": 10,                           //
 "userFansSize": 2,                       //粉丝数
 "userLevel": 1,                          //用户等级
 "cyRefreshToken": "qwerttyuio",          //
 "thousandFlowerNum": "1.5",              //赠送的鲜花数（千朵）
 "id": 100000004,                         //用户id
 "birthdayStr": "02-19",                  //
 "KBiMoney": 280,                         //k币数
 "cyExpiresIn": 48000,                    //
 "cyPersonalSign": "个人签名",            //
 "cyLevelJifen": 1000,                    //等级积分
 "cyXcxOpenId": "qwerttyuio",             //
 "userFollowsSize": 2,                    //关注数
 "createTimeUTC": 1559639082,             //创建时间戳秒
 "cyCounty": "",                          //国家
 "cyOpenid": "qwerttyuio",                //
 "thousandKBiMoney": 0,                   //k币数（千个）
 "cyUserName": "啤啤熊",                  //用户昵称
 "cyAccessToken": "qwerttyuio",           //
 "cyCity": "广州",                        //城市
 "cySex": 0,                              //0,未知，1 男,2 女
 "cyProvince": "广东",                    //省
 "cyIsNew": 0,                            //0旧用户  1新用户
 "age": 10                                //年龄
 */
@interface K18User : NSObject
@property(strong, nonatomic) NSString *flowerNum;
@property(strong, nonatomic) NSString *cyPhone;
@property(strong, nonatomic) NSString *cyImg;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *cyUnionid;
@property(strong, nonatomic) NSString *cyImgId;
@property(strong, nonatomic) NSString *userPhotosSize;//相册数
@property(strong, nonatomic) NSString *userFansSize;//粉丝数
@property(strong, nonatomic) NSString *userLevel;//用户等级
@property(strong, nonatomic) NSString *cyRefreshToken;
@property(strong, nonatomic) NSString *thousandFlowerNum;//赠送的鲜花数（千朵）
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *birthdayStr;
@property(strong, nonatomic) NSString *birthdayUTC;
@property(strong, nonatomic) NSString *KBiMoney;
@property(strong, nonatomic) NSString *cyExpiresIn;
@property(strong, nonatomic) NSString *cyPersonalSign;//个人签名
@property(strong, nonatomic) NSString *cyLevelJifen;//
@property(strong, nonatomic) NSString *userFollowsSize;//关注数
@property(strong, nonatomic) NSString *createTimeUTC;
@property(strong, nonatomic) NSString *cyCounty;
@property(strong, nonatomic) NSString *cyOpenid;
@property(strong, nonatomic) NSString *thousandKBiMoney;
@property(strong, nonatomic) NSString *cyUserName;
@property(strong, nonatomic) NSString *cyAccessToken;
@property(strong, nonatomic) NSString *cyCity;
@property(strong, nonatomic) NSString *cySex;
@property(strong, nonatomic) NSString *cyProvince;
@property(strong, nonatomic) NSString *cyIsNew;
@property(strong, nonatomic) NSString *age;

- (NSString *)genderStr;

@end

NS_ASSUME_NONNULL_END
