//
//  User.m
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "User.h"

@implementation User

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.id = @"";
	}
	return self;
}
@end

@implementation K18User

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.id = @"";
	}
	return self;
}

- (NSString *)genderStr
{
    if (self.cySex.intValue==1) {
        return @"男";
    }else if(self.cySex.intValue==2){
        return @"女";
    }
    return @"未知";
}

@end
