//
//  K18Music.h
//  18kcy
//
//  Created by beuady on 2019/8/27.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/**
 "cyImg": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/image/kgezhiwang.jpg,                    歌曲图片
 "cyCreateTime": "2019-06-06 23:40:33",
 "cySongNum": 37,
 "cyLyricFileId": 820,
 "cyOriginalSongFileId": 0,
 "cyLyricUrl": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/lyric/lrc-0-1563811187953984.krc,
 "cySongName": "K歌之王",              歌名
 "cyTypeZimus": "ab",
 "cySongSize": 1814049,                歌曲大小
 "cySinger": "陈奕迅",                 歌手
 "cySingerId": 1,
 "cySongFileId": 821,
 "cySongUrl": https://chunyin-1259292569.cos.ap-chengdu.myqcloud.com/song/banzou-0-1563811202501328.mp3,   歌曲url
 "cyFlag": 0,
 "cyOriginalSongUrl": "",
 "id": 1,                              歌曲id
 "cyImgFileId": 0,
 "singerImgUrl": http://y.gtimg.cn/music/photo_new/T001R150x150M000003IuZPu2KgknH.webp
 
 */
@interface K18Music : NSObject
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cySongName;
@property(strong, nonatomic) NSString *cySongUrl;
@property(strong, nonatomic) NSString *cySinger;

@end

NS_ASSUME_NONNULL_END
