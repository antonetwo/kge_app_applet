//
//  K18SlidePic.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18SlidePic : NSObject
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *cySort;
@property(strong, nonatomic) NSString *cyPostUrl;//
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyFlag;
@property(strong, nonatomic) NSString *cyImgUrl;//
@property(strong, nonatomic) NSString *id;
@end

NS_ASSUME_NONNULL_END
