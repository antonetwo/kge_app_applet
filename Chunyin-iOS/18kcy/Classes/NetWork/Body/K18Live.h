//
//  K18Live.h
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/**
 直播间列表元对象
 */
@interface K18Live : NSObject
@property(strong, nonatomic) NSString *cyLiveImgUrl;//直播缩略图
@property(strong, nonatomic) NSString *cyLat;
@property(strong, nonatomic) NSString *cyLng;
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyLocationFlag;
@property(strong, nonatomic) NSString *cyUserId;
@property(strong, nonatomic) NSString *cyVisitorNum;
@property(strong, nonatomic) NSString *cyTuiliuUrl;
@property(strong, nonatomic) NSString *cyContent;//直播间名称
@property(strong, nonatomic) NSString *cyExpiresDate;
@property(strong, nonatomic) NSString *cyLiveImgId;
@property(strong, nonatomic) NSString *cyFlag;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyLaliuUrl;
@property(strong, nonatomic) NSString *cyGiftFlower;
@property(strong, nonatomic) NSString *cyGiftValue;
@property(strong, nonatomic) NSString *cyLevel;

+ (instancetype)testNew;
@end

//礼物列表单元对象
@interface K18Gift : NSObject

@property(strong, nonatomic) NSString *cyLiveRoomId;
@property(strong, nonatomic) NSString *cyType;
@property(strong, nonatomic) NSString *userSex;
@property(strong, nonatomic) NSString *userImg;
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *userName;
@property(strong, nonatomic) NSString *cyTotalFlowerNum;//送花数
@property(strong, nonatomic) NSString *cySendUserId;//送花人的id

@end


NS_ASSUME_NONNULL_END
