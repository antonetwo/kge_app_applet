//
//  K18ListItem.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18ListItem : NSObject
@property(strong, nonatomic) NSString *commentSize;//评论数
@property(strong, nonatomic) NSString *cyLat;
@property(strong, nonatomic) NSString *cyLng;
@property(strong, nonatomic) NSString *cyCreateTime;
@property(strong, nonatomic) NSString *cyLocationFlag;
@property(strong, nonatomic) NSString *cyUserId;//用户id
@property(strong, nonatomic) NSString *createTimeUTC;

@property(strong, nonatomic) NSString *cySongName;
@property(strong, nonatomic) NSString *cyLevel;//分数评级
@property(strong, nonatomic) NSString *cyMood;
@property(strong, nonatomic) NSString *cyFlowerNum;
@property(strong, nonatomic) NSString *cySongLiveFileId;//?
@property(strong, nonatomic) NSString *cyPhotoIds;//"5_6"
@property(strong, nonatomic) NSString *cyShareNum;
@property(strong, nonatomic) NSString *userPhotoImg;//演唱完的歌曲背景图
@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cyListenNum;//收听数
@property(strong, nonatomic) NSString *cySongId;//伴奏id
@property(strong, nonatomic) NSString *cyScore;//分数
@property(strong, nonatomic) NSString *userHeadImg;//
@property(strong, nonatomic) NSString *cyUserName;//用户名
@property(strong, nonatomic) NSString *cySongLiveFileUrl;//演唱完的歌曲地址

@end

NS_ASSUME_NONNULL_END
