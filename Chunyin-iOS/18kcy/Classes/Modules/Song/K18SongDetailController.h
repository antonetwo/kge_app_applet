//
//  K18SongDetailController.h
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18SongDetailController : K18BaseController

@property(strong, nonatomic) NSString *songLiveId;

@end

NS_ASSUME_NONNULL_END
