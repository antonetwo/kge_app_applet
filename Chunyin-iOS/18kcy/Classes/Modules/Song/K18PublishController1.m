//
//  K18PublishController1.m
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18PublishController1.h"
#import "IconButton.h"
#import "K18Song.h"
#import "K18ReverbView.h"
#import "K18ScoreView.h"
#import "K18PublishController2.h"
#import "K18AudioService.h"
#import <math.h>
//#import "GLMusicPlayer.h"
#import "K18Slider.h"
#import "K18DoubleModel.h"
#import "K18AudioEngine.h"
#import "K18FileService.h"
#import "K18SongManager.h"

@interface K18PublishController1 ()<K18AudioEngineDelegate>
@property (weak, nonatomic) IBOutlet UIView *scoreBarView;

//
@property (weak, nonatomic) IBOutlet UIView *preView;
@property (weak, nonatomic) IBOutlet UIImageView *preView_Img;
@property (weak, nonatomic) IBOutlet UISlider *preview_Slide;
@property (weak, nonatomic) IBOutlet UILabel *preview_Label;
//
@property (weak, nonatomic) IBOutlet UIView *modeBarView;

//
@property (weak, nonatomic) IBOutlet UIButton *publishButton;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UILabel *accompanyTip;
@property (weak, nonatomic) IBOutlet K18Slider *accompanySlider;
@property (weak, nonatomic) IBOutlet K18Slider *manSlider;


@property(strong, nonatomic) K18ReverbView *reverbView;
@property(strong, nonatomic) K18ScoreView *scoreView;

@property(strong, nonatomic) K18DoubleModel *doubleModel;
@property(strong, nonatomic) id hud;

@property(strong, nonatomic) NSURL *offlineUrl;//导出的人声文件url
@property(assign, nonatomic) BOOL isSavedLocal;
@end

@implementation K18PublishController1

- (void)dealloc
{
    self.doubleModel = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.forbidGuestureBack = YES;
	self.navigationItem.leftBarButtonItem = self.leftBarBackItem;
}

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self addEvents];
//	NSLog(@"viewWillAppear");
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self removeEvents];
	[self.doubleModel stop];
    NSLog(@"[self.doubleModel stop];");
}

#pragma mark - init UI

- (void) initUI {
	self.publishButton.backgroundColor = k18_TopicC;
	
	IconButton *resetBtn = [[IconButton alloc]init];
	[resetBtn setTitle:@"重唱" forState:(UIControlStateNormal)];
	[resetBtn setImage:[UIImage imageNamed:@"chongchang"] forState:(UIControlStateNormal)];
	[resetBtn setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateNormal)];
	resetBtn.titleLabel.font = [UIFont systemFontOfSize:12];
	[self.view addSubview:resetBtn];
	[resetBtn addTarget:self action:@selector(resetA) forControlEvents:(UIControlEventTouchUpInside)];
	
	IconButton *saveBtn = [[IconButton alloc]init];
	[saveBtn setTitle:@"保存" forState:(UIControlStateNormal)];
	[saveBtn setImage:[UIImage imageNamed:@"baocun"] forState:(UIControlStateNormal)];
	[saveBtn setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateNormal)];
	saveBtn.titleLabel.font = [UIFont systemFontOfSize:12];
	[self.view addSubview:saveBtn];
	[saveBtn addTarget:self action:@selector(saveA) forControlEvents:(UIControlEventTouchUpInside)];
	
	[resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(25, 50));
		make.right.equalTo(self.publishButton.mas_left).offset(-37);
		make.centerY.equalTo(self.publishButton);
	}];
	[saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(25, 50));
		make.left.equalTo(self.publishButton.mas_right).offset(37);
		make.centerY.equalTo(self.publishButton);
	}];
	
	self.publishButton.layer.cornerRadius = 4;
	[self.publishButton addTarget:self action:@selector(publishA) forControlEvents:(UIControlEventTouchUpInside)];

	self.preView.layer.cornerRadius = 8;
	self.preView.layer.borderColor = RGBOF(0xDDDDDD).CGColor;
	self.preView.layer.borderWidth = 1;
//    self.preView_Img.image = [UIImage imageNamed:@"changpian"];
	
	__weak typeof(self)wself = self;
	_reverbView = [[K18ReverbView alloc]initWithFrame:self.modeBarView.frame];
	[self.view addSubview:_reverbView];
	_reverbView.clickBlock = ^(NSInteger index) {
		[wself changeReverbType:index];
	};
	
    [_reverbView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.modeBarView.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.mas_equalTo(100);
    }];
	
	_scoreView = [[K18ScoreView alloc]initWithFrame:CGRectMake(0, 5, MainScreenWidth()-28*2, 50)];
	[self.scoreBarView addSubview:_scoreView];
	
	[self.playButton addTarget:self action:@selector(togglePlay:) forControlEvents:(UIControlEventTouchUpInside)];
	[self.preview_Slide addTarget:self action:@selector(playChange:) forControlEvents:(UIControlEventValueChanged)];
    
    self.preView_Img.layer.cornerRadius = self.preView_Img.height/2.0;
    self.preView_Img.layer.masksToBounds = YES;

}

- (void) initData {
	
	self.title = self.model.song.cySongName;
	
	NSString *url = K18UserModel.shared.currentUserDetail.cyImg?K18UserModel.shared.currentUserDetail.cyImg : @"";
	[self.scoreView.scoreIcon sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
	
	NSString *imgUrl = self.model.song.cyImg ? self.model.song.cyImg : @"";
    [self.preView_Img sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"changpian"]];
    self.preView_Img.backgroundColor = [UIColor whiteColor];
//    self.preView_Img.layer.cornerRadius = self.preView_Img.height/2.0;
//    self.preView_Img.layer.masksToBounds = YES;
	
    [self updateTimeLabel:@0];
    
    //评分
    self.scoreView.score = self.model.recordModel.score;
    self.scoreView.rate = self.model.recordModel.rate;
	
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];

	//双音频播放
	K18Double *dModel = [K18Double new];
	dModel.firstVolume = 0.5;
	dModel.secondVolume = 0.5;
	dModel.firstUrl = self.model.recordModel.recordFileUrl;//第一声道用人声，时长<=伴奏
	dModel.secondUrl = self.model.recordModel.songFileUrl;//TODO 测试混响用了原唱
    self.doubleModel.model = dModel;
	self.doubleModel.useReverb = YES;
	
    self.doubleModel.firstPlayer.pitch = 0;
	self.doubleModel.secondPlayer.pitch = self.model.recordModel.pitch;
	
    
    [self.doubleModel playFromModel:dModel];
	
    self.accompanySlider.value = 0.5;
    self.manSlider.value = 0.5;
    
    if(self.model.recordModel.songFileUrl==nil){//清唱的时候
        [self updateUIToQingchang];
    }else{//伴奏的时候
        
    }
}

- (void)updateUIToQingchang{
    self.scoreView.hidden = YES;
    self.accompanyTip.hidden = YES;
    self.accompanySlider.hidden = YES;
}


#pragma mark - K18AudioEngineDelegate

- (void)onPlayStop:(K18AudioEngine *)engine
{
	self.preview_Slide.value = 1;
	[self updateTimeLabel:@(self.model.recordModel.recordDuration)];
	
	[self.playButton setSelected:NO];
	
	[self.doubleModel stop];
	
}

- (void)onPlaying:(K18AudioEngine *)engine currentTime:(NSTimeInterval)curTime
{
	CGFloat scaleTime = curTime/engine.playDuration;
	
	self.preview_Slide.value = scaleTime;
	[self updateTimeLabel:@(curTime)];

}

#pragma mark - actions

- (void) playChange:(K18Slider *)slider {
    NSTimeInterval currentSeconds = [K18AudioEngine shared].playDuration * slider.value;
    
    [self.doubleModel seekToPosition:currentSeconds];
}

- (void) changeReverbType:(NSInteger) type {
	self.doubleModel.reverbType = (K18ReverbType)(type+1);
}

- (void) resetA {
    [self.doubleModel pause];
    [HUDHelper alertTitle:@"录制未保存，确定重唱?" message:nil confirm:@"取消" cancel:@"确定" conBlock:^{
        [self.doubleModel play];
    } action:^{
        [self doReset];
        
    }];
}

- (void) doReset {
    [self.navigationController popViewControllerAnimated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:K18Event_RESONG object:nil];
    });
}

- (void) saveA {
    if (self.isSavedLocal) {
        [[HUDHelper sharedInstance] tipMessage:@"已保存"];
        return;
    }
    self.isSavedLocal = YES;
    
    if (self.model.recordModel.songFileUrl==nil) {//清唱
        
        self.hud = [[HUDHelper sharedInstance] loading:@"正在保存..."];
        __weak typeof(self)wself = self;
        K18RecordModel *m = self.model.recordModel;
        
        [self.doubleModel.firstPlayer playOfflineRenderFromURL:m.recordFileUrl succ:^(NSURL * _Nonnull exportURL) {
            [[HUDHelper sharedInstance] stopLoading:wself.hud];
            m.combineFileUrl = exportURL;
            wself.offlineUrl = exportURL;
            [wself doSave];
        } fail:^(NSError * _Nullable err) {
            [[HUDHelper sharedInstance] stopLoading:wself.hud message:@"保存异常" delay:1 completion:nil];
        }];
        
        
    }else{//伴奏
        
        [self combine:^{
            [self doSave];
        }];
    }
}

- (void) doSave {
    NSURL *url = self.model.recordModel.combineFileUrl;
    //TODO localSave
    NSError *err;
    NSString *fileName = [NSString stringWithFormat:@"%@.mp3",self.offlineUrl.lastPathComponent.stringByDeletingPathExtension];
    NSString *filePath = [[K18FileService recordSongPath] stringByAppendingPathComponent:fileName];
    [K18FileService cacheRecordSong:fileName
                          sourceURL:url error:&err];
    if(!err){
        [[HUDHelper sharedInstance] tipMessage:@"保存成功"];
        
        self.model.recordModel.combineFileUrl = [NSURL fileURLWithPath:filePath];
        [[K18SongManager shared] localSaveRecord:self.model];
        
    }else{
        [[HUDHelper sharedInstance] tipMessage:err.localizedDescription];
    }
}

- (void) startCombineWithUrls:(NSArray *)urls completion:(void(^)(void)) completion {
    
	NSLog(@"开始合并");
	__weak typeof(self)wself = self;
	K18RecordModel *m = self.model.recordModel;
	[[K18AudioService shared] combineRecordPath:urls[0]
										 bgmUrl:urls[1]
									   duration:m.recordDuration
									  srcVolume:self.doubleModel.model.firstVolume //人声是第一声道
									  bgmVolume:self.doubleModel.model.secondVolume resolve:^(id  _Nonnull result) {
										  [[HUDHelper sharedInstance] stopLoading:wself.hud];
										  wself.model.recordModel.combineFileUrl = result[@"exportURL"];
										  NSLog(@"合并完成");
										  NSLog(@"combineFileUrl = %@",wself.model.recordModel.combineFileUrl);
										  
										  if (completion) {
											  completion();
										  }
										  
										  
									  }];
}

static int step = 0;
- (void) combine:(void(^)(void)) completion {
	self.hud = [[HUDHelper sharedInstance] loading:@"正在合成..."];
	K18RecordModel *m = self.model.recordModel;
	__block NSURL *dealSongURL;
	__block NSURL *dealRecordURL;
	__weak typeof(self)wself = self;
	
	
	[self.doubleModel.firstPlayer playOfflineRenderFromURL:m.recordFileUrl succ:^(NSURL * _Nonnull exportURL) {
		dealRecordURL = exportURL;
        wself.offlineUrl = dealRecordURL;
		step++;
		if(step==2){
			[wself startCombineWithUrls:@[dealRecordURL, dealSongURL] completion:completion];
			step = 0;
		}
	} fail:^(NSError * _Nullable err) {
		[[HUDHelper sharedInstance] stopLoading:wself.hud message:@"合成异常" delay:1 completion:nil];
	}];
	
	[wself.doubleModel.secondPlayer playOfflineRenderFromURL:m.songFileUrl succ:^(NSURL * _Nonnull subExportURL) {
		dealSongURL = subExportURL;
		step++;
		if(step==2){
			[wself startCombineWithUrls:@[dealRecordURL, dealSongURL] completion:completion];
			step = 0;
		}
		
	} fail:^(NSError * _Nullable err) {
		[[HUDHelper sharedInstance] stopLoading:wself.hud message:@"合成异常" delay:1 completion:nil];
	}];
	
}

- (void) publishA {
    if (self.model.recordModel.songFileUrl==nil) {
        //清唱混响合成
        self.hud = [[HUDHelper sharedInstance] loading:@"正在合成..."];
        __weak typeof(self)wself = self;
        K18RecordModel *m = self.model.recordModel;
        
        [self.doubleModel.firstPlayer playOfflineRenderFromURL:m.recordFileUrl succ:^(NSURL * _Nonnull exportURL) {
            [[HUDHelper sharedInstance] stopLoading:wself.hud];
            m.combineFileUrl = exportURL;
            wself.offlineUrl = exportURL;
            
            K18PublishController2 *vc = [[K18PublishController2 alloc]initWithNibName:@"K18PublishController2" bundle:nil];
            vc.model = self.model;
            vc.hidesBottomBarWhenPushed = YES;
            [wself.navigationController pushViewController:vc animated:YES];
            
        } fail:^(NSError * _Nullable err) {
            [[HUDHelper sharedInstance] stopLoading:wself.hud message:@"合成异常" delay:1 completion:nil];
        }];
        
        
    }else{
        //合成伴奏
        
        __weak typeof(self)wself = self;
        [self combine:^{
            //
            K18PublishController2 *vc = [[K18PublishController2 alloc]initWithNibName:@"K18PublishController2" bundle:nil];
            vc.model = self.model;
            vc.hidesBottomBarWhenPushed = YES;
            [wself.navigationController pushViewController:vc animated:YES];
        }];
    }
}

- (void) togglePlay:(UIButton *)sender {
	self.playButton.selected = !self.playButton.selected;
	if (self.playButton.selected) {
		if([K18AudioEngine shared].isPausePlay){
			[self.doubleModel play];
		}else{
			[self.doubleModel playFromModel:self.doubleModel.model];
		}
		
	}else{

		[self.doubleModel pause];
		
	}
}

- (IBAction)changeVolume:(K18Slider *)sender {
    
    if (sender == self.accompanySlider) {
        self.doubleModel.model.secondVolume = sender.value;
    }else{
        self.doubleModel.model.firstVolume = sender.value;//第一声道是人声
    }
    [self.doubleModel updateVolume];
    
}

- (void)leftBarButtonAction
{
	[HUDHelper alertTitle:@"录制还没结束，确定要退出吗?" message:nil confirm:@"取消" cancel:@"退出" conBlock:^{
		
	} action:^{
		[self.navigationController popToRootViewControllerAnimated:YES];
	}];
}

#pragma mark - Notificatons

//- (void) onAudioStop:(NSNotification *)notify {
//	[self.playButton setSelected:NO];
//	[K18AudioService shared].player.url = [K18AudioService shared].player.url;
//}

//- (void)onAudioProgress:(NSNotification *)notify {
//    NSNumber *curTime = notify.userInfo[@"curTime"];
//
//    NSUInteger totalDur = ceil(self.model.recordModel.recordDuration);
//    CGFloat scaleTime = curTime.doubleValue/totalDur;
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//		self.preview_Slide.value = scaleTime;
//        [self updateTimeLabel:curTime];
//    });
//}

#pragma mark - private

- (void) updateTimeLabel:(NSNumber *)curTimeNum {
    NSUInteger curTime = floor(curTimeNum.doubleValue);
	K18RecordModel *m = self.model.recordModel;
	NSUInteger curSec = curTime%60;
	NSUInteger curMin = curTime/60;
	NSUInteger duration = floor(m.recordDuration);
	NSUInteger tSec = duration%60;
	NSUInteger tMin = duration/60;
	
	if (curMin == tMin) {
		curSec = MIN(curSec, tSec);
	}
	
	self.preview_Label.text = [NSString stringWithFormat:@"%02lu:%02lu/%02lu:%02lu",curMin,curSec,tMin,tSec];
}

- (void) addEvents {

	[K18AudioEngine shared].delegate = self;
	
}

- (void) removeEvents {

	[K18AudioEngine shared].delegate = nil;
}

- (K18DoubleModel *)doubleModel
{
	if (!_doubleModel) {
		_doubleModel = [K18DoubleModel new];
	}
		
	return _doubleModel;
}

@end
