//
//  K18CommentCell.m
//  18kcy
//
//  Created by beuady on 2019/6/15.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18CommentCell.h"
#import "K18Common.h"
@implementation K18CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.iconView.layer.cornerRadius = self.iconView.height/2;
    self.iconView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *nameL;
//@property (weak, nonatomic) IBOutlet UILabel *descL;
//@property (weak, nonatomic) IBOutlet UILabel *subL;
- (void) setModel:(K18SongComment *)model
{
	_model = model;
	NSString *url = model.cyImgUrl;
	[_iconView sd_setImageWithURL:[NSURL URLWithString:notNil(url)] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
	
	_nameL.text = model.cyCommentName;
	_descL.text = model.cyContent;
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	formatter.dateFormat = @"yyyy年MM月dd日 hh:mm";
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:model.cyCreateTimeUTC.longLongValue/1000.0];
	_subL.text = [formatter stringFromDate:date];
    
}

@end
