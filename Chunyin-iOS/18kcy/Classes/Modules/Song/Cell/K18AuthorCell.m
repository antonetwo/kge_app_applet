//
//  K18AuthorCell.m
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AuthorCell.h"
#import "K18Common.h"

@implementation K18AuthorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	self.imgView.layer.cornerRadius = self.imgView.height/2;
}

@end
