//
//  K18SongCell.h
//  18kcy
//
//  Created by 唐 on 2019/6/11.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Song.h"
#import "K18Common.h"

NS_ASSUME_NONNULL_BEGIN

@class K18SongCell;

@protocol K18SongCellDelegate <NSObject>

- (void) cell:(K18SongCell *)cell didSelect:(K18Song *) model;

@end

@interface K18SongCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property(strong, nonatomic) K18Song *model;
@property(weak, nonatomic) id<K18SongCellDelegate> delegate;

+ (CGFloat) cellHeight;

@end

NS_ASSUME_NONNULL_END
