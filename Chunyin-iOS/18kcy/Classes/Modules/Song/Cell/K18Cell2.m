//
//  K18Cell2.m
//  18kcy
//
//  Created by 唐 on 2019/6/11.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18Cell2.h"
#import "K18Common.h"
#import <SDWebImage/UIButton+WebCache.h>
@interface K18Cell2()
@property(strong, nonatomic) UIScrollView *scrollView;
@property(strong, nonatomic) NSMutableArray *buttons;

@property(nonatomic) CGRect aframe;
@end

@implementation K18Cell2

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.aframe = frame;
//		[self initUI];
	}
	return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDatas:(NSArray<K18Category *> *)datas
{
	_datas = datas;
	if(datas.count){
		[self initUI];
	}
}

- (void) initUI {
	[self removeAllSubViews];
	[self.buttons removeAllObjects];
	
//	NSArray *titles = @[@"热门歌手",@"节日",@"情感",@"主题",@"综艺",@"年代",@"流派",@"语种"];
//	NSArray *imgs = @[@"remengeshou",@"jieri",@"remengeshou",@"jieri",
//					  @"remengeshou",@"jieri",@"remengeshou",@"jieri"];
	
	_scrollView = [[UIScrollView alloc]initWithFrame:self.aframe];
	_scrollView.showsHorizontalScrollIndicator = NO;
	[self addSubview:_scrollView];
	
	_buttons = [NSMutableArray array];
	CGFloat w = 135;
	for (int i=0; i<self.datas.count; i++) {
		K18Category *model = self.datas[i];
		CGRect frame = CGRectMake(16+(w+9)*i, 0, w, w);
		UIButton *button = [[UIButton alloc]initWithFrame:frame];
		button.tag = i;
		[_buttons addObject:button];
		[self addSubview:button];
		[button addTarget:self action:@selector(clickAction:) forControlEvents:(UIControlEventTouchUpInside)];
		[button setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
		[button setTitle:model.cyTypeName forState:(UIControlStateNormal)];
//		[button setBackgroundImage:[UIImage imageNamed:imgs[i]] forState:(UIControlStateNormal)];
		NSString *url = model.cyImg?model.cyImg : @"";
		[button sd_setBackgroundImageWithURL:[NSURL URLWithString:url] forState:(UIControlStateNormal) placeholderImage:[UIImage imageNamed:@"fenlei1"]];
		button.titleEdgeInsets = UIEdgeInsetsZero;
		button.contentEdgeInsets = UIEdgeInsetsZero;
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        UIView *maskV = [[UIView alloc]initWithFrame:button.bounds];
        maskV.backgroundColor = RGBAOF(0, 0.3);
        maskV.userInteractionEnabled = NO;
        [button insertSubview:maskV belowSubview:button.titleLabel];
        
		[_scrollView addSubview:button];
	}
	
	UIButton *b = _buttons.lastObject;
	_scrollView.contentSize = CGSizeMake(CGRectGetMaxX(b.frame)+16, 0);
	
}

- (void) clickAction:(UIButton *)button {
	if (self.clickMenu) {
		self.clickMenu(button.tag, button.titleLabel.text);
	}
}

@end
