//
//  K18CommentCell.h
//  18kcy
//
//  Created by beuady on 2019/6/15.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Song.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18CommentCell : UITableViewCell

@property(strong, nonatomic) K18SongComment *model;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *descL;
@property (weak, nonatomic) IBOutlet UILabel *subL;

@end

NS_ASSUME_NONNULL_END
