//
//  K18QingChangController.m
//  18kcy
//
//  Created by beuady on 2019/8/23.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18QingChangController.h"
#import "IconButton.h"
#import "K18EarManager.h"
//#import "K18RecordModel.h"
#import "K18SongModel.h"
#import "K18SongService.h"
@interface K18QingChangController ()<K18AudioEngineDelegate>
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIView *recLayer;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeTipLabel;

@property(assign, nonatomic) BOOL isRecording;


@property(strong, nonatomic) K18RecordModel *recordModel;
@property(strong, nonatomic) K18EarManager *earManager;//耳返管理器
@property(strong, nonatomic) NSTimer *timer;
@property(assign, nonatomic) NSTimeInterval curTime;
@property(strong, nonatomic) NSString *songName;

@property(strong, nonatomic) UIAlertController *av;

@property(strong, nonatomic) UILabel *countLabel;//倒计时文本
@end

@implementation K18QingChangController
{
    dispatch_source_t countTimer;
}

- (void)dealloc
{
    [[K18AudioEngine shared] dispose];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:K18Event_RESONG object:nil];
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.recordModel = [K18RecordModel new];
        self.earManager = [K18EarManager new];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countdownRecord) name:K18Event_RESONG object:nil];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickLayer)];
    [self.recLayer addGestureRecognizer:tap];
    
    [self.startBtn addTarget:self action:@selector(countdownRecord) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)initUI
{
    IconButton *resetBtn = [[IconButton alloc]init];
    resetBtn.imageSize = CGSizeMake(25, 25);
    [resetBtn setTitle:@"重唱" forState:(UIControlStateNormal)];
    [resetBtn setImage:[UIImage imageNamed:@"q_chongchang"] forState:(UIControlStateNormal)];
    [resetBtn setTitleColor:RGBOF(0xffffff) forState:(UIControlStateNormal)];
    resetBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:resetBtn];
    [resetBtn addTarget:self action:@selector(resetA) forControlEvents:(UIControlEventTouchUpInside)];
    
    IconButton *saveBtn = [[IconButton alloc]init];
    saveBtn.imageSize = CGSizeMake(25, 25);
    [saveBtn setTitle:@"完成" forState:(UIControlStateNormal)];
    [saveBtn setImage:[UIImage imageNamed:@"q_wancheng"] forState:(UIControlStateNormal)];
    [saveBtn setTitleColor:RGBOF(0xffffff) forState:(UIControlStateNormal)];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:saveBtn];
    [saveBtn addTarget:self action:@selector(completionA) forControlEvents:(UIControlEventTouchUpInside)];
    
    [resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 50));
        make.right.equalTo(self.view.mas_centerX).offset(-100);
        make.bottom.equalTo(self.view.mas_bottomMargin).offset(-20);
    }];
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 50));
        make.left.equalTo(self.view.mas_centerX).offset(100);
        make.bottom.equalTo(self.view.mas_bottomMargin).offset(-20);
    }];
    
    _countLabel = [UILabel new];
    _countLabel.frame = self.startBtn.frame;
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.font = [UIFont systemFontOfSize:30];
    _countLabel.textColor = RGBOF(0x5A4F4A);
    [self.view addSubview:_countLabel];
    _countLabel.hidden = YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _countLabel.center = self.startBtn.center;
}

- (void)setIsRecording:(BOOL)isRecording
{
    _isRecording = isRecording;
    if (isRecording) {
        self.recLayer.hidden = NO;
        self.startBtn.hidden = YES;
    }else{
        self.recLayer.hidden = YES;
        self.startBtn.hidden = NO;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.lastNaviHidden = self.navigationController.navigationBarHidden;
    self.navigationController.navigationBarHidden = YES;
    [K18AudioEngine shared].delegate = self;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = self.lastNaviHidden;
    [K18AudioEngine shared].delegate = nil;
    [self.earManager stopListen];
    [[K18AudioEngine shared] stopRecord:nil];
}

#pragma mark - Actions

- (IBAction)backA:(id)sender {
    [self doPause];
    
    [HUDHelper alertTitle:@"录制未完成，确定退出?" message:nil confirm:@"取消" cancel:@"确定" conBlock:^{
        
        //        [self doResume];
    } action:^{
        [[K18AudioEngine shared] stopRecord:nil immediately:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}
- (void) resetA {
    [self doPause];
    
    [HUDHelper alertTitle:@"录制未完成，确定重唱?" message:nil confirm:@"取消" cancel:@"确定" conBlock:^{

//        [self doResume];
    } action:^{
        
        
        [self countdownRecord];
        
        
    }];
}

- (void) completionA {
#if NEED_SONG_10
    if (self.curTime < 10) {
        [[HUDHelper sharedInstance] tipMessage:@"录制时间太短，再唱一会吧~"];
        return;
    }
#endif
    
    //暂停录音
    [self doPause];
    
    UIAlertController *av = [UIAlertController alertControllerWithTitle:@"" message:@"提示" preferredStyle:(UIAlertControllerStyleAlert)];
    self.av = av;
    [av addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"给这段清唱命个名字吧";
    }];
    
    UIAlertAction *a1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleDefault) handler:nil];
    UIAlertAction *a2 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {

        if(!av.textFields.firstObject.text.length){

            [HUDHelper alert:@"请输入名称" action:^{

                [self completionA];
            }];
            return;
        }
        
        self.songName = av.textFields.firstObject.text;
        [[K18AudioEngine shared] stopRecord:nil];
        
    }];

    [av addAction:a1];
    [av addAction:a2];

    [self presentViewController:av animated:YES completion:nil];

}

-(void) clickLayer {
    
    if ([K18AudioEngine shared].isRecording) {

        [self doPause];
        
    }else{
        [self doResume];
        
    }
    
}

- (void) countdownRecord {
    static int counting = 0;
    if (counting!=0) {
        return;
    }
    counting = 0;
    _countLabel.hidden = NO;
    self.recLayer.hidden = YES;
    self.startBtn.hidden = YES;
    countTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(countTimer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, DISPATCH_TIME_FOREVER * NSEC_PER_SEC);
    dispatch_source_set_event_handler(countTimer, ^{
        counting++;
        
        self.countLabel.text = @(3-counting+1).stringValue;
        
        if (counting>3) {
            counting = 0;
            dispatch_cancel(self->countTimer);
            self->countTimer = NULL;
            self.countLabel.hidden = YES;
            [self startRecordA];
        }
    });
    dispatch_resume(countTimer);
}

-(void) startRecordA {
    [self.earManager startListen];
    __weak typeof(self)wself = self;

    self.isRecording = YES;
    self.timeLabel.text = @"00:00";
    self.curTime = 0;
    
    [K18AudioEngine setupAudioSession];
    
    //同时录制
    [[K18AudioEngine shared] startRecord:^{
        [wself startTimer];
        self.timeTipLabel.text = @"录制中";
        
    } fail:^(NSError * _Nonnull err) {
        [[HUDHelper sharedInstance] tipMessage:@"录制异常" delay:1 completion:^{
            [wself.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

#pragma mark - K18AudioEngineDelegate

- (void) record:(K18AudioEngine*_Nonnull)engine currentTime:(NSTimeInterval)curTime volume:(float)volume
{
    
}

- (void) onRecordStop:(NSURL *_Nullable)exportURL duration:(NSTimeInterval)duration
{
    K18RecordModel *m = [K18RecordModel new];
    self.recordModel = m;
    m.recordFileUrl = exportURL;
    m.recordDuration = self.curTime;
    
    K18SongModel *model = [K18SongModel new];
    model.song = [K18Song new];
    model.song.cySongName = self.songName;
    model.recordModel = self.recordModel;
    
    [K18SongService gotoPublishWithModel:model root:self];
    
}

#pragma mark - private

- (void) doPause {
    [self stopTimer];
    [[K18AudioEngine shared] pauseRecord];
    self.timeTipLabel.text = @"点击继续录制";
}

- (void) doResume {
    [self startTimer];
     [[K18AudioEngine shared] resumeRecord];
    self.timeTipLabel.text = @"录制中";
}

- (void) startTimer {
    if(!self.timer){
        __weak typeof(self)wself = self;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            wself.curTime++;
            [wself updateTimeLabel:wself.timeLabel curTime:wself.curTime];
        }];
    }
}

- (void) stopTimer {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}



@end
