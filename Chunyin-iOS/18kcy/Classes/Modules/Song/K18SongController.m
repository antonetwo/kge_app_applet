//
//  K18SongController.m
//  18kcy
//
//  Created by 唐 on 2019/6/10.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongController.h"
#import "JXPageListView.h"
#import "K18SongListView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Masonry/Masonry.h>
#import "K18Cell1.h"
#import "K18Cell2.h"
#import "K18SongListController.h"
#import "K18SongAuthorController.h"
#import "K18PublishController1.h"
#import "K18SongService.h"
#import "K18LiveService.h"
#import "TCLoginModel.h"
#import "K18SongRankController.h"
#import "K18QingChangController.h"
#import "K18AudioEngine.h"
#import "K18AudioUtil.h"

@interface K18SongController ()<JXPageListViewDelegate, UITableViewDataSource, UITableViewDelegate>
//@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXPageListView *pageListView;
@property (nonatomic, strong) NSMutableArray <K18SongListView *> *listViewArray;

@property(strong, nonatomic) NSArray<K18Category *> *categoryList;
@end

@implementation K18SongController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    if(K18UserModel.shared.currentUser){
        [self reqData];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:k18_LoginSuccessEvent object:nil];
    
    [self initUI_delay];
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted==NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUDHelper alert:@"开启直播和K歌需要访问麦克风" action:^{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:nil completionHandler:nil];
                }];
            });
        }
    }];
}
     
     
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loginSuccess:(NSNotification*)notify {
    [self reqData];
}

- (void) reqData {
    if(self.categoryList.count!=0){
        return;
    }
    __weak typeof(self)wself = self;
//    MBProgressHUD *hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_getSongTypes params:@{} succ:^(id  _Nonnull data) {
//        [[HUDHelper sharedInstance] stopLoading:hud];
        
        wself.categoryList = [NSArray yy_modelArrayWithClass:[K18Category class] json:data];
        
        [wself initUI_delay];
        
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
//        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
        [wself initUI_delay];
    }];
}

- (void)initUI {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemSearch) target:self action:@selector(searchAction)];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [btn setTitle:@"排行榜" forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(leftAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)initUI_delay
{
	
	//分类
	//热门歌手、节日、情感、主题、综艺、年代、流派、语种
	
	_titles = @[@"点唱榜", @"飙升榜", @"新歌榜"];
	
	_listViewArray = [NSMutableArray array];
	for (int i=0; i<3; i++) {
		K18SongListView *categoryListView = [[K18SongListView alloc] init];
		categoryListView.isNeedHeader = YES;
		categoryListView.rankType = i;
#if IsTestMode
		categoryListView.dataSource = [self initialDataWithIndex:i];//test
#endif
		categoryListView.rootVC = self;
		[_listViewArray addObject:categoryListView];
	}
	
	
	self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
	self.pageListView.listViewScrollStateSaveEnabled = YES;
	self.pageListView.pinCategoryViewVerticalOffset = 0;
	//Tips:pinCategoryViewHeight要赋值
	self.pageListView.pinCategoryViewHeight = 50;
	//Tips:操作pinCategoryView进行配置
	self.pageListView.pinCategoryView.titles = self.titles;
	
	//添加分割线，这个完全自己配置
	UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, self.view.bounds.size.width, 1)];
	lineView.backgroundColor = [UIColor lightGrayColor];
	[self.pageListView.pinCategoryView addSubview:lineView];
	
	//Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
	self.pageListView.mainTableView.dataSource = self;
	self.pageListView.mainTableView.delegate = self;
	self.pageListView.mainTableView.scrollsToTop = NO;
	[self.pageListView.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
	[self.view addSubview:self.pageListView];
	
	
	[self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.view.mas_topMargin);
		make.bottom.equalTo(self.view);
		make.left.equalTo(self.view);
		make.right.equalTo(self.view);
	}];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}


#pragma mark - JXPageViewDelegate
//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
	return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
	self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2 + 1;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 2) {
		//Tips:最后一个section（即listContainerCell所在的section）需要返回1
		return 1;
	}
	
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 2) {
		//Tips:最后一个section（即listContainerCell所在的section）返回listContainerCell的高度
		return [self.pageListView listContainerCellHeight];
	}else if (indexPath.section == 1) {
		return 135;
	}else{
		return 114;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 2) {
		//Tips:最后一个section（即listContainerCell所在的section）配置listContainerCell
		return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
	}
	__weak typeof(self)wself = self;
	if (indexPath.section==0) {
		
		K18Cell1 *cell = [[K18Cell1 alloc]init];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.clickMenu = ^(NSInteger index, NSString * _Nonnull title) {
			[wself clickMenuWithIndex:index];
		};
		return cell;
	}else {
		CGRect rect = self.view.bounds;
		rect.size.height = 135;
		K18Cell2 *cell = [[K18Cell2 alloc]initWithFrame:rect];
		cell.datas = self.categoryList;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.clickMenu = ^(NSInteger index, NSString * _Nonnull title) {
			[wself clickCagatoryWithIndex:index title:title];
		};
		return cell;
	}
	
	return nil;
}

#pragma mark - Actions

//分类菜单
- (void) clickCagatoryWithIndex:(NSInteger)index title:(NSString *)title {
	NSLog(@"%@",@(index));
	
	if (index >= self.categoryList.count) {
		NSLog(@"分类索引异常");
		return;
	}
	
	__weak typeof(self)wself = self;
	if (index==0) {//热门歌手
		
		K18Category *category = wself.categoryList[index];
		MBProgressHUD *hud = [[HUDHelper sharedInstance] loading];
		NSDictionary *params = @{@"cyZimu":category.cyZimu, @"pageSize":@(kPageSize*10), @"pageNumber":@1};
		[self sendWithPath:kAPI_getSongByType params:params succ:^(id  _Nonnull data) {
			[[HUDHelper sharedInstance] stopLoading:hud];
			NSString *title = data[@"cyTypeName"];
			NSArray *listData = data[@"singers"];
			NSMutableArray *dataSource = [NSArray yy_modelArrayWithClass:[K18Singer class] json:listData].mutableCopy;
			[K18SongService gotoSingerList:dataSource title:title root:wself];
		} failed:^(NSError * _Nonnull error) {
			[[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
		}];
		
	}else{
		
		K18Category *category = wself.categoryList[index];
		[K18SongService gotoSongListWithData:[NSMutableArray array] category:category root:wself];
//		MBProgressHUD *hud = [[HUDHelper sharedInstance] loading];
//		NSDictionary *params = @{@"cyZimu":category.cyZimu, @"pageSize":@(kPageSize), @"pageNumber":@1};
//		[self sendWithPath:kAPI_getSongByType params:params succ:^(id  _Nonnull data) {
//			[[HUDHelper sharedInstance] stopLoading:hud];
//			NSArray *listData = data[@"songs"];
//			NSMutableArray *dataSource = [NSArray yy_modelArrayWithClass:[K18Song class] json:listData].mutableCopy;
//			[K18SongService gotoSongListWithData:dataSource category:category root:wself];
//		} failed:^(NSError * _Nonnull error) {
//			[[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
//		}];
		
	}
}

- (void) clickMenuWithIndex:(NSInteger)index {
    if (index==2) {
        K18QingChangController *vc = [[K18QingChangController alloc]initWithNibName:@"K18QingChangController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"];
	switch (index) {
		case 0:
			
			break;
		case 1:
			
			break;
		case 2:
			
			break;
		case 3:
			
			break;

		default:
			break;
	}
	
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	//Tips:需要传入mainTableView的scrollViewDidScroll事件
	[self.pageListView mainTableViewDidScroll:scrollView];
}

- (void) searchAction {
    //TODO test
//    [K18AudioEngine setupAudioSession];
//    NSString *tmp = @"2019-08-25_16-14-30.wav";
//    NSString *url = [K18AudioUtil createFileDir:@"Records"];
//    url = [url stringByAppendingPathComponent:tmp];
//    [[K18AudioEngine shared] playOfflineRenderFromURL:[NSURL fileURLWithPath:url] succ:^(NSURL * _Nonnull exportURL) {
//        NSLog(@"exportURL=%@",exportURL);
//    } fail:^(NSError * _Nullable err) {
//
//    }];
//    return;
	[K18LiveService gotoSearchController:nil parent:self];
}

- (void) leftAction {
    K18SongRankController *vc = [[K18SongRankController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Datas

- (NSMutableArray*)initialDataWithIndex:(NSInteger)index {
	NSMutableArray *list = [NSMutableArray array];
	
	//TODO test data
	for (int i=0; i<10; i++) {
		K18Song *model = [K18Song testNew];
		[list addObject:model];
	}
	return list;
}

@end
