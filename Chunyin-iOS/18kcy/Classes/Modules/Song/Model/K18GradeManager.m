
//
//  K18GradeManager.m
//  18kcy
//
//  Created by beuady on 2019/7/7.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GradeManager.h"
#import "K18LrcModel.h"



@implementation K18GradeManager

- (void)start
{
    self.player = [GLMusicPlayer defaultPlayer];
    self.currentScore = 0;
}

- (NSUInteger)updateScoreWithVolume:(float)volume
                              model:(K18Accompany *)model
		   currentPlayTimeInSeconds:(NSTimeInterval)currentPlayTimeInSeconds
//                      musicLRCArray:(NSMutableArray *)musicLRCArray
                            krcInfo:(LyricsInfo *)krcInfo
                    currentLcrIndex:(NSInteger)currentLcrIndex
{
    NSUInteger score = self.currentScore;
    
    NSTimeInterval startTime = model.cyStartTime.doubleValue;
	NSTimeInterval curPlayTime = currentPlayTimeInSeconds;
    NSTimeInterval songDuration = model.cySongTime.doubleValue;
    songDuration = songDuration ? songDuration : 1;
    
    NSDictionary *linInfoTree = krcInfo.lineInfoTreeMap;
    
    LyricsLineInfo *curLrc = currentLcrIndex < linInfoTree.count ? linInfoTree[@(currentLcrIndex)] : nil;
    LyricsLineInfo *nextLrc;
    if (currentLcrIndex+1 < linInfoTree.count) {
        nextLrc = linInfoTree[@(currentLcrIndex+1)];
    }
//    NSLog(@"volume=%@",@(volume));
    
    curPlayTime = curPlayTime*1000;//统一毫秒
    if (volume > 0 &&
        curPlayTime >= startTime &&
        (curLrc!=nil && curLrc.lineLyric.length>0 &&
         curPlayTime >= curLrc.startTime) &&
        nextLrc!=nil) {
            
        CGFloat factor;
        CGFloat k= 0.2;
        if (nextLrc) {
            CGFloat rowTotalTime = nextLrc.startTime - curLrc.startTime;
            CGFloat pos = curPlayTime - curLrc.startTime;
            factor = pos / rowTotalTime;
        }else{
            factor = (curPlayTime - curLrc.startTime)/ songDuration;
        }
        if (volume<1) {
            volume += 2;
        }
        CGFloat cal = volume * (k + factor) ;//(140-volume) * (k + factor)
        score += cal;
        score = MIN(score, kMAXSCORE);
    }
    
    self.currentScore = score;
    return score;
}

@end
