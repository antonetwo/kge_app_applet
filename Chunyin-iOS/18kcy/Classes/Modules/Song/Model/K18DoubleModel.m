//
//  K18DoubleModel.m
//  18kcy
//
//  Created by 唐 on 2019/7/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18DoubleModel.h"

@implementation K18DoubleModel


- (instancetype)init
{
	self = [super init];
	if (self) {

        self.firstPlayer = [K18AudioEngine shared];
        self.secondPlayer = [K18AudioEngine new];

		
	}
	return self;
}



- (void) play
{
	[self.firstPlayer play];
    [self.secondPlayer play];
}

- (void) pause
{
	[self.firstPlayer pause];
	[self.secondPlayer pause];
}

- (void) stop
{
	[self.firstPlayer stop];
	[self.secondPlayer stop];
}

- (void) playFromModel:(K18Double *)model
{
	self.model = model;
	if(model.firstUrl){
        [self.firstPlayer playFromURL:model.firstUrl succ:nil fail:nil];
        self.firstPlayer.volume = model.firstVolume;
	}
	if (model.secondUrl) {
        [self.secondPlayer playFromURL:model.secondUrl succ:nil fail:nil];
        self.secondPlayer.volume = model.secondVolume;
	}
	
}

- (void)seekToPosition:(NSTimeInterval)position
{
    [self.firstPlayer seekToPosition:position];
    [self.secondPlayer seekToPosition:position];
}

- (void) replay {
	if(self.model.firstUrl){
		if (!self.firstPlayer.isPlaying) {
			[self.firstPlayer play];
		}
		
		[self.firstPlayer seekToPosition:0];
	}
	if (self.model.secondUrl) {
		if (!self.secondPlayer.isPlaying) {
			[self.secondPlayer play];
		}
		
		[self.secondPlayer seekToPosition:0];
		
	}
	
}

- (void) updateVolume {
    self.firstPlayer.volume = self.model.firstVolume;
    self.secondPlayer.volume = self.model.secondVolume;
}

- (void)setPitch:(NSInteger)pitch
{
	_pitch = pitch;
	self.firstPlayer.pitch = pitch;
	self.secondPlayer.pitch = pitch;
}

- (void)setUseReverb:(BOOL)useReverb
{
	_useReverb = useReverb;
	self.firstPlayer.useReverb = YES;
	self.secondPlayer.useReverb = YES;
}

- (void)setReverbType:(K18ReverbType *)reverbType
{
	_reverbType = reverbType;
	self.firstPlayer.reverbType = reverbType;
	self.secondPlayer.reverbType = reverbType;
}


@end

@implementation K18Double

@end
