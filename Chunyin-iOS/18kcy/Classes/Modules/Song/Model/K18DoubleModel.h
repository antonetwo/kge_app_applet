//
//  K18DoubleModel.h
//  18kcy
//
//  Created by 唐 on 2019/7/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLMusicPlayer.h"
#import "K18Common.h"
#import "K18AudioEngine.h"

NS_ASSUME_NONNULL_BEGIN

@class K18Double;

@interface K18DoubleModel : NSObject
@property(strong, nonatomic) K18AudioEngine *firstPlayer;
@property(strong, nonatomic) K18AudioEngine *secondPlayer;

@property(strong, nonatomic) K18Double *model;


- (void) play;
- (void) pause;
- (void) stop;
- (void) playFromModel:(K18Double *)model;
- (void) replay;
- (void)seekToPosition:(NSTimeInterval)position;
- (void) updateVolume;

@property(nonatomic) NSInteger pitch;//
@property(nonatomic) BOOL useReverb;//Default=NO
@property(nonatomic) K18ReverbType *reverbType;
@end



@interface K18Double : NSObject
@property(strong, nonatomic) NSURL *firstUrl;
@property(nonatomic) float firstVolume;

@property(strong, nonatomic) NSURL *secondUrl;
@property(nonatomic) float secondVolume;

@end

NS_ASSUME_NONNULL_END
