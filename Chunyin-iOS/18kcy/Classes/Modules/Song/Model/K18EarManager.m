//
//  K18EarManager.m
//  18kcy
//
//  Created by 唐 on 2019/7/25.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18EarManager.h"
#import <AVFoundation/AVFoundation.h>
#import "K18AudioEngine.h"
#import "K18Common.h"
#import "Appdelegate.h"

@implementation K18EarManager

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (instancetype)init
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

- (void) startListen
{
    if(!self.self.isListener){
        self.isListener = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeChange:) name:AVAudioSessionRouteChangeNotification object:nil];//耳机插拨
    }
}

- (void)stopListen
{
    self.isListener = NO;
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void) routeChange:(NSNotification *)notify {
    __weak typeof(self)wself = self;
	NSDictionary *interuptionDict = notify.userInfo;
	
	NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
	
	switch (routeChangeReason) {
			
		case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
		{
            NSLog(@"耳机插入");
			dispatch_async(dispatch_get_main_queue(), ^{
                
                [[K18AudioEngine shared] pauseRecord];
                [self.accompanyModel pause];
                
                if(self.av){
                    return;
                }
                UIAlertController *av = [UIAlertController alertControllerWithTitle:@"检测到耳机已插入，已开启耳返" message:nil preferredStyle:(UIAlertControllerStyleAlert)];
                self.av = av;
                UIAlertAction *a1 = [UIAlertAction actionWithTitle:@"重新录制" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"k18event_resong" object:nil];
                    wself.av = nil;
                }];
                
//                UIAlertAction *a2 = [UIAlertAction actionWithTitle:@"不开启" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                
//                }];
                
                [av addAction:a1];
//                [av addAction:a2];
                
                [[AppDelegate currentPresentestController] presentViewController:av animated:YES completion:nil];
                
			});
		}
			break;
			
		case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
		{
            NSLog(@"耳机拔出");
			dispatch_async(dispatch_get_main_queue(), ^{
				
                [[K18AudioEngine shared] pauseRecord];
                [self.accompanyModel pause];
                
                if(self.av){
                    return;
                }
                
                UIAlertController *av = [UIAlertController alertControllerWithTitle:@"检测到耳机已拔出，已关闭耳返" message:nil preferredStyle:(UIAlertControllerStyleAlert)];
                self.av = av;
                UIAlertAction *a1 = [UIAlertAction actionWithTitle:@"重新录制" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"k18event_resong" object:nil];
                    wself.av = nil;
                }];
                [av addAction:a1];
                
                [[AppDelegate currentPresentestController] presentViewController:av animated:YES completion:nil];
				
			});
		}
	}
}

@end
