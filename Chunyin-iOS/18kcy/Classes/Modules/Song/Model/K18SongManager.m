//
//  K18SongManager.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongManager.h"
#import <YYModel/YYModel.h>

#define kSongStore @"SongData.plist"
#define kRecordStore @"RecordData.plist"
#define kAcompanyStore @"AcompanyData.plist"


@implementation K18SongManager

+ (instancetype) shared
{
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
//        NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
//        NSString *songDataPath = [dir stringByAppendingPathComponent:kSongDataPath];
//        NSString *recordDataPath = [dir stringByAppendingPathComponent:kRecordDataPath];
//
//        if (![[NSFileManager defaultManager] fileExistsAtPath:songDataPath]){
//            [[NSFileManager defaultManager] createDirectoryAtPath:songDataPath withIntermediateDirectories:YES attributes:nil error:nil];
//            [[NSFileManager defaultManager] createDirectoryAtPath:recordDataPath withIntermediateDirectories:YES attributes:nil error:nil];
//
//        }
    }
    return self;
}

- (void)localSaveSong:(K18Song *)song
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *songJsonStr = [song yy_modelToJSONString];
    NSString *dataStore = [dir stringByAppendingPathComponent:kSongStore];
    NSMutableDictionary *dataDic = [[NSDictionary alloc] initWithContentsOfFile:dataStore].mutableCopy;
    if(!dataDic){
        dataDic = [NSMutableDictionary dictionary];
    }
    NSString *uid = song.id?song.id: @"-";
    dataDic[uid] = songJsonStr;
    [dataDic writeToFile:dataStore atomically:YES];
}

//- (void)localSaveAcompany:(K18Accompany *)accompany
//{
//
//}

- (void)localSaveRecord:(K18SongModel *)recordModel
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *recordJsonStr = [recordModel yy_modelToJSONString];
    NSString *dataStore = [dir stringByAppendingPathComponent:kRecordStore];
    NSMutableArray *dataList = [[NSArray alloc] initWithContentsOfFile:dataStore].mutableCopy;
    if(!dataList){
        dataList = [NSMutableArray array];
    }
    [dataList addObject:recordJsonStr];
    [dataList writeToFile:dataStore atomically:YES];
}

- (NSArray *)getLocalSongList
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *dataStore = [dir stringByAppendingPathComponent:kSongStore];
    NSDictionary *dataDic = [[NSDictionary alloc] initWithContentsOfFile:dataStore].mutableCopy;
    NSMutableArray *list = [NSMutableArray array];
    for (NSString *key in dataDic) {
        NSString *jsonStr = dataDic[key];
        K18Song *model = [K18Song yy_modelWithJSON:jsonStr];
        [list addObject:model];
    }
    return list;
}

- (NSArray *)getLocalRecordList
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *dataStore = [dir stringByAppendingPathComponent:kRecordStore];
    NSArray *datas = [[NSArray alloc] initWithContentsOfFile:dataStore].mutableCopy;
    NSMutableArray *list = [NSMutableArray array];
    for (NSString *jsonStr in datas) {
        K18SongModel *model = [K18SongModel yy_modelWithJSON:jsonStr];
        [list addObject:model];
    }
    return list;
}

- (void)clearSaveSongs
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *dataStore = [dir stringByAppendingPathComponent:kSongStore];
    [[NSFileManager defaultManager] removeItemAtPath:dataStore error:nil];
}

- (void)clearSaveRecords
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    NSString *dataStore = [dir stringByAppendingPathComponent:kRecordStore];
    [[NSFileManager defaultManager] removeItemAtPath:dataStore error:nil];
}

@end
