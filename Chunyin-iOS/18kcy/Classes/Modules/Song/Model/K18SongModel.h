//
//  K18SongModel.h
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18RecordModel.h"
#import "K18Song.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18SongModel : NSObject

@property(strong, nonatomic) K18Song *song;//歌曲
@property(strong, nonatomic) K18Accompany *accompanyModel;//伴奏
@property(strong, nonatomic) K18RecordModel *recordModel;//已唱内容
@property(strong, nonatomic) K18File *uploadedFile;

@property(strong, nonatomic) NSString *doneSongId;//演唱的音频id
@end

NS_ASSUME_NONNULL_END
