//
//  K18AccompanyModel.m
//  18kcy
//
//  Created by 唐 on 2019/7/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AccompanyModel.h"


@interface K18AccompanyModel()
@property(nonatomic,readwrite) BOOL isAccompany;
@end

@implementation K18AccompanyModel

- (void)dealloc
{
	[self.secondPlayer dispose];
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.isAccompany = YES;
	}
	return self;
}

- (void)switchPlayer:(BOOL)isAccompany
{
	self.isAccompany = isAccompany;
	if (self.isAccompany) {
		self.secondPlayer.volume = 0;
		self.firstPlayer.volume = self.model.firstVolume;
	}else{
		self.secondPlayer.volume = self.model.secondVolume;
		self.firstPlayer.volume = 0;
	}
}

- (void)playFromModel:(K18Double *)model
{
	[super playFromModel:model];
	[self switchPlayer:YES];
}

- (K18AudioEngine *)currentPlayer
{
	return self.isAccompany ? self.firstPlayer : self.secondPlayer;
}


@end
