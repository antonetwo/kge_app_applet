//
//  K18RecordModel.h
//  18kcy
//
//  Created by 唐 on 2019/6/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/**
 录制的歌曲的临时信息
 */
@interface K18RecordModel : NSObject
@property(strong, nonatomic) NSURL *songOriginFileUrl;//已经下载好的原唱地址
@property(strong, nonatomic) NSURL *songFileUrl;//已经下载好的伴奏地址
@property(strong, nonatomic) NSURL *songLRCFileUrl;//已经下载好的歌词地址
@property(nonatomic) NSUInteger songDuration;//歌曲时长

@property(nonatomic) CGFloat pitch;//升降调数值
@property(nonatomic) int reverbType;//混响类型
@property(nonatomic) CGFloat rate;//积分条比例
@property(nonatomic) NSUInteger score; // 积分数值
@property(nonatomic) NSTimeInterval recordDuration;//录制后的时长
@property(strong, nonatomic) NSURL *recordFileUrl;//录制的文件地址
@property(strong, nonatomic) NSURL *combineFileUrl;//合成的文件地址
@end

NS_ASSUME_NONNULL_END
