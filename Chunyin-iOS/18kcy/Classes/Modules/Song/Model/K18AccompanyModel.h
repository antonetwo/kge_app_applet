//
//  K18AccompanyModel.h
//  18kcy
//
//  Created by 唐 on 2019/7/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18DoubleModel.h"
NS_ASSUME_NONNULL_BEGIN

/**
 伴奏切换管理器
 */
@interface K18AccompanyModel : K18DoubleModel
@property(readonly, nonatomic) BOOL isAccompany;

/**

 @param isAccompany YES,伴奏， NO 原唱
 */
- (void) switchPlayer:(BOOL) isAccompany;


- (K18AudioEngine *) currentPlayer;

@end

NS_ASSUME_NONNULL_END
