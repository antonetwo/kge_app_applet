//
//  K18EarManager.h
//  18kcy
//
//  Created by 唐 on 2019/7/25.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18AccompanyModel.h"
#import "K18SongModel.h"
NS_ASSUME_NONNULL_BEGIN

/**
 耳返管理
 */
@interface K18EarManager : NSObject


@property(weak, nonatomic) K18AccompanyModel *accompanyModel;

@property(strong, nonatomic) K18SongModel *songModel;
@property(assign, nonatomic) BOOL isListener;
@property(strong, nonatomic) UIAlertController *av;

- (void) startListen;
- (void) stopListen;

@end

NS_ASSUME_NONNULL_END
