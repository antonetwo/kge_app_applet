//
//  K18SongManager.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18SongModel.h"
#import "K18Song.h"
NS_ASSUME_NONNULL_BEGIN


/**
 K歌录录制持久化管理
 */
@interface K18SongManager : NSObject

+ (instancetype) shared;

//已点伴奏相关
- (void) localSaveSong:(K18Song *)song;
//- (void) localSaveAcompany:(K18Accompany *)accompany;

//本地录制相关
- (void) localSaveRecord:(K18SongModel *)recordModel;

- (void) clearSaveSongs;
- (void) clearSaveRecords;

/**
 获取本地已点伴奏列表

 @return <#return value description#>
 */
- (NSArray *) getLocalSongList;


/**
 获取本地录制列表

 @return <#return value description#>
 */
- (NSArray *) getLocalRecordList;

@end

NS_ASSUME_NONNULL_END
