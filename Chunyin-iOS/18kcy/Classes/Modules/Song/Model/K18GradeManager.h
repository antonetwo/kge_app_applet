//
//  K18GradeManager.h
//  18kcy
//
//  Created by beuady on 2019/7/7.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18SongModel.h"
#import "GLMusicPlayer.h"
#import "LyricsInfo.h"
NS_ASSUME_NONNULL_BEGIN

#define kMAXSCORE 10000

/**
 评分管理
 */
@interface K18GradeManager : NSObject

@property(assign, nonatomic) NSUInteger currentScore;

@property(strong, nonatomic) K18SongModel *model;

@property(weak, nonatomic) GLMusicPlayer *player;


- (void) start;

- (NSUInteger)updateScoreWithVolume:(float)volume
							  model:(K18Accompany *)model
		   currentPlayTimeInSeconds:(NSTimeInterval)currentPlayTimeInSeconds
//                      musicLRCArray:(NSMutableArray *)musicLRCArray
                            krcInfo:(LyricsInfo *)krcInfo
                    currentLcrIndex:(NSInteger)currentLcrIndex;


@end

NS_ASSUME_NONNULL_END
