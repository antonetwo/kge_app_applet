//
//  K18SongAuthorController.m
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongAuthorController.h"
#import "K18AuthorCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "K18SongService.h"

@interface K18SongAuthorController ()<UICollectionViewDelegate, UICollectionViewDataSource>



@end

@implementation K18SongAuthorController

- (void)dealloc
{
	NSLog(@"%s dealloc",__FILE__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	CGFloat W = [UIScreen mainScreen].bounds.size.width;
	CGFloat horGap = 37;
	CGFloat col = 2;
	UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc]init];
	viewLayout.minimumInteritemSpacing = 0;
	viewLayout.minimumLineSpacing = 18;
	viewLayout.itemSize = CGSizeMake(W/col, 50);
	viewLayout.headerReferenceSize = CGSizeMake(W, 22);
	

	_collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:viewLayout];
	_collectionView.delegate = self;
	_collectionView.dataSource = self;
	_collectionView.backgroundColor = [UIColor whiteColor];
	[self.view addSubview:_collectionView];
	[_collectionView registerNib:[UINib nibWithNibName:@"K18AuthorCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
}

- (void)setDataSource:(NSMutableArray<K18Song *> *)dataSource
{
	_dataSource = dataSource;
	[self.collectionView reloadData];
}


#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.dataSource.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	K18AuthorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
	K18Singer *model = self.dataSource[indexPath.row];
	NSURL *url = [NSURL URLWithString:model.cyImgUrl ? model.cyImgUrl : @""];
	[cell.imgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:k18_DefaultSinger]];
	cell.label.text = model.cyName;
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	__weak typeof(self)wself = self;
	K18Singer *model = self.dataSource[indexPath.row];
	MBProgressHUD *hud = [[HUDHelper sharedInstance] loading];
	NSDictionary *params = @{@"singerId":model.id,@"pageSize":@(kPageSize),@"pageNumber":@1};
	[self sendWithPath:kAPI_getSongBySinger params:params succ:^(id  _Nonnull data) {
		[[HUDHelper sharedInstance] stopLoading:hud];
		NSString *title = data[@"singer"];
		NSArray *listData = data[@"song"];
		NSArray *datas = [NSArray yy_modelArrayWithClass:[K18Song class] json:listData];
		[K18SongService gotoSongListWithData:datas singer:model root:wself];
		
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
	}];
	
}


@end
