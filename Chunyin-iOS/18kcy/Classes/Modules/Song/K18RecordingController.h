//
//  K18RecordingController.h
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "K18Song.h"
NS_ASSUME_NONNULL_BEGIN



@interface K18RecordingController : K18BaseController
@property(strong, nonatomic) K18Song *songModel;


@end

NS_ASSUME_NONNULL_END
