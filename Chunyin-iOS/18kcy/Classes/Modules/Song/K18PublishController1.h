//
//  K18PublishController1.h
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "K18SongModel.h"
#import "K18RecordingController.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18PublishController1 : K18BaseController

@property(strong, nonatomic) K18SongModel *model;

//@property(weak, nonatomic) K18RecordingController *parentVC;

@end

NS_ASSUME_NONNULL_END
