//
//  K18SongRankController.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"

NS_ASSUME_NONNULL_BEGIN


/**
 K歌排行榜
 */
@interface K18SongRankController : K18BaseController

@end

NS_ASSUME_NONNULL_END
