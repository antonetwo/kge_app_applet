//
//  K18GoldRankCell.m
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GoldRankCell.h"
#import "K18Common.h"

@implementation K18GoldRankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.iconView.layer.cornerRadius = self.iconView.width/2;
    self.iconView.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight{
    return 50+10;
}

@end
