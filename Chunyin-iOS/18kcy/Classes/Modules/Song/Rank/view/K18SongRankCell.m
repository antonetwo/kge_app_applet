//
//  K18SongRankCell.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongRankCell.h"
#import "K18Common.h"

@implementation K18SongRankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.iconView.layer.cornerRadius = self.iconView.width/2;
    self.iconView.layer.masksToBounds = YES;

}

- (void)prepareForReuse
{
    [super prepareForReuse];
    id img = [[UIImage imageNamed:@"xianhua"] iTintColor:[UIColor whiteColor]];
    self.smallIcon.image = img;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

   
}

+ (CGFloat)cellHeight {
    return 266+10;
}

@end
