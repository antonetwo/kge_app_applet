//
//  K18GoldRankCell.h
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18GoldRankCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subTitleL;

+ (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
