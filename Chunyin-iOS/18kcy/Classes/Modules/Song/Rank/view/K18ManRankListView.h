//
//  K18ManRankListView.h
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JXPageListView.h"
#import "K18Common.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18ManRankListView : UIView< JXPageListViewListDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property(strong, nonatomic) NSMutableDictionary *dataSource;

@property(assign, nonatomic) int cyRankType;//0,人气榜 1,纯音榜，2金主榜
@property(assign, nonatomic) int cyWeekType;//0,本周 1上周 2总榜

@property(nonatomic,weak) UIViewController *rootVC;

- (void) setNeedRefresh;
- (void) requestData:(void(^)(void))callback;
@end

NS_ASSUME_NONNULL_END
