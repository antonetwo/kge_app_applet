//
//  K18ManRankListView.m
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ManRankListView.h"
#import "K18SongRankCell.h"
#import "K18Rank.h"
#import "K18SongRankCell.h"
#import "K18SongService.h"
#import "K18GoldRankCell.h"
#import "K18LiveService.h"

@interface K18ManRankListView()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property(assign, nonatomic) BOOL needRefresh;
@property(strong, nonatomic) UILabel *noTipLabel;
@end

@implementation K18ManRankListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = [NSMutableDictionary dictionary];

        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.tableFooterView = [UIView new];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.tableView registerNib:[UINib nibWithNibName:@"K18SongRankCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        [self.tableView registerNib:[UINib nibWithNibName:@"K18GoldRankCell" bundle:nil] forCellReuseIdentifier:@"gold_cell"];
        
        [self addSubview:self.tableView];
        
        _noTipLabel = [UILabel new];
        _noTipLabel.textColor = RGBOF(0x323232);
        [self addSubview:_noTipLabel];
        _noTipLabel.text = @"暂无内容";
        [_noTipLabel sizeToFit];
        _noTipLabel.hidden = YES;
        
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
        [self setNeedRefresh];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.noTipLabel.center = CGPointMake(self.width/2, self.height/2);
}

- (NSMutableArray *) dataWithInnerTypes {
    NSString *key = [NSString stringWithFormat:@"%d_%d",self.cyRankType,self.cyWeekType];
    NSMutableArray *list = self.dataSource[key];
    if (!list) {
        list = [NSMutableArray array];
        self.dataSource[key] = list;
    }
    return list;
}

- (void) requestData:(void(^)(void))callback {
    static int loading = 0;
    
    id hud = [[HUDHelper sharedInstance] loading];
    
    __weak typeof(self)wself = self;
    NSDictionary *params = @{@"cyRankType":@(self.cyRankType),
                             @"cyWeekType":@(self.cyWeekType)
                             };
    [self sendWithPath:kAPI_kGeRank params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:nil delay:0 completion:nil];
        loading = 0;
        NSMutableArray *originDatas = [wself dataWithInnerTypes];
        [originDatas removeAllObjects];
        NSArray *list;
        if(wself.cyRankType==2){
            list = [NSArray yy_modelArrayWithClass:[K18GoldRank class] json:data];
        }else{
            list = [NSArray yy_modelArrayWithClass:[K18Rank class] json:data];
        }
        [originDatas addObjectsFromArray:list];
        wself.needRefresh = NO;
        [wself.tableView reloadData];
        if (list.count==0) {
            wself.noTipLabel.hidden= NO;
        }else{
            wself.noTipLabel.hidden= YES;
        }
        if(callback) callback();
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud];
        loading = 0;
        wself.needRefresh = NO;
        if(callback) callback();
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self dataWithInnerTypes].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    @property (weak, nonatomic) IBOutlet UIImageView *iconView;
//    @property (weak, nonatomic) IBOutlet UILabel *nameL;
//    @property (weak, nonatomic) IBOutlet UILabel *songNameL;
//    @property (weak, nonatomic) IBOutlet UILabel *listemNum;
//    @property (weak, nonatomic) IBOutlet UIView *layerView;
//    @property (weak, nonatomic) IBOutlet UIImageView *bgView;
//    @property (weak, nonatomic) IBOutlet UIImageView *smallIcon;
    if(self.cyRankType==0 || self.cyRankType == 1){
        K18SongRankCell *cell = (K18SongRankCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];

        K18Rank *m = [self dataWithInnerTypes][indexPath.row];
        NSString *url = notNil(m.cyImg);
        [cell.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
        cell.nameL.text = m.cyUserName;
        cell.songNameL.text = [NSString stringWithFormat:@"NO%@ %@",m.rankNum, m.cySongName];
        [cell.bgView sd_setImageWithURL:[NSURL URLWithString:m.userPhotoImg] placeholderImage:[UIImage imageNamed:k18_DefaultBG]];
        
        if(self.cyRankType==0){
            cell.listemNum.text = m.cyFlowerNum;//鲜花数
        }else{
            cell.listemNum.text = m.cyScore;//积分？
            cell.smallIcon.image = [UIImage imageNamed:@"pingfen"];
        }
        return cell;
    }else{
        K18GoldRankCell *cell = (K18GoldRankCell*)[tableView dequeueReusableCellWithIdentifier:@"gold_cell"];
        K18GoldRank *m = [self dataWithInnerTypes][indexPath.row];
        NSString *url = notNil(m.cyImg);
        [cell.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
        cell.titleL.text = m.cyUserName;
        cell.subTitleL.text = [NSString stringWithFormat:@"贡献:%@",m.cyTotalFlowerNum];
        
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.cyRankType==0 || self.cyRankType==1) {
        return [K18SongRankCell cellHeight];
    }else{
        return [K18GoldRankCell cellHeight];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.cyRankType==2) {
        K18GoldRank *model = [self dataWithInnerTypes][indexPath.row];
        [K18LiveService gotoPersonDetail:model.cySendUserId parent:self.rootVC];
        
    }else{
        K18Rank *model = [self dataWithInnerTypes][indexPath.row];
        [K18SongService gotoSongDetailWithId:model.id root:self.rootVC];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback?:self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)listViewLoadDataIfNeeded {
    if (!self.needRefresh) {
        return;
    }
   
    [self requestData:^{
        
    }];
    
}

- (void)setNeedRefresh
{
    self.needRefresh = YES;
}

- (void)listViewRefreshData:(void (^)(void))callback
{
    [self requestData:callback];
}

@end
