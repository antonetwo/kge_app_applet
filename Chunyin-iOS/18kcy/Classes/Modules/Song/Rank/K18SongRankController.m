//
//  K18SongRankController.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongRankController.h"
#import "JXPageListView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Masonry/Masonry.h>
#import "K18ManRankListView.h"
#import <YBPopupMenu/YBPopupMenu.h>


@interface K18SongRankController ()<JXPageListViewDelegate, UITableViewDataSource, UITableViewDelegate,
YBPopupMenuDelegate>
//@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXPageListView *pageListView;
@property (nonatomic, strong) NSMutableArray *listViewArray;

@property(strong, nonatomic) UIButton *centerButton;

@property(assign, nonatomic) int cyRankType;//0,人气榜 1,纯音榜，2金主榜
@property(assign, nonatomic) int cyWeekType;//0,本周 1上周 2总榜

@end

@implementation K18SongRankController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"K歌排行榜";
    
    
    [self initUI_delay];
}

- (NSArray *) weekTitles {
    return @[@"本周",@"上周", @"总榜"];
}

- (void)initUI_delay
{
    
    //分类
    
    _titles = @[@"人气榜", @"纯音榜", @"金主榜"];
    
    _listViewArray = [NSMutableArray array];
    for (int i=0; i<3; i++) {
        K18ManRankListView *categoryListView = [[K18ManRankListView alloc] init];
        categoryListView.cyRankType = i;
        categoryListView.rootVC = self;
        [_listViewArray addObject:categoryListView];
    }
    
    
    self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
    self.pageListView.listViewScrollStateSaveEnabled = YES;
    self.pageListView.pinCategoryViewVerticalOffset = 0;
    //Tips:pinCategoryViewHeight要赋值
    self.pageListView.pinCategoryViewHeight = 50;
    //Tips:操作pinCategoryView进行配置
    self.pageListView.pinCategoryView.titles = self.titles;
    self.pageListView.centerContainer.frame = CGRectMake(0, 0, self.view.width, 50);
    
    UIButton *centerButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
    [self.pageListView.centerContainer addSubview:centerButton];
    centerButton.backgroundColor = RGBOF(0xf3f3f3);
    [centerButton setTitle:self.weekTitles[0] forState:(UIControlStateNormal)];
    [centerButton setImage:[UIImage imageNamed:@"icon_triangle"] forState:(UIControlStateNormal)];
    [centerButton setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
    [centerButton addTarget:self action:@selector(selectWeekTypeAction) forControlEvents:(UIControlEventTouchUpInside)];
    centerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    centerButton.layer.borderWidth = 0.5;
    self.centerButton = centerButton;
    
    //添加分割线，这个完全自己配置
//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - .5, self.view.bounds.size.width, .5)];
//    lineView.backgroundColor = [UIColor lightGrayColor];
//    [self.pageListView.pinCategoryView addSubview:lineView];
    
    //Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
    self.pageListView.mainTableView.dataSource = self;
    self.pageListView.mainTableView.delegate = self;
    self.pageListView.mainTableView.scrollsToTop = NO;
    [self.pageListView.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.pageListView];
    
    
    [self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
}

#pragma mark - JXPageViewDelegate
//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
    return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Tips:需要传入mainTableView的scrollViewDidScroll事件
    [self.pageListView mainTableViewDidScroll:scrollView];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.pageListView listContainerCellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
}

#pragma mark - Actions

- (void) selectWeekTypeAction {
    CGPoint p = self.centerButton.center;
    CGPoint newP = [self.centerButton convertPoint:p toView:self.view];
    newP.y += self.centerButton.height/2;
    [YBPopupMenu showAtPoint:newP titles:self.weekTitles icons:nil menuWidth:80 otherSettings:^(YBPopupMenu *popupMenu) {
        
        popupMenu.isShowShadow = NO;
        popupMenu.delegate = self;
        popupMenu.offset = 0;
        popupMenu.type = YBPopupMenuTypeDefault;
        popupMenu.rectCorner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
        
    }];
}

#pragma mark - YBPopupMenuDelegate

- (void)ybPopupMenu:(YBPopupMenu *)ybPopupMenu didSelectedAtIndex:(NSInteger)index
{
    NSInteger pageTypeIndex = self.pageListView.pinCategoryView.selectedIndex;
    self.cyWeekType = (int)index;
    [self.centerButton setTitle:self.weekTitles[index] forState:(UIControlStateNormal)];
    for (K18ManRankListView *view in self.listViewArray) {
        view.cyWeekType = (int)index;
        [view setNeedRefresh];
    }

    if (pageTypeIndex < self.listViewArray.count) {
        K18ManRankListView *view = self.listViewArray[pageTypeIndex];
        [view requestData:nil];
    }

    
    
}

@end
