//
//  K18SongDetailController.m
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongDetailController.h"
#import "K18DetailView.h"
#import "K18CommentCell.h"
#import "K18Song.h"
#import <MJRefresh/MJRefresh.h>
#import "CCPagedScrollView.h"
#import "GLMusicPlayer.h"
#import "K18SongService.h"
#import "K18BottomBar.h"
#import "UMService.h"
#import "UUKeyboardInputView.h"
#import "K18QingChangController.h"
#import "K18GiftView.h"

@interface K18SongDetailController ()<UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, K18BottomBarDelegate,K18GiftViewDelegate>
@property(strong, nonatomic) UITableView *tableView;
@property(strong, nonatomic) K18DetailView *detailView;

@property(strong, nonatomic) UITabBar *tabBar;
@property(strong, nonatomic) K18BottomBar *tabView;
@property(strong, nonatomic) UIView *tabViewContrainer;

@property(strong, nonatomic) NSMutableArray<K18SongComment*> *commentModels;
@property(strong, nonatomic) K18SongDetail *detailModel;
@property(assign, nonatomic) BOOL isSendFlower;
@property(strong, nonatomic) UIView *emptyView;
@property(strong, nonatomic) K18GiftView *giftView;
@end

@implementation K18SongDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.forbidGuestureBack = YES;
	
	__weak typeof(self)wself = self;
	self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//		MJRefreshNormalHeader
		[wself requestData];
		[wself requestComments];
	}];
	
	[self reqListenPlus];
}

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!self.detailModel) {
        [self requestData];
		[self requestComments];
    }
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self.detailView stopPlayAudio];
    self.navigationController.navigationBarHidden = self.lastNaviHidden;
}

#pragma mark - Init Data

- (void)initUI
{
	
	self.tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:(UITableViewStylePlain)];
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
	self.tableView.allowsSelection = NO;
	[self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"K18CommentCell" bundle:nil] forCellReuseIdentifier:@"cell"];
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"advert"];
	
	self.detailView = [[NSBundle mainBundle] loadNibNamed:@"K18DetailView" owner:self options:nil].firstObject;
//	self.tableView.tableHeaderView = self.detailView;

	
	[self initTabBar];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
	
	self.emptyView = [self makePlaceHolderView];
	[self.tableView addSubview:self.emptyView];
	self.emptyView.hidden = YES;
}

- (void) initTabBar {
	
	self.tabBar = [[UITabBar alloc]initWithParent:self.view];
	self.tabBar.frame = self.tabBarController.tabBar.frame;
	[self.view addSubview:self.tabBar];

//	CGRect frame = self.tabBar.frame;
	CGFloat tabBarHeight = self.tabBar.height;
	
	self.tabView = [[K18BottomBar alloc]initWithFrame:self.tabBar.bounds];
	self.tabView.delegate = self;
	[self.tabBar addSubview:self.tabView];

	
	UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, tabBarHeight)];
	self.tableView.tableFooterView = v;
}

- (void) updateUI {
    self.title = self.detailModel.cySongName;
    self.detailView.model = self.detailModel;
	
	[self.detailView startPlayAudio];
	
	[self.tabView buttonForIndex:3].selected = self.detailModel.collectFlag.boolValue;
    
    if ([self.detailModel.cyUserId isEqualToString:k18_loginUserId]){
        //rightAction
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"gengduo"] style:(UIBarButtonItemStyleDone) target:self action:@selector(rightAction)];
    }
}

#pragma mark - Request Data

- (void) requestData {
    if (self.songLiveId.length==0) {
		[self.tableView.mj_header endRefreshing];
        return;
    }
    NSString *userId = k18_loginUserId;
    NSDictionary *params = @{
                             @"songLiveId":self.songLiveId,
                             @"userId":notNil(userId),
                             };

    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_getSongLive params:params succ:^(id  _Nonnull data) {

        wself.detailModel = [K18SongDetail yy_modelWithJSON:data];
        [wself updateUI];
		[wself.tableView.mj_header endRefreshing];
    } failed:^(NSError * _Nonnull error) {
		[wself.tableView.mj_header endRefreshing];
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
}

- (void) requestComments {
	if (self.songLiveId.length==0) {
		return;
	}
	
	NSDictionary *params = @{
							 @"songLiveId":self.songLiveId,
							 };
	
	__weak typeof(self)wself = self;
	[self sendWithPath:kAPI_getSongLiveComments params:params succ:^(id  _Nonnull data) {
		NSArray *list = data[@"songLiveComments"];
		wself.commentModels = [NSArray yy_modelArrayWithClass:[K18SongComment class] json:list].mutableCopy;
		[wself.tableView reloadData];
		
		if (!wself.commentModels.count) {
			wself.emptyView.hidden = NO;
		}else{
			wself.emptyView.hidden = YES;
		}
		
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
	}];
	
}

- (void) reqListenPlus {
	//告诉后台收听了一次
	[self sendWithPath:kAPI_doListenSongLive params:@{@"songLiveId":notNil(self.songLiveId),@"cyUserId":notNil(k18_loginUserId)} succ:^(id  _Nonnull data) {
		
	} failed:^(NSError * _Nonnull error) {
		
	}];
}

#pragma mark - Actions

- (void) rightAction {
    
    UIAlertController *av = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:(UIAlertControllerStyleActionSheet)];
    //gengduo
    UIAlertAction *a1 = [UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
        
        [self doDeleteMySong];
        
    }];
    UIAlertAction *a2 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    
    [av addAction:a1];
    [av addAction:a2];
    
    [self presentViewController:av animated:YES completion:nil];
    
}

- (void) doDeleteMySong {
    id hud = [[HUDHelper sharedInstance] loading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doDeleteSongLive params:@{@"songLiveId":notNil(self.songLiveId)} succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud];
        [[HUDHelper sharedInstance] tipMessage:data];
        [wself.navigationController popViewControllerAnimated:YES];
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section==0) {
		return 1;
	}
	return self.commentModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section==0) {
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"advert"];
		[cell addSubview:self.detailView];
		[self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
			make.left.equalTo(cell);
			make.right.equalTo(cell);
			make.bottom.equalTo(cell);
			make.top.equalTo(cell);
		}];
		
		self.emptyView.y = CGRectGetMaxY(cell.frame)+50;
		
		return cell;
	}
    
	K18CommentCell *cell = (K18CommentCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
	cell.model = self.commentModels[indexPath.row];
	return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 40)];
    v.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 70, 40)];
    label.text = @"评论";
    label.font = [UIFont systemFontOfSize:18];
    label.textColor = RGBOF(0x323232);
    [v addSubview:label];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 39, self.view.width, 1)];
    line.backgroundColor = RGBOF(0xDDDDDD);
    [v addSubview:line];
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (section==0) {
		return 0;
	}
    return 40;
}

-  (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section==0) {
//        return self.detailView.height;
//    }
    return UITableViewAutomaticDimension;
}

- (UIView *)makePlaceHolderView
{
	UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, MainScreenWidth(), 30)];
	label.text = @"暂无评论";
	label.textColor = RGBOF(0xefefef);
	label.textAlignment = NSTextAlignmentCenter;
	return label;
}

#pragma mark - K18BottomBarDelegate

- (void)tabBar:(K18BottomBar *)tabBar didSelectButton:(UIButton *)button
{
    switch (button.tag) {
        case 0://送花
            [self sendFlower];
            break;
        case 1://分享
            [self sendShare];
            break;
        case 2://评论
            [self editComment];
            break;
        case 3://取消/收藏
            [self sendCollect];
            break;
        case 4://K歌
            [self gotoKSong];
            break;
        default:
            break;
    }

}

- (void) sendFlower {
//    if (self.isSendFlower) {
//
//        return;
//    }
//    wself.isSendFlower = YES;
//    __weak typeof(self)wself = self;
//    NSString *myUserId = k18_loginUserId;
//    NSDictionary *params = @{@"songLiveId":notNil(self.songLiveId),
//                             @"cyFlowerNum":@1,
//                             @"cySendUserId":notNil(myUserId),
//                             @"cyIncomeUserId":self.detailModel.cyUserId,
//                             };
//    id hud = [[HUDHelper sharedInstance] loading];
//    [self sendWithPath:kAPI_sendFlowers params:params succ:^(id  _Nonnull data) {
//        [[HUDHelper sharedInstance] stopLoading:hud message:data];
//
//        wself.detailModel.cyFlowerNum = @(wself.detailModel.cyFlowerNum.integerValue + 1);
//        wself.detailView.model = wself.detailModel;
//        [wself.tabView buttonForIndex:0].selected = YES;
//    } failed:^(NSError * _Nonnull error) {
//        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
//    }];
    
    if(!self.giftView){
        self.giftView = [[K18GiftView alloc]initWithTargetId:self.detailModel.cyUserId targetName:self.detailModel.userName songId:self.songLiveId];
        self.giftView.giftDelegate = self;
        [self.navigationController.view addSubview:self.giftView];
    }
    [self.giftView show];
}

- (void) sendShare {
//	- (void)shareToPlatform:(UMSocialPlatformType)platformType
//messageObject:(UMSocialMessageObject *)messageObject
//currentViewController:(id)currentViewController
//completion:(UMSocialRequestCompletionHandler)completion;
    NSString *key = self.detailModel.userPhotos.firstObject;
    [[SDWebImageManager sharedManager].imageCache queryImageForKey:key options:(SDWebImageQueryMemoryData) context:nil completion:^(UIImage * _Nullable image, NSData * _Nullable data, SDImageCacheType cacheType) {
        
        NSString *title = [NSString stringWithFormat:@"%@-%@",self.detailModel.cySongName, self.detailModel.userName];
        [UMService shareURL:self.detailModel.cyShareUrl title:title image:image comletion:^(NSError * _Nonnull error) {
            
        }];
    }];
}

- (void) editComment {
//    [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"];
    [UUKeyboardInputView showBlock:^(NSString * _Nullable contentStr) {
        
        [self sendComment:contentStr];
        
    }];
}

- (void) sendComment:(NSString *)content {
    NSDictionary *params = @{@"songLiveId":notNil(self.songLiveId),
                             @"cyCommentUserId":notNil(self.detailModel.cyUserId),
                             @"cyContent":notNil(content),
                             };
    id hud = [[HUDHelper sharedInstance] loading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doSongLiveComment params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:@"评论成功" delay:1 completion:nil];

        [wself requestComments];
        
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription delay:1 completion:nil];
    }];
    
}

- (void) sendCollect {
    BOOL expireFlag = !self.detailModel.collectFlag.boolValue;
    __weak typeof(self)wself = self;
    NSString *myUserId = k18_loginUserId;
    NSDictionary *params = @{@"songLiveId":notNil(self.songLiveId),
                             @"cyUserId":notNil(myUserId),
                             @"cyFlag":@(expireFlag),
                             };
	id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_doCollectSong params:params succ:^(id  _Nonnull data) {
		[[HUDHelper sharedInstance] stopLoading:hud message:data delay:1 completion:nil];
        
        wself.detailModel.collectFlag = @((int)expireFlag);
        //TODO update ui
		if(expireFlag){
        	[wself.tabView buttonForIndex:3].selected = YES;
		}else{
			[wself.tabView buttonForIndex:3].selected = NO;
		}
		
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
    }];
}

- (void) gotoKSong {
    //    @property(strong, nonatomic) NSString *cyImg;//封面
    //    @property(strong, nonatomic) NSNumber *cySongNum;//演唱次数
    //    [self.navigationController popViewControllerAnimated:NO];
    
    if (self.detailModel.cySongId.integerValue==0) {//清唱
        K18QingChangController *vc = [[K18QingChangController alloc]initWithNibName:@"K18QingChangController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];

    }else{
        K18SongDetail *dm = self.detailModel;
        K18Song *song = [K18Song new];
        song.id = dm.cySongId;
        song.cySongName = dm.cySongName;
        [K18SongService gotoRecordingWithModel:song root:self];
    }
}

#pragma mark - K18GiftViewDelegate

- (void)onSendFlowerSuccess:(int)sendCount
{
    self.detailModel.cyFlowerNum = @(self.detailModel.cyFlowerNum.integerValue + sendCount);
    self.detailView.model = self.detailModel;
    [self.tabView buttonForIndex:0].selected = YES;
    self.detailView.model = self.detailModel;
}

@end
