//
//  K18RecordingController.m
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecordingController.h"
#import "K18IntonationView.h"
#import "K18ScoreView.h"
#import "K18RecordStatus.h"
#import "K18LRCView.h"
#import "IconButton.h"
#import "K18DebugBar.h"
#import "K18SongService.h"
#import "K18LoadingView.h"
#import "K18FileService.h"
#import "K18AudioService.h"
#import "K18RecordModel.h"
#import "K18SongModel.h"
#import <math.h>
#import "K18LrcView.h"
#import "GLMusicPlayer.h"
#import "K18LrcModel.h"
#import "K18AccompanyModel.h"
#import "K18GradeManager.h"
#import "K18KRCReader.h"
#import "K18AudioEngine.h"
#import "K18EarManager.h"
#import "K18SongManager.h"

@interface K18RecordingController ()<GLMusicPlayerDelegate, K18AudioEngineDelegate>
@property(nonatomic,strong) K18ScoreView *scoreView;
@property(nonatomic,strong) K18IntonationView *intonationView;
@property(strong, nonatomic) K18RecordStatus *recordStatusView;
@property(strong, nonatomic) K18LrcView *lrcView;

@property(strong, nonatomic) IconButton *resetBtn;
@property(strong, nonatomic) IconButton *debugBtn;
@property(strong, nonatomic) IconButton *completeBtn;

@property(strong, nonatomic) K18DebugBar *debugBar;
@property(strong, nonatomic) UIViewController *debugBarController;
@property(strong, nonatomic) K18LoadingView *loadingView;

//Datas

@property(strong, nonatomic) K18Accompany *accompany;
@property(strong, nonatomic) K18RecordModel *recordModel;
@property(strong, nonatomic) NSURL *currentMusicUrl;//原唱或者伴奏
@property(strong, nonatomic) K18AccompanyModel *accompanyModel;//双轨道播放器
@property(strong, nonatomic) K18GradeManager *gradeManager;//评分管理器
@property(strong, nonatomic) K18EarManager *earManager;//耳返管理器

@end

@implementation K18RecordingController

#pragma mark - LifeCycle

- (void)dealloc
{
    [self.lrcView dispose];
	[[K18AudioEngine shared] dispose];
	self.accompanyModel = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self name:K18Event_RESONG object:nil];
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.recordModel = [K18RecordModel new];
		self.accompanyModel = [K18AccompanyModel new];
        self.gradeManager = [K18GradeManager new];
		self.earManager = [K18EarManager new];
		self.earManager.accompanyModel = self.accompanyModel;
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startToRecordSong) name:K18Event_RESONG object:nil];
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.leftBarButtonItem = self.leftBarBackItem;
    
    [self initLoadingView];
#if IsTestMode
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		self.navigationController.navigationBar.hidden = NO;
		[self.loadingView removeFromSuperview];
		[self initUI_delay];
		[self initDataUI];
	});
#else
	[self reqestAccompany];
#endif
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self addEvents];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self removeEvents];
	self.accompanyModel.model = nil;
    [[K18AudioEngine shared] stopRecord:nil];
}

#pragma mark - Service Flow

- (void) initLoadingView {
    self.navigationController.navigationBar.hidden = YES;
    _loadingView = [K18LoadingView new];
    [self.view addSubview:_loadingView];
    [_loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
	self.loadingView.hidden = YES;
	

}

- (void) reqestAccompany {
	__weak typeof(self)wself = self;
	[self sendWithPath:kAPI_getBanzou params:@{@"songId":self.songModel.id} succ:^(id  _Nonnull data) {
		wself.accompany = [K18Accompany yy_modelWithJSON:data];
        wself.songModel.accompany = wself.accompany;
    
        [[K18SongManager shared] localSaveSong:wself.songModel];
        
		[wself preloadAccompany];
		
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription delay:2 completion:^{
			[wself.navigationController popViewControllerAnimated:YES];
		}];
	}];
}


/**
 下载伴奏
 */
- (void) preloadAccompany {
	NSString *accompanyUrl = self.accompany.cySongUrl;
	NSString *localDownloadedUrl = [K18FileService accompanyFilePath:accompanyUrl];
	if ([K18FileService isExistFile:localDownloadedUrl]) {
//		self.navigationController.navigationBar.hidden = NO;
//		[self.loadingView removeFromSuperview];
		NSLog(@"本地已经存在伴奏文件\n%@",localDownloadedUrl);
		self.recordModel.songFileUrl = [NSURL fileURLWithPath:localDownloadedUrl];
		
		[self preloadAccompanyOrigin];
	}else{
		self.loadingView.hidden = NO;
		__weak typeof(self)wself = self;
		[K18FileService downloadWithUrl:accompanyUrl destinationPath:[K18FileService accompanyPath] progress:^(NSProgress * _Nonnull downloadProgress) {
			
			dispatch_async(dispatch_get_main_queue(), ^{
				wself.loadingView.progress = downloadProgress.fractionCompleted * 0.5;
			});
			
		} completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nonnull filePath, NSError * _Nonnull error) {
			if (error) {
				[wself dealDownloadError];
				return;
			}
            wself.recordModel.songFileUrl = filePath;
			[wself preloadAccompanyOrigin];
		}];
	}
}


/**
 下载原唱
 */
- (void) preloadAccompanyOrigin {
	
	NSString *songOriginUrl = self.accompany.cyOriginalSongUrl;
	__weak typeof(self)wself = self;
	NSString *localDownloadedUrl = [K18FileService accompanyFilePath:songOriginUrl];
	if (self.accompany.cyOriginalSongUrl.length==0){//no exist
        self.navigationController.navigationBar.hidden = NO;
		[self.loadingView removeFromSuperview];
		[self initUI_delay];
		[self initDataUI];
	}else if([K18FileService isExistFile:localDownloadedUrl]) {
		self.navigationController.navigationBar.hidden = NO;
		[self.loadingView removeFromSuperview];
		NSLog(@"本地已经存在原唱文件\n%@",localDownloadedUrl);
		self.recordModel.songOriginFileUrl = [NSURL fileURLWithPath:localDownloadedUrl];
		[self initUI_delay];
		[self initDataUI];
	}else{
		[K18FileService downloadWithUrl:songOriginUrl destinationPath:[K18FileService accompanyPath] progress:^(NSProgress * _Nonnull downloadProgress) {
			
			dispatch_async(dispatch_get_main_queue(), ^{
				wself.loadingView.progress = 0.5 + downloadProgress.fractionCompleted * 0.5;
			});
			
		} completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nonnull filePath, NSError * _Nonnull error) {
			if (error) {
				[wself dealDownloadError];
				return;
			}
			
			wself.navigationController.navigationBar.hidden = NO;
			[wself.loadingView removeFromSuperview];
			
			wself.recordModel.songOriginFileUrl = filePath;
			[wself initUI_delay];
			[wself initDataUI];
			
		}];
	}
	
}

- (void) dealDownloadError {
	//TODO
}

- (void)initUI_delay
{
    
    _scoreView = [[K18ScoreView alloc] initWithFrame:CGRectMake(0, 10, MainScreenWidth()-28*2, 50)];
    [self.view addSubview:_scoreView];
    
    _intonationView = [[K18IntonationView alloc] init];
    [self.view addSubview:_intonationView];
    
    _recordStatusView = [[NSBundle mainBundle] loadNibNamed:@"K18RecordStatus" owner:self options:nil].lastObject;
    [self.view addSubview:_recordStatusView];
    
    _lrcView = [[K18LrcView alloc]init];
    [self.view addSubview:_lrcView];
    
    _resetBtn = [[IconButton alloc]init];
    [_resetBtn setTitle:@"重唱" forState:(UIControlStateNormal)];
    [_resetBtn setImage:[UIImage imageNamed:@"chongchang"] forState:(UIControlStateNormal)];
    [_resetBtn setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateNormal)];
    [self.view addSubview:_resetBtn];
    [self setButtonStyle:_resetBtn];
    
    _debugBtn = [[IconButton alloc]init];
    [_debugBtn setTitle:@"升降调" forState:(UIControlStateNormal)];
    [_debugBtn setImage:[[UIImage imageNamed:@"tiaoshi"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
    [_debugBtn setImage:[[UIImage imageNamed:@"tiaoshi -xuanze"] iTintColor:k18_TopicC] forState:(UIControlStateSelected)];
    _debugBtn.selected = NO;
    [_debugBtn setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateNormal)];
    [_debugBtn setTitleColor:k18_TopicC forState:(UIControlStateSelected)];
    [self.view addSubview:_debugBtn];
    [self setButtonStyle:_debugBtn];
	
    _completeBtn = [[IconButton alloc]init];
    [_completeBtn setTitle:@"完成" forState:(UIControlStateNormal)];
    [_completeBtn setImage:[UIImage imageNamed:@"wancheng"] forState:(UIControlStateNormal)];
    [_completeBtn setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateNormal)];
    [self.view addSubview:_completeBtn];
    [self setButtonStyle:_completeBtn];

    [_resetBtn addTarget:self action:@selector(resetAction) forControlEvents:(UIControlEventTouchUpInside)];
	[_debugBtn addTarget:self action:@selector(debugAction) forControlEvents:(UIControlEventTouchUpInside)];
    [_completeBtn addTarget:self action:@selector(completionA) forControlEvents:(UIControlEventTouchUpInside)];
	
	__weak typeof(self)wself = self;
    _debugBar = [[NSBundle mainBundle] loadNibNamed:@"K18DebugBar" owner:self options:nil].lastObject;
	_debugBar.hidden = YES;
	[self.view addSubview:_debugBar];
	_debugBar.valueChanged = ^(NSInteger num) {
		[wself pitchValueChange:num];
	};
	
	_lrcView.clickLrcView = ^{
		[wself clickLrcView];
	};


    [_scoreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(28);
        make.right.equalTo(self.view).offset(-28);
        make.top.equalTo(self.view.mas_topMargin).offset(10);
        make.height.mas_equalTo(50);
    }];
    
    [_intonationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreView.mas_bottom).offset(13);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.mas_equalTo(76);
    }];
    
    [_recordStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.intonationView.mas_bottom);
        make.height.mas_equalTo(55);
    }];
    
    [_lrcView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.recordStatusView.mas_bottom).offset(10);
        make.bottom.equalTo(self.debugBtn.mas_top).offset(-20);
    }];
    
    [_debugBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 60));
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.equalTo(self.view.mas_bottomMargin).offset(-15);
    }];
    
    [_resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 60));
        make.centerX.equalTo(self.view.mas_centerX).offset(-104);
        make.centerY.equalTo(self.debugBtn.mas_centerY);
    }];
    
    [_completeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 60));
        make.centerX.equalTo(self.view.mas_centerX).offset(104);
        make.centerY.equalTo(self.debugBtn.mas_centerY);
    }];
    
    [_debugBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.debugBtn.mas_top).offset(-2);
        make.size.mas_equalTo(CGSizeMake(35, 108));
        make.centerX.equalTo(self.debugBtn);
    }];
	
	[self.recordStatusView.toggleBtn addTarget:self action:@selector(switchAorB:) forControlEvents:(UIControlEventTouchUpInside)];
	
}

- (void) initDataUI {
	self.title = self.songModel.cySongName;
	
	NSString *url = K18UserModel.shared.currentUserDetail.cyImg?K18UserModel.shared.currentUserDetail.cyImg : @"";
	[self.scoreView.scoreIcon sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
	
	self.scoreView.score = 0;
	
    self.earManager.songModel = self.songModel;
	
	[self startToRecordSong];
}


/**
 开始录歌
 */
- (void) startToRecordSong {
    [self.earManager startListen];
	NSURL *songFileURL = self.recordModel.songFileUrl;
	__weak typeof(self)wself = self;
	if (songFileURL) {
        [self.gradeManager start];
		[self initLrcViewData];
		
		//calculate the duration
		[[K18AudioService shared] getAudioDurationWithURL:songFileURL resolve:^(NSNumber *duration) {
			wself.recordModel.songDuration = duration.integerValue;
			wself.recordStatusView.totalDuration = duration.unsignedIntegerValue;
		}];
        
        [K18AudioEngine setupAudioSession];
        
        [[K18AudioEngine shared] startRecord:^{
            
            //开始播放
            if (wself.accompanyModel.model) {//重录
                [wself.accompanyModel replay];
                //同时录制
                [[K18AudioEngine shared] startRecord:^{
                    
                } fail:^(NSError * _Nonnull err) {
                    [[HUDHelper sharedInstance] tipMessage:@"播放异常" delay:1 completion:^{
                        [wself.navigationController popViewControllerAnimated:YES];
                    }];
                }];
            }else{
//                [K18AudioEngine shared].startPlayCallback = ^{
//
//
//                };
//
                K18Double *model = [K18Double new];
                model.firstUrl = wself.recordModel.songFileUrl;
                model.firstVolume = 1;
                model.secondUrl = wself.recordModel.songOriginFileUrl;
                model.secondVolume = 1;
                [wself.accompanyModel playFromModel:model];
            }
             wself.accompanyModel.pitch = 0;
            
            
            //更新UI状态
            //正在播放
            [wself.recordStatusView toggleState:YES];
            if (wself.accompanyModel.isAccompany) {
                [wself.accompanyModel switchPlayer:YES];
                [wself.recordStatusView.toggleBtn setTitle:@"打开原唱" forState:(UIControlStateNormal)];
            }else{
                [wself.accompanyModel switchPlayer:NO];
                [wself.recordStatusView.toggleBtn setTitle:@"关闭原唱" forState:(UIControlStateNormal)];
            }
            wself.debugBar.num = 0;
            
        } fail:^(NSError * _Nonnull err) {
            [[HUDHelper sharedInstance] tipMessage:@"播放异常" delay:1 completion:^{
                [wself.navigationController popViewControllerAnimated:YES];
            }];
        }];
        
		
		
	}else{
		NSLog(@"不存在下载好的伴奏文件");
	}
}

- (void) initLrcViewData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSMutableArray *list = [K18LrcModel musicLRCModelsWithLRCStrings:self.accompany.cyLyricStr].mutableCopy;
//        self.lrcView.musicLRCArray = list;
        
        self.lrcView.krcInfo = [K18KRCReader readInputText:self.accompany.cyKrcLyricStr];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.lrcView reloadData];
        });
    });
}


#pragma mark - Actions

- (void) leftBarButtonAction {
	
	//暂停录音
	[self.accompanyModel pause];
	[[K18AudioEngine shared] pauseRecord];
	
	__weak typeof(self)wself = self;
	[HUDHelper alertTitle:@"录制还没结束，确定要退出吗?" message:nil confirm:@"取消" cancel:@"退出" conBlock:^{
		//继续录音
		[wself.accompanyModel play];
		[[K18AudioEngine shared] resumeRecord];
	} action:^{
//		[[GLMusicPlayer defaultPlayer] stop];
		[[K18AudioEngine shared] stopRecord:nil immediately:YES];
		[wself.accompanyModel stop];
		[wself.navigationController popViewControllerAnimated:YES];
	}];
	
}


/**
 原唱/伴奏切换
 */
- (void) switchAorB:(UIButton *)sender {
	if (self.accompany.cyOriginalSongUrl.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"暂无原唱"];
		return;
	}
	if(self.accompanyModel.isAccompany){//是伴奏的话
		[self.accompanyModel switchPlayer:NO];
	}else{
		[self.accompanyModel switchPlayer:YES];
	}
	
	//To dest
	if (self.accompanyModel.isAccompany) {
		[sender setTitle:@"打开原唱" forState:(UIControlStateNormal)];
	}else{
		[sender setTitle:@"关闭原唱" forState:(UIControlStateNormal)];
	}
}


/**
 重录
 */
- (void) resetAction {
	//暂停录音
	[self.accompanyModel pause];
	[[K18AudioEngine shared] pauseRecord];
	
	__weak typeof(self)wself = self;
	//样式问题，调转确定和取消按钮
	[HUDHelper alertTitle:@"确定要重唱吗?" message:nil confirm:@"取消" cancel:@"确定" conBlock:^{
		//继续录音
		[wself.accompanyModel play];
		[[K18AudioEngine shared] resumeRecord];
	} action:^{
		
		if ([K18AudioEngine shared].isRecording) {
			
			[[K18AudioEngine shared] stopRecord:^(NSURL * _Nullable recordedURL, NSError * _Nullable error) {

				[wself startToRecordSong];
				
			} immediately:YES];
						
		}else{
			[wself startToRecordSong];
		}
		
	}];
}

- (void) completionA {
#if NEED_SONG_10
    if (self.recordStatusView.curTime < 10) {
        [[HUDHelper sharedInstance] tipMessage:@"录制时间太短，再唱一会吧~"];
        return;
    }
#endif
    
	//暂停录音
	[self.accompanyModel pause];

	[[K18AudioEngine shared] pauseRecord];
	
	__weak typeof(self)wself = self;
	//样式问题，调转确定和取消按钮
	[HUDHelper alertTitle:@"还没有唱完，确定要结束吗?" message:nil confirm:@"取消" cancel:@"确定" conBlock:^{
		//继续录音
		[wself.accompanyModel play];

		[[K18AudioEngine shared] resumeRecord];
	} action:^{
		
		[[K18AudioEngine shared] stopRecord:nil];
		

		[[HUDHelper sharedInstance] syncLoading];
		[self.recordStatusView toggleState:NO];
//        [self.accompanyModel stop];

	}];

}

- (void) debugAction {
	self.debugBtn.selected = !self.debugBtn.selected;
	if (self.debugBtn.selected) {
		CGRect frame = _debugBar.frame;
		[self presentViewController:self.debugBarController animated:NO completion:^{
			self.debugBar.hidden = NO;
			self.debugBar.frame = frame;
		}];
	}else{
		[self dismissViewControllerAnimated:NO completion:nil];
	}
}

- (void) debugBarDismiss {
	self.debugBtn.selected = NO;
	[self dismissViewControllerAnimated:NO completion:nil];
}

- (void)pitchValueChange:(NSInteger)num {
	self.accompanyModel.pitch = num*100;
}

- (void) clickLrcView {
	if ([K18AudioEngine shared].isRecording) {
		[self.accompanyModel pause];
		[[K18AudioEngine shared] pauseRecord];
		[[HUDHelper sharedInstance] tipMessage:@"轻触歌词继续录制"];
		
	}else{
		[[K18AudioEngine shared] resumeRecord];
		[self.accompanyModel play];
	}
}

#pragma mark - notifications

#pragma mark - K18AudioEngineDelegate

- (void)onRecordStop:(NSURL *)exportURL duration:(NSTimeInterval)duration
{
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		
		[[HUDHelper sharedInstance] syncStopLoading];
		
		//TODO 积分
		
		//录制导出信息
		self.recordModel.recordFileUrl = exportURL;
		self.recordModel.recordDuration = duration;
		self.recordModel.score = self.scoreView.score;
		self.recordModel.rate = self.scoreView.rate;
		self.recordModel.pitch = [K18AudioEngine shared].pitch;
		
		//TODO 跳转发布
		K18SongModel *model = [K18SongModel new];
		model.song = self.songModel;
		model.recordModel = self.recordModel;
		model.accompanyModel = self.accompany;
		[K18SongService gotoPublishWithModel:model root:self];
	});
}

- (void)record:(K18AudioEngine *)engine currentTime:(NSTimeInterval)curTime volume:(float)volume
{
	
	[self.gradeManager updateScoreWithVolume:volume model:self.accompany
					currentPlayTimeInSeconds:curTime
//                               musicLRCArray:self.lrcView.krcInfo.lineInfoTreeMap
                                     krcInfo:self.lrcView.krcInfo
							 currentLcrIndex:self.lrcView.currentLcrIndex];
	
	self.scoreView.rate = self.gradeManager.currentScore * 1.0 / kMAXSCORE;
	self.scoreView.score = self.gradeManager.currentScore;
}

- (void)onPlaying:(K18AudioEngine *)engine currentTime:(NSTimeInterval)curTime
{
	//更新进度条
	self.recordStatusView.curTime = curTime;
	
	//更新歌词
	[self.lrcView updateMusicLrcForRowWithCurrentTime:curTime];
}

- (void)onPlayStop:(K18AudioEngine *)engine
{
	// 伴奏停止时
	[[K18AudioEngine shared] stopRecord:nil];
	[self.recordStatusView toggleState:NO];
	[[HUDHelper sharedInstance] syncLoading];
}

#pragma mark - private

- (void) setButtonStyle:(IconButton *)button {
    button.imageSize = CGSizeMake(32, 31);
    button.titleLabel.font = [UIFont systemFontOfSize:14];
}

- (void) addEvents {
//    [self.earManager startListen];
	[K18AudioEngine shared].delegate = self;
}

- (void) removeEvents {
    [self.earManager stopListen];
	[K18AudioEngine shared].delegate = nil;
}

#pragma mark - getter

- (UIViewController *)debugBarController
{
	if (!_debugBarController) {
		UIViewController *vc = [UIViewController new];
		UIControl *control = [[UIControl alloc]initWithFrame:[UIScreen mainScreen].bounds];
		[control addTarget:self action:@selector(debugBarDismiss) forControlEvents:(UIControlEventTouchUpInside)];
		[vc.view addSubview:control];
		[control addSubview:_debugBar];
		vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
		
		_debugBarController = vc;

	}
	return _debugBarController;
}

@end
