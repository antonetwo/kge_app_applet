//
//  K18SongAuthorController.h
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "K18Singer.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18SongAuthorController : K18BaseController

@property (nonatomic, strong) NSMutableArray <K18Singer*> *dataSource;

@property(strong, nonatomic) UICollectionView *collectionView;
@end

NS_ASSUME_NONNULL_END
