//
//  K18SongListController.h
//  18kcy
//
//  Created by 唐 on 2019/6/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "K18Song.h"
#import "K18Singer.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18SongListController : K18BaseController

@property(strong, nonatomic) K18Singer *singer;
@property(strong, nonatomic) K18Category *category;
@property (nonatomic, strong) NSMutableArray <K18Song*> *dataSource;
//Data
- (instancetype)initWithType:(int)ctype;

@property(strong, nonatomic) UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
