//
//  K18PublishController2.h
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "K18SongModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18PublishController2 : K18BaseController
@property(strong, nonatomic) K18SongModel *model;
@end

NS_ASSUME_NONNULL_END
