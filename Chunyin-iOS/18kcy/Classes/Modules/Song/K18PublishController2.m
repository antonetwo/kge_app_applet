//
//  K18PublishController2.m
//  18kcy
//
//  Created by 唐 on 2019/6/13.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18PublishController2.h"
#import "K18AudioService.h"
#import "K18AlbumBar.h"
#import "K18UserModel.h"
#import "K18Song.h"
#import "GLMusicPlayer.h"
#import "K18PhotoController.h"
#import "K18Album.h"
#import "K18SongService.h"
@interface K18PublishController2 ()<GLMusicPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *preview_Img;
@property (weak, nonatomic) IBOutlet UIButton *publishButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UITextView *descLabel;

@property (weak, nonatomic) IBOutlet UIView *albumContainer;

@property(strong, nonatomic) K18AlbumBar *albumBar;

@property(strong, nonatomic) MBProgressHUD *uploadHud;
@property(strong, nonatomic) K18PhotoController *photoVC;
@end

@implementation K18PublishController2

- (void)viewDidLoad {
    [super viewDidLoad];
	self.forbidGuestureBack = YES;
    self.navigationItem.leftBarButtonItem = self.leftBarBackItem;
}

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self addEvents];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self removeEvents];
    [[GLMusicPlayer defaultPlayer] stop];
}

#pragma mark - init UI

- (void) initUI {
	self.publishButton.backgroundColor = k18_TopicC;
	
	self.descLabel.layer.cornerRadius = 8;
	self.descLabel.layer.borderColor = RGBOF(0xDDDDDD).CGColor;
	self.descLabel.layer.borderWidth = 1;
	
	self.publishButton.layer.cornerRadius = 4;
	
	_albumBar = [K18AlbumBar new];
	_albumBar.frame = self.albumContainer.bounds;
	[self.albumContainer addSubview:_albumBar];
	
    [_albumBar.addButton addTarget:self action:@selector(addImage) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void) initData
{
	self.title = self.model.song.cySongName;
	
	NSString *imgUrl = self.model.song.cyImg ? self.model.song.cyImg : @"";
    [self.preview_Img sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"changpian"]];
    self.preview_Img.backgroundColor = [UIColor blueColor];
    self.preview_Img.layer.cornerRadius = self.preview_Img.width/2;
    self.preview_Img.layer.masksToBounds = YES;
	
//	[self startPlayAudio];
}

- (void) startPlayAudio {
	NSURL *fileUrl = self.model.recordModel.combineFileUrl;
	
    [GLMusicPlayer defaultPlayer].volume = 1;
    [[GLMusicPlayer defaultPlayer] playFromURL:fileUrl];
		

}

#pragma mark - GLMusicPlayerDelegate

- (void)updateProgressWithCurrentPosition:(FSStreamPosition)currentPosition endPosition:(FSStreamPosition)endPosition
{
	
}

- (void)updateMusicLrc
{
	
}

- (void)onStopMusic:(FSAudioStreamState)state
{
	[self.playButton setSelected:NO];

}

#pragma mark - actions

- (void)leftBarButtonAction
{
    [HUDHelper alertTitle:@"录制还没结束，确定要退出吗?" message:nil confirm:@"取消" cancel:@"退出" conBlock:^{
        
    } action:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

- (void) addImage {
    if(!self.photoVC){
        K18PhotoController *vc = [K18PhotoController new];
        vc.editable = YES;
        vc.hidesBottomBarWhenPushed = YES;
        self.photoVC = vc;
        __weak typeof(self)wself = self;
        vc.selectPhotosBlock = ^(NSArray<K18Album *> * _Nonnull photos) {
            wself.albumBar.dataSource = photos.mutableCopy;
        };
    }
    
    [self.navigationController pushViewController:self.photoVC animated:YES];
}

- (IBAction)togglePlay:(id)sender {
	self.playButton.selected = !self.playButton.selected;
	
	if (self.playButton.selected) {
		
		[self startPlayAudio];
		
	}else{
//		[[K18AudioService shared] pausePlayAudio];
		[[GLMusicPlayer defaultPlayer] pause];
	}
}
- (IBAction)locationA:(id)sender {
    [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"];
}

- (IBAction)publishA:(id)sender {
//    K18SongModel *model = self.model;
//    K18Song *song = self.model.song;
	K18RecordModel *rm = self.model.recordModel;
    NSString *myId = k18_loginUserId;
//    NSString *mood = notNil(self.descLabel.text);
	NSURL *fileUrl = rm.combineFileUrl;
    
    __weak typeof(self)wself = self;
    
    self.uploadHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.uploadHud.mode = MBProgressHUDModeAnnularDeterminate;
    self.uploadHud.labelText = @"正在上传";
    NSLog(@"正在上传歌曲文件");
    [self uploadFile:fileUrl cyType:0 cyUserId:myId success:^(id  _Nonnull response) {
        [wself.uploadHud hide:NO];
        K18File *fileModel = [K18File yy_modelWithJSON:response];
        wself.model.uploadedFile = fileModel;
        [wself uploadSong];
        NSLog(@"发布歌曲");
    } failure:^(NSError * _Nonnull err) {
        [wself.uploadHud hide:NO];
        [[HUDHelper sharedInstance] tipMessage:err.localizedDescription];
    } progress:^(NSProgress * _Nonnull progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wself.uploadHud.progress = progress.fractionCompleted;
        });
    }];
	
}

- (void) uploadSong {
    K18SongModel *model = self.model;
    K18Song *song = self.model.song;
    K18RecordModel *rm = self.model.recordModel;
    NSString *myId = k18_loginUserId;
    NSString *mood = notNil(self.descLabel.text);
//    NSString *fileUrl = [NSURL fileURLWithPath:rm.recordFileUrl];
    
    __weak typeof(self)wself = self;
	NSMutableArray *albums = self.albumBar.dataSource;
	NSString *photos = @"";
	
	for (int i=0; i<albums.count; i++) {
		K18Album *model = albums[i];
		if (i==0) {
			photos = model.id;
		}else{
			photos = [photos stringByAppendingFormat:@"_%@",model.id];
		}
	}
    NSDictionary *params;
    if (model.recordModel.songFileUrl==nil) {
        params = @{
                   @"cyType":@(1),
                   @"cyUserId":myId,
                   @"cyQingChangName":notNil(model.song.cySongName),
                   @"cyMood":mood,
                   @"cyPhotoIds":photos,
                   @"songLiveFileId":self.model.uploadedFile.id,
                   @"cyLocationFlag":@"0"
                   };
    }else{
        params = @{
                   @"cyType":@(0),
                   @"songId":song.id,
                   @"cyUserId":myId,
                   @"cyScore":@(rm.score),
                   @"cyMood":mood,
                   @"cyPhotoIds":photos,
                   @"songLiveFileId":self.model.uploadedFile.id,
                   @"cyLocationFlag":@"0"
                   };
    }
    
    //
    
    MBProgressHUD *hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_doFabu params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud];
        
        wself.model.doneSongId = [data stringValue];
        //
//        [wself.navigationController popToRootViewControllerAnimated:YES];
        
        [wself testGotoDetailController];
        
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
    }];
}

- (void) testGotoDetailController{

    UIViewController *root = self.navigationController.viewControllers.firstObject;
    [self.navigationController popToRootViewControllerAnimated:NO];
    [K18SongService gotoSongDetailWithId:self.model.doneSongId root:root];

}

#pragma mark - Notificatons

//- (void) onAudioStop:(NSNotification *)notify {
//	[self.playButton setSelected:NO];
//	[K18AudioService shared].player.url = [K18AudioService shared].player.url;
//}

#pragma mark - private

- (void) addEvents {
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAudioStop:) name:K18Event_onAudioStop object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAudioProgress:) name:K18Event_onAudioProgress object:nil];
	[GLMusicPlayer defaultPlayer].glPlayerDelegate = self;
}

- (void) removeEvents {
//	[[NSNotificationCenter defaultCenter] removeObserver:self name:K18Event_onAudioStop object:nil];
//	[[NSNotificationCenter defaultCenter] removeObserver:self name:K18Event_onAudioProgress object:nil];
	[GLMusicPlayer defaultPlayer].glPlayerDelegate = nil;
	
}

@end
