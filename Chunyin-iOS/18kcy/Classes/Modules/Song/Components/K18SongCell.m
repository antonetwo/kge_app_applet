//
//  K18SongCell.m
//  18kcy
//
//  Created by 唐 on 2019/6/11.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongCell.h"

@implementation K18SongCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	UIImage *img = [UIImage imageNamed:@"K_ge_zhiwang"];
	img = [img imageWithCornerRadius:4];
	self.imgView.image = img;
	
	[self.button addTarget:self action:@selector(buttonA) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)prepareForReuse
{
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
	
}

- (void) setModel:(K18Song *)model
{
	_model = model;
	[self.imgView sd_setImageWithURL:[NSURL URLWithString:model.cyImg] placeholderImage:[UIImage imageNamed:@"K_ge_zhiwang"]];
	self.titleLabel.text = model.cySongName;
	self.authorLabel.text = model.cySinger;
	self.descLabel.text = [NSString stringWithFormat:@"%ld次演唱",model.cySongNum.integerValue];
}

+ (CGFloat)cellHeight
{
	return 86;
}

- (void) buttonA {
	if (self.delegate){
		[self.delegate cell:self didSelect:self.model];
	}
}

@end
