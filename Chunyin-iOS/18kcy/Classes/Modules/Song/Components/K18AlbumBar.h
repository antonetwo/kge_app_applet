//
//  K18AlbumBar.h
//  18kcy
//
//  Created by 唐 on 2019/6/21.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class AlbumCell;
@class K18Album;

@interface K18AlbumBar : UIView
@property(strong, nonatomic) UIButton *addButton;
@property(strong, nonatomic) UIScrollView *scrollView;

@property(strong, nonatomic) NSMutableArray<K18Album*> *dataSource;
@property(strong, nonatomic) NSMutableArray<AlbumCell*>* cells;
@end

@interface AlbumCell : UIView
@property(strong, nonatomic) UIImageView *imageView;
@property(strong, nonatomic) UIButton *cancelButton;
@end

NS_ASSUME_NONNULL_END
