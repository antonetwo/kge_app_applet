//
//  K18ModeView.h
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 混响模式菜单
 */
@interface K18ReverbView : UIScrollView
@property(strong, nonatomic) NSMutableArray *buttons;
@property(copy, nonatomic) void(^clickBlock) (NSInteger index);
@end

NS_ASSUME_NONNULL_END
