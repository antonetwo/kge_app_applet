//
//  K18ScoreView.h
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 计分栏
 */
@interface K18ScoreView : UIView
@property(strong, nonatomic) UIImageView *scoreIcon;
@property(strong, nonatomic) UIView *scoreBar;
@property(strong, nonatomic) UIView *scoreBarBg;
@property(strong, nonatomic) UIView *scoreBarMask;
@property(strong, nonatomic) UILabel *scoreLabel;

@property(nonatomic) CGFloat rate;
@property(nonatomic) NSInteger score;
@end

NS_ASSUME_NONNULL_END
