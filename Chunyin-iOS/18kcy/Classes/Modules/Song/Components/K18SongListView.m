//
//  K18SongListView.m
//  18kcy
//
//  Created by 唐 on 2019/6/10.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongListView.h"
#import <MJRefresh/MJRefresh.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "K18SongCell.h"
#import "K18SongService.h"

@interface K18SongListView () <UITableViewDataSource, UITableViewDelegate, K18SongCellDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) NSIndexPath *lastSelectedIndexPath;
@property(nonatomic) NSInteger pageNumber;
@property(nonatomic) BOOL isPageMax;
@end

@implementation K18SongListView

- (void)dealloc
{
	self.scrollCallback = nil;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.isFirstLoaded = YES;
		
		_tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
		self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
		self.tableView.backgroundColor = [UIColor whiteColor];
		self.tableView.tableFooterView = [UIView new];
		self.tableView.dataSource = self;
		self.tableView.delegate = self;
		[self.tableView registerNib:[UINib nibWithNibName:@"K18SongCell" bundle:nil] forCellReuseIdentifier:@"cell"];
		[self addSubview:self.tableView];
		
		[_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
			make.top.equalTo(self);
			make.bottom.equalTo(self);
			make.left.equalTo(self);
			make.right.equalTo(self);
		}];
	}
	return self;
}

- (void)setIsNeedHeader:(BOOL)isNeedHeader {
	_isNeedHeader = isNeedHeader;
	
	__weak typeof(self)weakSelf = self;
	if (self.isNeedHeader) {
		self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
			[weakSelf requestData:nil];
		}];
	}
}

- (void)loadMoreDataTest {
//	[self.dataSource addObject:@"加载更多成功"];
	[self.tableView reloadData];
}

#pragma mark - Data service

- (void) requestData:(void(^)(void))callback {
	self.isPageMax = NO;
	self.pageNumber = 0;
	__weak typeof(self)wself = self;
	NSDictionary *params = @{@"cyType":@(self.rankType), @"pageNumber":@(self.pageNumber+1),@"pageSize":@(kPageSize)};
	[self sendWithPath:kAPI_getSongRank params:params succ:^(id  _Nonnull data) {
		[wself.tableView.mj_header endRefreshing];
		wself.pageNumber ++;
		
		[wself dealDataSourceWithJson:data];
		[wself.tableView reloadData];
		[wself setRefreshFooter];
		if(callback)callback();
	} failed:^(NSError * _Nonnull error) {
		[wself.tableView.mj_header endRefreshing];
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
		if(callback)callback();
	}];
	
}

- (void) dealDataSourceWithJson:(id)json {
	NSArray *list = [NSArray yy_modelArrayWithClass:[K18Song class] json:json];
	K18Song *m1 = list.lastObject;
	K18Song *m2 = self.dataSource.lastObject;
	if (!m1) {
		self.isPageMax = YES;
		[self setRefreshFooter];
		return;
	}
	
	if ([m1.id isEqualToString:m2.id]) {
		self.isPageMax = YES;
		[self setRefreshFooter];
		return;
	}
	
	if (self.dataSource.count) {
		[self.dataSource addObjectsFromArray:list];
	}else{
		self.dataSource = list.mutableCopy;
	}
	
}

- (void)  setRefreshFooter {
	if (!self.isPageMax) {
		//模拟加载完数据之后添加footer
		__weak typeof(self)wself = self;
		self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
			
			NSDictionary *params = @{@"cyType":@(self.rankType), @"pageNumber":@(wself.pageNumber+1),@"pageSize":@(kPageSize)};
			[wself sendWithPath:kAPI_getSongRank params:params succ:^(id  _Nonnull data) {
				[wself.tableView.mj_footer endRefreshing];
				wself.pageNumber ++;
				[wself dealDataSourceWithJson:data];
				[wself.tableView reloadData];
				[wself setRefreshFooter];
			} failed:^(NSError * _Nonnull error) {
				[wself.tableView.mj_footer endRefreshing];
				[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
			}];
			
		}];
	}else {
		self.tableView.mj_footer = nil;
	}
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	K18SongCell *cell = (K18SongCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.delegate = self;
	K18Song *model = self.dataSource[indexPath.row];
	cell.model = model;
	
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [K18SongCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//	K18Song *model = self.dataSource[indexPath.row];
//	[K18SongService gotoSongDetailWithId:model root:self.rootVC];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	!self.scrollCallback?:self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate

- (UIScrollView *)listScrollView {
	return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
	self.scrollCallback = callback;
}

- (void)listViewLoadDataIfNeeded {
	if (!self.isFirstLoaded) {
		return;
	}
	self.isFirstLoaded = NO;
	if (self.isNeedHeader) {
		[self.tableView.mj_header beginRefreshing];
	}else {
		
		[self requestData:nil];
		
//		[MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
//		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//			[MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:NO];
//			[self.tableView reloadData];
//
//			[self setRefreshFooter];
//		});
	}
	
}

- (void)listViewRefreshData:(void (^)(void))callback
{
	[self requestData:callback];
}

#pragma mark - K18SongCellDelegate

- (void) cell:(K18SongCell *)cell didSelect:(K18Song *)model
{
	[K18SongService gotoRecordingWithModel:model root:self.rootVC];
}

@end
