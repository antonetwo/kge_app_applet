//
//  K18Cell2.h
//  18kcy
//
//  Created by 唐 on 2019/6/11.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Song.h"
NS_ASSUME_NONNULL_BEGIN


/**
 K歌页面分类菜单
 */
@interface K18Cell2 : UITableViewCell
@property(nonatomic, copy) void(^clickMenu) (NSInteger index, NSString *title);

@property(weak, nonatomic) NSArray<K18Category *> *datas;

@end

NS_ASSUME_NONNULL_END
