//
//  K18IntonationView.m
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18IntonationView.h"
#import "K18Common.h"

@implementation K18IntonationView

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self initUI];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	//
	self = [super initWithFrame:frame];
	if (self) {
		[self initUI];
		
	}
	return self;
}

- (void) initUI {
//    self.backgroundColor = k18_TopicC;
	self.alpha = 0.5;
    self.layer.borderColor = RGBOF(0xc1c1c1).CGColor;
    self.layer.borderWidth = 0.5;
}

@end
