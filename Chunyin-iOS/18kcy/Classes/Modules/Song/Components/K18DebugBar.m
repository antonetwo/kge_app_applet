//
//  K18DebugBar.m
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18DebugBar.h"
#import "K18Common.h"
#define kDebugNumMax 12 //调节值

@implementation K18DebugBar

//@property(weak, nonatomic) IBOutlet UIButton *plusBtn;
//@property(weak, nonatomic) IBOutlet UIButton *subtractBtn;
//@property (weak, nonatomic) IBOutlet UILabel *label;

- (void)awakeFromNib
{
	[super awakeFromNib];
	self.label.text = @"0";
	[self.plusBtn addTarget:self action:@selector(add) forControlEvents:(UIControlEventTouchUpInside)];
	[self.subtractBtn addTarget:self action:@selector(substract) forControlEvents:(UIControlEventTouchUpInside)];
    [self.plusBtn setImage:[[UIImage imageNamed:@"+"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
    [self.subtractBtn setImage:[[UIImage imageNamed:@"-"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
}

- (void)add{
	self.num = MIN(self.num+1, kDebugNumMax);
}
-(void)substract{
	self.num = MAX(self.num-1, -kDebugNumMax);
}

- (void)setNum:(NSInteger)num
{
	_num = num;
	self.label.text = @(self.num).stringValue;
	if (self.valueChanged) {
		self.valueChanged(self.num);
	}
}


@end
