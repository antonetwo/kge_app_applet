//
//  K18DetailView.m
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18DetailView.h"
#import "K18Common.h"
#import "CCPagedScrollView.h"
#import "K18AudioService.h"
#import "GLMusicPlayer.h"

@interface K18DetailView()<GLMusicPlayerDelegate>
@property(strong, nonatomic) CCPagedScrollView *advertView;
@property(nonatomic) BOOL isDrag;
@end

@implementation K18DetailView

- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    [GLMusicPlayer defaultPlayer].glPlayerDelegate = nil;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.slider.value = 0;
	CGFloat ah = 192.0/375.0 * MainScreenWidth();
	self.advertView = [[CCPagedScrollView alloc] initWithFrame:CGRectMake(0, 0, MainScreenWidth(), ah) animationDuration:5.0 isAuto:YES];
    [self.carouselContrainer insertSubview:self.advertView atIndex:0];
	
	[self.toggleConcern addTarget:self action:@selector(toggleConcernAction:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.slider addTarget:self action:@selector(valueChange:) forControlEvents:(UIControlEventValueChanged)];
	
}

- (void) touchDrageIn {
	NSLog(@"touchDrageIn");
	self.isDrag = YES;
}

- (void)touchDragOut {
	NSLog(@"touchDragOut");
	self.isDrag = NO;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	NSLog(@"bbbb:%@", self.carouselContrainer);
}

//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//@property (weak, nonatomic) IBOutlet UILabel *descLabel;
//@property (weak, nonatomic) IBOutlet UIButton *toggleConcern;
//@property (weak, nonatomic) IBOutlet UIView *carouselContrainer;
//@property (weak, nonatomic) IBOutlet UISlider *slider;
//@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
//@property (weak, nonatomic) IBOutlet UIButton *audioL;
//@property (weak, nonatomic) IBOutlet UIButton *shareL;
//@property (weak, nonatomic) IBOutlet UIButton *flowerL;
//@property (weak, nonatomic) IBOutlet UILabel *createTimeL;
- (void)setModel:(K18SongDetail *)model
{
    _model = model;
    [self updateUI];
}

- (void) updateUI {
    K18SongDetail *m = self.model;
    NSString *url = m.userImgUrl;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:notNil(url)] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
    self.iconView.layer.cornerRadius = self.iconView.width/2;
    
    self.nameLabel.text = m.userName;
    self.descLabel.text = m.cyMood;
    
    UIButton *button;
    button = self.audioL;
    [button setTitle:m.cyListenNum.stringValue forState:(UIControlStateNormal)];
    button = self.shareL;
    [button setTitle:m.cyShareNum.stringValue forState:(UIControlStateNormal)];
    button = self.flowerL;
    [button setTitle:m.cyFlowerNum.stringValue forState:(UIControlStateNormal)];
    
    if (m.userFollowFlag.boolValue) {
        [_toggleConcern setTitle:@"已关注" forState:(UIControlStateNormal)];
    }else{
        [_toggleConcern setTitle:@"关注" forState:(UIControlStateNormal)];
    }
    
    self.createTimeL.text = m.createTimeStr;
	NSMutableArray *showArray = [[NSMutableArray alloc] init];
	for (NSInteger i = 0; i < m.userPhotos.count; i++) {
		NSString *url = m.userPhotos[i];
		CCPagedScrollViewItem *item = [[CCPagedScrollViewItem alloc]init];
		item.itemImageUrl = url;
		item.itemurl = url;
		[showArray addObject:item];
	}
	[_advertView.pageControl setHidden:NO];
	[_advertView.pageView setHidden:YES];
	[_advertView setItems:showArray];
	[_advertView setTapActionBlock:^(CCPagedScrollViewItem *item) {
		
	}];
	[_advertView setupViews];
    
    if([self.model.cyUserId isEqualToString:k18_loginUserId]){
        self.toggleConcern.hidden = YES;
    }
    
    [self.iconView setupClickWithUserId:m.cyUserId];
}

- (void) toggleConcernAction:(UIButton *)sender {
	
	if ([k18_loginUserId isEqualToString:self.model.cyUserId]) {
		[[HUDHelper sharedInstance] tipMessage:@"不能关注自己"];
		return;
	}
	
	BOOL isFollow = !self.model.userFollowFlag.boolValue;
	
	NSDictionary *params = @{@"cyUserId":k18_loginUserId,
							 @"cyFollowId":self.model.cyUserId,
							 @"cyFollow":@(isFollow),
							 };
	__weak typeof(self)wself = self;
	//TODO 关注与取消关注
	id hud =[[HUDHelper sharedInstance] loading];
	[self sendWithPath:kAPI_doFollowUser params:params succ:^(id  _Nonnull data) {
		wself.model.userFollowFlag = @((int)isFollow);
		if (isFollow) {
			[wself.toggleConcern setTitle:@"已关注" forState:(UIControlStateNormal)];
		}else{
			[wself.toggleConcern setTitle:@"关注" forState:(UIControlStateNormal)];
		}
		if (isFollow) {
			[[HUDHelper sharedInstance] stopLoading:hud message:@"关注成功" delay:1 completion:nil];
		}else{
			[[HUDHelper sharedInstance] stopLoading:hud message:@"取消关注成功"  delay:1 completion:nil];
		}

	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
	}];
	
}

#pragma mark - actions

- (void) valueChange:(K18Slider *)slider {
    FSStreamPosition position = {};
    unsigned totalSeconds = [GLMusicPlayer defaultPlayer].duration.minute*60 + [GLMusicPlayer defaultPlayer].duration.second;
    unsigned currentSeconds = totalSeconds * slider.value;
    
    position.second = currentSeconds % 60;
    position.minute = currentSeconds / 60;
    
    [[GLMusicPlayer defaultPlayer] seekToPosition:position];
}


- (IBAction)togglePlayAction:(UIButton *)sender {
	
    if(![GLMusicPlayer defaultPlayer].isPlaying){
        [GLMusicPlayer defaultPlayer].glPlayerDelegate = self;
        [[GLMusicPlayer defaultPlayer] play];
        self.togglePlayBtn.selected = YES;
    }else{
        [[GLMusicPlayer defaultPlayer] pause];
        self.togglePlayBtn.selected = NO;
    }
    
//    sender.selected = !sender.selected;
}

- (void) startPlayAudio {
    [GLMusicPlayer defaultPlayer].glPlayerDelegate = self;
	[[GLMusicPlayer defaultPlayer] stop];
//	[[K18AudioService shared] stopPlayAudio];
	
	NSString *urlStr = notNil(self.model.cySongLiveUrl);
	NSURL *url = [NSURL URLWithString:urlStr];
	[[GLMusicPlayer defaultPlayer] playFromURL:url];
    
}

- (void) stopPlayAudio {
	[[GLMusicPlayer defaultPlayer] stop];
	[GLMusicPlayer defaultPlayer].glPlayerDelegate = nil;
    self.togglePlayBtn.selected = NO;

//	[[K18AudioService shared] stopPlayAudio];
}

- (void) updateTimeLabel:(UILabel *)label curTime:(CGFloat)curTimeNum totalTime:(CGFloat)totalTime {
	NSUInteger curTime = ceil(curTimeNum);
	NSUInteger curSec = curTime%60;
	NSUInteger curMin = curTime/60;
	NSUInteger duration = ceil(totalTime);
	NSUInteger tSec = duration%60;
	NSUInteger tMin = duration/60;
	
	if (curMin == tMin) {
		curSec = MIN(curSec, tSec);
	}
	
	label.text = [NSString stringWithFormat:@"%02lu:%02lu/%02lu:%02lu",curMin,curSec,tMin,tSec];
}

#pragma mark - GLMusicPlayerDelegate

- (void)updateProgressWithCurrentPosition:(FSStreamPosition)currentPosition endPosition:(FSStreamPosition)endPosition
{
	NSNumber *curTime = @(currentPosition.playbackTimeInSeconds);
	NSNumber *total = @(endPosition.playbackTimeInSeconds);
	
	NSUInteger totalDur = ceil(total.doubleValue);
	CGFloat scaleTime = curTime.doubleValue/totalDur;
	
	self.slider.value = scaleTime;
	[self updateTimeLabel:self.timeLabel curTime:curTime.doubleValue totalTime:total.doubleValue];
}

- (void)updateMusicLrc
{
   
}

- (void)onStopMusic:(FSAudioStreamState)state
{
    
    if(state == kFsAudioStreamPaused){
        self.togglePlayBtn.selected = NO;
    }else{
        self.togglePlayBtn.selected = YES;
    }

}

@end
