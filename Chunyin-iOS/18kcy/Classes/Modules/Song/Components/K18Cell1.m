//
//  K18Cell1.m
//  18kcy
//
//  Created by 唐 on 2019/6/11.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18Cell1.h"
#import "IconButton.h"
#import "K18Common.h"

@interface K18Cell1()

@property(strong, nonatomic) NSMutableArray<IconButton*> *buttons;


@end

@implementation K18Cell1

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self initUI];
	}
	return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) initUI {
	_buttons = [NSMutableArray array];
	CGFloat w = 42;
	CGFloat h = 66;
	CGFloat equalGap = ([UIScreen mainScreen].bounds.size.width - 42*4)/5.0;
	for (int i=0; i<4; i++) {
		CGRect frame = CGRectMake(equalGap + i*(w+equalGap), 25, w, h);
		IconButton *button = [[IconButton alloc]initWithFrame:frame];
		[self addSubview:button];
		[_buttons addObject:button];
		button.tag = i;
		[button addTarget:self action:@selector(clickAction:) forControlEvents:(UIControlEventTouchUpInside)];
		[button setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
	}
	//323232

	[_buttons[0] setImage:[[UIImage imageNamed:@"qiangmai"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[0] setTitle:@"抢麦" forState:(UIControlStateNormal)];
	
	[_buttons[1] setImage:[[UIImage imageNamed:@"hechang"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[1] setTitle:@"合唱" forState:(UIControlStateNormal)];
	
	[_buttons[2] setImage:[[UIImage imageNamed:@"qingchang"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[2] setTitle:@"清唱" forState:(UIControlStateNormal)];
	
	[_buttons[3] setImage:[[UIImage imageNamed:@"gefang"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[3] setTitle:@"歌房" forState:(UIControlStateNormal)];
	
}

- (void) clickAction:(IconButton *)button {
	if (self.clickMenu) {
		self.clickMenu(button.tag, button.titleLabel.text);
	}
}

@end
