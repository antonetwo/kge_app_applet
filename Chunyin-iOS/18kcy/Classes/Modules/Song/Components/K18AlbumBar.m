//
//  K18AlbumBar.m
//  18kcy
//
//  Created by 唐 on 2019/6/21.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AlbumBar.h"
#import "K18Common.h"
#import "K18Album.h"

@implementation K18AlbumBar

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initUI];
	}
	return self;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.cells = [NSMutableArray array];
		[self initUI];
	}
	return self;
}

- (void) initUI {
	_addButton = [UIButton new];
	_addButton.backgroundColor = RGBOF(0xefefef);
	_addButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
	[_addButton setImage:[UIImage imageNamed:@"jia"] forState:(UIControlStateNormal)];
	[self addSubview:_addButton];
	
	_scrollView = [UIScrollView new];
//	_scrollView.backgroundColor = [UIColor blueColor];
	[self addSubview:_scrollView];
	
	[_addButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(70, 70));
		make.left.equalTo(self);
		make.centerY.equalTo(self);
	}];
	
	[_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self).offset(4);
		make.left.equalTo(self.addButton.mas_right).offset(8);
		make.right.equalTo(self).offset(-20);
		make.bottom.equalTo(self).offset(-4);
	}];
}

- (void)setDataSource:(NSMutableArray *)dataSource
{
	_dataSource = dataSource;
	
	[self prepareForUse];
	for (int i=0; i<dataSource.count; i++) {
		K18Album *model = dataSource[i];
		AlbumCell *cell;
		if (i<self.cells.count) {
			//reuse
			cell = self.cells[i];
			[self.scrollView addSubview:cell];
		}else{
			//new
			cell = [AlbumCell new];
			cell.tag = 99;
			[self.scrollView addSubview:cell];
			[self.cells addObject:cell];
		}
		//edit UI
		[cell.imageView sd_setImageWithURL:[NSURL URLWithString:model.cyImgUrl]];
	}
	
	NSMutableArray *visibleCells = [NSMutableArray array];
	for (AlbumCell *each in self.cells) {
		if (each.superview) {
			[visibleCells addObject:each];
		}
	}
	
	if (visibleCells.count) {
		for (int i=0; i<visibleCells.count; i++) {
			AlbumCell *view = visibleCells[i];
			view.frame = CGRectMake(8+i*(70+8), 0, 70, 70);
		}
		UIView *v = visibleCells.lastObject;
		self.scrollView.contentSize = CGSizeMake(CGRectGetMaxX(v.frame)+8, 0);
	}
}

- (void) prepareForUse {
	[self.cells enumerateObjectsUsingBlock:^(AlbumCell *obj, NSUInteger idx, BOOL * _Nonnull stop) {
		[obj removeFromSuperview];
//		[obj removeConstraints:obj.constraints];
	}];
}

@end

@implementation AlbumCell : UIView

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		
		_imageView = [UIImageView new];
		[self addSubview:_imageView];
		
		_cancelButton = [UIButton new];
		[self addSubview:_cancelButton];
		
		[_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
			make.left.equalTo(self);
			make.right.equalTo(self);
			make.top.equalTo(self);
			make.bottom.equalTo(self);
		}];
		
		[_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
			make.right.equalTo(self);
			make.top.equalTo(self);
			make.size.mas_equalTo(CGSizeMake(20,20));
		}];
	}
	return self;
}

@end

