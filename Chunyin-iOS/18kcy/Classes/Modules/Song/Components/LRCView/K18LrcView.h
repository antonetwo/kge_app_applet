//
//  K18LrcView.h
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LyricsInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18LrcView : UITableView

//@property(strong, nonatomic) NSMutableArray *musicLRCArray;

@property(strong, nonatomic) LyricsInfo *krcInfo;

@property (readonly) NSInteger currentLcrIndex;

@property(copy, nonatomic) void(^clickLrcView)(void);

- (void) dispose;

- (void)updateMusicLrcForRowWithCurrentTime:(NSTimeInterval)currentTime;

@end

NS_ASSUME_NONNULL_END
