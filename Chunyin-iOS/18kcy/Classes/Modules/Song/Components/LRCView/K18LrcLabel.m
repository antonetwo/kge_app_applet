//
//  K18LrcLabel.m
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LrcLabel.h"

@implementation K18LrcLabel

- (instancetype)init
{
	self = [super init];
	if (self) {

	}
	return self;
}

- (void)setProgress:(CGFloat)progress
{
	_progress = progress;
	//重绘
	[self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
	[super drawRect:rect];
	
	CGRect fillRect = CGRectMake(0, 0, self.bounds.size.width * _progress, self.bounds.size.height);
	
	[self.highlightColor set];
	
	UIRectFillUsingBlendMode(fillRect, kCGBlendModeSourceIn);
}

@end
