//
//  K18LrcModel.h
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18LrcModel : NSObject

//该段歌词对应的时间
@property (nonatomic,assign) NSTimeInterval time;
//歌词
@property (nonatomic,strong) NSString *title;


/**
 *
 将特点的歌词格式进行转换
 *
 **/
+ (id)musicLRCWithString:(NSString *)string;

/**
 *
 根据歌词的路径返回歌词模型数组
 *
 **/
+ (NSArray <K18LrcModel *>*)musicLRCModelsWithLRCFileName:(NSString *)name;
+ (NSArray <K18LrcModel *>*)musicLRCModelsWithLRCStrings:(NSString *)lrcString;

@end

NS_ASSUME_NONNULL_END
