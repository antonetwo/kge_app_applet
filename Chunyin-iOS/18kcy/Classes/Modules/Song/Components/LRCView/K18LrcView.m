//
//  K18LrcView.m
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LrcView.h"
#import "K18LrcCell.h"
#import "GLMusicPlayer.h"
#import "K18LrcModel.h"
#import "K18AudioEngine.h"
#import "K18Common.h"

@interface K18LrcView()<UITableViewDelegate, UITableViewDataSource>
//当前歌词所在行
@property (nonatomic,assign,readwrite) NSInteger currentLcrIndex;

@property (nonatomic,assign) BOOL isDrag;

@property(strong, nonatomic) UILabel *noTipLabel;
@end

@implementation K18LrcView

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self initUI];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initUI];
	}
	return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_noTipLabel sizeToFit];
    _noTipLabel.center = CGPointMake(self.width/2, self.height/2);
}

- (void) initUI {
	self.dataSource = self;
	self.delegate = self;
	self.separatorColor = [UIColor clearColor];
	self.tableFooterView = [[UITableView alloc] init];
	[self registerNib:[UINib nibWithNibName:@"K18LrcCell" bundle:nil] forCellReuseIdentifier:@"cell"];
	self.backgroundColor = [UIColor clearColor];
	
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickMe)];
	[self addGestureRecognizer:tap];
    
    _noTipLabel = [UILabel new];
    _noTipLabel.text = @"暂无歌词";
    [_noTipLabel sizeToFit];
    [self addSubview:_noTipLabel];
    _noTipLabel.hidden = YES;
    
}

- (void)clickMe {
	if (self.clickLrcView) {
		self.clickLrcView();
	}
}

- (void)dispose
{
	self.dataSource = nil;
	self.delegate = nil;
}

#pragma mark == UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.krcInfo.lineInfoTreeMap.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	K18LrcCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.backgroundColor = [UIColor clearColor];
	cell.contentView.backgroundColor = [UIColor clearColor];
	
	cell.lrcModel = self.krcInfo.lineInfoTreeMap[@(indexPath.row)];
	
	if (indexPath.row == self.currentLcrIndex) {
		[cell reloadCellForSelect:YES];
	}else{
		[cell reloadCellForSelect:NO];
	}
	
	return cell;
}

#pragma mark == UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark == UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	self.isDrag = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	self.isDrag = NO;
//	[self scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentLcrIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

#pragma mark -

/**
 逐字歌词更新逻辑
 */
- (void)updateMusicLrcForRowWithCurrentTime:(NSTimeInterval)currentTime //秒
{
    if (self.krcInfo.lineInfoTreeMap.count==0) {
        _noTipLabel.hidden = NO;
    }else{
        _noTipLabel.hidden = YES;
    }
    currentTime = currentTime * 1000;//转换成毫秒
    for (int i = 0; i < self.krcInfo.lineInfoTreeMap.count; i ++) {
        LyricsLineInfo *lineInfo = self.krcInfo.lineInfoTreeMap[@(i)];
        CGFloat offset = self.krcInfo.offset;
        NSInteger next = i + 1;
        
        LyricsLineInfo *nextLineInfo = self.krcInfo.lineInfoTreeMap[@(next)];
        
        if (self.currentLcrIndex != i && currentTime >= lineInfo.startTime+offset)
        {
            BOOL show = NO;
            if (nextLineInfo) {
                if (currentTime < nextLineInfo.startTime) {
                    show = YES;
                }
            }else{
                show = YES;
            }
            
            if (show) {
                NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
                NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:self.currentLcrIndex inSection:0];
                
                self.currentLcrIndex = i;
                
                K18LrcCell *currentCell = [self cellForRowAtIndexPath:currentIndexPath];
                K18LrcCell *previousCell = [self cellForRowAtIndexPath:previousIndexPath];
                
                //设置当前行的状态
                [currentCell reloadCellForSelect:YES];
                //取消上一行的选中状态
                [previousCell reloadCellForSelect:NO];
                
                
                if (!self.isDrag) {
                    [self scrollToRowAtIndexPath:currentIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                }
            }
        }
        
        if (self.currentLcrIndex == i) {
            NSArray *words = lineInfo.wordInfos;
            
            K18LrcCell *cell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            CGFloat totalTime = lineInfo.endTime - lineInfo.startTime;//当前行的持续时间
            CGFloat progressTime = currentTime - lineInfo.startTime;//当前行的偏移时间
            
            CGFloat oneWordProgress = 1.0/words.count;
            for (NSInteger j=words.count-1; j>=0; j--){//逐字解析
                LyricsWord *word = words[j];
                
                CGFloat curWordStartTime = word.startTime;//每个字持续时间
                curWordStartTime += offset;//krc整体的歌词偏移量，同步到显示速率里面。
                if (progressTime > curWordStartTime ) {//找到刚好大于这个字的时间
                    CGFloat minProgress = j*oneWordProgress + (progressTime-curWordStartTime)*1.0/(lineInfo.endTime-lineInfo.startTime);
                    cell.lrcLabel.progress = progressTime / totalTime;
                    cell.lrcLabel.progress = MAX(minProgress, cell.lrcLabel.progress);
                    break;
                }
                
            }
            
//            cell.lrcLabel.progress = progressTime / totalTime;
            
            
            
        }
    }
}

/*
//逐行更新歌词
- (void)updateMusicLrcForRowWithCurrentTime:(NSTimeInterval)currentTime
{
    for (int i = 0; i < self.musicLRCArray.count; i ++) {
        K18LrcModel *model = self.musicLRCArray[i];
        
        NSInteger next = i + 1;
        
        K18LrcModel *nextLrcModel = nil;
        if (next < self.musicLRCArray.count) {
            nextLrcModel = self.musicLRCArray[next];
        }
        
        if (self.currentLcrIndex != i && currentTime >= model.time)
        {
            BOOL show = NO;
            if (nextLrcModel) {
                if (currentTime < nextLrcModel.time) {
                    show = YES;
                }
            }else{
                show = YES;
            }
            
            if (show) {
                NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
                NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:self.currentLcrIndex inSection:0];
                
                self.currentLcrIndex = i;
                
                K18LrcCell *currentCell = [self cellForRowAtIndexPath:currentIndexPath];
                K18LrcCell *previousCell = [self cellForRowAtIndexPath:previousIndexPath];
                
                //设置当前行的状态
                [currentCell reloadCellForSelect:YES];
                //取消上一行的选中状态
                [previousCell reloadCellForSelect:NO];
                
                
                if (!self.isDrag) {
                    [self scrollToRowAtIndexPath:currentIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                }
            }
        }
        
        if (self.currentLcrIndex == i) {
            K18LrcCell *cell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            CGFloat totalTime = 0;
            if (nextLrcModel) {
                totalTime = nextLrcModel.time - model.time;
            }else{
                totalTime = [GLMusicPlayer defaultPlayer].duration.minute * 60 +  [GLMusicPlayer defaultPlayer].duration.second - model.time;
            }
            CGFloat progressTime = currentTime - model.time;
            cell.lrcLabel.progress = progressTime / totalTime;
        }
    }
}
 */

@end
