//
//  K18LrcModel.m
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LrcModel.h"

@implementation K18LrcModel

+(id)musicLRCWithString:(NSString *)string
{
	K18LrcModel *model = [[K18LrcModel alloc] init];
	NSArray *lrcLines =[string componentsSeparatedByString:@"]"];
	if (lrcLines.count == 2) {
		model.title = lrcLines[1];
		NSString *timeString = lrcLines[0];
		timeString = [timeString stringByReplacingOccurrencesOfString:@"[" withString:@""];
		timeString = [timeString stringByReplacingOccurrencesOfString:@"]" withString:@""];
		NSArray *times = [timeString componentsSeparatedByString:@":"];
		if (times.count == 2) {
			NSTimeInterval time = [times[0] integerValue]*60 + [times[1] floatValue];
			model.time = time;
		}
	}else if(lrcLines.count == 1){
		
	}
	
	return model;
}


+(NSArray <K18LrcModel *>*)musicLRCModelsWithLRCFileName:(NSString *)name
{
	NSString *lrcPath = [[NSBundle mainBundle] pathForResource:name ofType:nil];
	NSString *lrcString = [NSString stringWithContentsOfFile:lrcPath encoding:NSUTF8StringEncoding error:nil];
    return [self musicLRCModelsWithLRCStrings:lrcString];
}

+(NSArray <K18LrcModel *>*)musicLRCModelsWithLRCStrings:(NSString *)lrcString
{
	lrcString = [lrcString stringByReplacingOccurrencesOfString:@"[" withString:@"\n["];
	NSArray *lrcLines = [lrcString componentsSeparatedByString:@"\n"];
    NSMutableArray *lrcModels = [NSMutableArray array];
    for (NSString *lrcLineString in lrcLines) {
        
        if ([lrcLineString hasPrefix:@"[ti"] || [lrcLineString hasPrefix:@"[ar"] || [lrcLineString hasPrefix:@"[al"] || ![lrcLineString hasPrefix:@"["]) {
            continue;
        }
        K18LrcModel *lrcModel = [K18LrcModel musicLRCWithString:lrcLineString];
        [lrcModels addObject:lrcModel];
    }
    return lrcModels;
}

+ (NSArray *)matchString:(NSString *)string toRegexString:(NSString *)regexStr
{
	NSError *err = nil;
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexStr options:NSRegularExpressionCaseInsensitive error:&err];
	NSLog(@"%@",err);
	NSArray * matches = [regex matchesInString:string options:0 range:NSMakeRange(0, [string length])];
	
	//match: 所有匹配到的字符,根据() 包含级
	NSMutableArray *array = [NSMutableArray array];
	for (NSTextCheckingResult *match in matches) {
		for (int i = 0; i < [match numberOfRanges]; i++) {
			//以正则中的(),划分成不同的匹配部分  这里会莫名其妙的遍历两次，导致输出两个
			//相同的数据，我们可以像下面这样只取第一个值，也可以不进行遍历，直接取出来第一
			//个值。
			NSLog(@"~~~~~~~~~~~~[match numberOfRanges]:%lu",(unsigned long)[match numberOfRanges]);
			if (i == 0) {
				NSString *component = [string substringWithRange:[match rangeAtIndex:i]];
				
				[array addObject:component];
			}
			
		}
	}
	
	return array;
}

@end
