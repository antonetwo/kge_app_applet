//
//  K18LrcCell.h
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18LrcLabel.h"
#import "K18LrcModel.h"
#import "LyricsInfo.h"
NS_ASSUME_NONNULL_BEGIN


@interface K18LrcCell : UITableViewCell

@property (weak, nonatomic) IBOutlet K18LrcLabel *lrcLabel;

@property (nonatomic,strong) LyricsLineInfo *lrcModel;

//是否选中该行
- (void)reloadCellForSelect:(BOOL)select;

@end

NS_ASSUME_NONNULL_END
