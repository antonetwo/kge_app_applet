//
//  K18LrcLabel.h
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18LrcLabel : UILabel
//进度
@property (nonatomic,assign) CGFloat progress;

@property(strong, nonatomic) UIColor *highlightColor;

@end

NS_ASSUME_NONNULL_END
