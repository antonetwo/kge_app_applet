//
//  K18KRCReader.h
//  18kcy
//
//  Created by 唐 on 2019/7/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LyricsInfo.h"

NS_ASSUME_NONNULL_BEGIN

//歌曲名 字符串
#define LEGAL_SONGNAME_PREFIX @"[ti:"
//歌手名 字符串
#define LEGAL_SINGERNAME_PREFIX @"[ar:"
//时间补偿值 字符串
#define LEGAL_OFFSET_PREFIX @"[offset:"
//歌词上传者
#define LEGAL_BY_PREFIX @"[by:"
//
#define LEGAL_HASH_PREFIX @"[hash:"
//专辑
#define LEGAL_AL_PREFIX @"[al:"
//
#define LEGAL_SIGN_PREFIX @"[sign:"
//
#define LEGAL_QQ_PREFIX @"[qq:"
//
#define LEGAL_TOTAL_PREFIX @"[total:"
//
#define LEGAL_LANGUAGE_PREFIX @"[language:"
//
#define LEGAL_SONGNAME_PREFIX @"[ti:"


@interface K18KRCReader : NSObject

+ (LyricsInfo *) readInputText:(NSString *)lyricsTextStr;

@end

NS_ASSUME_NONNULL_END
