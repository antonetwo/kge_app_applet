//
//  LyricsInfo.m
//  18kcy
//
//  Created by 唐 on 2019/7/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "LyricsInfo.h"

@implementation LyricsLineInfo

- (void)setWords:(NSMutableArray *)words
{
//	if (lyricsWords == null) return;
//	String[] tempArray = new String[lyricsWords.length];
//	for (int i = 0; i < lyricsWords.length; i++) {
//		String temp = lyricsWords[i];
//		if (TextUtils.isEmpty(temp)) {
//			tempArray[i] = "";
//		} else {
//			tempArray[i] = temp.replaceAll("\r|\n", "");
//		}
//	}
//	this.mLyricsWords = tempArray;
	NSMutableArray *targets = [NSMutableArray array];
	for (int i=0; i < words.count; i++) {
		NSString *tmp = words[i];
		if (!tmp) {
			[targets addObject:@""];
		}else{
			NSString *newStr = [tmp stringByReplacingOccurrencesOfString:@"\r|\n" withString:@"" options:(NSRegularExpressionSearch) range:NSMakeRange(0, tmp.length)];
			[targets addObject:newStr];
		}
	}
    _words = targets;
}

- (void)setLineLyric:(NSString *)lineLyric
{
	if (lineLyric.length) {
		_lineLyric = [lineLyric stringByReplacingOccurrencesOfString:@"\r|\n" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, lineLyric.length)];
	}
}

@end

@implementation LyricsInfo

- (NSString *)title
{
	return self.tags[TAG_TITLE];
}

- (NSString *)Artist
{
	return self.tags[TAG_ARTIST];
}

- (CGFloat)offset
{
	return [self.tags[TAG_OFFSET] doubleValue];
}

- (NSString *)by
{
	return self.tags[TAG_BY];
}

- (NSString *)total
{
	return self.tags[TAG_TOTAL];
}

@end

@implementation LyricsWord
@end







