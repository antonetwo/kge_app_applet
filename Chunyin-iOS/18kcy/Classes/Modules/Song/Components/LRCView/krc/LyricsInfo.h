//
//  LyricsInfo.h
//  18kcy
//
//  Created by 唐 on 2019/7/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class LyricsWord;

@interface LyricsLineInfo : NSObject

@property(nonatomic) int startTime;
@property(nonatomic) int endTime;
@property(strong, nonatomic) NSString *lineLyric;//该行歌词
@property(strong, nonatomic) NSMutableArray *words;//每个字
//@property(strong, nonatomic) NSMutableArray *disIntervals; //存放每个歌词的持续时间

@property(strong, nonatomic) NSMutableArray *wordInfos;//

@property(strong, nonatomic) NSMutableArray *splitDynamicLrcLineInfos;//分割歌词行歌词

@property(strong, nonatomic) NSMutableArray *tags;// 歌词标签
@end

@interface LyricsInfo : NSObject

@property(strong, nonatomic) NSMutableDictionary <NSNumber*,LyricsLineInfo*>*lineInfoTreeMap;

@property(strong, nonatomic) NSMutableDictionary* tags;

@property(readonly, nonatomic) NSString *title;//歌曲名称
@property(readonly, nonatomic) NSString *Artist;//歌手
@property(readonly, nonatomic) CGFloat offset;//时间补偿值
@property(readonly, nonatomic) NSString *by;//上传者
@property(readonly, nonatomic) NSString *total;//歌词总时长

@end


/**
 每只字
 */
@interface LyricsWord : NSObject
@property(assign, nonatomic) int startTime;//此字开始的时刻距此行开始时刻的毫秒数
@property(assign, nonatomic) int endTime;//此字开始的时刻距此行结束时刻的毫秒数
@property(strong, nonatomic) NSString *word;
@end

/**
 * 歌曲名称
 */
 static const NSString *TAG_TITLE = @"lyrics.tag.title";
/**
 * 歌手
 */
 static const NSString *TAG_ARTIST = @"lyrics.tag.artist";
/**
 * 时间补偿值
 */
 static const NSString *TAG_OFFSET = @"lyrics.tag.offset";
/**
 * 上传者
 */
 static const NSString *TAG_BY = @"lyrics.tag.by";
/**
 * 歌词总时长
 */
static const NSString *TAG_TOTAL = @"lyrics.tag.total";


