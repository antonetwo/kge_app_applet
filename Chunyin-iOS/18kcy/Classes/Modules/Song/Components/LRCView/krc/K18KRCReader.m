//
//  K18KRCReader.m
//  18kcy
//
//  Created by 唐 on 2019/7/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18KRCReader.h"
#import <RegExCategories/RegExCategories.h>
@implementation K18KRCReader

+ (LyricsInfo *)readInputText:(NSString *)lyricsTextStr
{
//	LyricsInfo lyricsIfno = new LyricsInfo();
//	String lyricsTextStr = StringCompressUtils.decompress(zip_byte,
//														  getDefaultCharset());
//	String[] lyricsTexts = lyricsTextStr.split("\n");
//	TreeMap<Integer, LyricsLineInfo> lyricsLineInfos = new TreeMap<Integer, LyricsLineInfo>();
//	Map<String, Object> lyricsTags = new HashMap<String, Object>();
//	int index = 0;
//
//	for (int i = 0; i < lyricsTexts.length; i++) {
//		String lineInfo = lyricsTexts[i];
//
//		// 行读取，并解析每行歌词的内容
//		LyricsLineInfo lyricsLineInfo = parserLineInfos(lyricsTags,
//														lineInfo, lyricsIfno);
//		if (lyricsLineInfo != null) {
//			lyricsLineInfos.put(index, lyricsLineInfo);
//			index++;
//		}
//	}
//	in.close();
//	in = null;
//	// 设置歌词的标签类
//	lyricsIfno.setLyricsTags(lyricsTags);
//	//
//	lyricsIfno.setLyricsLineInfoTreeMap(lyricsLineInfos);
	
	LyricsInfo *info = [LyricsInfo new];
	
	NSArray *lyricsTextStrs = [lyricsTextStr componentsSeparatedByString:@"\n"];
	NSMutableDictionary *lineInfos = [NSMutableDictionary dictionary];
	NSMutableDictionary *tags = [NSMutableDictionary dictionary];
	int index = 0;
	
	for (int i=0; i<lyricsTextStrs.count; i++) {
		NSString *lineInfoStr = lyricsTextStrs[i];
		LyricsLineInfo *lineInfo = [self parserLineInfos:tags lineInfoStr:lineInfoStr info:info];
		
		if (lineInfo) {
			lineInfos[@(index)] = lineInfo;
			index++;
		}
	}
	
	info.tags = tags;
	info.lineInfoTreeMap = lineInfos;
	
	return info;
}

+ (LyricsLineInfo *)parserLineInfos:(NSMutableDictionary *)tags lineInfoStr:(NSString *)lineInfoStr info:(LyricsInfo *) info {
	LyricsLineInfo *lineInfo;
	
	if ([lineInfoStr hasPrefix:LEGAL_SONGNAME_PREFIX]) {
		NSUInteger start = LEGAL_SONGNAME_PREFIX.length;
		NSUInteger end = [lineInfoStr rangeOfString:@"]"].location-1;
		tags[TAG_TITLE] = [lineInfoStr substringWithRange:NSMakeRange(start, end-start+1)];
	}else if([lineInfoStr hasPrefix:LEGAL_SINGERNAME_PREFIX]){
		NSUInteger start = LEGAL_SONGNAME_PREFIX.length;
		NSUInteger end = [lineInfoStr rangeOfString:@"]"].location-1;
		tags[TAG_ARTIST] = [lineInfoStr substringWithRange:NSMakeRange(start, end-start+1)];
	} else if ([lineInfoStr hasPrefix:LEGAL_OFFSET_PREFIX]) {
		NSUInteger start = LEGAL_OFFSET_PREFIX.length;
		NSUInteger end = [lineInfoStr rangeOfString:@"]"].location-1;
		tags[TAG_OFFSET] = [lineInfoStr substringWithRange:NSMakeRange(start, end-start+1)];
	}else if([lineInfoStr hasPrefix:LEGAL_BY_PREFIX] ||
			 [lineInfoStr hasPrefix:LEGAL_HASH_PREFIX] ||
			 [lineInfoStr hasPrefix:LEGAL_SIGN_PREFIX] ||
			 [lineInfoStr hasPrefix:LEGAL_QQ_PREFIX] ||
			 [lineInfoStr hasPrefix:LEGAL_TOTAL_PREFIX] ||
			 [lineInfoStr hasPrefix:LEGAL_AL_PREFIX] ){
		NSUInteger start = [lineInfoStr rangeOfString:@"["].location + 1;
		NSUInteger end = [lineInfoStr rangeOfString:@"]" options:(NSBackwardsSearch)].location-1;
		NSArray *temp = [[lineInfoStr substringWithRange:NSMakeRange(start, end-start+1)] componentsSeparatedByString:@":"];
        NSString *key = [@"lyrics.tag." stringByAppendingString:temp[0]];
		tags[key] = temp.count==1? @"" : temp[1];
	}else if([lineInfoStr hasPrefix:LEGAL_LANGUAGE_PREFIX]){
//        NSUInteger start = [lineInfoStr rangeOfString:@"["].location + 1;
//        NSUInteger end = [lineInfoStr rangeOfString:@"]" options:(NSBackwardsSearch)].location-1;
//        NSArray *temp = [[lineInfoStr substringWithRange:NSMakeRange(start, end)] componentsSeparatedByString:@":"];
		//翻译歌词不用解析。
		
	}else {
		NSString *regrex = @"\\[\\d+,\\d+\\]";
        
		if ([lineInfoStr isMatch:RX(regrex)]) {//匹配行歌词
			
			lineInfo = [LyricsLineInfo new];
			
			// [此行开始时刻距0时刻的毫秒数,此行持续的毫秒数]<0,此字持续的毫秒数,0>歌<此字开始的时刻距此行开始时刻的毫秒数,此字持续的毫秒数,0>词<此字开始的时刻距此行开始时刻的毫秒数,此字持续的毫秒数,0>正<此字开始的时刻距此行开始时刻的毫秒数,此字持续的毫秒数,0>文
            NSString *curLineParam = [lineInfoStr firstMatch:RX(regrex)];
            NSString *matchStr = curLineParam;
			// 获取行的出现时间和结束时间
			NSUInteger start = [matchStr rangeOfString:@"["].location+1;
			NSUInteger end = [matchStr rangeOfString:@"]" options:(NSBackwardsSearch)].location-1;
			NSString *tmp = [matchStr substringWithRange:NSMakeRange(start, end-start+1)];
			
			lineInfo.startTime = [[tmp componentsSeparatedByString:@","][0] intValue];
			lineInfo.endTime = lineInfo.startTime + [[tmp componentsSeparatedByString:@","][1] intValue];
			
			// 获取歌词信息
			NSString *lineContent = [lineInfoStr substringWithRange:NSMakeRange(matchStr.length, lineInfoStr.length-matchStr.length)];
			
			// 歌词匹配的正则表达式
			NSString *s_Regex = @"\\<\\d+,\\d+,\\d+\\>";
            NSArray *s_matchs = [lineInfoStr matches:RX(s_Regex)];//  <xxx,xxx,0> x
			
			if (s_matchs.count==0) {
				return nil;
			}
			
			// 歌词分隔
			NSArray *lineWordStrs = [lineContent split:RX(s_Regex)];
			NSMutableArray *words = [self getLyricsWords:lineWordStrs];
			lineInfo.words = words;
			
            NSMutableArray *wordInfos = [NSMutableArray array];
            
			// 获取每个歌词的时间
			NSMutableArray *wordsDisInterval = [NSMutableArray arrayWithCapacity:lineWordStrs.count];
			int s_index = 0;
			for (NSString *eachTimeStr in s_matchs) {
				//验证
				if (s_index >= words.count) {
					@throw [NSException exceptionWithName:@"" reason:@"字标签个数与字时间标签个数不相符" userInfo:nil];
				}
				
                LyricsWord *wordInfo = [LyricsWord new];
				NSUInteger start = [eachTimeStr rangeOfString:@"<"].location+1;
				NSUInteger end = [eachTimeStr rangeOfString:@">"].location-1;
				NSString *each_temp = [eachTimeStr substringWithRange:NSMakeRange(start, end-start+1)];
				NSArray *each_times = [each_temp componentsSeparatedByString:@","];
//                [wordsDisInterval addObject:each_times[1]];
                
                wordInfo.word = words[s_index];
                wordInfo.startTime = [each_times[0] intValue];
                wordInfo.endTime = wordInfo.startTime + [each_times[1] intValue];
                [wordInfos addObject:wordInfo];
				s_index++;
			}
            
            lineInfo.wordInfos = wordInfos;
//            lineInfo.disIntervals = wordsDisInterval;
			
			// 获取当行歌词
            NSString *lineLyric = [lineContent replace:RX(s_Regex) with:@""];
			lineInfo.lineLyric = lineLyric;
			
		}
	}
	
	return lineInfo;
}

/**
 @param lineWordStrs 数组，元素字符示例:   <xxx,xxx,0> x
 @return
 */
+ (NSMutableArray *) getLyricsWords:(NSArray *)lineWordStrs {
	if (lineWordStrs.count<2) {
		return @[].mutableCopy;
	}
	NSMutableArray *temp = [NSMutableArray array];
	for (int i=1; i < lineWordStrs.count; i++) {//
		[temp addObject:lineWordStrs[i]];
	}
	
	return temp;
}

@end
