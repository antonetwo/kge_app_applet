//
//  K18LrcCell.m
//  18kcy
//
//  Created by 唐 on 2019/7/2.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LrcCell.h"
#import "K18LrcLabel.h"
#import "K18LrcModel.h"
#import "K18Common.h"

@implementation K18LrcCell

- (void)setLrcModel:(LyricsLineInfo*)lrcModel//(K18LrcModel *)lrcModel
{
	_lrcModel = lrcModel;
	_lrcLabel.text = lrcModel.lineLyric;
}

- (void)reloadCellForSelect:(BOOL)select
{
	if (select) {
		_lrcLabel.font = [UIFont systemFontOfSize:20];
	}else{
		_lrcLabel.font = [UIFont systemFontOfSize:17];
		_lrcLabel.progress = 0;
	}
}

- (void)awakeFromNib {
	[super awakeFromNib];
	// Initialization code
	self.lrcLabel.highlightColor = k18_TopicC;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

@end
