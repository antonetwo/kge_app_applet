//
//  K18Slider.m
//  18kcy
//
//  Created by beuady on 2019/6/23.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18Slider.h"
#import "K18Common.h"

@implementation K18Slider

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void) initUI {
//    0xDDDDDD
    self.maximumTrackTintColor = RGBOF(0xDDDDDD);
    [self setThumbImage:[[UIImage imageNamed:@"luzhi jindu"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
    self.minimumTrackTintColor = k18_TopicC;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[super touchesEnded:touches withEvent:event];
//	self.isTouch = NO;
	
	CGFloat kThumbHeight = self.currentThumbImage.size.width;
	CGFloat CURRENT_FRAME_WIDTH = self.width;
	CGPoint touch = [[touches anyObject]locationInView:self];
	CGFloat x = touch.x;
	if (touch.x <= kThumbHeight/2.0) {
		x = kThumbHeight/2.0;
	}else if (touch.x >= (CURRENT_FRAME_WIDTH - kThumbHeight/2.0)){
		x = CURRENT_FRAME_WIDTH - kThumbHeight/2.0;
	}
	
	self.value = 1.0*(x-kThumbHeight/2.0)/ (CURRENT_FRAME_WIDTH - kThumbHeight);
	
	for (id target in [self allTargets]) {
		NSArray *actions = [self actionsForTarget:target forControlEvent:UIControlEventValueChanged];
		for (NSString *action in actions) {
			[self sendAction:NSSelectorFromString(action) to:target forEvent:event];
		}
	}
}

@end
