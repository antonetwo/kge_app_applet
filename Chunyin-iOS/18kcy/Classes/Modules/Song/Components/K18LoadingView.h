//
//  K18LoadingView.h
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 歌曲加载过渡View
 */
@interface K18LoadingView : UIView
@property(strong, nonatomic) UIProgressView *progressView;
@property(strong, nonatomic) UILabel *progressLabel;
@property(strong, nonatomic) UILabel *tipLabel;

@property(assign, nonatomic) CGFloat progress;//0-1
@end

NS_ASSUME_NONNULL_END
