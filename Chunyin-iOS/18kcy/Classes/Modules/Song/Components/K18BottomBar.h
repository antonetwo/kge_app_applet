//
//  K18BottomBar.h
//  18kcy
//
//  Created by 唐 on 2019/7/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IconButton.h"

@class K18BottomBar;

@protocol K18BottomBarDelegate <NSObject>
@required
- (void) tabBar:(K18BottomBar*)tabBar didSelectButton:(UIButton *)button;

@end

NS_ASSUME_NONNULL_BEGIN

@interface K18BottomBar : UIView

@property(strong, nonatomic) NSMutableArray *buttons;//@"送花",@"分享",@"评论",@"收藏",@"K歌"
@property(weak, nonatomic) id<K18BottomBarDelegate> delegate;


- (IconButton *) buttonForIndex:(NSInteger) index;



@end

NS_ASSUME_NONNULL_END
