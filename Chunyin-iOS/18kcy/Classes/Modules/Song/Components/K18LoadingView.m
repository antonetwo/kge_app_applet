//
//  K18LoadingView.m
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LoadingView.h"
#import "K18Common.h"
@interface K18LoadingView()
//@property(strong, nonatomic) UIProgressView *progressView;
//@property(strong, nonatomic) UILabel *progressLabel;
//@property(strong, nonatomic) UILabel *tipLabel;
@end

@implementation K18LoadingView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void) initUI {
    _progressView = [[UIProgressView alloc]initWithProgressViewStyle:(UIProgressViewStyleDefault)];
    [self addSubview:_progressView];
	_progressView.tintColor = k18_TopicC;
	_progressView.trackTintColor = RGBOF(0xDDDDDD);
	_progressView.layer.masksToBounds = YES;
	_progressView.layer.cornerRadius = 13/2.0;
    
    _progressLabel = [UILabel new];
    _progressLabel.textAlignment = NSTextAlignmentCenter;
    _progressLabel.text = @"歌曲加载中 0%";
    _progressLabel.textColor = RGBOF(0x323232);
    _progressLabel.font = [UIFont systemFontOfSize:18];
    [self addSubview:_progressLabel];
    
    _tipLabel = [UILabel new];
    _tipLabel.text = @"点击歌词区域可暂停录制";
    _tipLabel.textAlignment = NSTextAlignmentCenter;
    _tipLabel.textColor = RGBOF(0x323232);
    _tipLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:_tipLabel];
    
    [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(301, 13));
        make.center.equalTo(self);
    }];
    
    [_progressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(180, 20));
        make.centerX.equalTo(self);
        make.top.equalTo(self.progressView.mas_bottom).offset(41);
    }];
    
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(180, 20));
        make.centerX.equalTo(self);
        make.top.equalTo(self.progressLabel.mas_bottom).offset(25);
    }];
}

- (void)setProgress:(CGFloat)progress
{
	_progress = progress;
	_progressLabel.text = [NSString stringWithFormat:@"歌曲加载中 %.0f％", progress * 100];
	_progressView.progress = progress;
}

@end
