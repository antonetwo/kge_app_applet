//
//  K18ScoreView.m
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ScoreView.h"
#import "K18Common.h"

@interface K18ScoreView()
@end

@implementation K18ScoreView
/*
 @property(strong, nonatomic) UIImageView *scoreIcon;
 @property(strong, nonatomic) UIView *scoreBar;
 @property(strong, nonatomic) UIView *scoreBarMask;
 @property(strong, nonatomic) UILabel *scoreLabel;
 */
- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
//		self.backgroundColor = [UIColor blackColor];
		CGFloat H = frame.size.height;
		CGFloat W = frame.size.width;
		self.scoreIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, H, H)];
		self.scoreIcon.image = [UIImage imageNamed:k18_DefaultIcon];
		self.scoreIcon.layer.cornerRadius = H/2.0;
		self.scoreIcon.layer.masksToBounds = YES;
		
		CGFloat h = 24;
		self.scoreBar = [[UIView alloc]initWithFrame:CGRectMake(0, (H-h)/2, W, h)];
		self.scoreBar.layer.cornerRadius = h/2.0;
		self.scoreBar.backgroundColor = k18_TopicC;
		self.scoreBarBg = [[UIView alloc]initWithFrame:self.scoreBar.frame];
		self.scoreBarBg.layer.cornerRadius = h/2.0;
		_scoreBarBg.backgroundColor = RGBOF(0xECECEC);
		self.scoreBarMask = [[UIView alloc]initWithFrame:self.scoreBar.bounds];
		self.scoreBarMask.backgroundColor = [UIColor whiteColor];
		
		[self addSubview:self.scoreBarBg];
		[self addSubview:self.scoreBar];
		[self.scoreBar addSubview:self.scoreBarMask];
		self.scoreBar.maskView = self.scoreBarMask;
		
		self.scoreLabel = [[UILabel alloc] init];
		_scoreLabel.font = [UIFont systemFontOfSize:14];
//		_scoreLabel.textColor = [UIColor whiteColor];
		_scoreLabel.textAlignment = NSTextAlignmentCenter;
//        _scoreLabel.textColor = RGBOF(0x323232);
        _scoreLabel.textColor = [UIColor whiteColor];
		[self addSubview:self.scoreLabel];
        
		
		
		[self addSubview:_scoreIcon];
		
		//
		[self setRate:0];
		self.score = 0;
	}
	return self;
}

- (void) setRate:(CGFloat) rate {
	_rate = rate;
	self.scoreBarMask.width = self.width*(0.14 + 0.86 * rate);
//    self.scoreLabel.right = self.scoreBarMask.right;
}

- (void)setScore:(NSInteger)score
{
	_score = score;
	self.scoreLabel.text = [NSString stringWithFormat:@"%d分",(int)score];
	[_scoreLabel sizeToFit];
	
	self.scoreLabel.right = self.scoreBarMask.right;
	self.scoreLabel.center = CGPointMake(0, self.scoreBar.center.y);
    self.scoreLabel.left = 56;
//    NSLog(@"%@",NSStringFromCGRect(self.scoreLabel.frame));
}

@end
