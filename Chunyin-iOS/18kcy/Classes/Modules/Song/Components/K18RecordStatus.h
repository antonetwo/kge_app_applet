//
//  K18RecordStatus.h
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/**
 正在录制的状态栏
 */
@interface K18RecordStatus : UIView

/**
 原唱/伴唱切换
 */
@property (weak, nonatomic) IBOutlet UIButton *toggleBtn;
@property (weak, nonatomic) IBOutlet UILabel *timesLabel;
@property (weak, nonatomic) IBOutlet UILabel *toggleLabel;

/**
 录制状态图
 */
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

- (void) toggleState:(BOOL) play;
@property(nonatomic) NSUInteger totalDuration;
@property(nonatomic) NSUInteger curTime;

@end

NS_ASSUME_NONNULL_END
