//
//  K18ModeView.m
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ReverbView.h"
#import "IconButton.h"
#import "K18Common.h"

@implementation K18ReverbView

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initUI];
	}
	return self;
}

- (void) initUI {
//	录音棚、KTV、磁性、空灵、悠远、老唱片、3D迷幻
    
    NSArray *list = @[@{@"name":@"KTV",
                        @"img1":@"KTV xuanze",
                        @"img2":@"KTV",
                        },
                      @{@"name":@"磁性",
                        @"img1":@"cixing-xuanze",
                        @"img2":@"cixing",
                        },
                      @{@"name":@"空灵",
                        @"img1":@"kongling-xuanze",
                        @"img2":@"kongling",
                        },
                      @{@"name":@"悠远",
                        @"img1":@"youyuan-xuanze",
                        @"img2":@"youyuan",
                        },
                      @{@"name":@"录音棚",
                        @"img1":@"luyingpeng-xuanze",
                        @"img2":@"luyingpeng",
                        },
                      @{@"name":@"老唱片",
                        @"img1":@"laochangpian-xuanze",
                        @"img2":@"laochangpian",
                        },
                      @{@"name":@"3D迷幻",
                        @"img1":@"3Dmihuan-xuanze",
                        @"img2":@"3Dmihuan",
                        }
                      ];
    
//    NSArray *list = @[@"KTV",@"录音棚",@"磁性",@"空灵",@"悠远",@"老唱片",@"3D迷幻"];
//    NSArray *list2 = @[@"KTV xuanze",@"luyingpeng-xuanze",@"cixing-xuanze",@"kongling-xuanze",@"youyuan-xuanze",@"laochangpian-xuanze",@"3Dmihuan-xuanze"];
//    NSArray *list3 = @[@"KTV",@"luyingpeng",@"cixing",@"kongling",@"youyuan",@"laochangpian",@"3Dmihuan"];

	//录音棚、KTV、磁性、空灵、悠远、老唱片、3D迷幻
	self.buttons = [NSMutableArray array];
	for (int i=0; i<list.count; i++) {
        NSDictionary *data = list[i];
		CGRect frame = CGRectMake((50+20)*i, 0, 50, self.frame.size.height);
		IconButton *button = [[IconButton alloc]initWithFrame:frame];
		[self addSubview:button];
		[button setTitle:data[@"name"] forState:(UIControlStateNormal)];
		[button setImage:[UIImage imageNamed:data[@"img2"]] forState:(UIControlStateNormal)];
		[button setImage:[UIImage imageNamed:data[@"img1"]] forState:(UIControlStateSelected)];
		[button setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
		[button setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateDisabled)];
		button.titleLabel.font = [UIFont systemFontOfSize:15];
		button.tag = i;
		[button addTarget:self action:@selector(clickA:) forControlEvents:(UIControlEventTouchUpInside)];
		[self.buttons addObject:button];
		//
		if (i>=4) {
			button.enabled = NO;
		}
		
		if (i == list.count-1) {
			self.contentSize = CGSizeMake(CGRectGetMaxX(button.frame), 0);
		}
	}
}

- (void) clickA:(UIButton *)button {
	BOOL last = button.selected;
	for (UIButton *each in self.buttons) {
		each.selected = NO;
	}
	button.selected = !last;
	NSInteger index = -1;
	if (button.selected) {
		index = button.tag;
	}
	if (self.clickBlock) {
		self.clickBlock(index);
	}
}


@end
