//
//  K18SongListView.h
//  18kcy
//
//  Created by 唐 on 2019/6/10.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPageListView.h"
#import "K18Song.h"
#import "K18Common.h"

NS_ASSUME_NONNULL_BEGIN

//0，点唱榜 1，飙升榜 2，新歌榜
typedef NS_ENUM(NSUInteger, RankType) {
	RankType_1 = 0,
	RankType_2,
	RankType_3,
};

/**
 k歌首页的歌曲列表
 */
@interface K18SongListView : UIView< JXPageListViewListDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <K18Song*> *dataSource;
@property (nonatomic, assign) BOOL isNeedHeader;
@property (nonatomic, assign) BOOL isFirstLoaded;

@property(nonatomic) RankType rankType;

@property(nonatomic,weak) UIViewController *rootVC;
@end

NS_ASSUME_NONNULL_END
