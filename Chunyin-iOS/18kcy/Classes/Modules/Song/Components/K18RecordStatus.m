//
//  K18RecordStatus.m
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecordStatus.h"
#import "K18Common.h"

@interface K18RecordStatus()
@property(strong, nonatomic) CABasicAnimation *animation;
@end

@implementation K18RecordStatus
//@property (weak, nonatomic) IBOutlet UIButton *toggleBtn;
//@property (weak, nonatomic) IBOutlet UILabel *timesLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
//@property (weak, nonatomic) IBOutlet UILabel *toggleLabel;
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.toggleBtn k18_setStyle];
    
    self.statusImage.image = [self.statusImage.image iTintColor:k18_TopicC];
	
	CABasicAnimation *moveAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	moveAnimation.duration = 1.2;//动画时间
	//动画起始值和终止值的设置
	moveAnimation.fromValue = @(1);
	moveAnimation.toValue = @(0.1);
	//一个时间函数，表示它以怎么样的时间运行
	moveAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	moveAnimation.repeatCount = MAXFLOAT;
	moveAnimation.autoreverses = YES;
	moveAnimation.removedOnCompletion = NO;
	moveAnimation.fillMode = kCAFillModeForwards;
	self.animation = moveAnimation;
}

- (void)toggleState:(BOOL)play
{
	if (play) {
		
		self.statusImage.alpha = 1;
		self.toggleLabel.text = @"录制";
		
		[self.statusImage.layer addAnimation:self.animation forKey:@"opacityKey"];
		
		
	}else{
		
		self.statusImage.alpha = 0;
		self.toggleLabel.text = @"暂停";
		
		[self.statusImage.layer removeAnimationForKey:@"opacityKey"];
	}
}

- (void)setCurTime:(NSUInteger)curTime
{
	_curTime = curTime;
	[self updateUI];
}

- (void)setTotalDuration:(NSUInteger)totalDuration
{
	_totalDuration = totalDuration;
	[self updateUI];
}

- (void) updateUI {
	int curSec = self.curTime%60;
	int curMin = (int)self.curTime/60;
	int tSec = self.totalDuration%60;
	int tMin = (int)self.totalDuration/60;

	self.timesLabel.text = [NSString stringWithFormat:@"%02d:%02d/%02d:%02d",curMin,curSec,tMin,tSec];
}

@end
