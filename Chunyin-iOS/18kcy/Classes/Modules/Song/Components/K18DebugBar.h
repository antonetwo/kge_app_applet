//
//  K18DebugBar.h
//  18kcy
//
//  Created by beuady on 2019/6/16.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 录制页面的调试面板
 */
@interface K18DebugBar : UIView
@property(weak, nonatomic) IBOutlet UIButton *plusBtn;
@property(weak, nonatomic) IBOutlet UIButton *subtractBtn;
@property (weak, nonatomic) IBOutlet UILabel *label;

@property(nonatomic) NSInteger num;
@property(nonatomic, copy) void(^valueChanged)(NSInteger num);
@end

NS_ASSUME_NONNULL_END
