//
//  K18BottomBar.m
//  18kcy
//
//  Created by 唐 on 2019/7/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BottomBar.h"
#import "K18Common.h"

@interface K18BottomBar()

@end

@implementation K18BottomBar

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initUI];
	}
	return self;
}

- (void)initUI {
	_buttons = [NSMutableArray array];

	NSArray *btn1 = @[@"送花",@"分享",@"评论",@"收藏",@"K歌"];
	NSArray *btn2 = @[@"songhua",@"fenxiang",@"pinglun",@"shoucang",@"K ge"];
	NSArray *btn3 = @[@"songhua -xaunze",@"fenxiang-xuaze",@"pinglun -xuanze",@"shoucang -xuanze",@"K ge-uanze"];
	
	CGFloat w = 50;
	CGFloat gap = 20;
	CGFloat equalGap = ([UIScreen mainScreen].bounds.size.width - gap*2 - w*btn1.count)/4;
	
	for (int i=0; i<btn1.count; i++) {
		IconButton *button = [[IconButton alloc]initWithFrame:CGRectMake(20+i*(50+equalGap), 2, 50, 50)];
		UIImage *hImage = [[UIImage imageNamed:btn3[i]] iTintColor:k18_TopicC];
		[button setTitle:btn1[i] forState:(UIControlStateNormal)];
		[button setImage:hImage forState:(UIControlStateHighlighted)];
		[button setImage:hImage forState:(UIControlStateSelected)];
		[button setImage:[UIImage imageNamed:btn2[i]] forState:(UIControlStateNormal)];
		[button setTitleColor:RGBOF(0xA0A0A0) forState:(UIControlStateNormal)];
		[button setTitleColor:k18_TopicC forState:(UIControlStateSelected)];
		[button setTitleColor:k18_TopicC forState:(UIControlStateHighlighted)];
		button.titleLabel.font = [UIFont systemFontOfSize:12];
		button.tag = i;
		[_buttons addObject:button];
		[self addSubview:button];
		[button addTarget:self action:@selector(clickIndex:) forControlEvents:(UIControlEventTouchUpInside)];
	}
	
	
}

- (void) clickIndex:(UIButton *)sender {
	[self.delegate tabBar:self didSelectButton:sender];
}

- (IconButton *)buttonForIndex:(NSInteger)index
{
	if (index>=0 && index < self.buttons.count) {
		return self.buttons[index];
	}
	return nil;
}

@end
