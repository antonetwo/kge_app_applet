//
//  K18Cell1.h
//  18kcy
//
//  Created by 唐 on 2019/6/11.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/**
 K歌页面顶部菜单
 */
@interface K18Cell1 : UITableViewCell
@property(nonatomic, copy) void(^clickMenu) (NSInteger index, NSString *title);
@end

NS_ASSUME_NONNULL_END
