//
//  K18DetailView.h
//  18kcy
//
//  Created by 唐 on 2019/6/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Song.h"
#import "K18Slider.h"
NS_ASSUME_NONNULL_BEGIN

/**
 K歌详情
 */
@interface K18DetailView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *toggleConcern;
@property (weak, nonatomic) IBOutlet UIView *carouselContrainer;
@property (weak, nonatomic) IBOutlet K18Slider *slider;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *audioL;
@property (weak, nonatomic) IBOutlet UIButton *shareL;
@property (weak, nonatomic) IBOutlet UIButton *flowerL;
@property (weak, nonatomic) IBOutlet UILabel *createTimeL;
@property (weak, nonatomic) IBOutlet UIButton *togglePlayBtn;

@property(strong, nonatomic) K18SongDetail *model;

- (void) startPlayAudio;
- (void) stopPlayAudio;

@end

NS_ASSUME_NONNULL_END
