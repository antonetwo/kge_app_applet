//
//  K18SongListController.m
//  18kcy
//
//  Created by 唐 on 2019/6/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongListController.h"
#import <MJRefresh/MJRefresh.h>
#import "K18SongCell.h"
#import "K18SongService.h"

@interface K18SongListController ()<UITableViewDataSource, UITableViewDelegate,K18SongCellDelegate>

@property(nonatomic) NSInteger ctype;//0歌手的歌曲列表, 1其他分类歌曲列表

@property(nonatomic) NSInteger currentPage;//当前页数
@property(nonatomic) BOOL isPageMax;
@end

@implementation K18SongListController

- (void)dealloc
{
	NSLog(@"%s",__func__);
}

- (instancetype)initWithType:(int)ctype
{
	self = [super init];
	if (self) {
		self.ctype = ctype;
		self.currentPage = 0;
	}
	return self;
}

- (void)setDataSource:(NSMutableArray<K18Song *> *)dataSource
{
	_dataSource = dataSource;
	[self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];

	_tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
	self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
	self.tableView.backgroundColor = [UIColor whiteColor];
	self.tableView.tableFooterView = [UIView new];
	self.tableView.dataSource = self;
	self.tableView.delegate = self;
	[self.tableView registerNib:[UINib nibWithNibName:@"K18SongCell" bundle:nil] forCellReuseIdentifier:@"cell"];
	[self.view addSubview:self.tableView];
	
	__weak typeof(self)weakSelf = self;
	self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
		__strong typeof(weakSelf)__sself = weakSelf;
		if(!__sself) return;
		
		__sself.currentPage = 0;
		[__sself.dataSource removeAllObjects];
		__sself.isPageMax = NO;

		[__sself requestData];
		
	}];
	
	[self requestData];
}

- (void) requestData {
	if (self.ctype==0) {//歌手的歌曲列表
		[self requestSingerListDataWithNumber:self.currentPage com:^{
			
			[self updateDataUI];
		}];
	}else{
		[self requestDataWithNumber:self.currentPage com:^{
			
			[self updateDataUI];
		}];
	}
}

- (void) updateDataUI {
	[self.tableView reloadData];
	[self.tableView.mj_header endRefreshing];
	if (self.isPageMax==NO) {
		__weak typeof(self)wself = self;
		self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
			if(wself.ctype==0){
				[wself requestSingerListDataWithNumber:wself.currentPage com:^{
					[wself.tableView reloadData];
					[wself.tableView.mj_footer endRefreshing];
				}];
			}else{
				[wself requestDataWithNumber:wself.currentPage com:^{
					[wself.tableView reloadData];
					[wself.tableView.mj_footer endRefreshing];
				}];
			}
			
		}];
	}else {
		NSLog(@"到达最大数了");
		self.tableView.mj_footer = nil;
	}
}

- (void) requestSingerListDataWithNumber:(NSInteger)number com:(void(^)(void))com {
	NSInteger pageNumber = number+1;
	__weak typeof(self)wself = self;
		NSDictionary *params = @{@"singerId":self.singer.id,@"pageSize":@(kPageSize),@"pageNumber":@(pageNumber)};
	[self sendWithPath:kAPI_getSongBySinger params:params succ:^(id  _Nonnull data) {
		NSArray *listData = data[@"song"];
		[wself dealSongWithJson:listData com:com];
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
		if(com)com();
	}];
}

- (void) requestDataWithNumber:(NSInteger)number com:(void(^)(void))com {
	NSInteger pageNumber = number+1;
	__weak typeof(self)wself = self;
	NSDictionary *params = @{@"cyZimu":self.category.cyZimu,@"pageSize":@(kPageSize),@"pageNumber":@(pageNumber)};
	[self sendWithPath:kAPI_getSongByType params:params succ:^(id  _Nonnull data) {
		NSArray *listData = data[@"songs"];
		[wself dealSongWithJson:listData com:com];
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
		if(com)com();
	}];
}

- (void) dealSongWithJson:(id)json com:(void(^)(void))com {
	__weak typeof(self)wself = self;
	NSArray *datas = [NSArray yy_modelArrayWithClass:[K18Song class] json:json];
	
	K18Song *m1 = datas.lastObject;
	K18Song *m2 = wself.dataSource.lastObject;
	
	if (!m1) {
		wself.isPageMax = YES;
		wself.tableView.mj_footer = nil;
		if (com) {
			com();
		}
		return;
	}
	
	if ([m1.id isEqualToString:m2.id]) {
		wself.isPageMax = YES;
		wself.tableView.mj_footer = nil;
		if (com) {
			com();
		}
		return;
	}
	
	wself.currentPage++;
	
	if (wself.dataSource.count) {
		[wself.dataSource addObjectsFromArray:datas];
	}else{
		wself.dataSource = datas.mutableCopy;
	}
	
	if (wself.dataSource.count<kPageSize) {
		wself.isPageMax = YES;
		wself.tableView.mj_footer = nil;
	}
	
	if (com) {
		com();
	}
}


- (void)listViewLoadDataIfNeeded {
	
	[self requestData];
	
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	K18SongCell *cell = (K18SongCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.delegate = self;
	K18Song *model = self.dataSource[indexPath.row];
	cell.model = model;		
	
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [K18SongCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    K18Song *model = self.dataSource[indexPath.row];
//    [K18SongService gotoRecordingWithModel:model root:self];
}

#pragma mark - K18SongCellDelegate

- (void) cell:(K18SongCell *)cell didSelect:(K18Song *) model
{
    [K18SongService gotoRecordingWithModel:model root:self];
}


@end
