//
//  K18ChatListController.m
//  18kcy
//
//  Created by beuady on 2019/8/10.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ChatListController.h"
#import "TUIConversationListController.h"
#import "K18ChatController.h"
#import "TPopView.h"
#import "TPopCell.h"
#import "THeader.h"
#import "Toast/Toast.h"
#import "TUIContactSelectController.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "TIMUserProfile+DataProvider.h"
#import "TNaviBarIndicatorView.h"
#import "TUIKit.h"
#import "THelper.h"
#import <ImSDK/ImSDK.h>
#import <MJRefresh/MJRefresh.h>

@interface K18ChatListController ()<TUIConversationListControllerDelegagte, TPopViewDelegate>
@property (nonatomic, strong) TNaviBarIndicatorView *titleView;
@property(strong, nonatomic) TUIConversationListController *conversationVC;
@end


@implementation K18ChatListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.conversationVC = [[TUIConversationListController alloc] init];
    self.conversationVC.delegate = self;
    [self addChildViewController:self.conversationVC];
    [self.view addSubview:self.conversationVC.view];

    [self setup];
    
    self.conversationVC.tableView.backgroundColor = [UIColor whiteColor];
}

- (void) setup {
    [self setupNavigation];
    
    TUIKitConfig *config = [TUIKitConfig defaultConfig];
    // 修改默认头像
    config.defaultAvatarImage = [UIImage imageNamed:k18_DefaultIcon];
    config.avatarType = TAvatarTypeRounded;
    
    self.conversationVC.viewModel.listFilter = ^BOOL(TUIConversationCellData * _Nonnull data) {
        if (data.convType == TIM_GROUP) {
            return NO;
        }

        //过滤自己
        if ([data.convId isEqualToString:[K18UserModel shared].liveAccount]) {
            return NO;
        }
        
        return YES;
    };
    
    
    self.conversationVC.viewModel.subTitleMap = ^NSString * _Nonnull(NSString * _Nonnull subTitle) {
        if([subTitle hasPrefix:@"{"]){
            subTitle = [subTitle stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[subTitle dataUsingEncoding:(NSUTF8StringEncoding)] options:(NSJSONReadingAllowFragments) error:nil];
            if ([json isKindOfClass:[NSDictionary class]]) {
                return json[@"msg"];
            }
        }
        return subTitle;
    };

    [self.conversationVC.viewModel loadConversation];
    
    //
    __weak typeof(self)wself = self;
    self.conversationVC.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [wself.conversationVC.viewModel loadConversation];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [wself.conversationVC.tableView.mj_header endRefreshing];
        });
    }];
    
}

/**
 *初始化导航栏
 */
- (void)setupNavigation
{
    _titleView = [[TNaviBarIndicatorView alloc] init];
    _titleView.label.textColor = [UIColor whiteColor];
    [_titleView setTitle:@"消息"];
    self.navigationItem.titleView = _titleView;
    self.navigationItem.title = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNetworkChanged:) name:TUIKitNotification_TIMConnListener object:nil];
}

/**
 *初始化导航栏Title，不同连接状态下Title显示内容不同
 */
- (void)onNetworkChanged:(NSNotification *)notification
{
    TUINetStatus status = (TUINetStatus)[notification.object intValue];
    switch (status) {
        case TNet_Status_Succ:
            [_titleView setTitle:@"消息"];
            [_titleView stopAnimating];
            break;
        case TNet_Status_Connecting:
            [_titleView setTitle:@"连接中..."];
            [_titleView startAnimating];
            break;
        case TNet_Status_Disconnect:
            [_titleView setTitle:@"未连接"];
            [_titleView stopAnimating];
            break;
        case TNet_Status_ConnFailed:
            [_titleView setTitle:@"未连接"];
            [_titleView stopAnimating];
            break;
            
        default:
            break;
    }
}

/**
 *在消息列表内，点击了某一具体会话后的响应函数
 */
- (void)conversationListController:(TUIConversationListController *)conversationController didSelectConversation:(TUIConversationCell *)conversation
{
    K18ChatController *chat = [[K18ChatController alloc] init];
    chat.hidesBottomBarWhenPushed = YES;
    chat.conversationData = conversation.convData;
    [self.navigationController pushViewController:chat animated:YES];
}

- (void)conversationListController:(TUIConversationListController *)conversationController displayCell:(TUIConversationCell *)cell data:(TUIConversationCellData *)data
{
    if ([data.convId isEqualToString:@"admin"]) {
        cell.headImageView.image = [UIImage imageNamed:@"guanliyuan"];
        
    }
}

@end
