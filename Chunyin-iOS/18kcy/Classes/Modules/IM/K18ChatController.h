//
//  K18ChatController.h
//  18kcy
//
//  Created by beuady on 2019/8/10.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "TUIChatController.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18ChatController : K18BaseController
@property (nonatomic, strong) TUIConversationCellData *conversationData;
@end

NS_ASSUME_NONNULL_END
