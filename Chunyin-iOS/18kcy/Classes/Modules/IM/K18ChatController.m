//
//  K18ChatController.m
//  18kcy
//
//  Created by beuady on 2019/8/10.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ChatController.h"
//#import "GroupInfoController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "TUIVideoMessageCell.h"
#import "TUIFileMessageCell.h"
//#import "TUserProfileController.h"
#import <ImSDK/ImSDK.h>
#import "TUIKit.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "MMLayout/UIView+MMLayout.h"
#import "MyCustomCell.h"
#import "K18Common.h"
#import "K18LiveService.h"

#define CUSTOM 0

@interface K18ChatController ()<TUIChatControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate>
@property (nonatomic, strong) TUIChatController *chat;

@end

@implementation K18ChatController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    k18_readReportEvent
    
    TIMConversation *conv = [[TIMManager sharedInstance] getConversation:_conversationData.convType receiver:_conversationData.convId];
    _chat = [[TUIChatController alloc] initWithConversation:conv];
    _chat.delegate = self;
    [self addChildViewController:_chat];
    [self.view addSubview:_chat.view];
    
    
    RAC(self, title) = [RACObserve(_conversationData, title) distinctUntilChanged];

    //left
    [self setupNavigator];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onRefreshNotification:)
                                                 name:TUIKitNotification_TIMRefreshListener
                                               object:nil];
    
    
    if ([self.conversationData.convId isEqualToString:@"admin"]) {
        _chat.inputController.view.hidden = YES;
        CGRect r = self.view.bounds;
        CGFloat h = self.navigationController.navigationBar.height;;
        r.origin.y = h;
        r.size.height -= h;
        _chat.messageController.view.frame = r;
        self.title = @"管理员";
    }
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    if (parent == nil) {
        [_chat saveDraft];
    }
}

// 聊天窗口标题由上层维护，需要自行设置标题
- (void)onRefreshNotification:(NSNotification *)notifi
{
    NSArray<TIMConversation *> *convs = notifi.object;
    if ([convs isKindOfClass:[NSArray class]]) {
        for (TIMConversation *conv in convs) {
            if ([[conv getReceiver] isEqualToString:_conversationData.convId]) {
                if (_conversationData.convType == TIM_GROUP) {
                    _conversationData.title = [conv getGroupName];
                } else if (_conversationData.convType == TIM_C2C) {
                    TIMUserProfile *user = [[TIMFriendshipManager sharedInstance] queryUserProfile:_conversationData.convId];
                    if (user) {
                        _conversationData.title = [user showName];
                    }
                }
            }
        }
    }
}

- (void)setupNavigator
{
    if ([self.conversationData.convId isEqualToString:@"admin"]) {
        
    }else{
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"yonghu"] style:(UIBarButtonItemStylePlain) target:self action:@selector(rightBarButtonClick)];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = self.lastNaviHidden;
}

- (void)rightBarButtonClick
{
    NSString *userid = [self.conversationData.convId substringFromIndex:2];
    [K18LiveService gotoPersonDetail:userid parent:self];
}

- (void)chatController:(TUIChatController *)chatController onSelectMoreCell:(TUIInputMoreCell *)cell
{
    
}

- (TUIMessageCellData *)chatController:(TUIChatController *)controller onNewMessage:(TIMMessage *)msg
{
    return nil;
}

- (TUIMessageCell *)chatController:(TUIChatController *)controller onShowMessageData:(TUIMessageCellData *)data
{
#if CUSTOM
    if ([data isKindOfClass:[MyCustomCellData class]]) {
        MyCustomCell *myCell = [[MyCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell"];
        [myCell fillWithData:(MyCustomCellData *)data];
        return myCell;
    }
#endif
    return nil;
}

- (void)chatController:(TUIChatController *)controller onSelectMessageContent:(TUIMessageCell *)cell
{
    
}


- (void)chatController:(TUIChatController *)controller onSelectMessageAvatar:(TUIMessageCell *)cell
{
    if ([self.conversationData.convId isEqualToString:@"admin"]) {
        
    }else{
        
        if ([cell.messageData.identifier isEqualToString:K18UserModel.shared.liveAccount]) {
            [K18LiveService gotoPersonDetail:k18_loginUserId parent:self];
        }else{
        
            [self rightBarButtonClick];
        }
    }
}

@end
