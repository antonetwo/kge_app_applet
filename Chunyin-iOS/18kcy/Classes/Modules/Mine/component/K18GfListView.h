//
//  K18GiftListView.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseListView.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18GfListView : K18BaseListView

@property(assign, nonatomic) int flag;//0=收，1=发

@end

NS_ASSUME_NONNULL_END
