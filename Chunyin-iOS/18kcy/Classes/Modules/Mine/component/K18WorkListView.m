//
//  K18WorkListView.m
//  18kcy
//
//  Created by 唐 on 2019/7/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18WorkListView.h"
#import "K18Work.h"
#import "K18SongService.h"
#import "K18Fund.h"

@implementation K18WorkListView

- (NSString *) requestPath
{
	return kAPI_getMySongLives;
}
- (NSString *) cellNibName
{
	return @"K18WorkCell";
}

- (NSDictionary *)requestExtraParams
{
	return @{@"cyUserId":notNil(k18_loginUserId)};
}

- (Class)modelClass
{
	return [K18Fund class];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18Fund *model = self.dataSource[indexPath.row];
    [K18SongService gotoSongDetailWithId:model.id root:self.rootVC];
}

- (NSString *)noneDataTip
{
    return @"你还没有发布作品哦";
}

@end
