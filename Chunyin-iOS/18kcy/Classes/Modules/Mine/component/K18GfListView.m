//
//  K18GiftListView.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GfListView.h"
#import "K18GfListCell.h"
#import "K18Mine.h"

@implementation K18GfListView

- (NSString *) requestPath
{
    return kAPI_myFlowerRecord;
}
- (NSString *) cellNibName
{
    return @"K18GfListCell";
}

- (NSDictionary *)requestExtraParams
{
    return @{@"cyUserId":notNil(k18_loginUserId),
             @"cyFlag":@(self.flag),
             };
}

- (Class)modelClass
{
    return [K18GfList class];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (NSString *)noneDataTip
{
    return @"暂无内容";
}

@end
