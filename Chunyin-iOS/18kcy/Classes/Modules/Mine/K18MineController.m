//
//  K18MineController.m
//  18kcy
//
//  Created by beuady on 2019/6/23.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18MineController.h"
#import "JXPageListView.h"
#import "K18SongListView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Masonry/Masonry.h>
#import "K18Cell1.h"
#import "K18Cell2.h"
#import "K18SongListController.h"
#import "K18SongAuthorController.h"
#import "K18PublishController1.h"
#import "K18SongService.h"
#import "K18LiveService.h"
#import "K18MineCell1.h"
#import "K18MineCell2.h"
#import "K18NoneView.h"
#import "K18UserModel.h"
#import "User.h"
#import <MJRefresh/MJRefresh.h>
#import "K18WorkListView.h"
#import "TCLoginModel.h"
#import "K18SettingController.h"
#import "K18FensiController.h"
#import "K18ConcernController.h"
#import "K18PhotoController.h"
#import "K18ShopController.h"
#import "K18CollectionController.h"
#import "K18GiftListController.h"
#import "K18RecordController.h"
#import "K18SongUpController.h"
#import "K18RecordUpController.h"

/**
 我的页面
 */
@interface K18MineController ()<JXPageListViewDelegate, UITableViewDataSource, UITableViewDelegate>
//@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXPageListView *pageListView;
@property (nonatomic, strong) NSMutableArray *listViewArray;

@property(strong, nonatomic) NSArray<K18Category *> *categoryList;

@property(assign, nonatomic) BOOL isNeedUpdateData;
@end

@implementation K18MineController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	__weak typeof(self)wself = self;
    
//    self.pageListView.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [wself reqData];
//
//        wself.pageListView
//
//    }];
//    [self.pageListView.mainTableView.mj_header beginRefreshing];
    [self reqData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMe:) name:@"k18_RefreshMineEvent" object:nil];
}

- (void) refreshMe:(id)sender {
    self.isNeedUpdateData = YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) reqData {
	NSString *myId = notNil(k18_loginUserId);
	__weak typeof(self)wself = self;
	[self sendWithPath:kAPI_getUserInfo params:@{@"cyUserId":myId} succ:^(id  _Nonnull data) {
		K18UserModel.shared.currentUserDetail = [K18User yy_modelWithJSON:data];
		[wself.pageListView.mainTableView reloadData];
		[wself.pageListView.mainTableView.mj_header endRefreshing];
        wself.isNeedUpdateData = NO;
	} failed:^(NSError * _Nonnull error) {
		[wself.pageListView.mainTableView.mj_header endRefreshing];
	}];
}

- (void) initData {

    

}


- (void)initUI {
	UIButton *button = [UIButton new];
	[button setImage:[UIImage imageNamed:@"shezhi"] forState:(UIControlStateNormal)];
	[button addTarget:self action:@selector(settingAction) forControlEvents:(UIControlEventTouchUpInside)];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
	
    //分类
    //热门歌手、节日、情感、主题、综艺、年代、流派、语种
    __weak typeof(self)wself = self;
    _titles = @[@"作品", @"伴奏", @"合唱"];
    
    _listViewArray = [NSMutableArray array];
    for (int i=0; i<3; i++) {
		if(i==0){
			K18WorkListView *categoryListView = [[K18WorkListView alloc] init];
			categoryListView.isNeedHeader = YES;
			categoryListView.rootVC = self;
			[_listViewArray addObject:categoryListView];
            
            if(i==0){
                categoryListView.onRefreshData = ^{
                    
                    [wself reqData];
                    
                };
            }
            
		}else{
			K18NoneView *v = [K18NoneView new];
			[_listViewArray addObject:v];
		}
    }
	
	
    
    
    self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
    self.pageListView.listViewScrollStateSaveEnabled = YES;
    self.pageListView.pinCategoryViewVerticalOffset = 0;
	self.pageListView.mainTableView.separatorInset = UIEdgeInsetsZero;
    self.pageListView.pinCategoryView.contentEdgeInsetLeft = 45;
    self.pageListView.pinCategoryView.contentEdgeInsetRight = 45;
    //Tips:pinCategoryViewHeight要赋值
    self.pageListView.pinCategoryViewHeight = 50;
    //Tips:操作pinCategoryView进行配置
    self.pageListView.pinCategoryView.titles = self.titles;
    
    //添加分割线，这个完全自己配置
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, self.view.bounds.size.width, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self.pageListView.pinCategoryView addSubview:lineView];
    
    //Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
    self.pageListView.mainTableView.dataSource = self;
    self.pageListView.mainTableView.delegate = self;
    self.pageListView.mainTableView.scrollsToTop = NO;
    [self.pageListView.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
	[self.pageListView.mainTableView registerNib:[UINib nibWithNibName:@"K18MineCell1" bundle:nil] forCellReuseIdentifier:@"cell0"];
    [self.view addSubview:self.pageListView];
    
    
    [self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.isNeedUpdateData){
        [self reqData];
    }
}


#pragma mark - JXPageViewDelegate
//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
    return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2 + 1;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        //Tips:最后一个section（即listContainerCell所在的section）需要返回1
        return 1;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        //Tips:最后一个section（即listContainerCell所在的section）返回listContainerCell的高度
        return [self.pageListView listContainerCellHeight];
    }else if (indexPath.section == 1) {
        return 180;
    }else{
        return UITableViewAutomaticDimension;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        //Tips:最后一个section（即listContainerCell所在的section）配置listContainerCell
        return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
    }
    __weak typeof(self)wself = self;
    if (indexPath.section==0) {
		
		K18MineCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"cell0"];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.model = K18UserModel.shared.currentUserDetail;
        cell.parentVC = self;
        __weak typeof(self)wself = self;
        cell.clickControl = ^(NSInteger index) {
            [wself clickControl:index];
        };
        return cell;
		
    }else {
        CGRect rect = self.view.bounds;
        rect.size.height = 180;
        K18MineCell2 *cell = [[K18MineCell2 alloc]initWithFrame:rect];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.clickMenu = ^(NSInteger index, NSString * _Nonnull title) {
            [wself clickCagatoryWithIndex:index title:title];
        };
        return cell;
    }
    
    return nil;
}

#pragma mark - Actions

- (void) clickControl:(NSInteger)index {
    if (index==0) {
        K18FensiController *vc = [[K18FensiController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if(index==1){
        K18ConcernController *vc = [[K18ConcernController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];

    }else if(index==2){
            //等级
    }else if(index==3){
        K18PhotoController *vc = [K18PhotoController new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void) settingAction {
    
    K18SettingController *vc =[[K18SettingController alloc]initWithNibName:@"K18SettingController" bundle:nil];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    	
}

//分类菜单
- (void) clickCagatoryWithIndex:(NSInteger)index title:(NSString *)title {
    NSLog(@"%@",@(index));
    if(index==0){
        K18SongUpController *vc = [[K18SongUpController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if(index==1){
        K18RecordUpController *vc = [[K18RecordUpController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if(index==2){
        K18RecordController *vc = [[K18RecordController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];

    }else if(index==3){//
        K18ShopController *vc = [[K18ShopController alloc]initWithNibName:@"K18ShopController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if(index==4){
        K18CollectionController *vc = [[K18CollectionController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if(index==5){
        K18GiftListController *vc = [[K18GiftListController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        HUDShowDeveloping;
    }

}

- (void) clickMenuWithIndex:(NSInteger)index {
    [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"];
    switch (index) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        case 3:
            
            break;
            
        default:
            break;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Tips:需要传入mainTableView的scrollViewDidScroll事件
    [self.pageListView mainTableViewDidScroll:scrollView];
}

#pragma mark - Datas

- (NSMutableArray*)initialDataWithIndex:(NSInteger)index {
    NSMutableArray *list = [NSMutableArray array];
    
    //TODO test data
    for (int i=0; i<10; i++) {
        K18Song *model = [K18Song testNew];
        [list addObject:model];
    }
    return list;
}


@end
