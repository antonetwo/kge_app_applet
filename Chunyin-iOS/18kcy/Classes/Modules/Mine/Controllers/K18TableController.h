//
//  K18TableController.h
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import <MJRefresh.h>


NS_ASSUME_NONNULL_BEGIN

@interface K18TableController : K18BaseController<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(strong, nonatomic) UILabel *noneTipL;
@property(strong, nonatomic) NSArray *dataSource;

- (void) refreshData;

@end

NS_ASSUME_NONNULL_END
