//
//  K18ShopController.m
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ShopController.h"
#import "IconButton.h"
#import "K18ShopAccountController.h"

@interface K18ShopController ()
@property (weak, nonatomic) IBOutlet IconButton *leftBtn;
@property (weak, nonatomic) IBOutlet UILabel *leftL;
@property (weak, nonatomic) IBOutlet IconButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *rightL;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;

@end

@implementation K18ShopController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"K币账户";
    // Do any additional setup after loading the view from its nib.
    K18User *m = K18UserModel.shared.currentUserDetail;
    self.leftL.text = m.KBiMoney;
    self.rightL.text = @(m.cyFlowerNum.intValue).stringValue;
    
    [self.buyBtn k18_setStyle];
    self.view.backgroundColor = RGBOF(0xcccccc);
}

- (IBAction)buyA:(id)sender {
    HUDShowDeveloping;
}

- (IBAction)moneyA:(id)sender {
    K18ShopAccountController *vc = [[K18ShopAccountController alloc]initWithNibName:@"K18ShopAccountController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
