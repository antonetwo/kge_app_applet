//
//  K18ShopAccountController.m
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ShopAccountController.h"
#import "K18UserModel.h"

@interface K18ShopAccountController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@property (weak, nonatomic) IBOutlet UIButton *outBtn;//提现
@end

@implementation K18ShopAccountController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账户余额";
    self.label.text = K18UserModel.shared.currentUserDetail.KBiMoney;
    
    [self.payBtn k18_setStyle];
    [self.outBtn k18_setStyle];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 30)];
    [button setTitle:@"明细" forState:(UIControlStateNormal)];
    [button setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [button addTarget:self action:@selector(detailA) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barItem;
}

- (IBAction)chongzhiA:(id)sender {
    HUDShowDeveloping;
}
- (IBAction)tixianA:(id)sender {
    HUDShowDeveloping;
}

- (void)detailA {
    HUDShowDeveloping;
}

@end
