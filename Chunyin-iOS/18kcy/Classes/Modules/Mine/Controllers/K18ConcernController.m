//
//  K18ConcernController.m
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ConcernController.h"
#import "K18FensiCell.h"
#import "K18MineManager.h"
@interface K18ConcernController ()

@end

@implementation K18ConcernController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关注";
    
    self.noneTipL.text = @"暂无关注";
    
    [self refreshData];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18FensiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"K18FensiCell" owner:self options:nil] firstObject];
    }
    cell.type = 1;
    cell.model = self.dataSource[indexPath.row];
    __weak typeof(self)wself = self;
    cell.didCancel = ^(K18Mine *model){
        NSMutableArray *list = wself.dataSource.mutableCopy;
        [list removeObject:model];
        wself.dataSource = list;
        K18MineManager.shared.concernList = list;
        [wself.tableView reloadData];
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)refreshData
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{
                             @"cyUserId":notNil(k18_loginUserId)
                             };
    [self sendWithPath:kAPI_myFollow params:params succ:^(id  _Nonnull data) {
        NSMutableArray *list = [NSArray yy_modelArrayWithClass:[K18Mine class] json:data].mutableCopy;
        K18MineManager.shared.concernList = list;
        wself.dataSource = list;
        [wself.tableView reloadData];
        [wself.tableView.mj_header endRefreshing];
        wself.noneTipL.hidden = self.dataSource.count!=0;
    } failed:^(NSError * _Nonnull error) {
        [wself.tableView.mj_header endRefreshing];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

@end
