//
//  K18EditTextController.h
//  18kcy
//
//  Created by beuady on 2019/8/15.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18EditTextController : K18BaseController
@property(assign, nonatomic) int type;//0昵称，1签名

@end

NS_ASSUME_NONNULL_END
