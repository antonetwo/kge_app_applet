//
//  K18FensiController.m
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18FensiController.h"
#import "K18FensiCell.h"
#import "K18MineManager.h"

@interface K18FensiController ()

@end

@implementation K18FensiController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"粉丝";
    
    self.noneTipL.text = @"暂无粉丝";
    
    [self refreshData];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18FensiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"K18FensiCell" owner:self options:nil] firstObject];
    }
    
    cell.model = self.dataSource[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)refreshData
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{
                             @"cyUserId":notNil(k18_loginUserId)
                             };
    [self sendWithPath:kAPI_myFans params:params succ:^(id  _Nonnull data) {
        
        NSMutableArray *list = [NSArray yy_modelArrayWithClass:[K18Mine class] json:data].mutableCopy;
        K18MineManager.shared.fensiList = list;
        wself.dataSource = list;
        [wself.tableView reloadData];
        [wself.tableView.mj_header endRefreshing];
        wself.noneTipL.hidden = self.dataSource.count!=0;
    } failed:^(NSError * _Nonnull error) {
        [wself.tableView.mj_header endRefreshing];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}


@end
