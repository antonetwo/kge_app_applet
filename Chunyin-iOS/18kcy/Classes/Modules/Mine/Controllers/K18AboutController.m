//
//  K18AboutController.m
//  18kcy
//
//  Created by beuady on 2019/8/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AboutController.h"

@interface K18AboutController ()
@property (weak, nonatomic) IBOutlet UILabel *versionL;

@property (weak, nonatomic) IBOutlet UILabel *buildVersionL;
@end

@implementation K18AboutController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于18K纯音";
    
    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    NSString*build = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
    
    self.versionL.text = [NSString stringWithFormat:@"版本号: %@",build];
    self.buildVersionL.text = [NSString stringWithFormat:@"版本名称: %@",version];

}



@end
