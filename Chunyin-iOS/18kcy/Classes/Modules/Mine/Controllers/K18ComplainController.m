//
//  K18ComplainController.m
//  18kcy
//
//  Created by beuady on 2019/8/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18ComplainController.h"
#import "UITextView+Placeholder.h"
#import "MMPickerView.h"
#import "K18AlbumBar.h"
#import "K18Album.h"
#import "K18Common.h"
#import "K18Img.h"
#define kMaxLength 400

@interface K18ComplainController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *selectView;
@property (weak, nonatomic) IBOutlet UILabel *selectL;
@property (weak, nonatomic) IBOutlet UILabel *numL;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *albumContrainer;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;
@property(strong, nonatomic) K18AlbumBar *albumBar;


@property(assign,nonatomic) BOOL lastNavHidden;
@property(strong, nonatomic) NSString *selectType;

@end

@implementation K18ComplainController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"投诉";
 
    
}



- (void)initUI
{
    self.selectL.text = @"请选择";
    [self.commitBtn k18_setStyle];
    
    self.numL.text = [NSString stringWithFormat:@"0/%d",kMaxLength];
    
    self.textView.placeholder = @"请写下您的投诉内容，可附上界面截图以便处理，非常感谢。";
    self.textView.placeholderColor = RGBOF(0xcccccc);
    self.textView.delegate = self;
    
    self.selectView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectTypeAction)];
    [self.selectView addGestureRecognizer:tap];
    
    _albumBar = [K18AlbumBar new];
    _albumBar.frame = self.albumContrainer.bounds;
    [self.albumContrainer addSubview:_albumBar];
    
    [_albumBar.addButton addTarget:self action:@selector(selectImage:) forControlEvents:(UIControlEventTouchUpInside)];
}

- (NSArray *) types {
    return @[@"骚扰", @"广告", @"诈骗", @"色情", @"暴力", @"反动", @"盗歌", @"传销", @"其他"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.lastNavHidden = self.navigationController.navigationBarHidden;
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = self.lastNavHidden;
}

#pragma mark - textview delegate

- (void) textViewDidChange:(UITextView *)textView {
    
    if (textView.text.length>=kMaxLength) {
        textView.text = [textView.text substringToIndex:kMaxLength-1];
    }
    
    self.numL.text = [NSString stringWithFormat:@"%lu/%d", textView.text.length, kMaxLength];
    
}

#pragma mark - Actions

- (void) selectTypeAction {
    __weak typeof(self)wself = self;
    [MMPickerView showPickerViewInView:self.view withStrings:self.types withOptions:nil completion:^(NSString *selectedString) {
        
        wself.selectType = selectedString;
        wself.selectL.text = selectedString;
        
    }];
}

- (IBAction)selectImage:(id)sender {
    __weak typeof(self)wself = self;
    [self chooseImage:^(UIImage * _Nonnull image, PHAsset * _Nonnull asset) {
        [wself startUploadImage:image];
    }];
    
}

- (void) startUploadImage:(UIImage *)image {
    __weak typeof(self)wself = self;
    NSString *userId = notNil(k18_loginUserId);
    NSString *filename = @"";
    NSData *data = UIImageJPEGRepresentation(image, kCompressRate);
    id hud = [[HUDHelper sharedInstance] loading:@"图片上传中，请稍后"];
    [self uploadImage:data fileName:filename userId:userId cyType:7 success:^(id  _Nonnull response) {
        [[HUDHelper sharedInstance] stopLoading:hud];
        K18Album *model = [K18Album yy_modelWithDictionary:response];
        
        if(wself.albumBar.dataSource.count){
            [wself.albumBar.dataSource addObject:model];
        }else{
            wself.albumBar.dataSource = @[model].mutableCopy;
        }
    } failure:^(NSError * _Nonnull err) {
         [[HUDHelper sharedInstance] stopLoading:hud message:err.localizedDescription delay:1 completion:nil];
    }];
    
}

- (IBAction)commitAction:(id)sender {
    if (_selectType.length==0) {
         [[HUDHelper sharedInstance] tipMessage:@"请选择投诉类型"];
        return;
    }
    
    NSString *text = self.textView.text;
    if (text.length==0) {
        [[HUDHelper sharedInstance] tipMessage:@"请输入投诉内容"];
        return;
    }
    
    // photos
    NSMutableArray *albums = self.albumBar.dataSource;
    NSString *photos = @"";
    for (int i=0; i<albums.count; i++) {
        K18Album *model = albums[i];
        if (i==0) {
            photos = model.id;
        }else{
            photos = [photos stringByAppendingFormat:@"_%@",model.id];
        }
    }
    
    NSDictionary *params = @{@"cyUserId":notNil(k18_loginUserId),
                             @"cyComplaintUserId":notNil(self.targetId),
                             @"cyPhotoIds":notNil(photos),
                             @"cyReason":notNil(text),
                             @"cyTypeStr":notNil(self.selectType),
                             };
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doComplaint params:params succ:^(id  _Nonnull data) {
        [wself.navigationController popViewControllerAnimated:YES];
        [[HUDHelper sharedInstance] tipMessage:@"感谢你的投诉，我们会尽快处理"];
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

@end
