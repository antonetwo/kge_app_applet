//
//  K18SettingController.m
//  18kcy
//
//  Created by beuady on 2019/8/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SettingController.h"
#import "LoginService.h"
#import "K18Usermodel.h"
#import "K18MineDetailController.h"
#import "K18AboutController.h"
//#import "K18BindPhoneController.h"
#import "K18Common.h"
#import "K18BindController.h"
#import "K18SongManager.h"

@interface K18SettingController ()<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@end

@implementation K18SettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    _tableView = [[UITableView alloc]init];
    _tableView.backgroundColor = RGBOF(0xf8f8f8);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorInset = UIEdgeInsetsZero;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
    }];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (IBAction)logoutA:(id)sender {
    [LoginService logout];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        return 2;
    }
    if(section==1){
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
//        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = RGBOF(0x323232);
        cell.detailTextLabel.textColor = RGBAOF(0, 0.6);
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];

    }
    
    if(indexPath.section == 0){
        if (indexPath.row==0) {
            cell.textLabel.text = @"账号管理";
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 37, 37)];
            imageView.image = [UIImage imageNamed:k18_DefaultIcon];
            imageView.layer.cornerRadius = 37/2.0;
            imageView.layer.masksToBounds = YES;
            [cell addSubview:imageView];
            
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(cell).offset(-40);
                make.centerY.equalTo(cell);
                make.size.mas_equalTo(CGSizeMake(37, 37));
            }];
            NSString *url = K18UserModel.shared.currentUserDetail.cyImg;
            [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
            
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kMainScreenWidth, 0.5)];
            line.backgroundColor = RGBOF(0xdddddd);
            [cell addSubview:line];
        }else if(indexPath.row==1){
            cell.textLabel.text = @"关联手机号";
            NSString *phone = K18UserModel.shared.currentUserDetail.cyPhone;
            if (phone.length) {
                cell.detailTextLabel.text = phone;
            }else{
                cell.detailTextLabel.text = @"未关联";
            }

        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else if(indexPath.section==1){
        if (indexPath.row==0) {
            cell.textLabel.text = @"关于18K纯音";
            NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"版本 %@",version];
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kMainScreenWidth, 0.5)];
            line.backgroundColor = RGBOF(0xdddddd);
            [cell addSubview:line];

        }else if(indexPath.row==1){
            cell.textLabel.text = @"清除缓存";

        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
//        cell.textLabel.text = @"";
        UILabel *label = [[UILabel alloc]init];
        label.text = @"退出登录";
        label.textColor = RGBOF(0xCD014B);
        label.font = [UIFont boldSystemFontOfSize:15];
        label.textAlignment = NSTextAlignmentCenter;
        [label sizeToFit];
        [cell addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(cell);
        }];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
        return nil;
    return @" ";
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController *vc;
    if(indexPath.section==0 && indexPath.row==0) {
        vc = [[K18MineDetailController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if(indexPath.section==0 && indexPath.row==1){
        K18BindController *vc = [[K18BindController alloc]initWithNibName:@"K18BindController" bundle:nil];
        vc.fromVC = @"K18SettingController";
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if(indexPath.section==1 && indexPath.row==0){
        UIViewController *vc = [[K18AboutController alloc] initWithNibName:@"K18AboutController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else if(indexPath.section==1 && indexPath.row==1){
        
        //TODO clear cache
        [self doClearCache];
        
    }else if(indexPath.section==2 && indexPath.row==0){
        [LoginService logout];
    }
    
}

- (void) doClearCache {
    
    NSString *cacheDir = NSTemporaryDirectory();
    NSError *err;
    
    CGFloat sizeMB = [self folderSizeAtPath:cacheDir];
    NSString *formatSize = [NSString stringWithFormat:@"%.2lfMB",sizeMB];
    
    [[HUDHelper sharedInstance] tipMessage:[NSString stringWithFormat:@"清除缓存完成，共清除缓存%@",formatSize]];
    
    [[NSFileManager defaultManager] removeItemAtPath:cacheDir error:&err];
    NSLog(@"NSFileManager.removeItemAtPath.err%@",err);
    
    //清理数据对象
    [[K18SongManager shared] clearSaveRecords];
    [[K18SongManager shared] clearSaveSongs];
    
}

- (float)folderSizeAtPath:(NSString *)folderPath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil)
    {
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/(1024.0*1024.0);
}

- (long long)fileSizeAtPath:(NSString*)filePath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}


@end
