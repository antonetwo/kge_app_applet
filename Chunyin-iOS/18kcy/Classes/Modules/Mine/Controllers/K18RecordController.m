//
//  K18RecordController.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecordController.h"
#import "K18MineManager.h"
#import "K18SongService.h"
#import "K18Mine.h"
#import "K18RecordCell.h"
@interface K18RecordController ()

@end

@implementation K18RecordController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"播放记录";
    
    self.noneTipL.text = @"暂无记录";
    
    [self refreshData];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"K18RecordCell" owner:self options:nil] firstObject];
    }
    
    cell.model = self.dataSource[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    K18Record *m = self.dataSource[indexPath.row];
    [K18SongService gotoSongDetailWithId:m.songLiveId root:self];
}

- (void)refreshData
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{
                             @"cyUserId":notNil(k18_loginUserId)
                             };
    [self sendWithPath:kAPI_myBroadRecord params:params succ:^(id  _Nonnull data) {
        NSMutableArray *list = [NSArray yy_modelArrayWithClass:[K18Record class] json:data].mutableCopy;
        K18MineManager.shared.recordList = list;
        wself.dataSource = list;
        [wself.tableView reloadData];
        [wself.tableView.mj_header endRefreshing];
        wself.noneTipL.hidden = self.dataSource.count!=0;
    } failed:^(NSError * _Nonnull error) {
        [wself.tableView.mj_header endRefreshing];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

@end
