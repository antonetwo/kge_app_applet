//
//  K18GiftListController.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GiftListController.h"
#import "JXPageListView.h"
#import "CCPagedScrollView.h"
#import <MJRefresh.h>
#import "K18GfListView.h"


@interface K18GiftListController ()<JXPageListViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXPageListView *pageListView;

@property(strong, nonatomic) NSMutableArray *listViewArray;


@end

@implementation K18GiftListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//kAPI_myFlowerRecord
    
}

- (void) initUI {
    self.title = @"收发礼物";
    _titles = @[@"收到的礼物", @"送出的礼物"];
    _listViewArray = [NSMutableArray array];
    
    for (int i=0; i<_titles.count; i++) {
        if(i==0){
            K18GfListView *categoryListView = [[K18GfListView alloc] init];
            categoryListView.rootVC = self;
            categoryListView.flag = 0;
            [_listViewArray addObject:categoryListView];
        }else{
            K18GfListView *categoryListView = [[K18GfListView alloc] init];
            categoryListView.rootVC = self;
            categoryListView.flag = 1;
            [_listViewArray addObject:categoryListView];
        }
    }
    
    self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
    self.pageListView.listViewScrollStateSaveEnabled = YES;
    self.pageListView.pinCategoryViewVerticalOffset = 0;
    self.pageListView.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //Tips:pinCategoryViewHeight要赋值
    self.pageListView.pinCategoryViewHeight = 50;
    //Tips:操作pinCategoryView进行配置
    self.pageListView.pinCategoryView.titles = self.titles;
    
//    //添加分割线，这个完全自己配置
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, self.view.bounds.size.width, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self.pageListView.pinCategoryView addSubview:lineView];
    
    //Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
    self.pageListView.mainTableView.dataSource = self;
    self.pageListView.mainTableView.delegate = self;
    self.pageListView.mainTableView.scrollsToTop = NO;
    [self.pageListView.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"c0"];
    [self.view addSubview:self.pageListView];
    
    
    [self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
}

#pragma mark - JXPageViewDelegate
//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
    return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        return 197/343.0 * MainScreenWidth();
    }
    return [self.pageListView listContainerCellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Tips:需要传入mainTableView的scrollViewDidScroll事件
    [self.pageListView mainTableViewDidScroll:scrollView];
}


@end
