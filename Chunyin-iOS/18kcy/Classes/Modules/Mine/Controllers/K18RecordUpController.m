//
//  K18RecordUpController.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecordUpController.h"
#import "K18SongManager.h"
#import "K18SongUpCell.h"
#import "K18SongService.h"
#import "K18SongModel.h"
#import "K18PublishController2.h"
#import "K18FileService.h"
/**
 本地录制列表
 */
@interface K18RecordUpController ()

@end

@implementation K18RecordUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"本地录制";
    
    self.noneTipL.text = @"暂无录制";
    
    
    self.tableView.mj_header = nil;
    [self refreshData];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18SongUpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"K18SongUpCell" owner:self options:nil] firstObject];
    }
    
    K18SongModel *m = self.dataSource[indexPath.row];
    NSString *url = notNil(m.song.cyImg);
    [cell.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultBG]];
    
    cell.songNameL.text = m.song.cySongName;
    cell.singerL.text = m.song.cySinger;
    NSString *tmp = m.recordModel.combineFileUrl.lastPathComponent;
    NSString *path = [[K18FileService recordSongPath] stringByAppendingPathComponent:tmp];
    if([[NSFileManager defaultManager] fileExistsAtPath:path]){
        m.recordModel.combineFileUrl = [NSURL fileURLWithPath:path];
        NSDictionary *fileAtt = [[NSFileManager defaultManager] attributesOfItemAtPath: path error:nil];
        cell.songSizeL.text = [NSString stringWithFormat:@"%.2fM", fileAtt.fileSize/1024.0/1024];
    }
    [cell.button setTitle:@"发布" forState:(UIControlStateNormal)];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    K18SongModel *m = self.dataSource[indexPath.row];
    K18PublishController2 *vc = [[K18PublishController2 alloc]initWithNibName:@"K18PublishController2" bundle:nil];
    vc.model = m;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)refreshData
{
    NSArray *data = [[K18SongManager shared] getLocalRecordList];
    self.dataSource = data;
    [self.tableView reloadData];
    NSLog(@"%@",data.firstObject);
    
}



@end
