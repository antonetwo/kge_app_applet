//
//  K18ComplainController.h
//  18kcy
//
//  Created by beuady on 2019/8/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18ComplainController : K18BaseController

@property(strong, nonatomic) NSString *targetId;

@end

NS_ASSUME_NONNULL_END
