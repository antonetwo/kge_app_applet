//
//  K18SongUpController.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongUpController.h"
#import "K18SongManager.h"
#import "K18SongUpCell.h"
#import "K18SongService.h"
/**
 本地已点伴奏列表
 */
@interface K18SongUpController ()

@end

@implementation K18SongUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"已唱伴奏";
    
    self.noneTipL.text = @"暂无记录";
    
    
    self.tableView.mj_header = nil;
    [self refreshData];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18SongUpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"K18SongUpCell" owner:self options:nil] firstObject];
    }
    
    K18Song *song = self.dataSource[indexPath.row];
    NSString *url = notNil(song.cyImg);
    [cell.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultBG]];
    
    cell.songNameL.text = song.cySongName;
    cell.singerL.text = song.accompany.cySinger;
    NSUInteger size = song.accompany.cySongSize.longLongValue;
    cell.songSizeL.text = [NSString stringWithFormat:@"%.2fM", size/1024.0/1024];    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    K18Song *m = self.dataSource[indexPath.row];
    [K18SongService gotoRecordingWithModel:m root:self];
}

- (void)refreshData
{
    NSArray *data = [[K18SongManager shared] getLocalSongList];
    self.dataSource = data;
    [self.tableView reloadData];
    NSLog(@"%@",data.firstObject);
    
}


@end
