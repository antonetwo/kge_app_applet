//
//  K18CollectionController.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18CollectionController.h"
#import "K18Mine.h"
#import "K18CollectCell.h"
#import "K18MineManager.h"
#import "K18SongService.h"
@interface K18CollectionController ()

@end

@implementation K18CollectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"收藏";
    
    self.noneTipL.text = @"暂无收藏";
    
    [self refreshData];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18CollectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"K18CollectCell" owner:self options:nil] firstObject];
    }

    cell.model = self.dataSource[indexPath.row];
    __weak typeof(self)wself = self;
    cell.didCancel = ^(K18Collect *model){
        NSMutableArray *list = wself.dataSource.mutableCopy;
        [list removeObject:model];
        wself.dataSource = list;
        K18MineManager.shared.collectList = list;
        [wself.tableView reloadData];
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    K18Collect *m = self.dataSource[indexPath.row];
    [K18SongService gotoSongDetailWithId:m.cyCollectSongId root:self];
}

- (void)refreshData
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{
                             @"cyUserId":notNil(k18_loginUserId)
                             };
    [self sendWithPath:kAPI_myCollects params:params succ:^(id  _Nonnull data) {
        NSMutableArray *list = [NSArray yy_modelArrayWithClass:[K18Collect class] json:data].mutableCopy;
        K18MineManager.shared.collectList = list;
        wself.dataSource = list;
        [wself.tableView reloadData];
        [wself.tableView.mj_header endRefreshing];
        wself.noneTipL.hidden = self.dataSource.count!=0;
    } failed:^(NSError * _Nonnull error) {
        [wself.tableView.mj_header endRefreshing];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

@end
