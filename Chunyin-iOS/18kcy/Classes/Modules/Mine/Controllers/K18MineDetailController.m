//
//  K18MineDetailController.m
//  18kcy
//
//  Created by beuady on 2019/8/14.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18MineDetailController.h"
#import "K18UserModel.h"
#import "MMPickerView.h"
#import "K18EditTextController.h"
#import "K18PhotoController.h"
#import <PGPickerView/PGPickerView.h>
#import "PGDatePickManager.h"
#import "JHAddressPickView.h"
#import "JHAddressToolBar.h"
#import "TCUserInfoModel.h"

@interface K18MineDetailController ()<UITableViewDelegate, UITableViewDataSource,PGDatePickerDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(strong, nonatomic) MMPickerView *pickerView;
@property(strong, nonatomic) JHAddressPickView *addrPicker;
@end

@implementation K18MineDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人资料";
    _tableView = [[UITableView alloc]init];
    _tableView.backgroundColor = RGBOF(0xf8f8f8);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorInset = UIEdgeInsetsZero;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollEnabled = NO;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


#pragma mark - UITableViewDelegate & DataSourceDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        return 2;
    }
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kMainScreenWidth, 0.5)];
        line.backgroundColor = RGBOF(0xdddddd);
        [cell addSubview:line];
        
        cell.textLabel.textColor = RGBOF(0x323232);
        cell.detailTextLabel.textColor = RGBAOF(0, 0.6);
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];

    }
    
    K18User *m = K18UserModel.shared.currentUserDetail;
    if(indexPath.section == 0){
        if (indexPath.row==0) {
            cell.textLabel.text = @"头像";
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 37, 37)];
            imageView.image = [UIImage imageNamed:k18_DefaultIcon];
            [cell addSubview:imageView];
            imageView.layer.cornerRadius = 37/2.0;
            imageView.layer.masksToBounds = YES;
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(cell).offset(-40);
                make.centerY.equalTo(cell);
                make.size.mas_equalTo(CGSizeMake(37, 37));
            }];
            NSString *url = K18UserModel.shared.currentUserDetail.cyImg;
            [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
        }else if(indexPath.row==1){
            cell.textLabel.text = @"昵称";
            cell.detailTextLabel.text = m.cyUserName;
        }
    }else if(indexPath.section==1){
        if(indexPath.row==0){
            cell.textLabel.text = @"性别";
            cell.detailTextLabel.text = m.genderStr;
        }else if(indexPath.row==1){
            cell.textLabel.text = @"生日";
            if(m.birthdayStr.length){
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:m.birthdayUTC.longLongValue/1000];
                NSDateFormatter *format = [[NSDateFormatter alloc]init];
                format.dateFormat = @"yyyy-MM-dd";
                cell.detailTextLabel.text = [format stringFromDate:date];
            }else{
                cell.detailTextLabel.text = @"未填写";
            }
        }else if (indexPath.row==2) {
            cell.textLabel.text = @"居住地";
            if(m.cyProvince.length){
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@",m.cyProvince,m.cyCity];
            }else{
                cell.detailTextLabel.text = @"未填写";
            }
        }else if(indexPath.row==3){
            cell.textLabel.text = @"个性签名";
            if(m.cyPersonalSign.length){
                cell.detailTextLabel.text = m.cyPersonalSign;
            }else{
                cell.detailTextLabel.text = @"暂无签名";
            }
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
        return nil;
    return @" ";
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    __weak typeof(self)wself = self;

    if(indexPath.section==0 && indexPath.row==0) {

        [self showLocalPhotoBrowser];


    }else if(indexPath.section==0 && indexPath.row==1){
        
        K18EditTextController *vc = [[K18EditTextController alloc]initWithNibName:@"K18EditTextController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        vc.type = 0;
        [self.navigationController pushViewController:vc animated:YES];
        
        
    }else if(indexPath.section==1 && indexPath.row==0){
        
        [MMPickerView showPickerViewInView:self.view withStrings:@[@"男",@"女"] withOptions:nil completion:^(NSString *selectedString) {
            
            NSString *genderType;
            if([selectedString isEqualToString:@"男"]){
                genderType = @"1";
            }else{
                genderType = @"2";
            }
            NSString *last = K18UserModel.shared.currentUserDetail.cySex;
            if ([last isEqualToString:genderType]) {
                return;
            }
            [wself reqEdit:genderType];
            
        }];
        
    }else if(indexPath.section==1 && indexPath.row==1){
        
        [self showDatePicker];
        
    }else if(indexPath.section==1 && indexPath.row==2){
        
        [self showAddress];

    }else if(indexPath.section==1 && indexPath.row==3){
        K18EditTextController *vc = [[K18EditTextController alloc]initWithNibName:@"K18EditTextController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];

    }
    
}

#pragma mark - 修改地址

- (void) showAddress {
    if(!self.addrPicker){
        self.addrPicker = [[JHAddressPickView alloc] init];
        _addrPicker.hideWhenTapGrayView = YES;
        _addrPicker.columns = 2;    // 省市二级选择
        [_addrPicker.toolBar.rightButton setTitleColor:k18_TopicC forState:(UIControlStateNormal)];
        [_addrPicker.toolBar.leftButton setTitleColor:RGBOF(0xcccccc) forState:(UIControlStateNormal)];        
        __weak typeof(self)wself = self;
        _addrPicker.pickBlock = ^(NSDictionary *dic) {
//            {
//                city = "北京市";
//                cityCode = 110100;
//                province = "北京";
//                provinceCode = 110000;
//            }
            K18User *m = K18UserModel.shared.currentUserDetail;
            m.cyCity = dic[@"city"];
            m.cyProvince = dic[@"province"];
            
            //TODO syn data
            [wself reqSyncAddress:m.cyCity province:m.cyProvince];
            
            
            
            
        };
        
    }
    [_addrPicker showInView:self.view];
}

- (void) reqSyncAddress:(NSString*)city province:(NSString *)province {
    
    id hud = [[HUDHelper sharedInstance] loading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doUserInfo params:@{@"userId":notNil(k18_loginUserId),
                                                @"cyCity":notNil(city),
                                                @"cyProvince":notNil(province),
                                                } succ:^(id  _Nonnull data) {
                                                    [[HUDHelper sharedInstance] stopLoading:hud];
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:k18_RefreshMineEvent object:nil];

                                                    [wself.tableView reloadData];
                                                    
                                                } failed:^(NSError * _Nonnull error) {
                                                    [[HUDHelper sharedInstance] stopLoading:hud];
                                                    
                                                    [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
                                                }];
    
}

#pragma mark - 修改头像

- (void) showLocalPhotoBrowser {
    __weak typeof(self)wself = self;
    [self chooseImage:^(UIImage * _Nonnull image, PHAsset * _Nonnull asset) {
        [wself uploadImage:image asset:asset];
    }];
}

- (void) uploadImage:(UIImage *)image asset:(PHAsset*)asset {
    __weak typeof(self)wself = self;
    PHAssetResource *resource = [PHAssetResource assetResourcesForAsset:asset].firstObject;
    NSString *fileName = resource.originalFilename;
    NSData *data = UIImagePNGRepresentation(image);
    id hud = [[HUDHelper sharedInstance] loading:@"正在上传"];
    [self uploadImage:data fileName:fileName userId:k18_loginUserId cyType:1 success:^(id  _Nonnull response) {
        K18Img *album = [K18Img yy_modelWithDictionary:response];
        [[HUDHelper sharedInstance] stopLoading:hud];
        
        K18UserModel.shared.currentUserDetail.cyImgId = album.id;
        K18UserModel.shared.currentUserDetail.cyImg = album.cyImgUrl;
        [wself.tableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:k18_RefreshMineEvent object:nil];

        [[TCUserInfoModel sharedInstance] saveUserFace:album.cyImgUrl handler:^(int errCode, NSString *strMsg) {
            NSLog(@"IM修改头像,err=%@",strMsg);
        }];
        
    } failure:^(NSError * _Nonnull err) {
        [[HUDHelper sharedInstance] stopLoading:hud message:err.localizedDescription delay:1 completion:nil];
    }];
}

#pragma mark - 修改签名&昵称

- (void) reqEdit:(NSString*)gender {
    
    id hud = [[HUDHelper sharedInstance] loading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doUserInfo params:@{@"userId":notNil(k18_loginUserId),
                                                @"cySex":notNil(gender)
                                                } succ:^(id  _Nonnull data) {
                                                    [[HUDHelper sharedInstance] stopLoading:hud];
                                                    
                                                    K18UserModel.shared.currentUserDetail.cySex = gender;
                                                    K18UserModel.shared.currentUserDetail.cySex = gender;
                                                    [wself.tableView reloadData];
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:k18_RefreshMineEvent object:nil];

    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud];

        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
    [[TCUserInfoModel sharedInstance] saveUserGender:gender.intValue handler:^(int errCode, NSString *strMsg) {
        NSLog(@"IM修改性别err=%@",strMsg);
    }];
    
}

#pragma mark - 修改生日

- (void) showDatePicker {
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    datePickManager.titleLabel.text = @"请选择出生日期";
    datePickManager.confirmButtonTextColor = k18_TopicC;
    datePickManager.isShadeBackground = true;
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.delegate = self;
    datePicker.datePickerType = PGDatePickerType3;
    datePicker.isHiddenMiddleText = false;
    datePicker.isCycleScroll = true;
    datePicker.datePickerMode = PGDatePickerModeDate;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:K18UserModel.shared.currentUserDetail.birthdayUTC.longLongValue/1000];
    [datePicker setDate:date];
    [self presentViewController:datePickManager animated:false completion:nil];
}

#pragma mark - PGDatePickerDelegate

- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents
{
    NSString *dateStr = [NSString stringWithFormat:@"%ld-%02ld-%02ld",dateComponents.year,dateComponents.month,dateComponents.day];
    NSLog(@"%@",dateStr);
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    NSUInteger dateInt = date.timeIntervalSince1970*1000;
    [self reqBirth:dateStr timestamp:@(dateInt).stringValue];
}

- (void) reqBirth:(NSString *)birthStr timestamp:(NSString *)timestamp{
    [[HUDHelper sharedInstance] syncLoading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doUserInfo
                params:@{@"userId":notNil(k18_loginUserId),
                         @"cyBirthday":notNil(timestamp)
                         }
                  succ:^(id  _Nonnull data) {
                      [[HUDHelper sharedInstance] syncStopLoading];
                      
                      K18UserModel.shared.currentUserDetail.birthdayStr = birthStr;
                      K18UserModel.shared.currentUserDetail.birthdayUTC = timestamp;
                      [wself.tableView reloadData];
                      [[NSNotificationCenter defaultCenter] postNotificationName:k18_RefreshMineEvent object:nil];
                  } failed:^(NSError * _Nonnull error) {
                      [[HUDHelper sharedInstance] syncStopLoading];
                      [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
                  }];
}



@end
