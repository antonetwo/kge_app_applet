//
//  K18EditTextController.m
//  18kcy
//
//  Created by beuady on 2019/8/15.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18EditTextController.h"
#import "UITextView+Placeholder.h"
#import "K18UserModel.h"
#import "TCUserInfoModel.h"

#define kMaxNickName 8
#define kMaxSign 60

@interface K18EditTextController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation K18EditTextController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGBOF(0xf8f8f8);
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 20)];
    [btn setTitle:@"确定" forState:(UIControlStateNormal)];
    [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(okAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.textView.delegate = self;
    self.textView.textContainerInset = UIEdgeInsetsMake(12, 20, 12, 20);
    self.textView.placeholder = @"";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.type==0) {
        self.title = @"修改昵称";
        self.textView.text = K18UserModel.shared.currentUserDetail.cyUserName;
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view.mas_topMargin).offset(10);
            make.height.mas_equalTo(50);
        }];
        self.textView.placeholder = @"请输入昵称";
    }else{
        self.title = @"修改个人签名";
        self.textView.text = K18UserModel.shared.currentUserDetail.cyPersonalSign;
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view.mas_topMargin).offset(10);
            make.height.mas_equalTo(150);
        }];
        self.textView.placeholder = @"请输入个性签名,60字以内";

    }
    [self.textView becomeFirstResponder];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSString *text = textView.text;
    if (self.type==0) {
        if(text.length>kMaxNickName){
            textView.text = [text substringWithRange:NSMakeRange(0, kMaxNickName)];
        }
    }else{
        if(text.length>kMaxSign){
            textView.text = [text substringWithRange:NSMakeRange(0, kMaxSign)];
        }
    }
}

- (void) okAction {
    NSDictionary *params;
    NSString *text = _textView.text;
    if (self.type==0) {
        
        params = @{@"userId":notNil(k18_loginUserId),
                   @"cyName":notNil(text),
                   };
        
        if (text.length==0) {
            [[HUDHelper sharedInstance] tipMessage:@"请输入昵称"];
            return;
        }
        
    }else{
        params = @{@"userId":notNil(k18_loginUserId),
                   @"cyPersonalSign":notNil(text),
                   };
    }
    
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_doUserInfo params:params succ:^(id  _Nonnull data) {
        if (wself.type==0) {
            K18UserModel.shared.currentUser.cyUserName = text;
            K18UserModel.shared.currentUserDetail.cyUserName = text;
            [[HUDHelper sharedInstance] tipMessage:@"昵称修改成功" delay:1 completion:^{
                [wself.navigationController popViewControllerAnimated:YES];
            }];
            
            [[TCUserInfoModel sharedInstance] saveUserNickName:text handler:^(int errCode, NSString *strMsg) {
                NSLog(@"IM修改昵称,err=%@",strMsg);
            }];
            
        }else{
            K18UserModel.shared.currentUser.cyPersonalSign = text;
            K18UserModel.shared.currentUserDetail.cyPersonalSign = text;
            [[HUDHelper sharedInstance] tipMessage:@"签名修改成功" delay:1 completion:^{
                [wself.navigationController popViewControllerAnimated:YES];
            }];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:k18_RefreshMineEvent object:nil];

    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
}

@end
