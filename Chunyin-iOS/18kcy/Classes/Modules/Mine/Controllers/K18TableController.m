//
//  K18TableController.m
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18TableController.h"

@interface K18TableController ()

@end

@implementation K18TableController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    _tableView = [[UITableView alloc]init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorInset = UIEdgeInsetsZero;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
    }];
    
    _noneTipL = [UILabel new];
    _noneTipL.textAlignment = NSTextAlignmentCenter;
    _noneTipL.textColor = RGBOF(0x323232);
    _noneTipL.numberOfLines = 0;
    [self.view addSubview:_noneTipL];
    [_noneTipL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_topMargin).offset(20);
        make.height.mas_equalTo(20);
    }];
    
    _noneTipL.hidden = YES;
    _noneTipL.text = @"暂无粉丝";
    
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
}

- (void)refreshData;
{
    // override
    @throw [NSError errorWithDomain:@"need override" code:0 userInfo:nil];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}




@end
