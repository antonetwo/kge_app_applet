//
//  K18MineManager.h
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "K18Mine.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18MineManager : NSObject
+(instancetype)shared;

@property(strong, nonatomic) NSMutableArray *fensiList;//粉丝列表
@property(strong, nonatomic) NSMutableArray *concernList;//关注列表
@property(strong, nonatomic) NSMutableArray *collectList;//收藏列表
@property(strong, nonatomic) NSMutableArray *recordList;//播放记录

@end

NS_ASSUME_NONNULL_END
