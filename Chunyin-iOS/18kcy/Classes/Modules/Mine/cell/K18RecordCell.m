//
//  K18RecordCell.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecordCell.h"
#import "K18Mine.h"
#import "K18Common.h"

@implementation K18RecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(K18Record *)model
{
    _model = model;
    NSString *url = notNil(model.photoUrl);
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
    
    _label1.text = model.songName;
    _label2.text = model.userName;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:model.broadTimeUTC.longLongValue/1000.0];
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    format.dateFormat = @"yyyy-MM-dd hh:mm";
    _rightLabel.text = [format stringFromDate:date];

//    @property (weak, nonatomic) IBOutlet UIImageView *iconView;
//    @property (weak, nonatomic) IBOutlet UILabel *label1;
//    @property (weak, nonatomic) IBOutlet UILabel *label2;
//    @property (weak, nonatomic) IBOutlet UILabel *rightLabel;
    
    
}

@end
