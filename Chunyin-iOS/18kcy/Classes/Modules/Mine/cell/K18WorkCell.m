//
//  K18WorkCell.m
//  18kcy
//
//  Created by 唐 on 2019/7/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18WorkCell.h"
#import "K18Common.h"
#import "K18Work.h"

@implementation K18WorkCell
{
	id _model;
}

@synthesize model;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *songNameL;
//@property (weak, nonatomic) IBOutlet UILabel *listenL;
//@property (weak, nonatomic) IBOutlet UILabel *shareL;
//@property (weak, nonatomic) IBOutlet UILabel *flowerL;
- (void)setModel:(K18Work *)model
{
	_model = model;
	
	[self.iconView sd_setImageWithURL:[NSURL URLWithString:model.userPhotoImg] placeholderImage:[UIImage imageNamed:k18_DefaultBG]];
	
	self.songNameL.text = model.cySongName;
	self.listenL.text = model.cyListenNum;
	self.shareL.text = model.cyShareNum;
	self.flowerL.text = model.cyFlowerNum;
	self.timeL.text = model.cyCreateTime;
}

+ (CGFloat)cellHeight {
	return 101;
}



@end
