//
//  K18GfListCell.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18BaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18GfListCell : UITableViewCell<K18BaseCell>

@property (weak, nonatomic) IBOutlet UILabel *leftL;

@property (weak, nonatomic) IBOutlet UILabel *rightL;

@end

NS_ASSUME_NONNULL_END
