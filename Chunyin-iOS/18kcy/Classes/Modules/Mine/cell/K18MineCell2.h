//
//  K18MineCell2.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18MineCell2 : UITableViewCell
@property(nonatomic, copy) void(^clickMenu) (NSInteger index, NSString *title);
@end

NS_ASSUME_NONNULL_END
