//
//  K18SongUpCell.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 伴奏列表/录制列表的cell
 */
@interface K18SongUpCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *songNameL;
@property (weak, nonatomic) IBOutlet UILabel *singerL;
@property (weak, nonatomic) IBOutlet UILabel *songSizeL;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

NS_ASSUME_NONNULL_END
