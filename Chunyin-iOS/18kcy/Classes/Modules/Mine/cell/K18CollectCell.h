//
//  K18CollectCell.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Mine.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18CollectCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *labelTop;
@property (weak, nonatomic) IBOutlet UILabel *labelSub;
@property (weak, nonatomic) IBOutlet UIButton *button;


@property(strong, nonatomic) K18Collect *model;

@property(copy, nonatomic) void(^didCancel)(K18Collect *model);//回调取消收藏

@end

NS_ASSUME_NONNULL_END
