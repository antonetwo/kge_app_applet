//
//  K18SongUpCell.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SongUpCell.h"
#import "K18Common.h"

@implementation K18SongUpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.button k18_setStyle];
    [self.button setEnabled:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


}

@end
