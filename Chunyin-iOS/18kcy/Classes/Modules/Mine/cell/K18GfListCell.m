//
//  K18GfListCell.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GfListCell.h"
#import "K18Mine.h"
#import "K18Common.h"

@implementation K18GfListCell
{
    id _model;
}

@synthesize model;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setModel:(K18GfList *)model
{
    _model = model;
    
    NSString *sign;
    if([model.cyIncomeUserId isEqualToString:k18_loginUserId]){
        sign = @"+";
    }else{
        sign = @"-";

    }
    self.leftL.text = [NSString stringWithFormat:@"%@%@", sign,model.cyFlowerNum];
    self.rightL.text = model.userName;
}

+ (CGFloat)cellHeight
{
    return 44;
}

@end
