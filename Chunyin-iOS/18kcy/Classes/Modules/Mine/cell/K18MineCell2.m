//
//  K18MineCell2.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18MineCell2.h"
#import "IconButton.h"
#import "K18Common.h"
@interface K18MineCell2()
@property(strong, nonatomic) NSMutableArray *buttons;
@end

@implementation K18MineCell2

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initUI];
	}
	return self;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self initUI];
	}
	return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];
	// Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

- (void) initUI {
	_buttons = [NSMutableArray array];
	const CGFloat ROW = 2;
	const CGFloat COL = 4;
	CGFloat horizontalGap = 37;
	CGFloat h = 55;
	CGFloat w = 55;
	CGFloat equalGap = (MainScreenWidth() - 2*horizontalGap - w*COL)/(COL-1);
	for (int i=0; i<ROW; i++) {
		for (int col=0; col<COL; col++) {
            if(col>5){
                continue;
            }
			CGRect frame = CGRectMake(horizontalGap + col*(w+equalGap), 20 + i*(20+h), w, h);
			IconButton *button = [[IconButton alloc]initWithFrame:frame];
			button.imageSize = CGSizeMake(35, 33);
			button.imageView.contentMode = UIViewContentModeScaleToFill;
			[self addSubview:button];
			[_buttons addObject:button];
			button.tag = i*COL+col;
			[button addTarget:self action:@selector(clickAction:) forControlEvents:(UIControlEventTouchUpInside)];
			[button setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
            button.titleEdgeOffsets = UIEdgeOffsetsMake(-w/2, 2, w, 15);
            button.titleLabel.font = [UIFont systemFontOfSize:15];
            
		}
	}
	//323232
	
	[_buttons[0] setImage:[[UIImage imageNamed:@"yichangbanzou"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[0] setTitle:@"已唱伴奏" forState:(UIControlStateNormal)];
	
	[_buttons[1] setImage:[[UIImage imageNamed:@"bendiluzhi"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[1] setTitle:@"本地录制" forState:(UIControlStateNormal)];
	
	[_buttons[2] setImage:[[UIImage imageNamed:@"bofangjilu"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[2] setTitle:@"播放记录" forState:(UIControlStateNormal)];
	
	[_buttons[3] setImage:[[UIImage imageNamed:@"Kbizhanghu"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[3] setTitle:@"K币账户" forState:(UIControlStateNormal)];
	
	[_buttons[4] setImage:[[UIImage imageNamed:@"mine_shoucang"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[4] setTitle:@"收藏" forState:(UIControlStateNormal)];

	
	[_buttons[5] setImage:[[UIImage imageNamed:@"shoufaliwu"] iTintColor:k18_TopicC] forState:(UIControlStateNormal)];
	[_buttons[5] setTitle:@"收发礼物" forState:(UIControlStateNormal)];
	
	
}

- (void) clickAction:(IconButton *)button {
	if (self.clickMenu) {
		self.clickMenu(button.tag, button.titleLabel.text);
	}
}


@end
