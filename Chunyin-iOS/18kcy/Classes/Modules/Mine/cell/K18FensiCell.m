//
//  K18FnesiwCell.m
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18FensiCell.h"
#import "K18Common.h"
#import "K18UserModel.h"
#import "K18LiveService.h"

@implementation K18FensiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.button addTarget:self action:@selector(conernAction:) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.iconView.layer.masksToBounds = YES;
    self.iconView.layer.cornerRadius = 37/2.0;
    
    self.button.layer.borderColor = k18_TopicC.CGColor;
    self.button.layer.borderWidth = 0.5;
    [self.button setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
    self.button.layer.cornerRadius = self.button.height/2;
    self.button.layer.masksToBounds = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.iconView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetail)];
    [self.iconView addGestureRecognizer:tap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(K18Mine *)model
{
    _model = model;
    NSString *url = notNil(model.cyImg);
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
    
    self.label.text = model.cyUserName;
    
    if(model.cyFollow.intValue){
        [self.button setTitle:@"取消关注" forState:(UIControlStateNormal)];
    }else{
        [self.button setTitle:@"关注TA" forState:(UIControlStateNormal)];
    }
    
   
    
}

- (void) gotoDetail{
    [K18LiveService gotoPersonDetail:self.model.cyUserId parent:nil];
}

- (void) conernAction:(UIButton *)sender {
    
    BOOL follow = !(self.model.cyFollow.boolValue);
    __weak typeof(self)wself = self;
    [K18UserModel doFollowForFollowId:self.model.cyUserId isFollow:follow completion:^(id  _Nonnull params, NSError * _Nullable error) {
        if (error) {
            [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
            return;
        }
        
        wself.model.cyFollow = @(follow).stringValue;
        if(wself.model.cyFollow.intValue){
            [[HUDHelper sharedInstance] tipMessage:@"关注成功"];
            [wself.button setTitle:@"取消关注" forState:(UIControlStateNormal)];
            self.button.layer.borderColor = [UIColor blackColor].CGColor;
        }else{
            
            if (self.type==0) {//粉丝逻辑
                [[HUDHelper sharedInstance] tipMessage:@"取消关注成功"];
                [wself.button setTitle:@"关注TA" forState:(UIControlStateNormal)];
                self.button.layer.borderColor = k18_TopicC.CGColor;
            }else{//我的关注项被取消                

                if (wself.didCancel) {
                    wself.didCancel(wself.model);
                }
                
            }
        }
        
    }];
    
}

@end
