//
//  K18WorkCell.h
//  18kcy
//
//  Created by 唐 on 2019/7/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18BaseCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18WorkCell : UITableViewCell<K18BaseCell>
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *songNameL;
@property (weak, nonatomic) IBOutlet UILabel *listenL;
@property (weak, nonatomic) IBOutlet UILabel *shareL;
@property (weak, nonatomic) IBOutlet UILabel *flowerL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;

@end

NS_ASSUME_NONNULL_END
