//
//  K18FnesiwCell.h
//  18kcy
//
//  Created by beuady on 2019/8/17.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Mine.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18FensiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property(strong, nonatomic) K18Mine *model;

@property(assign, nonatomic) int type;//0,粉丝, 1关注
@property(copy, nonatomic) void(^didCancel)(K18Mine *model);//回调取消关注

@end

NS_ASSUME_NONNULL_END
