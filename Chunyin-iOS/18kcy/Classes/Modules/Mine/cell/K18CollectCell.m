//
//  K18CollectCell.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18CollectCell.h"
#import "K18UserModel.h"
#import "K18Common.h"

@implementation K18CollectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.button addTarget:self action:@selector(buttonA) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(K18Collect *)model
{
    _model = model;
    NSString *url = model.photoUrl;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultBG]];
    self.labelTop.text = model.songName;
    self.labelSub.text = model.userName;
        
}

- (void) buttonA {
    __weak typeof(self)wself = self;
    BOOL expireFlag = !self.model.cyFlag.boolValue;
    NSString *myUserId = k18_loginUserId;
    NSDictionary *params = @{@"songLiveId":notNil(self.model.cyCollectSongId),
                             @"cyUserId":notNil(myUserId),
                             @"cyFlag":@(expireFlag),
                             };
    id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_doCollectSong params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:data delay:1 completion:nil];
        
        wself.model.cyFlag = @(expireFlag).stringValue;
       
        if(wself.didCancel){
            wself.didCancel(wself.model);
        }
        
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
    }];
    
}

@end
