//
//  K18RecordCell.h
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18Mine.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18RecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@property(strong, nonatomic) K18Record *model;

@end

NS_ASSUME_NONNULL_END
