//
//  K18MineCell1.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18MineCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *kidL;
@property (weak, nonatomic) IBOutlet UIView *tapView;

@property (weak, nonatomic) IBOutlet UILabel *likeNum;
@property (weak, nonatomic) IBOutlet UILabel *followNum;
@property (weak, nonatomic) IBOutlet UILabel *levelNum;
@property (weak, nonatomic) IBOutlet UILabel *albumNum;
@property (weak, nonatomic) IBOutlet UILabel *infoL;
@property (weak, nonatomic) IBOutlet UILabel *signL;

@property(strong, nonatomic) id model;

@property(weak, nonatomic) UIViewController *parentVC;

@property (weak, nonatomic) IBOutlet UIView *control1;
@property (weak, nonatomic) IBOutlet UIView *control2;
@property (weak, nonatomic) IBOutlet UIView *control3;
@property (weak, nonatomic) IBOutlet UIView *control4;
@property(copy, nonatomic) void(^clickControl)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
