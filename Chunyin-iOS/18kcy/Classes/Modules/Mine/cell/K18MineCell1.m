//
//  K18MineCell1.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18MineCell1.h"
#import "User.h"
#import "K18Common.h"
#import "K18MineDetailController.h"
#import "K18MineManager.h"
@implementation K18MineCell1

- (void)awakeFromNib {
    [super awakeFromNib];
	self.iconView.layer.cornerRadius = self.iconView.width/2;
	self.iconView.layer.masksToBounds = YES;
    
    self.tapView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetailAction)];
    [self.tapView addGestureRecognizer:tap];
    self.signL.userInteractionEnabled = YES;
    self.infoL.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap_1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetailAction)];
    [self.signL addGestureRecognizer:tap_1];
    UITapGestureRecognizer *tap_2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetailAction)];
    [self.infoL addGestureRecognizer:tap_2];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickIndexAction:)];
    [self.control1 addGestureRecognizer:tap1];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickIndexAction:)];
    [self.control2 addGestureRecognizer:tap2];

    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickIndexAction:)];
    [self.control3 addGestureRecognizer:tap3];

    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickIndexAction:)];
    [self.control4 addGestureRecognizer:tap4];

    
}

- (void) clickIndexAction:(UITapGestureRecognizer *)sender {
    NSInteger index = NSNotFound;
    if ([sender.view isEqual:self.control1]) {
        index = 0;
    }else if ([sender.view isEqual:self.control2]) {
        index = 1;
    }else if ([sender.view isEqual:self.control3]) {
        index = 2;
    }else if ([sender.view isEqual:self.control4]) {
        index = 3;
    }

    if (self.clickControl) {
        self.clickControl(index);
    }
}

- (void) gotoDetailAction {
    
    K18MineDetailController *vc = [[K18MineDetailController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.parentVC.navigationController pushViewController:vc animated:YES];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *nameL;
//@property (weak, nonatomic) IBOutlet UILabel *kidL;
//
//@property (weak, nonatomic) IBOutlet UILabel *likeNum;
//@property (weak, nonatomic) IBOutlet UILabel *followNum;
//@property (weak, nonatomic) IBOutlet UILabel *levelNum;
//@property (weak, nonatomic) IBOutlet UILabel *albumNum;
//@property (weak, nonatomic) IBOutlet UILabel *infoL;
//@property (weak, nonatomic) IBOutlet UILabel *signL;

- (void)setModel:(K18User *)model
{
	_model = model;
	K18User *m = model;
	[self.iconView sd_setImageWithURL:[NSURL URLWithString:notNil(m.cyImg)] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
	self.nameL.text = m.cyUserName;
	self.kidL.text = [NSString stringWithFormat:@"K歌号:%@",m.id];
	self.likeNum.text = m.userFansSize;
	self.followNum.text = m.userFollowsSize;
	self.levelNum.text = m.userLevel;
	self.albumNum.text = m.userPhotosSize;
    
//    if (K18MineManager.shared.fensiList) {
//        self.likeNum.text = @(K18MineManager.shared.fensiList.count).stringValue;
//    }
    if (K18MineManager.shared.concernList) {
        self.followNum.text = @(K18MineManager.shared.concernList.count).stringValue;
    }

	
	NSString *gender = m.cySex.intValue == 0 ? @"" : ( m.cySex.intValue == 1 ? @"男" : @"女");
	self.infoL.text = [NSString stringWithFormat:@"%@ %@岁 %@ %@",gender, m.age, m.cyProvince, m.cyCity ];
	
	if(m.cyPersonalSign.length){
		self.signL.text = [NSString stringWithFormat:@"个性签名: %@", m.cyPersonalSign];
	}else{
		self.signL.text = @"个性签名: 暂无签名";
	}
}

@end
