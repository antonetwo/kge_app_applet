//
//  K18PhotoBar.m
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18PhotoBar.h"
#import "K18Common.h"

@interface K18PhotoBar()
@property(strong, nonatomic) UILabel *label;
@property(strong, nonatomic) UIButton *button;
@end

@implementation K18PhotoBar

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = RGBOF(0xf6f7f8);
		_button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 88, 33)];
		[_button setTitle:@"选好了" forState:(UIControlStateNormal)];
		[_button setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
		_button.titleLabel.font = [UIFont boldSystemFontOfSize:16];
		[self addSubview:_button];
		[_button addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
		[_button k18_setStyle];
		
		_label = [UILabel new];
		_label.textColor = RGBOF(0xcccccc);
		_label.text = @"已选0/8";
		_label.font = [UIFont systemFontOfSize:14];
		[self addSubview:_label];
		_label.frame = CGRectMake(16, 4, 120, 40);
		
		_label.center = CGPointMake(20+_label.width/2, self.height/2);
		
		
		_button.center = CGPointMake(self.width - _button.width/2 - 20, self.height/2);
	}
	return self;
}

- (void)setDatas:(NSArray *)datas
{
	_datas = datas;
	_label.text = [NSString stringWithFormat:@"已选%lu/%lu",datas.count, self.maxNum];
}

- (void) click {
	if (self.buttonClick) {
		self.buttonClick();
	}
}



@end
