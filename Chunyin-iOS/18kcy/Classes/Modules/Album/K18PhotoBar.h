//
//  K18PhotoBar.h
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18PhotoBar : UIView

@property(nonatomic) NSUInteger maxNum;
@property(strong, nonatomic) NSArray *datas;

@property(copy, nonatomic) void(^buttonClick)(void);

@end

NS_ASSUME_NONNULL_END
