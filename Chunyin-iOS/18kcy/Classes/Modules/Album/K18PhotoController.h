//
//  K18PhotoController.h
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"
#import "K18Album.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18PhotoController : K18BaseController

@property(assign, nonatomic) int type;//上传的图片类型,默认相册
@property(nonatomic) int maxSelectNum;
@property(nonatomic, copy) void(^selectPhotosBlock) (NSArray<K18Album*>* photos);
@property(assign, nonatomic) BOOL editable;//


@end

NS_ASSUME_NONNULL_END
