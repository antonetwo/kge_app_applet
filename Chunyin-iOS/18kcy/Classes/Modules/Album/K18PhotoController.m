//
//  K18PhotoController.m
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18PhotoController.h"
#import <ZLPhotoBrowser/ZLPhotoBrowser.h>
#import <ZLPhotoBrowser/ZLPhotoActionSheet.h>
#import "K18PhotoCell.h"
#import "K18PhotoBar.h"
#import <MJRefresh/MJRefresh.h>
#import "K18Album.h"

#define kMAX 8
#define kPhotoSize 20

@interface K18PhotoController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property(strong, nonatomic) UICollectionView *collectionView;
@property(strong, nonatomic) K18PhotoBar *photoBar;//底部bar

@property(strong, nonatomic) NSMutableArray *dataSource;

@property(nonatomic) NSInteger pageNumber;
@property(nonatomic) BOOL isMaxNumber;

@property(strong, nonatomic) NSMutableArray *selectModels;

@end

@implementation K18PhotoController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"我的相册";
	self.pageNumber = 0;
	self.selectModels = [NSMutableArray array];
    self.type = 0;
}

- (void)initUI
{
	CGFloat barH = 60;//底部图片状态栏
	
//	CGFloat W = [UIScreen mainScreen].bounds.size.width;
//	CGFloat marginGap = 16;
//	CGFloat horGap = 16;
//	CGFloat col = 4;
    CGFloat itemSize = (self.view.width - 32)/4 - 16;
	UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc]init];
	viewLayout.minimumInteritemSpacing = 16;
	viewLayout.minimumLineSpacing = 16;
	viewLayout.itemSize = CGSizeMake(itemSize, itemSize);
	viewLayout.sectionInset = UIEdgeInsetsMake(20, 16, 20, 16);
	
	CGRect abounds = self.view.bounds;
	abounds.size.height -= barH;
	_collectionView = [[UICollectionView alloc]initWithFrame:abounds collectionViewLayout:viewLayout];
	_collectionView.delegate = self;
	_collectionView.dataSource = self;
	_collectionView.backgroundColor = [UIColor whiteColor];
	[self.view addSubview:_collectionView];
	[_collectionView registerNib:[UINib nibWithNibName:@"K18PhotoCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
	[_collectionView registerNib:[UINib nibWithNibName:@"K18PhotoCell" bundle:nil] forCellWithReuseIdentifier:@"cell0"];
	
	
	__weak typeof(self)wself = self;
	self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
		wself.pageNumber = 0;
		wself.isMaxNumber = NO;
		wself.dataSource = [NSMutableArray array];
		[wself requestData:^{
			[wself.collectionView.mj_header endRefreshing];
		}];
		
	}];
	
	[self.collectionView.mj_header beginRefreshing];
	
	
	_photoBar = [[K18PhotoBar alloc]initWithFrame:CGRectMake(0, MainScreenHeight()-barH, MainScreenWidth(), barH)];
	_photoBar.maxNum = kMAX;
	[self.view addSubview:_photoBar];
	_photoBar.buttonClick = ^{
		[wself lastChoicePhotos];
	};
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.editable == NO) {
        self.photoBar.hidden = YES;
    }
}



/**
 最终选择的图
 */
- (void) lastChoicePhotos {
	if (self.selectPhotosBlock){
		self.selectPhotosBlock(self.selectModels);
	}
	[self.navigationController popViewControllerAnimated:YES];
}

- (void) requestData:(void(^)(void)) callback {
	__weak typeof(self)wself = self;
//	id hud = [[HUDHelper sharedInstance] loading];
	NSDictionary *params = @{@"pageSize":@(kPhotoSize), @"pageNumber":@(self.pageNumber+1), @"cyUserId":notNil(k18_loginUserId)};
	[self sendWithPath:kAPI_getMyPhotos params:params succ:^(id  _Nonnull data) {
		
//		[[HUDHelper sharedInstance] stopLoading:hud];
		
		NSArray *list = [NSArray yy_modelArrayWithClass:[K18Album class] json:data[@"list"]];
		
		K18Album *m1 = wself.dataSource.lastObject;
		K18Album *m2 = list.lastObject;
		
		
		if (!m2) {
			wself.collectionView.mj_footer = nil;
			callback();
			return;
		}
		
		if ([m1.id isEqualToString:m2.id]) {
			wself.collectionView.mj_footer = nil;
			[[HUDHelper sharedInstance] tipMessage:@"没有更多了"];
			callback();
			return;
		}
		
		
		
		if (wself.dataSource.count) {
			[wself.dataSource addObjectsFromArray:list];
			wself.pageNumber++;
		}else{
			wself.dataSource = list.mutableCopy;
			wself.pageNumber = 1;
		}
		
		[wself.collectionView reloadData];
		
		if (list.count < kPhotoSize) {
			wself.collectionView.mj_footer = nil;
		}else{
			[wself addFooter];
		}
		
		callback();
	} failed:^(NSError * _Nonnull error) {
		callback();
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
//		[[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
	}];
}

- (void) addFooter {
	__weak typeof(self)wself = self;
	self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
		
		[wself requestData:^{
			[wself.collectionView.mj_footer endRefreshing];
		}];
		
	}];
}

- (void)initData
{
	
}

#pragma mark - Actions

#pragma mark -  UICollectionViewDelegate & UICollectionViewDataSource

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
	if (indexPath.row==0) {
		K18PhotoCell *cell = (K18PhotoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell0" forIndexPath:indexPath];
		cell.imageView.image = [UIImage imageNamed:@"jia"];
		cell.selectButton.hidden = YES;

		return cell;

	}else{
		K18PhotoCell *cell = (K18PhotoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
		K18Album *model = self.dataSource[indexPath.row-1];
		cell.model = model;
		[cell.imageView sd_setImageWithURL:[NSURL URLWithString:model.cyImgUrl] placeholderImage:nil];
		__weak typeof(self)wself = self;
		cell.toggleAction = ^(id  _Nonnull m) {
			if ([wself.selectModels indexOfObject:m]!=NSNotFound) {
				[wself.selectModels removeObject:m];
			}else{
				[wself.selectModels addObject:m];
			}
			wself.photoBar.datas = wself.selectModels;
		};
        if(self.editable==NO){
            cell.selectButton.hidden = YES;
        }
		
		
		return cell;
	}
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return 1 + self.dataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row==0) {
		
		[self showLocalPhotoBrowser];
		
	}else{
        
        if (self.editable==NO) {
            return;
        }
        
		K18Album *model = self.dataSource[indexPath.row-1];
		
		K18PhotoCell *cell = (K18PhotoCell*)[collectionView cellForItemAtIndexPath:indexPath];
		if ([self.selectModels indexOfObject:model]!=NSNotFound) {
			cell.selectButton.selected = NO;
			[self.selectModels removeObject:model];
		}else{
			
			if (self.selectModels.count < kMAX) {
				cell.selectButton.selected = YES;
				[self.selectModels addObject:model];
			}else{
				[[HUDHelper sharedInstance] tipMessage:@"不能超过8张图片"];
			}
			
		}
		
		self.photoBar.datas = self.selectModels;
		
	}
}

#pragma mark - private

- (void) showLocalPhotoBrowser {
	__weak typeof(self)wself = self;
	[self chooseImage:^(UIImage * _Nonnull image, PHAsset * _Nonnull asset) {
		[wself uploadImage:image asset:asset];
	}];
}

- (void) uploadImage:(UIImage *)image asset:(PHAsset*)asset {
	__weak typeof(self)wself = self;
	PHAssetResource *resource = [PHAssetResource assetResourcesForAsset:asset].firstObject;
	NSString *fileName = resource.originalFilename;
	NSData *data = UIImagePNGRepresentation(image);
	id hud = [[HUDHelper sharedInstance] loading:@"正在上传"];
	[self uploadImage:data fileName:fileName userId:k18_loginUserId cyType:self.type success:^(id  _Nonnull response) {
		
		[[HUDHelper sharedInstance] stopLoading:hud];
		
		[wself.collectionView.mj_header beginRefreshing];
		
	} failure:^(NSError * _Nonnull err) {
		[[HUDHelper sharedInstance] stopLoading:hud message:err.localizedDescription delay:1 completion:nil];
	}];
}


@end
