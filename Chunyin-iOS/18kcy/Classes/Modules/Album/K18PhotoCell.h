//
//  K18PhotoCell.h
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18PhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property(strong, nonatomic) id model;

@property(nonatomic, copy) void(^toggleAction)(id model);
@end

NS_ASSUME_NONNULL_END
