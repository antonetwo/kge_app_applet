//
//  K18PhotoCell.m
//  18kcy
//
//  Created by 唐 on 2019/7/9.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18PhotoCell.h"

@implementation K18PhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
	
	[self.selectButton addTarget:self action:@selector(toggle) forControlEvents:(UIControlEventTouchUpInside)];

}

- (void) toggle {
	self.selectButton.selected = !self.selectButton.selected;
	if (self.toggleAction) {
		self.toggleAction(self.model);
	}
}

@end
	
