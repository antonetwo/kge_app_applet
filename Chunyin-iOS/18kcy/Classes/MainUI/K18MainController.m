//
//  MainController.m
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18MainController.h"
#import "LoginService.h"
#import "K18FundService.h"
#import "JXPageListView.h"
#import "K18SongModel.h"
#import "K18SongService.h"
#import "K18LiveService.h"
#import "K18RecommendListView.h"
#import "K18NoneView.h"
#import "CCPagedScrollView.h"
#import "K18SlidePic.h"
#import "K18AttentionListView.h"
#import "K18LiveListView.h"
#import "K18PhotoController.h"
#import <MJRefresh.h>
#import "TCLVBWebViewController.h"
#import "AppDelegate.h"
/**
 发现模块
 */
@interface K18MainController ()
<JXPageListViewDelegate, UITableViewDataSource, UITableViewDelegate>
//@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXPageListView *pageListView;

@property(strong, nonatomic) CCPagedScrollView *advertView;

@property(strong, nonatomic) NSMutableArray *listViewArray;
@property(strong, nonatomic) NSMutableArray <K18SlidePic*>*advertDatas;

@end

@implementation K18MainController

- (instancetype)init
{
    self = [super init];
    if (self) {
//        self.advertDatas = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(testAction:) name:@"testGotoDetailController" object:nil];
    }
    return self;
}

- (void)initUI {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemSearch) target:self action:@selector(searchAction)];
}

- (void) testAction:(NSNotification *)notify {
    [[LoginService mainTabController] setSelectedIndex:0];
    K18SongModel *model = notify.object;
    [K18SongService gotoSongDetailWithId:model.doneSongId root:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"首页";

    [self initUI_delay];
    [self loadAdvert];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;

}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	//TODO 如果是拉黑了，强制弹出登录
	//UserModel.shared.blackStr
}

#pragma mark - init UI

- (void)initUI_delay
{
    
    //分类
    _titles = @[@"推荐", @"关注", @"直播",@"歌房",@"短视频",@"MV",@"歌单"];
    _listViewArray = [NSMutableArray array];
    
    for (int i=0; i<_titles.count; i++) {
        if(i==0){
            K18RecommendListView *categoryListView = [[K18RecommendListView alloc] init];
            categoryListView.rootVC = self;
            [_listViewArray addObject:categoryListView];
        }else if (i==1) {
            K18AttentionListView *v = [[K18AttentionListView alloc] init];
            v.rootVC = self;
            [_listViewArray addObject:v];
        }else if(i==2){
            K18LiveListView *v = [[K18LiveListView alloc] init];
            v.rootVC = self;
            [_listViewArray addObject:v];
        }else{
            K18NoneView *categoryListView = [[K18NoneView alloc] init];
            [_listViewArray addObject:categoryListView];
        }
    }
    
    self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
    self.pageListView.listViewScrollStateSaveEnabled = YES;
    self.pageListView.pinCategoryViewVerticalOffset = 0;
    self.pageListView.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //Tips:pinCategoryViewHeight要赋值
    self.pageListView.pinCategoryViewHeight = 50;
    //Tips:操作pinCategoryView进行配置
    self.pageListView.pinCategoryView.titles = self.titles;
    
    //添加分割线，这个完全自己配置
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, self.view.bounds.size.width, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self.pageListView.pinCategoryView addSubview:lineView];
    
    //Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
    self.pageListView.mainTableView.dataSource = self;
    self.pageListView.mainTableView.delegate = self;
    self.pageListView.mainTableView.scrollsToTop = NO;
    [self.pageListView.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"c0"];
    [self.view addSubview:self.pageListView];
    
    
    [self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
}

#pragma mark - Load & Request

- (void) loadAdvert {
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_getSlidePicture params:@{@"cyType":@0} succ:^(id  _Nonnull data) {
        wself.advertDatas = [NSArray yy_modelArrayWithClass:[K18SlidePic class] json:data].mutableCopy;
//        [wself.categoryView refreshDataSource];
        [wself.pageListView.mainTableView reloadData];
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
}

#pragma mark - JXPageViewDelegate
//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
    return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        return 197/343.0 * MainScreenWidth();
    }
    return [self.pageListView listContainerCellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"c0"];
        cell.clipsToBounds = YES;
        if(!self.advertView){
            CGFloat aW = MainScreenWidth();
            CGFloat ah = 197/343.0 * aW;
            CGFloat gap = 10;
            self.advertView = [[CCPagedScrollView alloc] initWithFrame:CGRectMake(gap, gap, aW -4*gap, ah-4*gap) animationDuration:5.0 isAuto:YES];
            [_advertView.pageControl setHidden:NO];
            [_advertView.pageView setHidden:YES];
            _advertView.backgroundColor = [UIColor blueColor];
			__weak typeof(self)wself = self;
            [_advertView setTapActionBlock:^(CCPagedScrollViewItem *item) {
                TCLVBWebViewController *webVC = [[TCLVBWebViewController alloc]initWithURL:item.itemurl];
                webVC.hidesBottomBarWhenPushed = YES;
                [wself.navigationController pushViewController:webVC animated:YES];
//				K18PhotoController *vc = [K18PhotoController new];
//				vc.hidesBottomBarWhenPushed = YES;
//				[wself.navigationController pushViewController:vc animated:YES];
            }];
            [cell addSubview:self.advertView];

        }
        self.advertView.scrollView.layer.cornerRadius = 8;
        self.advertView.scrollView.layer.masksToBounds = YES;

        NSMutableArray *showArray = [NSMutableArray array];
        for (NSInteger i = 0; i < self.advertDatas.count; i++) {
            K18SlidePic *m = self.advertDatas[i];
            CCPagedScrollViewItem *item = [[CCPagedScrollViewItem alloc]init];
            item.itemImageUrl = m.cyImgUrl;
            item.itemurl = m.cyPostUrl;
            [showArray addObject:item];
        }
        [self.advertView setItems:showArray];
        [self.advertView setupViews];
        return cell;
    }
    
    return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Tips:需要传入mainTableView的scrollViewDidScroll事件
    [self.pageListView mainTableViewDidScroll:scrollView];
}

#pragma mark - Actions

- (void) searchAction {
    [K18LiveService gotoSearchController:nil parent:self];
}

@end
