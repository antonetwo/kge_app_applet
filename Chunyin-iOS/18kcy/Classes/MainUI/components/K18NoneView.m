//
//  K18NoneView.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18NoneView.h"
#import "K18Common.h"

@implementation K18NoneView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void) initUI {
    self.backgroundColor = [UIColor whiteColor];
    _label = [[UILabel alloc]init];
    _label.text = @"开发中，敬请期待";
    [self addSubview:_label];
    [_label sizeToFit];
    [_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.size.mas_equalTo(self.label.frame.size);
        make.top.equalTo(self.mas_top).offset(10);
    }];
}

#pragma mark - delegate

- (UIScrollView *)listScrollView
{
    return nil;
}



- (void)listViewDidScrollCallback:(void (^)(UIScrollView *scrollView))callback
{
    
}

- (void)listViewLoadDataIfNeeded
{
    
}

@end
