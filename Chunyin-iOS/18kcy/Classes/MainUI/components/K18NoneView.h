//
//  K18NoneView.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPageListView.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18NoneView : UIView<JXPageListViewListDelegate>
@property(strong, nonatomic) UILabel *label;
@end

NS_ASSUME_NONNULL_END
