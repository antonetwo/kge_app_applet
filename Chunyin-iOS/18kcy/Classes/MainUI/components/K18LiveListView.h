//
//  K18LiveListView.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPageListView.h"
#import "K18Common.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18LiveListView :UIView<JXPageListViewListDelegate>

@property(nonatomic,strong) UIViewController *playVC;

//@property(strong, nonatomic) NSString *searchKey;

@property(nonatomic,weak) UIViewController *rootVC;

- (void) setNeedRefresh;
- (void) requestData:(void(^)(void))callback;

@end

NS_ASSUME_NONNULL_END
