//
//  K18AttentionListView.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AttentionListView.h"
#import "K18Fund.h"
#import "K18SongService.h"

@implementation K18AttentionListView

- (NSString *) requestPath
{
    return kAPI_userFollow;
}
- (NSString *) cellNibName
{
    return @"K18AttentionCell";
}

- (NSDictionary *)requestExtraParams
{
    return @{@"cyUserId":k18_loginUserId};
}

- (Class)modelClass
{
    return [K18Fund class];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18Fund *model = self.dataSource[indexPath.row];
    [K18SongService gotoSongDetailWithId:model.id root:self.rootVC];
}

@end
