//
//  K18RecommendListView.m
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecommendListView.h"
#import "K18RecommendCell.h"
#import "K18Fund.h"
#import "K18SongService.h"

@implementation K18RecommendListView


- (NSString *) requestPath
{
    return kAPI_getTuijianSongLives;
}
- (NSString *) cellNibName
{
    return @"K18RecommendCell";
}

- (NSDictionary *)requestExtraParams
{
    return nil;
}

- (Class)modelClass
{
    return [K18Fund class];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	K18Fund *model = self.dataSource[indexPath.row];
    [K18SongService gotoSongDetailWithId:model.id root:self.rootVC];
}

@end
