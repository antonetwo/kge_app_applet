//
//  K18LiveListView.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LiveListView.h"
#import <BlocksKit/BlocksKit.h>
#import <MJRefresh/MJRefresh.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "K18SongCell.h"
#import "K18SongService.h"
#import "K18RankCell.h"
#import "K18SearchM.h"
#import "K18LiveService.h"
#import "K18LiveCell.h"
#import "TCLiveListModel.h"
#import "TCBaseAppDelegate.h"
#import "TCPlayViewController.h"
#import "TCVodPlayViewController.h"

@interface K18LiveListView() <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) NSIndexPath *lastSelectedIndexPath;
@property(nonatomic) NSInteger pageNumber;
@property(nonatomic) BOOL isPageMax;
@property(assign, nonatomic) BOOL needRefresh;

@property(strong, nonatomic) UICollectionView *collectionView;
@property(strong, nonatomic) NSMutableArray *dataSource;

@property(strong, nonatomic) TCLiveListMgr *liveListMgr;
@property BOOL isLoading;

@property(copy, nonatomic) void(^callback)(void);
@end

@implementation K18LiveListView
{
    BOOL             _hasEnterplayVC;
    UIView           *_emptyDataView;
    CGFloat _itemSize;
    
}
- (void)dealloc
{
    self.scrollCallback = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGFloat W = [UIScreen mainScreen].bounds.size.width;
        CGFloat marginGap = 16;
        CGFloat horGap = 16;
        CGFloat col = 2;
        UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc]init];
        viewLayout.minimumInteritemSpacing = 16;
        viewLayout.minimumLineSpacing = 16;
        viewLayout.itemSize = CGSizeMake((W-2*marginGap - horGap)/col, 233);
        _itemSize = viewLayout.itemSize.width;
        viewLayout.sectionInset = UIEdgeInsetsMake(20, 16, 20, 16);
        //    viewLayout.headerReferenceSize = CGSizeMake(W, 22);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:viewLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_collectionView];
        [_collectionView registerNib:[UINib nibWithNibName:@"K18LiveCell" bundle:nil] forCellWithReuseIdentifier:@"K18LiveCell"];
        
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
        
        _emptyDataView = [[UIView alloc] initWithFrame:CGRectZero];
        _emptyDataView.backgroundColor = UIColor.clearColor;
        _emptyDataView.hidden = YES;
        _emptyDataView.userInteractionEnabled = NO;
        [self addSubview:_emptyDataView];
        [_emptyDataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(self);
            make.center.equalTo(self);
        }];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.text = @"暂无直播";
        label.font = [UIFont systemFontOfSize:16];
        label.textColor = UIColorFromRGB(0x333333);
        label.alpha = 0.3;
        [_emptyDataView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self->_emptyDataView);
        }];
        
        
        
        self.pageNumber = 0;
        self.dataSource = [NSMutableArray array];
        self.liveListMgr = [TCLiveListMgr sharedMgr];
        [self setNeedRefresh];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newDataAvailable:) name:kTCLiveListNewDataAvailable object:nil];
        
//        __weak typeof(self)wself = self;
//        self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//            __strong __typeof(wself) self = wself;
//            if (!self) return;
//            self.isLoading = YES;
//            [self.liveListMgr queryVideoList:VideoType_LIVE_Online getType:GetType_Down];
//        }];
    }
    return self;
}

//- (void)setSearchKey:(NSString *)searchKey
//{
//    _searchKey = searchKey;
//    self.isPageMax = NO;
//    self.pageNumber = 0;
//    [self.dataSource removeAllObjects];
//}

#pragma mark - Data service

- (void) requestData:(void(^)(void))callback {
    self.callback = callback;
    self.isLoading = YES;
    [self.liveListMgr queryVideoList:VideoType_LIVE_Online getType:GetType_Up];
}

- (void)_doFetchList {
    NSRange range = NSMakeRange(self.dataSource.count, 100);
    BOOL finish;
    NSArray *result = [_liveListMgr readLives:range finish:&finish];
    if (result.count) {
        result = [self mergeResult:result];        
        [self.dataSource addObjectsFromArray:result];
        [self.collectionView reloadData];
        
        self.needRefresh = NO;
        self.isLoading = NO;
    }else{
        if (!finish) {
            return; // 等待新数据的通知过来
        }
    }
    
    if (self.callback) {
        self.callback();
        self.callback = nil;
    }
    
//    [self.collectionView.mj_footer endRefreshing];
    if (self.dataSource.count == 0) {
//        self.collectionView.mj_footer.hidden = YES;
        _emptyDataView.hidden = NO;
    }else{
        _emptyDataView.hidden = YES;
    }
    
}

-(void) newDataAvailable:(NSNotification *)notify {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self _doFetchList];
    });
}

#pragma mark -  UICollectionViewDelegate & UICollectionViewDataSource

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    K18LiveCell *cell = (K18LiveCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"K18LiveCell" forIndexPath:indexPath];
    
    NSInteger index = indexPath.item;
    
    if (self.dataSource.count > index) {
        TCLiveInfo *live = self.dataSource[index];
        cell.model = live;
    }
    cell.clipsToBounds = YES;
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // 此处一定要用cell的数据，live中的对象可能已经清空了
    K18LiveCell *cell = (K18LiveCell *)[collectionView cellForItemAtIndexPath:indexPath];
    TCLiveInfo *info = cell.model;
    
    __weak __typeof(self) wself = self;
    // MARK: 打开播放界面
    if (_playVC == nil) {
        void(^onPlayError)(void) = ^{
            __strong __typeof(wself) self = wself; if (self == nil) return;
            //加房间失败后，刷新列表，不需要刷新动画
            self.dataSource = [NSMutableArray array];
            self.isLoading = YES;
            [self->_liveListMgr queryVideoList:VideoType_LIVE_Online getType:GetType_Up];
        };
        if (info.type == TCLiveListItemType_Live) {
            _playVC = [[TCPlayViewController alloc] initWithPlayInfo:info videoIsReady:^{
                __strong __typeof(wself) self = wself; if (self == nil) return;
                if (!self->_hasEnterplayVC) {
                    self->_playVC.hidesBottomBarWhenPushed = YES;
                    [[TCBaseAppDelegate sharedAppDelegate] pushViewController:self->_playVC animated:YES];
                    self->_hasEnterplayVC = YES;
                }
            }];
            [(TCPlayViewController*)_playVC setOnPlayError:onPlayError];
        } else {
            if (info.hls_play_url.length == 0) {
                return;
            }
            _playVC = [[TCVodPlayViewController alloc] initWithPlayInfo:info videoIsReady:^{
                __strong __typeof(wself) self = wself; if (self == nil) return;
                if (!self->_hasEnterplayVC) {
                    [[TCBaseAppDelegate sharedAppDelegate] pushViewController:self->_playVC animated:YES];
                    self->_hasEnterplayVC = YES;
                }
            }];
            [(TCVodPlayViewController*)_playVC setOnPlayError:onPlayError];
        }
    }
    
    [self performSelector:@selector(enterPlayVC:) withObject:_playVC afterDelay:0.5];
}

-(void)enterPlayVC:(NSObject *)obj{
    _playVC.hidesBottomBarWhenPushed = YES;
    [self.rootVC.navigationController pushViewController:_playVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(_itemSize, 214);
}


#pragma mark - JXPagingViewListViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback?:self.scrollCallback(scrollView);
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)listViewLoadDataIfNeeded {
    if (!self.needRefresh) {
        return;
    }
    
    [self requestData:nil];
    
}

- (void)listViewRefreshData:(void (^)(void))callback
{
    [self.dataSource removeAllObjects];
    [self.collectionView reloadData];
    [self requestData:callback];
}

#pragma mark - actions

/**
 *  将取到的数据于已存在的数据进行合并。
 *
 *  @param result 新拉取到的数据
 *
 *  @return 新数据去除已存在记录后，剩余的数据
 */
- (NSArray *)mergeResult:(NSArray *)result {
    
    // 每个直播的播放地址不同，通过其进行去重处理
    NSArray *existArray = [self.dataSource bk_map:^id(TCLiveInfo *obj) {
        return obj.playurl;
    }];
    NSArray *newArray = [result bk_reject:^BOOL(TCLiveInfo *obj) {
        return [existArray containsObject:obj.playurl];
    }];
    
    return newArray;
}

- (void)setNeedRefresh
{
    self.needRefresh = YES;
}



@end
