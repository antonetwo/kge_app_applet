//
//  K18RecommendListView.h
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseListView.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18RecommendListView : K18BaseListView<K18BaseListProtocol>

@end

NS_ASSUME_NONNULL_END
