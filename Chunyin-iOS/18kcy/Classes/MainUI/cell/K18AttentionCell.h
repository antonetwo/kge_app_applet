//
//  K18AttentionCell.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18BaseCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18AttentionCell : UITableViewCell<K18BaseCell>
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UIImageView *songIconView;
@property (weak, nonatomic) IBOutlet UILabel *songNameL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;

@property (weak, nonatomic) IBOutlet UILabel *listenNumL;
@property (weak, nonatomic) IBOutlet UILabel *shareNumL;
@property (weak, nonatomic) IBOutlet UILabel *flowerNumL;


@end

NS_ASSUME_NONNULL_END
