//
//  K18AttentionCell.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AttentionCell.h"
#import "K18Fund.h"
#import "K18Common.h"

@implementation K18AttentionCell
{
    id _model;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.iconView.layer.cornerRadius = self.iconView.width/2;
    self.iconView.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *nameL;
//@property (weak, nonatomic) IBOutlet UIImageView *songIconView;
//@property (weak, nonatomic) IBOutlet UILabel *songNameL;
//@property (weak, nonatomic) IBOutlet UILabel *timeL;
//
//@property (weak, nonatomic) IBOutlet UILabel *listenNumL;
//@property (weak, nonatomic) IBOutlet UILabel *shareNumL;
//@property (weak, nonatomic) IBOutlet UILabel *flowerNumL;
- (void)setModel:(K18Fund *)model
{
    _model = model;
    NSString *url = model.userHeadImg;
    __weak UIImageView *weakImgView = self.iconView;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
//        weakImgView.image = [image imageWithCornerRadius:image.size.width/2];
        
    }];
    url = model.userPhotoImg;
    [self.songIconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultBG]];
    
    self.nameL.text = model.cyUserName;
    self.songNameL.text = model.cySongName;
    self.flowerNumL.text = model.cyFlowerNum;
    self.shareNumL.text = model.cyShareNum;
    self.listenNumL.text = model.cyListenNum;
    
    self.timeL.text = model.cyCreateTime;
    
    [self.iconView setupClickWithUserId:model.cyUserId];
}


- (id)model
{
    return _model;
}

+ (CGFloat)cellHeight {
    return 160;
}

@end
