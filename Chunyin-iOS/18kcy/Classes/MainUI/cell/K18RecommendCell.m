//
//  K18RecommendCell.m
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecommendCell.h"
#import "K18Fund.h"
#import "K18Common.h"

@implementation K18RecommendCell
{
    id _model;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.smallIcon.image = [self.smallIcon.image iTintColor:[UIColor whiteColor]];
    self.iconView.layer.cornerRadius = self.iconView.width/2;
    self.iconView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *nameL;
//@property (weak, nonatomic) IBOutlet UILabel *songNameL;
//@property (weak, nonatomic) IBOutlet UILabel *listemNum;
//@property (weak, nonatomic) IBOutlet UIView *layerView;
//@property (weak, nonatomic) IBOutlet UIImageView *bgView;
- (void)setModel:(K18Fund *)model
{
    _model = model;
    
    K18Fund *m = model;
    NSString *url = m.userHeadImg;
    __weak typeof(self)wself = self;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:notNil(url)] placeholderImage:[UIImage imageNamed:k18_DefaultIcon] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
		
		//overflow...
//        UIImage *oimage = [image imageWithCornerRadius:image.size.width/2];
//        wself.iconView.image = oimage;
		
    }];
    
    url = m.userPhotoImg;
    [self.bgView sd_setImageWithURL:[NSURL URLWithString:notNil(url)] placeholderImage:[UIImage imageNamed:k18_DefaultBG] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {

//        UIImage *oimage = [image imageWithCornerRadius:16];
//        wself.bgView.image = oimage;
        
    }];

    self.nameL.text = m.cyUserName;
    
    self.songNameL.text = m.cySongName;
    
    self.listemNum.text = m.cyListenNum;
    
    [self.iconView setupClickWithUserId:model.cyUserId];
}

- (id)model
{
    return _model;
}

+ (CGFloat)cellHeight {
    return 266+10;
}


@end
