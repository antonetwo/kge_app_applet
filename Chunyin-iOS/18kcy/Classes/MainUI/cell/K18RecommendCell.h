//
//  K18RecommendCell.h
//  18kcy
//
//  Created by beuady on 2019/6/29.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18BaseCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface K18RecommendCell : UITableViewCell<K18BaseCell>
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *songNameL;
@property (weak, nonatomic) IBOutlet UILabel *listemNum;
@property (weak, nonatomic) IBOutlet UIView *layerView;
@property (weak, nonatomic) IBOutlet UIImageView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *smallIcon;

@end

NS_ASSUME_NONNULL_END
