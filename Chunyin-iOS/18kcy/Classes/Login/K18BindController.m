//
//  K18BindController.m
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BindController.h"
#import "LoginService.h"
#import "K18UserModel.h"

@interface K18BindController ()
@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIButton *passBtn;

@property(assign, nonatomic) int sendCodeType;
@end

@implementation K18BindController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"绑定手机号";
    
    self.sendCodeType = 2;//绑定
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.fromVC isEqualToString:@"K18SettingController"]){
        self.passBtn.hidden = YES;        
    }else{
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:[UIView new]];
    }
}

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self.button k18_setStyle];
	[self.sendCodeBtn k18_setStyle];
}

#pragma mark - actions

- (IBAction)okA:(id)sender {
    NSString *phone = self.accountTF.text;
    NSString *code = self.codeTF.text;
    if (![self checkPhone:phone]) {
        return;
    }
    if(!code.length)
    {
        [[HUDHelper sharedInstance] tipMessage:@"请输入验证码"];
        return;
    }
	//
    [K18UserModel bindWithPhone:phone userId:k18_loginUserId code:code completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
        if (error) {
            [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
            return;
        }
        K18UserModel.shared.currentUser.cyPhone = phone;
        K18UserModel.shared.currentUserDetail.cyPhone = phone;
        [[HUDHelper sharedInstance] tipMessage:@"绑定成功" delay:1 completion:^{
            
            if ([self.fromVC isEqualToString:@"K18SettingController"]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [LoginService enterMainUI];
            }
        }];
        
    }];
	
	//
	
}

- (IBAction)passA:(id)sender {
	[LoginService enterMainUI];
}

- (IBAction)sendCodeAction:(id)sender {
    NSString *phone = self.accountTF.text;
    if (![self checkPhone:phone]) {
        return;
    }
    
    [K18UserModel sendCode:self.sendCodeType phone:phone completion:^(id  _Nonnull params, NSError * _Nullable error) {
        if(error){
            [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
            return;
        }
        
        [[HUDHelper sharedInstance] tipMessage:@"发送成功"];
        
        
    }];
    
}

@end
