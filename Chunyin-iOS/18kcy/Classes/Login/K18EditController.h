//
//  K18EditController.h
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"

NS_ASSUME_NONNULL_BEGIN


/**
 修改头像、昵称页面
 */
@interface K18EditController : K18BaseController

@end

NS_ASSUME_NONNULL_END
