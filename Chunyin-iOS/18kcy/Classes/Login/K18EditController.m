//
//  K18EditController.m
//  18kcy
//
//  Created by 唐 on 2019/6/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18EditController.h"
#import "K18BindController.h"
#import <ZLPhotoBrowser/ZLPhotoActionSheet.h>
#import <Photos/Photos.h>
#import "K18UserModel.h"
#import "K18Img.h"
#import "TCUserInfoModel.h"

@interface K18EditController ()
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UIImageView *iconIV;

@property(strong, nonatomic) UIImage *image;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) MBProgressHUD *hud;
@end

@implementation K18EditController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.iconIV.layer.cornerRadius = self.iconIV.width/2;
    self.iconIV.layer.masksToBounds = YES;
	
	self.nameTF.text = [K18UserModel shared].currentUser.cyUserName;
	if ([K18UserModel shared].currentUser.cyImg) {
		[self.iconIV sd_setImageWithURL:[NSURL URLWithString:[K18UserModel shared].currentUser.cyImg] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
	}
}

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self.button k18_setStyle];
	
	self.iconIV.userInteractionEnabled = YES;
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editImage)];
	[self.iconIV addGestureRecognizer:tap];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	self.navigationController.navigationBar.hidden = NO;
}

- (IBAction)buttonA:(id)sender {
//	UIImage *image = self.iconIV.image;
	self.name = self.nameTF.text;
	if (!self.name.length) {
		[HUDHelper.sharedInstance tipMessage:@"请输入昵称"];
		return;
	}
    
    if (self.name.length>8) {
        [HUDHelper.sharedInstance tipMessage:@"请输入8个字以内的昵称"];
        return;
    }

	self.hud = [[HUDHelper sharedInstance] loading];
	if (self.image) {
		[self uploadHeadImg];
	}else{
		[self requestEdit];
	}
	
	
}

- (void) requestEdit {
	
	__weak typeof(self)wself = self;
	[K18UserModel editUserInfo:K18UserModel.shared.currentUser.headImg.id nickname:self.name completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
		[[HUDHelper sharedInstance] stopLoading:wself.hud];
		
		K18UserModel.shared.currentUser.cyUserName = wself.name;
		
		//编辑后，同步小直播后台
		NSString *faceUrl = K18UserModel.shared.currentUser.cyImg;
		NSString *nickname = K18UserModel.shared.currentUser.cyUserName;
		if(faceUrl.length){
			[TCUserInfoModel sharedInstance].getUserProfile.faceURL = faceUrl;
		}
		if(nickname.length){
			[TCUserInfoModel sharedInstance].getUserProfile.nickName = nickname;
		}
		
		[[TCUserInfoModel sharedInstance] uploadUserInfo:^(int errCode, NSString *strMsg) {
			
			[wself gotoNext];
		}];
		
	}];
}

-(void) uploadHeadImg {
    __weak typeof(self)wself = self;
    @autoreleasepool {
        
		UIImage *image = self.image;
        NSData *imageData = UIImageJPEGRepresentation(image, kCompressRate);
        NSString *imageName;
        imageName = [[NSDate date] description];
        imageName = imageName.md5;
        imageName = [imageName stringByAppendingString:@".jpeg"];
        
        NSString *userId = K18UserModel.shared.userId;
        
        [K18UserModel uploadImage:imageData fileName:imageName userId:userId cyType:1 success:^(id  _Nonnull response) {

            K18Img *model = [K18Img yy_modelWithDictionary:response];
            NSLog(@"%@",response);
            K18UserModel.shared.currentUser.headImg = model;
            K18UserModel.shared.currentUser.cyImg = model.cyImgUrl;

			
			[wself requestEdit];
			
        } failure:^(NSError * _Nonnull err) {
			[[HUDHelper sharedInstance] tipMessage:err.localizedDescription];
        }];
        
    }
}

- (void) gotoNext {
    [[HUDHelper sharedInstance] tipMessage:@"编辑成功" delay:1 completion:^{
        if (!K18UserModel.shared.currentUser.cyPhone.length) {
            K18BindController *vc = [[K18BindController alloc]initWithNibName:@"K18BindController" bundle:nil];
            vc.fromVC = @"K18EditController";
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [LoginService enterMainUI];
        }
    }];
}

- (void) editImage {
	ZLPhotoActionSheet *ac = [[ZLPhotoActionSheet alloc] init];
    ac.configuration.navBarColor = k18_TopicC;
    ac.configuration.bottomBtnsNormalTitleColor = k18_TopicC;
	ac.configuration.maxSelectCount = 1;
	ac.configuration.maxPreviewCount = 10;
	ac.configuration.allowTakePhotoInLibrary = YES;
    ac.configuration.showCaptureImageOnTakePhotoBtn = YES;
	ac.configuration.allowEditImage = YES;
	ac.configuration.allowSelectVideo = NO;
	ac.configuration.allowSelectGif = NO;
    ac.configuration.allowSelectOriginal = NO;
    ac.configuration.showSelectBtn = NO;
    
	
	ac.sender = self;
	[ac showPhotoLibrary];
	
	__weak typeof(self)wself = self;
	[ac setSelectImageBlock:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nonnull assets, BOOL isOriginal) {
		
		wself.iconIV.image = images.firstObject;
        wself.image = images.firstObject;
		
	}];
}


@end
