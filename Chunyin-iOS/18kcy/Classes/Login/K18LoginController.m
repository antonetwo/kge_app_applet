//
//  K18LoginController.m
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LoginController.h"
#import "K18RegisterController.h"
#import "K18PasswordController.h"
#import "K18BindController.h"
#import "K18RegisterController.h"
#import "K18EditController.h"
#import "K18UserModel.h"
#import "LoginService.h"
#import "UMService.h"
#import "TCLoginModel.h"
#import "TCUserInfoModel.h"

@interface K18LoginController ()
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIButton *togglePwdBtn;
@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *pswTF;
@property (weak, nonatomic) IBOutlet UIButton *toggleProtocolBtn;

@end

@implementation K18LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
//	self.title = @"";
}

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self.button k18_setStyle];
	
	[self.toggleProtocolBtn setImage:[[self.toggleProtocolBtn imageForState:(UIControlStateSelected)] imageWithTintColor:k18_TopicC]  forState:(UIControlStateSelected)];
}

- (void) initUserData {
	if (K18UserModel.shared.account.length) {
		self.accountTF.text = K18UserModel.shared.account;
		self.pswTF.text = K18UserModel.shared.password;
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationController.navigationBar.hidden = YES;
	[self initUserData];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	self.navigationController.navigationBar.hidden = NO;
}

#pragma mark Actions

- (IBAction)toggleA:(UIButton *)sender {
	sender.selected = !sender.selected;
	
	if (sender == self.togglePwdBtn) {
		
		self.pswTF.secureTextEntry = !self.pswTF.secureTextEntry;

	}
//
//	if (sender == self.toggleProtocolBtn) {
//
//	}
	
}

- (IBAction)loginA:(id)sender {
	NSString *phone = self.accountTF.text;
	NSString *psw = self.pswTF.text;
	
	if (phone.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入手机号"];
		return;
	}
	
	if (psw.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入密码"];
		return;
	}
	if (!phone.isMobilphone) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入正确的手机号"];
		return;
	}
    
    if (!self.toggleProtocolBtn.selected) {
        [[HUDHelper sharedInstance] tipMessage:@"请同意并勾选协议"];
        return;
    }
	
	__weak typeof(self)wself = self;

	[K18UserModel loginWithPhone:phone pwd:psw completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
		
        [wself gotoLiveLogin];
		
	}];
	
}

- (void) gotoLiveLogin {
    NSLog(@"开始登录直播服务");
    __weak typeof(self)wself = self;
    
    
    NSString *userName = K18UserModel.shared.liveAccount;
    NSString *pwd = K18UserModel.shared.livePassword;
    
    [[HUDHelper sharedInstance] syncLoading];
    [[TCLoginModel sharedInstance] loginWithUsername:userName password:pwd succ:^(NSString *userName, NSString *md5pwd) {
        dispatch_async(dispatch_get_main_queue(), ^{
			
            TCLoginParam *loginParam = [TCLoginParam loadFromLocal];
            loginParam.identifier = userName;
            loginParam.hashedPwd = md5pwd;
            [loginParam saveToLocal];
			
            //登录成功后
            K18UserModel.shared.autoLogin = YES;
            
            [[HUDHelper sharedInstance] syncStopLoading];
            [wself synLoginInfo];
            [wself gotoNextPage];
        });
        
    } fail:^(int errCode, NSString *errMsg) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[HUDHelper sharedInstance] syncStopLoading];
            if(errCode == 620){
				NSLog(@"小直播账号未注册");
//                [HUDHelper alertTitle:nil message:@"账号未注册" cancel:@"确定"];
                
                [wself gotoRegisterLiveAccount];
                
            }
            else if(errCode == 621){
                [HUDHelper alertTitle:nil message:@"密码错误" cancel:@"确定"];
            }
            else{
//                [HUDHelper alertTitle:nil message:@"IM登录失败" cancel:@"确定"];
            }
            NSLog(@"%s %d %@", __func__, errCode, errMsg);
        });
    }];
    
}

- (void) synLoginInfo {
//    [[NSNotificationCenter defaultCenter] postNotificationName:k18IMSyncUserInfoEvent object:nil];
}

- (void) gotoRegisterLiveAccount {
	NSLog(@"自动注册小直播账号");
	__weak typeof(self)wself = self;
    NSString *userName = K18UserModel.shared.liveAccount;
    NSString *pwd = K18UserModel.shared.livePassword;
	
	[[TCLoginModel sharedInstance] registerWithUsername:userName password:pwd succ:^(NSString *userName, NSString *md5pwd) {
		// 注册成功后直接登录
		[wself gotoLiveLogin];
		
	} fail:^(int errCode, NSString *errMsg) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[HUDHelper sharedInstance] syncStopLoading];
			NSLog(@"小直播账号注册失败");
//			[HUDHelper alertTitle:@"注册失败" message:errMsg cancel:@"确定"];
			NSLog(@"%s %d %@", __func__, errCode, errMsg);
		});
	}];
	
    
    [self gotoLiveLogin];
}

- (void) gotoNextPage {
     [[NSNotificationCenter defaultCenter] postNotificationName:k18_LoginSuccessEvent object:nil];
    User *user = K18UserModel.shared.currentUser;
    NSLog(@"%@",user);
    if(K18UserModel.shared.currentUser.cyUserName.length==0 || NEED_EDIT_EVERYTIMES){
        K18EditController *editVC = [[K18EditController alloc] initWithNibName:@"K18EditController" bundle:nil];
        [self.navigationController pushViewController:editVC animated:YES];
    }else if(K18UserModel.shared.currentUser.cyPhone.length==0 &&
             K18UserModel.shared.isFirstLogin){
        K18BindController *vc = [[K18BindController alloc]initWithNibName:@"K18BindController" bundle:nil];
        vc.fromVC = @"K18LoginController";
        [self.navigationController pushViewController:vc animated:YES];
        K18UserModel.shared.isFirstLogin = YES;
    }else{
        [LoginService enterMainUI];
    }
}

- (IBAction)registerA:(id)sender {
	K18RegisterController *vc = [[K18RegisterController alloc]initWithNibName:@"K18RegisterController" bundle:nil];
	[self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)forgotA:(id)sender {
    K18PasswordController *vc = [[K18PasswordController alloc]initWithNibName:@"K18PasswordController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)weixinA:(id)sender {
    
    __weak typeof(self)wself = self;
    [UMService getUserInfoForPlatform:(UMSocialPlatformType_WechatSession) completion:^(id result, NSError *error) {
		
		if (error) {
			return;
		}
		
        UMSocialUserInfoResponse *resp = result;
        [K18UserModel loginWithThridOpenId:resp.openid
                               accessToken:resp.accessToken
                              refreshToken:resp.refreshToken
                                    cyType:0
                                   unionId:resp.unionId
                               userInfoDic:resp.originalResponse
                                completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
			
            [wself gotoLiveLogin];
        }];
        
    }];
}

- (IBAction)QQA:(id)sender {
     __weak typeof(self)wself = self;
    [UMService getUserInfoForPlatform:(UMSocialPlatformType_QQ) completion:^(id result, NSError *error) {
		
		if (error) {
			return;
		}
		
        UMSocialUserInfoResponse *resp = result;
        [K18UserModel loginWithThridOpenId:resp.openid
                               accessToken:resp.accessToken
                              refreshToken:resp.refreshToken
                                    cyType:1
                                   unionId:resp.unionId
                               userInfoDic:resp.originalResponse
                                completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            
            [wself gotoLiveLogin];
            
        }];
        
    }];
}

//打开服务许可协议
- (IBAction)openProcotolA:(id)sender {
	[LoginService openWebWithUrl:K18UserModel.shared.serviceUrl];
}

//打开隐私协议
- (IBAction)openPrivacyA:(id)sender {
	[LoginService openWebWithUrl:K18UserModel.shared.privacyUrl];
}

@end
