//
//  K18PasswordController.m
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18PasswordController.h"
#import "K18UserModel.h"

@interface K18PasswordController ()
@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UITextField *psTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPsTF;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

@property (weak, nonatomic) IBOutlet UIButton *togglePsBtn;//显示隐藏密码按钮
@property (weak, nonatomic) IBOutlet UIButton *toggleCPsBtn;//显隐确认密码按钮
@property (weak, nonatomic) IBOutlet UIButton *button;
@end

@implementation K18PasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"重置密码";
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.sendCodeBtn k18_setStyle];
    [self.button k18_setStyle];
}

- (IBAction)buttonA:(id)sender {
	NSString *phone = self.accountTF.text;
	NSString *psw = self.psTF.text;
	NSString *code = self.codeTF.text;
    if (![self checkPhone:phone]) {
		return;
	}
	
	if (code.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入验证码"];
		return;
	}
	
	if (psw.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入密码"];
		return;
	}
	
	if (![psw isEqualToString:self.confirmPsTF.text]) {
		[[HUDHelper sharedInstance] tipMessage:@"确认密码不匹配，请重新输入"];
		return;
	}
	
    __weak typeof(self)wself = self;
    [K18UserModel resetPsw:phone psw:psw code:code completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
        [wself.navigationController popViewControllerAnimated:YES];
    }];
	
}

- (IBAction)sendCodeA:(id)sender {
	NSString *phone = self.accountTF.text;
    if (![self checkPhone:phone]) {
		return;
	}
	
	[K18UserModel sendCode:CodeTypeReset
				  phone:phone
			 completion:^(NSDictionary * _Nonnull params, NSError * _Nonnull error) {
				 
				 
				 
			 }];
}

- (IBAction)toggleA:(UIButton *)sender {
	sender.selected = !sender.selected;
	if (sender == self.togglePsBtn) {
		self.psTF.secureTextEntry = !self.psTF.secureTextEntry;
	}
	
	if (sender == self.toggleCPsBtn) {
		self.confirmPsTF.secureTextEntry = !self.confirmPsTF.secureTextEntry;
	}
	
}



@end
