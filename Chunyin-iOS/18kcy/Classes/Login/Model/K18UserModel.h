//
//  UserModel.h
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

//发送验证码类型
typedef NS_ENUM(NSUInteger, CodeType) {
	CodeTypeRegist = 0,
	CodeTypeReset,
	CodeTypeBind,
};

NS_ASSUME_NONNULL_BEGIN

#define k18_loginUserId (K18UserModel.shared.currentUser.id?K18UserModel.shared.currentUser.id:@"")


typedef void(^K18Completion)(id params,NSError * _Nullable error);

@interface K18UserModel : NSObject

@property(strong, nonatomic) User *currentUser;
@property(strong, nonatomic) K18User *currentUserDetail;

@property(strong, nonatomic) NSString *privacyUrl;//隐私协议
@property(strong, nonatomic) NSString *serviceUrl;//服务协议
@property(strong, nonatomic) NSString *blackStr;//来自服务端拉黑描述
@property(strong, nonatomic) NSString *anthorTreaty;//直播管理协议
@property(strong, nonatomic) NSString *appDownUrl;//下载链接

@property(nonatomic) BOOL autoLogin;//Default=NO
@property(readonly, nonatomic) NSString *account;
@property(readonly, nonatomic) NSString *password;
@property(readonly, nonatomic) NSString *liveAccount;
@property(readonly, nonatomic) NSString *livePassword;
@property(readonly, nonatomic) NSString *userId;//上次登录的userId
@property(readonly, nonatomic) NSString *openId;
@property(readonly, nonatomic) NSString *unionId;
@property(assign, nonatomic) NSInteger cyType;//注册类型,0，微信注册，1，qq注册，2，手机号注册
@property(nonatomic) BOOL isFirstLogin;//是否首次登录

@property(nonatomic) NSNumber *authorStatus;//主播端认证状态, -1，未提交 0,待审核，1，审核通过  2，审核不通过

+(instancetype)shared;

- (void) cacheWithAccount:(NSString *_Nullable)account
                      psw:(NSString *_Nullable)psw
                   openId:(NSString *_Nullable)openId
                  unionId:(NSString *_Nullable)unionId
                   cyType:(int)cyType
                   userId:(NSString *)userId;//持久化
- (void) clearAccount;

- (BOOL) hasUserId;

@end

@interface K18UserModel (Service)

+ (void) init:(K18Completion)com;

/**
 手机登录
 @param name <#name description#>
 @param pwd <#pwd description#>
 @param com <#com description#>
 */
+ (void)loginWithPhone:(NSString *)name pwd:(NSString *)pwd completion:(K18Completion)com;


/**
 手机注册

 @param phone <#phone description#>
 @param pwd <#pwd description#>
 @param code <#code description#>
 @param com <#com description#>
 */
+ (void)registerWithPhone:(NSString *)phone pwd:(NSString *)pwd code:(NSString *)code completion:(K18Completion)com;


/**
 第三方应用注册

 @param openId <#openId description#>
 @param deviceId <#deviceId description#>
 @param accessToken <#accessToken description#>
 @param refreshToken <#refreshToken description#>
 @param cyType <#cyType description#>
 @param unionId <#unionId description#>
 */
+ (void) loginWithThridOpenId:(NSString *)openId
				  accessToken:(NSString *)accessToken
				 refreshToken:(NSString*)refreshToken
					   cyType:(int)cyType //注册类型,0，微信注册，1，qq注册，2，手机号注册
					  unionId:(NSString*)unionId
                  userInfoDic:(NSDictionary *)userInfoDic
                   completion:(K18Completion)com;


/**
 绑定手机

 @param phone <#phone description#>
 @param userId <#userId description#>
 @param code <#code description#>
 @param com <#com description#>
 */
+ (void) bindWithPhone:(NSString*)phone userId:(NSString *)userId code:(NSString *)code completion:(K18Completion) com;


/**
 重置密码

 @param phone <#phone description#>
 @param psw <#psw description#>
 @param userId <#userId description#>
 @param code <#code description#>
 */
+ (void) resetPsw:(NSString *)phone psw:(NSString*)psw code:(NSString *)code completion:(K18Completion)com;


/**
 发送验证码
0,手机号注册 1,重置密码 2,绑定手机号
 @param cyType <#cyType description#>
 @param phone <#phone description#>
 @param com <#com description#>
 */
+ (void) sendCode:(int)type phone:(NSString *)phone completion:(K18Completion) com;



/**
 编辑头像和昵称

 */
+ (void) editUserInfo:(NSString *)avatarId nickname:(NSString *)nickname completion:(K18Completion)com;



/**
 编辑关注

 */
+(void) doFollowForFollowId:(NSString*)followId isFollow:(BOOL)isFollow completion:(K18Completion) completion;
/**
 是否关注
 */
+ (void) getIsFollowWithId:(NSString*)followId completion:(K18Completion) completion;


/**
 直播间送礼物

 */
+ (void) sendLiveFlowerWithRoomId:(NSString*)roomId sendUserid:(NSString *)sendUserid receiveUserid:(NSString *)receiveUserid flowers:(int)flowers completion:(K18Completion)completion;


/**
 K歌送礼物
*/
+ (void) sendSongFlowerWithSongId:(NSString*)songId recieveId:(NSString *)recieveId flowers:(int)flowers completion:(K18Completion)completion;


/**
 A拉黑B

 */
+ (void) doLahei:(NSString *)myUserid
        targetID:(NSString *)targetId
      completion:(K18Completion)completion;


/**
 检查用户A是否拉黑用户B

 */
+ (void) getLahei:(NSString *)myUserid
         targetID:(NSString *)targetId
       completion:(K18Completion)completion;

@end

NS_ASSUME_NONNULL_END
