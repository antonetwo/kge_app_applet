//
//  UserModel.m
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18UserModel.h"
#import "K18Common.h"

@implementation K18UserModel

- (void)cacheWithAccount:(NSString *)account
                     psw:(NSString * _Nullable)psw
                  openId:(NSString * _Nullable)openId
                 unionId:(NSString * _Nullable)unionId
                  cyType:(int)cyType
                  userId:(nonnull NSString *)userId
{
    [self clearAccount];
    
	SetUserDefaultObject(@"k18.account", account);
	SetUserDefaultObject(@"k18.password", psw);
	SetUserDefaultObject(@"k18.userId", userId);
    SetUserDefaultObject(@"k18.openId", openId);
    SetUserDefaultObject(@"k18.unionId", unionId);
    SetUserDefaultObject(@"k18.cyType", @(cyType));
}

- (NSString *)account
{
	return UserDefaultString(@"k18.account");
}

- (NSString *)password
{
	return UserDefaultString(@"k18.password");
}

- (NSString *)userId
{
    return UserDefaultString(@"k18.userId");
}

- (NSString *)unionId
{
    return UserDefaultString(@"k18.unionId");
}

- (NSString *)openId
{
    return UserDefaultString(@"k18.openId");
}

- (NSInteger)cyType
{
    NSInteger type = UserDefaultInteger(@"k18.cyType");
    return type;
}

- (BOOL)autoLogin
{
	return [UserDefaultString(@"k18.autoLogin") boolValue];
}

- (void)setAutoLogin:(BOOL)autoLogin
{
	SetUserDefaultObject(@"k18.autoLogin", @(autoLogin));
}

- (BOOL)isFirstLogin
{
    NSArray *list = UserDefaultArray(@"k18.isFirstLogin");
    if(!list){
        return YES;
    }
    BOOL tmp = [list indexOfObject:self.currentUser.id] == NSNotFound;
    return tmp;
}

- (void)setIsFirstLogin:(BOOL)isFirstLogin
{
    NSMutableArray *list = UserDefaultArray(@"k18.isFirstLogin").mutableCopy;
    if(!list){
        list = [NSMutableArray array];
    }
    if (isFirstLogin) {
        [list addObject:self.currentUser.id];
    }else{
        [list removeObject:self.currentUser.id];
    }
    SetUserDefaultObject(@"k18.isFirstLogin", list);
}

- (void)clearAccount
{
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"account"];
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unionId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
}

- (BOOL)hasUserId
{
    return self.userId!=nil;
}

- (NSString *)liveAccount
{
//    if (self.cyType==2) {//手机号
//        return [@"cy" stringByAppendingString:self.account];
//    }else{
//        return [@"cy" stringByAppendingString:self.openId];
//    }
	return [@"cy" stringByAppendingString:self.currentUser.id];
}

- (NSString *)livePassword
{
//    if (self.cyType==2) {//手机号
//        return [@"cy" stringByAppendingString:self.account];
//    }else{
//        return [@"cy" stringByAppendingString:self.openId];
//    }
	return [@"cy" stringByAppendingString:self.currentUser.id];
}

@end

@implementation K18UserModel (Service)

+ (instancetype) shared
{
	static dispatch_once_t once;
	static id instance;
	dispatch_once(&once, ^{
		instance = [self new];
	});
	return instance;
}

+ (void)init:(K18Completion)com
{
	NSString *userId = K18UserModel.shared.userId;
	NSDictionary *params = userId.length ? @{@"userId":userId} : @{};
	[self sendWithPath:kAPI_init params:params succ:^(NSDictionary *data) {
		
		K18UserModel.shared.privacyUrl = data[@"privacyTreaty"];
		K18UserModel.shared.serviceUrl = data[@"serviceTreaty"];
		K18UserModel.shared.blackStr = data[@"laheiStr"];
		K18UserModel.shared.anthorTreaty = data[@"anthorTreaty"];
        K18UserModel.shared.appDownUrl = data[@"appUrl"];
        if (com) {
            com(nil,nil);
        }
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
        if (com) {
            com(nil,error);
        }
	}];
}

+ (void)loginWithPhone:(NSString *)name pwd:(NSString *)pwd completion:(K18Completion)com
{
	NSDictionary *params = @{@"cyPhone":name, @"cyPassword":pwd};
	[self sendWithPath:kAPI_phoneLogin params:params succ:^(id  _Nonnull data) {
		User *user = [User yy_modelWithDictionary:data];
		K18UserModel.shared.currentUser = user;
		
		//persistence
		[K18UserModel.shared cacheWithAccount:name psw:pwd openId:nil unionId:nil cyType:2 userId:user.id];
		
        if(com){
            com(data,nil);
        }		
		
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
	}];
}

+ (void)registerWithPhone:(NSString *)phone pwd:(NSString *)pwd code:(NSString *)code completion:(K18Completion)com
{
	NSDictionary *params = @{@"cyPhone":phone, @"cyPassword":pwd, @"code":code};
	[self sendWithPath:kAPI_createUserByPhone params:params succ:^(id  _Nonnull data) {
		[[HUDHelper sharedInstance] tipMessage:@"注册成功"];
		User *user = [User yy_modelWithDictionary:data];
		K18UserModel.shared.currentUser = user;
		
		//persistence
        [K18UserModel.shared cacheWithAccount:phone psw:pwd openId:nil unionId:nil cyType:2 userId:user.id];
		
		if(com){
			com(data,nil);
		}
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
	}];
}

//ctType 注册类型,0，微信注册，1，qq注册，2，手机号注册
+ (void)loginWithThridOpenId:(NSString *)openId
				 accessToken:(NSString *)accessToken
				refreshToken:(NSString *)refreshToken
					  cyType:(int)cyType
					 unionId:(NSString *)unionId
                 userInfoDic:(NSDictionary *)userInfoDic
                  completion:(K18Completion)com
{

    NSParameterAssert(openId);
//    NSParameterAssert(unionId);
    NSDictionary *params;
    
    if (userInfoDic==nil) {
        params = @{@"openId":notNil(openId),
                   @"accessToken":notNil(accessToken),
                   @"refreshToken":notNil(refreshToken),
                   @"unionId":notNil(unionId),
                   @"cyType":@(cyType)
                   };
        
    }else{
        //    city = "";
        //    country = "";
        //    headimgurl = "";
        //    language = "zh_CN";
        //    nickname = "";
        //    openid = "";
        //    privilege =     (
        //    );
        //    province = "";
        //    sex = 1;
        //    unionid = "";
        if(cyType==0){//微信
            params =@{@"openId":notNil(openId),
                      @"accessToken":notNil(accessToken),
                      @"refreshToken":notNil(refreshToken),
                      @"unionId":notNil(unionId),
                      @"cyType":@(cyType),
                      @"cyUserName":notNil(userInfoDic[@"nickname"]),
                      @"cyProvince":notNil(userInfoDic[@"province"]),
                      @"cySex":notNil(userInfoDic[@"sex"]),
                      @"cyCounty":notNil(userInfoDic[@"country"]),
                      @"cyCity":notNil(userInfoDic[@"city"]),
                      @"cyImg":notNil(userInfoDic[@"headimgurl"]),
                      
                      };
        }else{//QQ
//            city = "\U5e7f\U5dde";
//            constellation = "";
//            figureurl
//            "figureurl_1"
//            "figureurl_2"
//            "figureurl_qq"
//            "figureurl_qq_1"
//            "figureurl_qq_2"
//            "figureurl_type"
//            gender = "\U7537";
//            "is_lost" = 0;
//            "is_yellow_vip" = 0;
//            "is_yellow_year_vip" = 0;
//            level = 0;
//            msg = "";
//            nickname =
//            province =
//            ret = 0;
//            vip = 0;
//            year = 0
//            "yellow_vip_level" = 0;
            int sex = [userInfoDic[@"gender"] isEqualToString:@"女"] ? 2 : 1;
            params =@{@"openId":notNil(openId), @"accessToken":notNil(accessToken),@"refreshToken":notNil(refreshToken),
//                      @"unionId":notNil(unionId),
                      @"cyType":@(cyType),
                      @"cyUserName":notNil(userInfoDic[@"nickname"]),
                      @"cyProvince":userInfoDic[@"province"],
                      @"cySex":@(sex),
//                      @"cyCounty":userInfoDic[@"country"],
                      @"cyCity":userInfoDic[@"city"],
                      @"cyImg":userInfoDic[@"figureurl_qq_2"],
                      
                      };
        }
    }
    [self sendWithPath:kAPI_createUserByWXQQ params:params succ:^(id  _Nonnull data) {
       
        [[HUDHelper sharedInstance] tipMessage:@"登录成功"];
        User *user = [User yy_modelWithDictionary:data];
        K18UserModel.shared.currentUser = user;
        K18UserModel.shared.currentUser.cyImg = params[@"cyImg"];
        
        //persistence
        [K18UserModel.shared cacheWithAccount:nil psw:nil openId:openId
                                   unionId:unionId
                                    cyType:cyType
                                    userId:user.id];
        if(com)com(nil,nil);
//        [self synUserInfo:params com:com];
        
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];

}

+ (void) synUserInfo:(NSDictionary *)userInfo com:(K18Completion)com {
    if(userInfo[@"cyUserName"]==nil){//自动登录，不需要同步用户信息
        if(com) com(nil, nil);
        return;
    }
    if (userInfo[@"cyImg"] && K18UserModel.shared.currentUser.cyImg.length==0) {//没有更新过头像的话，更一次
        dispatch_async(dispatch_queue_create("com.k18.synuserinfo", 0), ^{
            NSString *path = userInfo[@"cyImg"];
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
            [self uploadImage:data fileName:path.lastPathComponent userId:k18_loginUserId cyType:1 success:^(id  _Nonnull response) {
                NSString *imgId = response[@"id"];
                NSDictionary *params = @{
                                         @"cyName":notNil(userInfo[@"cyUserName"]),
                                         @"userId":notNil(k18_loginUserId),
                                         @"cyProvince":notNil(userInfo[@"cyProvince"]),
                                         @"cySex":notNil(userInfo[@"cySex"]),
                                         @"cyCity":notNil(userInfo[@"cyCity"]),
                                         @"userPhotoId":notNil(imgId),
                                         };
                [self sendWithPath:kAPI_doUserInfo params:params succ:^(id  _Nonnull data) {
                    if(com){
                        com(data,nil);
                    }
                } failed:^(NSError * _Nonnull error) {
                    if(com){
                        com(nil,error);
                    }
                }];
            } failure:^(NSError * _Nonnull err) {
                if(com){
                    com(nil,err);
                }
            }];
        });
        
    }else{
        NSDictionary *params = @{
                                 @"cyName":notNil(userInfo[@"cyUserName"]),
                                 @"userId":notNil(k18_loginUserId),
                                 @"cyProvince":notNil(userInfo[@"cyProvince"]),
                                 @"cySex":notNil(userInfo[@"cySex"]),
                                 @"cyCity":notNil(userInfo[@"cyCity"]),
                                 
                                 };
        [self sendWithPath:kAPI_doUserInfo params:params succ:^(id  _Nonnull data) {
            if(com){
                com(data,nil);
            }
        } failed:^(NSError * _Nonnull error) {
            if(com){
                com(nil,error);
            }
        }];
    }

    
}

+ (void)sendCode:(int)type phone:(NSString *)phone completion:(K18Completion)com
{
	NSDictionary *params = @{@"cyPhone":phone, @"cyType":@(type)};
	[self sendWithPath:kAPI_sendCode params:params succ:^(id  _Nonnull data) {
		
		[[HUDHelper sharedInstance] tipMessage:@"发送成功"];
		if(com){
			com(data,nil);
		}
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
	}];
}

+ (void)editUserInfo:(NSString *)avatarId nickname:(NSString *)nickname completion:(K18Completion)com
{
	//TODO 接口有问题。。。。
    NSString *userId = k18_loginUserId;
	userId = notNil(userId);
	NSDictionary *params = @{@"cyName":nickname,@"userId":userId, @"userPhotoId":notNil(avatarId)};
	[self sendWithPath:kAPI_doUserInfo params:params succ:^(id  _Nonnull data) {
        if(com){
            com(data,nil);
        }		
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
	}];
}

+ (void)resetPsw:(NSString *)phone psw:(NSString *)psw code:(NSString *)code completion:(K18Completion)com
{
    NSDictionary *params = @{@"cyPhone":phone, @"cyPassword":psw, @"code":code};
    [self sendWithPath:kAPI_resetPassword params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] tipMessage:@"修改成功"];        
        
        if(com){
            com(data,nil);
        }
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
}

+ (void)bindWithPhone:(NSString *)phone userId:(NSString *)userId code:(NSString *)code completion:(K18Completion)com
{
    NSDictionary *params = @{@"cyPhone":phone, @"userId":userId, @"code":code};
    [self sendWithPath:kAPI_resetPassword params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] tipMessage:@"绑定成功"];
        
        K18UserModel.shared.currentUser.cyPhone = phone;
        
        if(com){
            com(data,nil);
        }
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
}


+(void) doFollowForFollowId:(NSString*)followId isFollow:(BOOL)isFollow completion:(K18Completion) completion {
    
    NSDictionary *params = @{@"cyUserId":notNil(k18_loginUserId),
                             @"cyFollowId":notNil(followId),
                             @"cyFollow":@(isFollow),
                             };
    
    [self sendWithPath:kAPI_doFollowUser params:params succ:^(id  _Nonnull data) {
        [[NSNotificationCenter defaultCenter] postNotificationName:k18_RefreshMineEvent object:nil];
        if(data){
            completion(data, nil);
        }
    } failed:^(NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

+ (void) getIsFollowWithId:(NSString*)followId completion:(K18Completion) completion {
   
    NSString *myId = k18_loginUserId;
    NSString *targetId = followId;
//    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_getFollow params:@{@"cyUserId":notNil(myId), @"cyOtherUserId":notNil(targetId)} succ:^(id  _Nonnull data) {

        BOOL isFollow = [data[@"cyFollow"] boolValue];
        NSLog(@"%@关注targetId, result=%d",k18_loginUserId,followId,isFollow);
        if(completion){
            completion(@(isFollow), nil);
        }
        
    } failed:^(NSError * _Nonnull error) {
        if(completion){
            completion(nil, error);
        }
    }];
}

+ (void) sendLiveFlowerWithRoomId:(NSString*)roomId sendUserid:(NSString *)sendUserid receiveUserid:(NSString *)receiveUserid flowers:(int)flowers completion:(K18Completion)completion {
   
    __weak typeof(self)wself = self;
    NSString *myUserId = k18_loginUserId;
    NSDictionary *params = @{@"liveRoomId":notNil(roomId),
                             @"cyFlowerNum":@(flowers),
                             @"cySendUserId":notNil(sendUserid),
                             @"cyIncomeUserId":notNil(receiveUserid),
                             };
    id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_liveRoomSendFlowers params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:data];
        if (completion) {
            completion(data,nil);
        }
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
        if (completion) {
            completion(nil,error);
        }
    }];
}

+ (void) sendSongFlowerWithSongId:(NSString*)songId recieveId:(NSString *)recieveId flowers:(int)flowers completion:(K18Completion)completion {
    NSString *myUserId = k18_loginUserId;
    NSDictionary *params = @{@"songLiveId":notNil(songId),
                             @"cyFlowerNum":@(flowers),
                             @"cySendUserId":notNil(myUserId),
                             @"cyIncomeUserId":notNil(recieveId),
                             };
    id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_sendFlowers params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:data];
        if (completion) {
            completion(data,nil);
        }
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
        if (completion) {
            completion(nil,error);
        }
    }];
}

//A拉黑B
+ (void) doLahei:(NSString *)myUserid targetID:(NSString *)targetId completion:(K18Completion)completion
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{@"cyUserId":notNil(myUserid),
                             @"cyLaheiUserId":notNil(targetId),
                             };
    id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_doLahei params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:data];
        if (completion) {
            completion(data,nil);
        }
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
        if (completion) {
            completion(nil,error);
        }
    }];
}

//检查用户A是否拉黑用户B
+ (void) getLahei:(NSString *)myUserid targetID:(NSString *)targetId completion:(K18Completion)completion
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{@"cyUserId":notNil(myUserid),
                             @"cyLaheiUserId":notNil(targetId),
                             };
//    id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_getLahei params:params succ:^(id  _Nonnull data) {
//        [[HUDHelper sharedInstance] stopLoading:hud message:data];
        if (completion) {
            completion(data,nil);
        }
    } failed:^(NSError * _Nonnull error) {
//        [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
        if (completion) {
            completion(nil,error);
        }
    }];
}

@end
