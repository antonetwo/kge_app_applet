//
//  K18RegisterController.m
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RegisterController.h"
#import "K18UserModel.h"
#import "LoginService.h"



@interface K18RegisterController ()
@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UITextField *psTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPsTF;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

@property (weak, nonatomic) IBOutlet UIButton *togglePBtn;//勾选协议按钮
@property (weak, nonatomic) IBOutlet UIButton *togglePsBtn;//显示隐藏密码按钮
@property (weak, nonatomic) IBOutlet UIButton *toggleCPsBtn;//显隐确认密码按钮

@end

@implementation K18RegisterController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"立即注册";
	
	UIImage *img = [[self.togglePBtn imageForState:(UIControlStateSelected)] iTintColor:k18_TopicC];
	[self.togglePBtn setImage:img forState:(UIControlStateSelected)];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.sendCodeBtn k18_setStyle];
    [self.actionBtn k18_setStyle];
}

#pragma mark - Actions

- (IBAction)toggleA:(UIButton*)sender {
    sender.selected = !sender.selected;
}


- (IBAction)registerA:(id)sender {
	NSString *phone = self.accountTF.text;
	NSString *psw = self.psTF.text;
	NSString *code = self.codeTF.text;
	if (![self checkPhone:phone]) {
		return;
	}
	
	if (code.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入验证码"];
		return;
	}
	
	if (psw.length==0) {
		[[HUDHelper sharedInstance] tipMessage:@"请输入密码"];
		return;
	}
	
	if (![psw isEqualToString:self.confirmPsTF.text]) {
		[[HUDHelper sharedInstance] tipMessage:@"确认密码不匹配，请重新输入"];
		return;
	}
    
    if (!self.togglePBtn.selected) {
        [[HUDHelper sharedInstance] tipMessage:@"请同意并勾选协议"];
        return;
    }
	
	
	[K18UserModel registerWithPhone:phone pwd:psw code:code completion:^(NSDictionary * _Nonnull params, NSError * _Nonnull error) {
		if(error)
		{
			return;
		}
		
		[self.navigationController popViewControllerAnimated:YES];
	}];
}


- (IBAction)sendCodeA:(id)sender {
	NSString *phone = self.accountTF.text;
	
    if (![self checkPhone:phone]) {
		return;
	}
	
	[K18UserModel sendCode:CodeTypeRegist
				  phone:phone
			 completion:^(NSDictionary * _Nonnull params, NSError * _Nonnull error) {
		
				 
				 
	}];
}

- (IBAction)openProtocolA:(id)sender {
	[LoginService openWebWithUrl:K18UserModel.shared.serviceUrl];
}

- (IBAction)openPrivacyA:(id)sender {
    [LoginService openWebWithUrl:K18UserModel.shared.privacyUrl];
}


@end
