//
//  K18LiveMainController.m
//  18kcy
//
//  Created by 唐 on 2019/6/25.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LiveMainController.h"
#import "K18LiveCell.h"
#import "K18Live.h"
#import "K18AuthenticationController.h"
#import "K18LiveService.h"
#import "K18SearchController.h"
#import "K18SearchMainController.h"
#import "K18NavigationController.h"
#import <MJRefresh/MJRefresh.h>

@interface K18LiveMainController ()<UICollectionViewDelegate, UICollectionViewDataSource,UISearchControllerDelegate>

@property(strong, nonatomic) UICollectionView *collectionView;

@property(strong, nonatomic) NSMutableArray *dataSource;

@property(nonatomic) NSInteger pageNumber;
@property(nonatomic) BOOL isMaxNumber;
@end

@implementation K18LiveMainController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.pageNumber = 0;
    self.dataSource = [NSMutableArray array];
    [self reqIsApproved];
}

- (void)initUI
{
	CGFloat W = [UIScreen mainScreen].bounds.size.width;
	CGFloat marginGap = 16;
	CGFloat horGap = 16;
	CGFloat col = 2;
	UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc]init];
	viewLayout.minimumInteritemSpacing = 16;
	viewLayout.minimumLineSpacing = 16;
	viewLayout.itemSize = CGSizeMake((W-2*marginGap - horGap)/col, 233);
	viewLayout.sectionInset = UIEdgeInsetsMake(20, 16, 20, 16);
//	viewLayout.headerReferenceSize = CGSizeMake(W, 22);
	
	_collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:viewLayout];
	_collectionView.delegate = self;
	_collectionView.dataSource = self;
	_collectionView.backgroundColor = [UIColor whiteColor];
	[self.view addSubview:_collectionView];
	[_collectionView registerNib:[UINib nibWithNibName:@"K18LiveCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
	
	UIButton *button = [UIButton new];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:16];
	[button setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
	[button setTitle:@"开播" forState:(UIControlStateNormal)];
	[button addTarget:self action:@selector(startLive) forControlEvents:(UIControlEventTouchUpInside)];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
	
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemSearch) target:self action:@selector(searchAction)];
	
	__weak typeof(self)wself = self;
	self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
		wself.isMaxNumber = NO;
        [wself.dataSource removeAllObjects];
		[wself requestData:^{
			[wself.collectionView.mj_header endRefreshing];
		}];
		
	}];
	
	//init
    [wself requestData:nil];
}

- (void) requestData:(void(^)(void)) callback {
	__weak typeof(self)wself = self;
	id hud = [[HUDHelper sharedInstance] loading];
	NSDictionary *params = @{@"pageSize":@(kPageSize), @"pageNumber":@(self.pageNumber+1)};
	[self sendWithPath:kAPI_liveRooms params:params succ:^(id  _Nonnull data) {
		
		[[HUDHelper sharedInstance] stopLoading:hud];
		
		NSArray *list = [NSArray yy_modelArrayWithClass:[K18Live class] json:data[@"list"]];
		
		K18Live *m1 = wself.dataSource.lastObject;
		K18Live *m2 = list.lastObject;
		
		
		if (!m2) {
			wself.collectionView.mj_footer = nil;
			callback();
			return;
		}
		
		if ([m1.id isEqualToString:m2.id]) {
			wself.collectionView.mj_footer = nil;
			[[HUDHelper sharedInstance] tipMessage:@"没有更多了"];
			callback();
			return;
		}
				
		
		
		if (wself.dataSource.count) {
			[wself.dataSource addObjectsFromArray:list];
			wself.pageNumber++;
		}else{
			wself.dataSource = list.mutableCopy;
			wself.pageNumber = 1;
		}
		
		[wself.collectionView reloadData];
		
        if (list.count < kPageSize) {
            wself.collectionView.mj_footer = nil;
        }else{
            [wself addFooter];
        }
		
		callback();
	} failed:^(NSError * _Nonnull error) {
		callback();
		[[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
	}];
}

- (void) addFooter {
	__weak typeof(self)wself = self;
	self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
		
		[wself requestData:^{
			[wself.collectionView.mj_footer endRefreshing];
		}];
		
	}];
}

- (void) reqIsApproved {
//    __weak typeof(self)wself = self;
//    id hud = [[HUDHelper sharedInstance] loading];
    [self sendWithPath:kAPI_anchorAuthentication params:@{@"cyUserId":k18_loginUserId} succ:^(id  _Nonnull data) {
        //"cyExamine": -1,  //-1，未提交 0,待审核，1，审核通过  2，审核不通过
        [K18UserModel shared].authorStatus = data[@"cyExamine"];
        
    } failed:^(NSError * _Nonnull error) {
        
    }];
}


- (void)initData
{
//	self.dataSource = [NSMutableArray array];
//	//test
//	for (int i=0; i<11; i++) {
//		[self.dataSource addObject:[K18Live testNew]];
//	}

}


#pragma mark - Actions

- (void) startLive {
    if ([K18UserModel shared].authorStatus.integerValue==1) {//审核通过
        NSLog(@"进入开播页面");
        [K18LiveService showPushSettingView:self];
    }else{        //
        K18AuthenticationController *vc = [[K18AuthenticationController alloc]initWithNibName:@"K18AuthenticationController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}

- (void) searchAction {
	[K18LiveService gotoSearchController:nil parent:self];
}

#pragma mark -  UICollectionViewDelegate & UICollectionViewDataSource

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
	K18LiveCell *cell = (K18LiveCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
	cell.model = self.dataSource[indexPath.row];
	return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return self.dataSource.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	
}

@end
