//
//  K18AuthenticationController.m
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AuthenticationController.h"
#import "K18Img.h"

@interface K18AuthenticationController ()
@property (weak, nonatomic) IBOutlet UITextField *nameL;
@property (weak, nonatomic) IBOutlet UITextField *cardL;
@property (weak, nonatomic) IBOutlet UIButton *foreButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *requestButton;

@property(strong, nonatomic) UIImage *foreImage;
@property(strong, nonatomic) UIImage *backImage;

@property(strong, nonatomic) K18Img *foreImg;
@property(strong, nonatomic) K18Img *backImg;

@property(strong, nonatomic) id hud;
@end

@implementation K18AuthenticationController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"主播认证";
}


#pragma mark - Actions

- (IBAction)requestAction:(id)sender {
	
	if (!self.foreImage || !self.backImage) {
		[[HUDHelper sharedInstance] tipMessage:@"请选择证件"];
		return;
	}
	
	NSString *name = self.nameL.text;
	NSString *card = self.cardL.text;
	if(!name.length){
		[[HUDHelper sharedInstance] tipMessage:@"请输入姓名"];
		return;
	}
	if(!card.length){
		[[HUDHelper sharedInstance] tipMessage:@"请输入身份证"];
		return;
	}
	
	if (!card.isIdCard) {
		[[HUDHelper sharedInstance] tipMessage:@"身份证格式不正确"];
		return;
	}
	
	self.hud = [[HUDHelper sharedInstance] loading];
	__weak typeof(self)wself = self;
	
	NSLog(@"正在上传第一张图");
	NSData *data = UIImagePNGRepresentation(self.foreImage);
    [self uploadImage:data fileName:@"foreImage" userId:k18_loginUserId cyType:5 success:^(id  _Nonnull response) {
        
        K18Img *model = [K18Img yy_modelWithDictionary:response];
        
        wself.backImg = model;
        
        [wself sendNextImage];
        
	} failure:^(NSError * _Nonnull err) {
		[[HUDHelper sharedInstance] stopLoading:wself.hud message:err.localizedDescription];
	}];
	
	
}

- (void) sendNextImage {
	NSLog(@"正在上传第二张图");
	__weak typeof(self)wself = self;
	NSData *data = UIImagePNGRepresentation(self.backImage);
    [self uploadImage:data fileName:@"backImage" userId:k18_loginUserId cyType:5 success:^(id  _Nonnull response) {
        
        K18Img *model = [K18Img yy_modelWithDictionary:response];
        
        wself.foreImg = model;
        
        [wself sendNextRequest];
        
    } failure:^(NSError * _Nonnull err) {
        [[HUDHelper sharedInstance] stopLoading:wself.hud message:err.localizedDescription];
	}];
}

- (void) sendNextRequest {
	__weak typeof(self)wself = self;
	NSString *name = self.nameL.text;
	NSString *card = self.cardL.text;
	NSString *userId = k18_loginUserId;
	NSString *imgids = [NSString stringWithFormat:@"%@_%@",wself.foreImg.id,wself.backImg.id];
	NSAssert(self.foreImg && self.backImg, @"上传的证件图片id异常");
	NSDictionary *params = @{@"cyUserId":notNil(userId),
							 @"cyReallyName":name,
							 @"cyIdCard":card,
							 @"cyIdCardImgs":imgids,
							 };
	[self sendWithPath:kAPI_doAnchorAuthentication params:params succ:^(id  _Nonnull data) {
		[[HUDHelper sharedInstance] stopLoading:wself.hud message:data[@"msg"] delay:2 completion:^{
			
		}];
		
	} failed:^(NSError * _Nonnull error) {
		[[HUDHelper sharedInstance] stopLoading:wself.hud message:error.localizedDescription];
	}];
}

- (IBAction)addForeAction:(id)sender {
	__weak typeof(self)wself = self;
	[self chooseImage:^(UIImage * _Nonnull image, PHAsset * _Nonnull asset) {
		wself.foreImg = nil;//重新上传新图，清理上次上传返回的缓存
		wself.foreImage = image;
		[wself.foreButton setBackgroundImage:image forState:(UIControlStateNormal)];
	}];
}

- (IBAction)addBackAction:(id)sender {
	__weak typeof(self)wself = self;
	[self chooseImage:^(UIImage * _Nonnull image, PHAsset * _Nonnull asset) {
		wself.backImg = nil;
		wself.backImage = image;
		[wself.backButton setBackgroundImage:image forState:(UIControlStateNormal)];
	}];
}


@end
