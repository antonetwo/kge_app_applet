//
//  K18MusicListController.h
//  18kcy
//
//  Created by beuady on 2019/8/27.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18TableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18MusicListController : K18TableController

@property(copy, nonatomic) void(^didSelectItem)(NSURL *url);

@end

NS_ASSUME_NONNULL_END
