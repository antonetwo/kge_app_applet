//
//  BeautyView.m
//  18kcy
//
//  Created by beuady on 2019/8/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "BeautyView.h"
#import "V8HorizontalPickerView.h"
#import "K18Common.h"
#import "TCConstants.h"

@interface BeautyView()

@end

@implementation BeautyView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)setHidden:(BOOL)hidden
{
    [super setHidden:hidden];
}

- (void) initUI {
    float   beauty_center_y = self.height - 40;
    
    //美颜拉杆浮层
    float   beauty_btn_width  = 85;
    float   beauty_btn_height = 19;
#if POD_PITU
    float   beauty_btn_count  = 4;
#else
    float   beauty_btn_count  = 2;
#endif
    float   beauty_center_interval = (self.width - 30*2 - beauty_btn_width)/(beauty_btn_count - 1);
    float   first_beauty_center_x  = 30 + beauty_btn_width/2;
    int ib = 0;
    
    _beautyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _beautyBtn.center = CGPointMake(first_beauty_center_x, beauty_center_y);
    _beautyBtn.bounds = CGRectMake(0, 0, beauty_btn_width, beauty_btn_height);
    [_beautyBtn setImage:[UIImage imageNamed:@"white_beauty"] forState:UIControlStateNormal];
    [_beautyBtn setImage:[[UIImage imageNamed:@"white_beauty_press"] iTintColor:k18_TopicC] forState:UIControlStateSelected];
    [_beautyBtn setTitle:@"美颜" forState:UIControlStateNormal];
    [_beautyBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_beautyBtn setTitleColor:k18_TopicC forState:UIControlStateSelected];
    _beautyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    _beautyBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    _beautyBtn.tag = 0;
    _beautyBtn.selected = YES;
//    [_beautyBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    ib++;
    
    _filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _filterBtn.center = CGPointMake(first_beauty_center_x + ib*beauty_center_interval, beauty_center_y);
    _filterBtn.bounds = CGRectMake(0, 0, beauty_btn_width, beauty_btn_height);
    [_filterBtn setImage:[UIImage imageNamed:@"beautiful"] forState:UIControlStateNormal];
    [_filterBtn setImage:[[UIImage imageNamed:@"beautiful_press"] iTintColor:k18_TopicC] forState:UIControlStateSelected];
    [_filterBtn setTitle:@"滤镜" forState:UIControlStateNormal];
    [_filterBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_filterBtn setTitleColor:k18_TopicC forState:UIControlStateSelected];
    _filterBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    _filterBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    _filterBtn.tag = 1;
//    [_filterBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    ib++;
    
    UIView *line = [UIView new];
    line.frame = CGRectMake(0, _beautyBtn.top-20, self.width, 0.5);
    line.backgroundColor = RGBOF(0xcccccc);
    [self addSubview:line];
    
#if POD_PITU
    _motionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _motionBtn.center = CGPointMake(first_beauty_center_x + ib*beauty_center_interval, beauty_center_y);
    _motionBtn.bounds = CGRectMake(0, 0, beauty_btn_width, beauty_btn_height);
    [_motionBtn setImage:[UIImage imageNamed:@"move"] forState:UIControlStateNormal];
    [_motionBtn setImage:[UIImage imageNamed:@"move_press"] forState:UIControlStateSelected];
    [_motionBtn setTitle:@"动效" forState:UIControlStateNormal];
    [_motionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_motionBtn setTitleColor:UIColorFromRGB(0x0ACCAC) forState:UIControlStateSelected];
    _motionBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    _motionBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    _motionBtn.tag = 2;
//    [_motionBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    ib++;
    
    _greenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _greenBtn.center = CGPointMake(first_beauty_center_x + ib*beauty_center_interval, beauty_center_y);
    _greenBtn.bounds = CGRectMake(0, 0, beauty_btn_width, beauty_btn_height);
    [_greenBtn setImage:[UIImage imageNamed:@"greens"] forState:UIControlStateNormal];
    [_greenBtn setImage:[UIImage imageNamed:@"greens_press"] forState:UIControlStateSelected];
    [_greenBtn setTitle:@"绿幕" forState:UIControlStateNormal];
    [_greenBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_greenBtn setTitleColor:UIColorFromRGB(0x0ACCAC) forState:UIControlStateSelected];
    _greenBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    _greenBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    _greenBtn.tag = 3;
//    [_greenBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    ib++;
#endif
    
    
#if POD_PITU
    _beautyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,  _beautyBtn.top - 40, 40, 20)];
#else
    _beautyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,  _beautyBtn.top - 95, 40, 20)];
#endif
    _beautyLabel.text = @"美颜";
    _beautyLabel.font = [UIFont systemFontOfSize:12];
    _sdBeauty = [[UISlider alloc] init];
#if POD_PITU
    _sdBeauty.frame = CGRectMake(_beautyLabel.right, _beautyBtn.top - 40, self.width / 2 - _beautyLabel.right - 7, 20);
#else
    _sdBeauty.frame = CGRectMake(_beautyLabel.right, _beautyBtn.top - 95, self.width - _beautyLabel.right - 10, 20);
#endif
    _sdBeauty.minimumValue = 0;
    _sdBeauty.maximumValue = 9;
    _sdBeauty.value = 6.3;
    [_sdBeauty setThumbImage:[[UIImage imageNamed:@"slider"] iTintColor:k18_TopicC] forState:UIControlStateNormal];
    [_sdBeauty setMinimumTrackImage:[[UIImage imageNamed:@"green"] iTintColor:k18_TopicC] forState:UIControlStateNormal];
    [_sdBeauty setMaximumTrackImage:[UIImage imageNamed:@"gray"] forState:UIControlStateNormal];
//    [_sdBeauty addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    _sdBeauty.tag = 0;
    
#if POD_PITU
    _whiteLabel = [[UILabel alloc] initWithFrame:CGRectMake(_sdBeauty.right + 15, _beautyBtn.top - 40, 40, 20)];
#else
    _whiteLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _beautyBtn.top - 55, 40, 20)];
#endif
    _whiteLabel.text = @"美白";
    _whiteLabel.font = [UIFont systemFontOfSize:12];
    _sdWhitening = [[UISlider alloc] init];
#if POD_PITU
    _sdWhitening.frame =  CGRectMake(_whiteLabel.right, _beautyBtn.top - 40, self.width - _whiteLabel.right - 10, 20);
#else
    _sdWhitening.frame =  CGRectMake(_whiteLabel.right, _beautyBtn.top - 55, self.width - _whiteLabel.right - 10, 20);
#endif
    _sdWhitening.minimumValue = 0;
    _sdWhitening.maximumValue = 9;
    _sdWhitening.value = 6.0;
    //    [_sdWhitening setThumbImage:[UIImage imageNamed:@"slider"] forState:UIControlStateNormal];
    //    [_sdWhitening setMinimumTrackImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
    [_sdWhitening setThumbImage:[[UIImage imageNamed:@"slider"] iTintColor:k18_TopicC] forState:UIControlStateNormal];
    [_sdWhitening setMinimumTrackImage:[[UIImage imageNamed:@"green"] iTintColor:k18_TopicC] forState:UIControlStateNormal];
    [_sdWhitening setMaximumTrackImage:[UIImage imageNamed:@"gray"] forState:UIControlStateNormal];
//    [_sdWhitening addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    _sdWhitening.tag = 1;
    
#if POD_PITU
    _bigEyeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _sdBeauty.top - 60, 40, 20)];
    _bigEyeLabel.text = @"大眼";
    _bigEyeLabel.font = [UIFont systemFontOfSize:12];
    _sdBigEye = [[UISlider alloc] init];
    _sdBigEye.frame =  CGRectMake(_bigEyeLabel.right, _sdBeauty.top - 60, self.width / 2 - _bigEyeLabel.right - 7, 20);
    _sdBigEye.minimumValue = 0;
    _sdBigEye.maximumValue = 9;
    _sdBigEye.value = 0;
    //    [_sdBigEye setThumbImage:[UIImage imageNamed:@"slider"] forState:UIControlStateNormal];
    //    [_sdBigEye setMinimumTrackImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
    [_sdBigEye setThumbImage:[[UIImage imageNamed:@"slider"] iTintColor:k18_TopicC] forState:UIControlStateNormal];
    [_sdBigEye setMinimumTrackImage:[[UIImage imageNamed:@"green"] iTintColor:k18_TopicC] forState:UIControlStateNormal];
    [_sdBigEye setMaximumTrackImage:[UIImage imageNamed:@"gray"] forState:UIControlStateNormal];
//    [_sdBigEye addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    _sdBigEye.tag = 2;
    
    _slimFaceLabel = [[UILabel alloc] initWithFrame:CGRectMake(_sdBigEye.right + 15, _sdBeauty.top - 60, 40, 20)];
    _slimFaceLabel.text = @"瘦脸";
    _slimFaceLabel.font = [UIFont systemFontOfSize:12];
    _sdSlimFace = [[UISlider alloc] init];
    _sdSlimFace.frame =  CGRectMake(_slimFaceLabel.right, _sdBeauty.top - 60, self.width - _slimFaceLabel.right - 10, 20);
    _sdSlimFace.minimumValue = 0;
    _sdSlimFace.maximumValue = 9;
    _sdSlimFace.value = 0;
    [_sdSlimFace setThumbImage:[UIImage imageNamed:@"slider"] forState:UIControlStateNormal];
    [_sdSlimFace setMinimumTrackImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
    [_sdSlimFace setMaximumTrackImage:[UIImage imageNamed:@"gray"] forState:UIControlStateNormal];
//    [_sdSlimFace addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    _sdSlimFace.tag = 3;
    
#if ENABLE_LOG
    _tmplBar = [[MCCameraDynamicView alloc] initWithFrame:CGRectMake(0.f, 0, self.width, 115.f)];
#else
    _tmplBar = [[MCCameraDynamicView alloc] initWithFrame:CGRectMake(0.f, 0, self.width, 105.f)];
#endif
    _tmplBar.delegate = self;
    _tmplBar.hidden = YES;
    [self addSubview:_tmplBar];
    // 美颜默认是3
    _sdBeauty.minimumValue = 0;
    _sdBeauty.maximumValue = 9;
    _sdBeauty.value = 6.3;
    
    // TODO: pitu无美白接口
    _sdWhitening.minimumValue = 0;
    _sdWhitening.maximumValue = 9;
    _sdWhitening.value = 6.0;
    
    _greenPickerView = [[V8HorizontalPickerView alloc] initWithFrame:CGRectMake(0, _beautyBtn.top - 96, self.width, 66)];
    _greenPickerView.selectedTextColor = [UIColor blackColor];
    _greenPickerView.textColor = [UIColor grayColor];
    _greenPickerView.elementFont = [UIFont fontWithName:@"" size:14];
//    _greenPickerView.delegate = self;
//    _greenPickerView.dataSource = self;
    _greenPickerView.hidden = YES;
    _greenPickerView.selectedMaskView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"greens_selected.png"]];
    _greenIndex = 0;
#endif
    
    _filterPickerView = [[V8HorizontalPickerView alloc] initWithFrame:CGRectMake(0, 10, self.width , 90)];
    
    _filterPickerView.textColor = [UIColor grayColor];
    _filterPickerView.elementFont = [UIFont fontWithName:@"" size:14];
//    _filterPickerView.delegate = self;
//    _filterPickerView.dataSource = self;
    _filterPickerView.hidden = YES;
    
    UIImageView *sel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_selected"]];
    
    _filterPickerView.selectedMaskView = sel;    
    
    
    [self addSubview:_beautyLabel];
    [self addSubview:_whiteLabel];
    [self addSubview:_sdWhitening];
    [self addSubview:_sdBeauty];
    [self addSubview:_beautyBtn];
    [self addSubview:_bigEyeLabel];
    [self addSubview:_sdBigEye];
    [self addSubview:_slimFaceLabel];
    [self addSubview:_sdSlimFace];
    [self addSubview:_filterPickerView];
    [self addSubview:_filterBtn];
    [self addSubview:_motionBtn];
    [self addSubview:_greenPickerView];
    [self addSubview:_greenBtn];
    
    [self.beautyBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    [self.filterBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    [self.motionBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
    [self.greenBtn addTarget:self action:@selector(selectBeauty:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)selectBeauty:(UIButton *)button{
    switch (button.tag) {
        case 0://美颜
        {
            _sdWhitening.hidden = NO;
            _sdBeauty.hidden    = NO;
            _beautyLabel.hidden = NO;
            _whiteLabel.hidden  = NO;
            _bigEyeLabel.hidden = NO;
            _sdBigEye.hidden    = NO;
            _slimFaceLabel.hidden = NO;
            _sdSlimFace.hidden    = NO;
            _beautyBtn.selected  = YES;
            _filterBtn.selected = NO;
            _motionBtn.selected = NO;
            _greenBtn.selected  = NO;
            _tmplBar.hidden = YES;
            _filterPickerView.hidden = YES;
            _greenPickerView.hidden = YES;
            self.frame = CGRectMake(0, self.superview.height-170, self.superview.width, 170);
        }
            break;
        case 1://滤镜
        {
            _sdWhitening.hidden = YES;
            _sdBeauty.hidden    = YES;
            _beautyLabel.hidden = YES;
            _whiteLabel.hidden  = YES;
            _bigEyeLabel.hidden = YES;
            _sdBigEye.hidden    = YES;
            _slimFaceLabel.hidden = YES;
            _sdSlimFace.hidden    = YES;
            _beautyBtn.selected  = NO;
            _filterBtn.selected = YES;
            _motionBtn.selected = NO;
            _greenBtn.selected  = NO;
            _tmplBar.hidden = YES;
            _filterPickerView.hidden = NO;
            _greenPickerView.hidden = YES;
            [_filterPickerView scrollToElement:_filterType animated:NO];
        }
            break;
        case 2: {//动效
            _sdWhitening.hidden = YES;
            _sdBeauty.hidden    = YES;
            _beautyLabel.hidden = YES;
            _whiteLabel.hidden  = YES;
            _bigEyeLabel.hidden = YES;
            _sdBigEye.hidden    = YES;
            _slimFaceLabel.hidden = YES;
            _sdSlimFace.hidden    = YES;
            _beautyBtn.selected  = NO;
            _filterBtn.selected = NO;
            _motionBtn.selected = YES;
            _greenBtn.selected  = NO;
            _tmplBar.hidden = NO;
            _filterPickerView.hidden = YES;
            _greenPickerView.hidden = YES;
        }
            break;
        case 3: {//绿幕"
            _sdWhitening.hidden = YES;
            _sdBeauty.hidden    = YES;
            _beautyLabel.hidden = YES;
            _whiteLabel.hidden  = YES;
            _bigEyeLabel.hidden = YES;
            _sdBigEye.hidden    = YES;
            _slimFaceLabel.hidden = YES;
            _sdSlimFace.hidden    = YES;
            _beautyBtn.selected  = NO;
            _filterBtn.selected = NO;
            _motionBtn.selected = NO;
            _greenBtn.selected  = YES;
            _tmplBar.hidden = YES;
            _filterPickerView.hidden = YES;
            _greenPickerView.hidden = NO;
            [_greenPickerView scrollToElement:_greenIndex animated:NO];
        }
        default:
            break;
    }
    _beautyBtn.center = CGPointMake(_beautyBtn.center.x, self.frame.size.height - 35);
    _filterBtn.center = CGPointMake(_filterBtn.center.x, self.frame.size.height - 35);
    _greenBtn.center = CGPointMake(_greenBtn.center.x, self.frame.size.height - 35);
    _motionBtn.center = CGPointMake(_motionBtn.center.x, self.frame.size.height - 35);
}

@end
