//
//  BeautyView.h
//  18kcy
//
//  Created by beuady on 2019/8/4.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCConstants.h"

@class V8HorizontalPickerView;

NS_ASSUME_NONNULL_BEGIN
/**
 UIButton              *_beautyBtn;
 UIButton              *_filterBtn;
 
 UILabel               *_beautyLabel;
 UILabel               *_whiteLabel;
 UILabel               *_bigEyeLabel;
 UILabel               *_slimFaceLabel;
 
 UISlider              *_sdBeauty;
 UISlider              *_sdWhitening;
 UISlider              *_sdBigEye;
 UISlider              *_sdSlimFace;
 */
@interface BeautyView : UIView
{
    @public
    NSInteger    _filterType;
    NSInteger    _greenIndex;
}
@property(strong, nonatomic) UILabel *beautyLabel;
@property(strong, nonatomic) UILabel *whiteLabel;
@property(strong, nonatomic) UISlider *sdWhitening;
@property(strong, nonatomic) UISlider *sdBeauty;
@property(strong, nonatomic) UIButton *beautyBtn;
@property(strong, nonatomic) UIButton *filterBtn;
@property(strong, nonatomic) UILabel *bigEyeLabel;
@property(strong, nonatomic) UISlider *sdBigEye;
@property(strong, nonatomic) UILabel *slimFaceLabel;
@property(strong, nonatomic) UISlider *sdSlimFace;
@property(strong, nonatomic) V8HorizontalPickerView *filterPickerView;
@property(strong, nonatomic) UIButton *motionBtn;
@property(strong, nonatomic) V8HorizontalPickerView *greenPickerView;
@property(strong, nonatomic) UIButton *greenBtn;
#if POD_PITU
@property(strong, nonatomic) MCCameraDynamicView   *tmplBar;
#else
@property(strong, nonatomic) UIView                *tmplBar;
#endif

@end

NS_ASSUME_NONNULL_END
