//
//  TCPushPrepareViewController.h
//  TCLVBIMDemo
//
//  Created by annidyfeng on 16/8/3.
//  Copyright © 2016年 tencent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "K18BaseController.h"

/**
 *  推流设置页，设置推流的封面、标题及定位功能
 */
@interface TCPushPrepareViewController : K18BaseController

@end
