//
//  K18MusicListController.m
//  18kcy
//
//  Created by beuady on 2019/8/27.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18MusicListController.h"
#import "K18Music.h"

@interface K18MusicListController ()
@property NSInteger selectIndex;
@end

@implementation K18MusicListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择背景音乐";
    self.selectIndex = -1;
    self.noneTipL.text = @"暂无音乐";
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorStyle = UITableViewSeparatorInsetFromAutomaticInsets;
    
    self.navigationItem.leftBarButtonItem = self.leftBarBackItem;
    
    [self refreshData];
}

- (void)leftBarButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDelegate & DataSourceDelegate
//yulan_xuanze
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
    }
    K18Music *m = self.dataSource[indexPath.row];
    cell.textLabel.text = m.cySongName;
    cell.detailTextLabel.text = m.cySinger;
    if (indexPath.row==self.selectIndex) {
        cell.imageView.image = [UIImage imageNamed:@"yulan_xuanze"];
    }else{
        cell.imageView.image = nil;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectIndex = indexPath.row;
    [self.tableView reloadData];
    K18Music *m = self.dataSource[indexPath.row];
    if (self.didSelectItem) {
        NSString *url = m.cySongUrl ? m.cySongUrl : @"";
        self.didSelectItem([NSURL URLWithString:url]);
    }
}

- (void)refreshData
{
    __weak typeof(self)wself = self;
    NSDictionary *params = @{
                             @"cyUserId":notNil(k18_loginUserId)
                             };
    [self sendWithPath:kAPI_getSongs params:params succ:^(id  _Nonnull data) {
        NSMutableArray *list = [NSArray yy_modelArrayWithClass:[K18Music class] json:data].mutableCopy;
        wself.dataSource = list;
        [wself.tableView reloadData];
        [wself.tableView.mj_header endRefreshing];
        wself.noneTipL.hidden = self.dataSource.count!=0;
    } failed:^(NSError * _Nonnull error) {
        [wself.tableView.mj_header endRefreshing];
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

@end
