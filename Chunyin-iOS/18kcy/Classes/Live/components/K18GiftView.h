//
//  K18GiftView.h
//  18kcy
//
//  Created by beuady on 2019/8/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCLiveListModel.h"
NS_ASSUME_NONNULL_BEGIN



@protocol K18GiftViewDelegate <NSObject>

- (void) onSendFlowerSuccess:(int)sendCount;

@end

/**
 选择礼物发送的界面
 */
@interface K18GiftView : UIControl

@property(strong, nonatomic) TCLiveInfo *model;

@property(weak, nonatomic) id<K18GiftViewDelegate> giftDelegate;

- (instancetype) initWithModel:(TCLiveInfo *)model;
- (instancetype) initWithTargetId:(NSString *)targetId targetName:(NSString *)targetName;
- (instancetype) initWithTargetId:(NSString *)targetId targetName:(NSString *)targetName songId:(NSString*)songId;
- (void) show;
- (void) showWithModel:(TCLiveInfo*)model;

- (void) dismiss;





@end

NS_ASSUME_NONNULL_END
