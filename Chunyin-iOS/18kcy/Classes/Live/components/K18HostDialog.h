//
//  K18HostDialog.h
//  18kcy
//
//  Created by 唐 on 2019/8/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCLiveListModel.h"
NS_ASSUME_NONNULL_BEGIN


/**
 主播信息
 */
@interface K18HostDialog : UIControl
@property(strong, nonatomic) UIView *centerView;
@property(strong, nonatomic) UIImageView *iconView;
@property(strong, nonatomic) UILabel *nameL;
@property(strong, nonatomic) UILabel *levelL;
@property(strong, nonatomic) UILabel *roomIdL;
@property(strong, nonatomic) UIView	 *line;
@property(strong, nonatomic) UILabel *roomL;
@property(strong, nonatomic) UIView	 *line2;
@property(strong, nonatomic) UIButton *guanzhuBtn;
@property(strong, nonatomic) UIButton *sixinBtn;
@property(strong, nonatomic) UIButton *dashangBtn;

@property(strong, nonatomic) UIButton *tousuBtn;


@property(strong, nonatomic) TCLiveInfo *model;

+ (instancetype) shared;

- (void) showByIsHost:(BOOL)isHost;//主播打开，还是观众打开

- (void) dismiss;

- (void) reqIsFollow:(void(^)(NSError*err)) completion;

@end

NS_ASSUME_NONNULL_END
