//
//  K18GiftCell.h
//  18kcy
//
//  Created by beuady on 2019/8/6.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface K18GiftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameL;

@end

NS_ASSUME_NONNULL_END
