//
//  K18UserDialog.m
//  18kcy
//
//  Created by 唐 on 2019/8/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18UserDialog.h"
#import "K18Common.h"
#import "AppDelegate.h"
#import "K18LiveService.h"
#import "K18ChatController.h"
#import "TUIConversationCellData.h"
#import "K18ChatController.h"


@interface K18UserDialog()
@property(strong, nonatomic) UIView *centerView;
@property(assign, nonatomic) BOOL isFollow;//是否关注
@property(assign, nonatomic) BOOL reqSuc;
@end

@implementation K18UserDialog

+ (instancetype) shared
{
	static dispatch_once_t once;
	static id instance;
	dispatch_once(&once, ^{
		instance = [self new];
	});
	return instance;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self initUI];
        [self addTarget:self action:@selector(close) forControlEvents:(UIControlEventTouchUpInside)];
        
	}
	return self;
}

- (void) close {
	[self dismiss];
}

- (void)tmpAction{
    
}

- (void) initUI {
	
//laheiBtn
	self.frame = [UIScreen mainScreen].bounds;
	
	self.backgroundColor = RGBAOF(0, 0.3);
	_centerView = [UIView new];
	_centerView.backgroundColor = RGBAOF(0xffffff, 0.8);
	_centerView.layer.cornerRadius = 8;
	[self addSubview:_centerView];
    UITapGestureRecognizer *tmpTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tmpAction)];
    [_centerView addGestureRecognizer:tmpTap];
	
	_iconView = [[UIImageView alloc] init];
	[self.centerView addSubview:_iconView];
	_iconView.layer.cornerRadius = 73/2.0;
	_iconView.layer.masksToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetailView)];
    [_iconView addGestureRecognizer:tap];
    _iconView.userInteractionEnabled = YES;
	
	_titleL = [UILabel new];
	_titleL.font = [UIFont systemFontOfSize:18];
	_titleL.textColor = RGBOF(0x8A3CE8);
	_titleL.textAlignment = NSTextAlignmentCenter;
	[self.centerView addSubview:_titleL];
	
	_laheiBtn = [UIButton new];
	[_laheiBtn setTitle:@"拉黑" forState:(UIControlStateNormal)];
	_laheiBtn.titleLabel.font = [UIFont systemFontOfSize:12];
	[_laheiBtn setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
	[self.centerView addSubview:_laheiBtn];
	
	_guanzhuBtn = [UIButton new];
	[_guanzhuBtn setImage:[UIImage imageNamed:@"guanzhu"] forState:(UIControlStateNormal)];
	[_guanzhuBtn setImage:[UIImage imageNamed:@"quxiaoguanzhu"] forState:(UIControlStateSelected)];
	[self.centerView addSubview:_guanzhuBtn];
	
	_sixinBtn = [UIButton new];
	[_sixinBtn setImage:[UIImage imageNamed:@"sixin"] forState:(UIControlStateNormal)];
	[self.centerView addSubview:_sixinBtn];
	
	_dashangBtn = [UIButton new];
	[_dashangBtn setImage:[UIImage imageNamed:@"dashaang"] forState:(UIControlStateNormal)];
	[self.centerView addSubview:_dashangBtn];
	
	
	[_centerView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(329, 212));
		make.center.equalTo(self);
	}];
	
	[_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.mas_equalTo(25);
		make.centerX.equalTo(self.centerView);
		make.size.mas_equalTo(CGSizeMake(73, 73));
	}];
	
	[_titleL mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(300, 17));
		make.top.equalTo(self.iconView.mas_bottom).offset(10);
		make.centerX.equalTo(self.centerView);
	}];
	
	[_sixinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(76, 26));
		make.centerX.equalTo(self.centerView);
		make.top.equalTo(self.titleL.mas_bottom).offset(36);
	}];
	
	[_guanzhuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(76, 26));
		make.centerY.equalTo(self.sixinBtn);
		make.right.equalTo(self.sixinBtn.mas_left).offset(-20);
	}];
	
	[_dashangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(76, 26));
		make.centerY.equalTo(self.sixinBtn);
		make.left.equalTo(self.sixinBtn.mas_right).offset(20);
	}];
	
	[_laheiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(44, 44));
		make.top.equalTo(self.centerView).offset(10);
		make.right.equalTo(self.centerView).offset(-10);
	}];
	
    [self addEvents];
}

- (void) addEvents {
    [_laheiBtn addTarget:self action:@selector(laheiA) forControlEvents:(UIControlEventTouchUpInside)];
    [_sixinBtn addTarget:self action:@selector(sixinA) forControlEvents:(UIControlEventTouchUpInside)];
    [_guanzhuBtn addTarget:self action:@selector(guanzhuA) forControlEvents:(UIControlEventTouchUpInside)];
    [_dashangBtn addTarget:self action:@selector(dashangA) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)setModel:(TCUserInfoData *)model
{
	_model = model;
	_titleL.text = model.nickName;
	UIImage *img = [UIImage imageNamed:k18_DefaultIcon];
	[_iconView sd_setImageWithURL:[NSURL URLWithString:model.faceURL] placeholderImage:img];
	
	//设置不可对自己卡片编辑
	if ([model.identifier isEqualToString:K18UserModel.shared.liveAccount]) {//自己的话
		for (UIView *each in self.centerView.subviews) {
			if ([each isKindOfClass:[UIButton class]]) {
				each.hidden = YES;
			}
		}
	}else{
		for (UIView *each in self.centerView.subviews) {
			if ([each isKindOfClass:[UIButton class]]) {
				each.hidden = NO;
			}
		}
	}
    

    self.laheiBtn.hidden = !self.isHost;
    
    
    [self reqIsFollow:nil];
}

- (void)showWithIsHost:(BOOL)isHost
{
    self.isHost = isHost;
	AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[del.window addSubview:self];
}

- (void)dismiss
{
	[self removeFromSuperview];
}

#pragma mark - Actions

- (void) guanzhuA {
    __weak typeof(self)wself = self;
    void(^reqGuanzhu)(void) = ^(){
        NSString *targetId = [self.model.identifier substringFromIndex:2];
        [K18UserModel doFollowForFollowId:targetId isFollow:!wself.isFollow completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            wself.isFollow = !wself.isFollow;
            if(wself.isFollow==YES){
                [[HUDHelper sharedInstance] syncStopLoadingMessage:@"关注成功" delay:1 completion:nil];
            }else{
                [[HUDHelper sharedInstance] syncStopLoadingMessage:@"取消关注成功" delay:1 completion:nil];
            }
            
            if (wself.isFollow) {
                self.guanzhuBtn.selected = YES;
            }else{
                self.guanzhuBtn.selected = NO;
            }
            
            
            
        }];
    };
    
    if (!self.reqSuc) {
        [self reqIsFollow:^(NSError *err) {
            reqGuanzhu();
        }];
    }else{
        reqGuanzhu();
    }
    
    
}

- (void) laheiA {
    [self dismiss];
    UIAlertController *avc = [UIAlertController alertControllerWithTitle:@"拉黑后用户将会退出直播间" message:nil preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *a1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    UIAlertAction *a2 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        NSString *targetId = [self.model.identifier substringFromIndex:2];
        [K18UserModel doLahei:k18_loginUserId targetID:targetId completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            
            if (!error) {
                [[HUDHelper sharedInstance] tipMessage:@"拉黑成功"];
                
                //交给服务端走im直接推送
//                [[NSNotificationCenter defaultCenter] postNotificationName:k18_laheiEvent object:self.model.identifier];
                
            }
            
        }];
        
    }];
    [avc addAction:a1];
    [avc addAction:a2];
    
    [[AppDelegate currentPresentestController] presentViewController:avc animated:YES completion:nil];
}

- (void) dashangA {
    [[NSNotificationCenter defaultCenter] postNotificationName:k18_dashangEvent object:self.model];
    [self dismiss];
}


- (void) sixinA {
    [self dismiss];
    TUIConversationCellData *data = [TUIConversationCellData new];
    data.convType = TIM_C2C;
    data.title = self.model.nickName;
    data.convId = self.model.identifier;
    data.avatarUrl = [NSURL URLWithString: self.model.faceURL];
    K18ChatController *vc = [[K18ChatController alloc]init];
    vc.conversationData = data;
    [[AppDelegate sharedAppDelegate] pushViewController:vc animated:YES];
    
}

- (void)gotoDetailView {
    [self dismiss];
    NSString *uid = [self.model.identifier substringFromIndex:2];
    [K18LiveService gotoPersonDetail:uid parent:nil];
}

#pragma mark - Req


- (void) reqIsFollow:(void(^)(NSError*err)) completion {
    static int reqing = NO;
    if(reqing){
        return;
    }
    reqing = YES;
    NSString *myId = k18_loginUserId;
    NSString *targetId = [self.model.identifier substringFromIndex:2];
    //    [[HUDHelper sharedInstance] syncLoading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_getFollow params:@{@"cyUserId":notNil(myId), @"cyOtherUserId":notNil(targetId)} succ:^(id  _Nonnull data) {
        reqing = NO;
        //        [[HUDHelper sharedInstance] syncStopLoading];
        wself.reqSuc = YES;
        NSLog(@"是否关注:%@",data);
        BOOL isFollow = [data[@"cyFollow"] boolValue];
        wself.isFollow = isFollow;
        if (wself.isFollow) {
            self.guanzhuBtn.selected = YES;
        }else{
            self.guanzhuBtn.selected = NO;
        }
        
        if(completion){
            completion(nil);
        }
        
    } failed:^(NSError * _Nonnull error) {
        reqing = NO;
        wself.reqSuc = NO;
        [[HUDHelper sharedInstance] syncStopLoadingMessage:error.localizedDescription delay:1 completion:nil];
        if(completion){
            completion(error);
        }
    }];
}

@end
