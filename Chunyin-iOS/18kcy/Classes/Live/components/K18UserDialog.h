//
//  K18UserDialog.h
//  18kcy
//
//  Created by 唐 on 2019/8/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCUserInfoModel.h"
NS_ASSUME_NONNULL_BEGIN


/**
 在线用户信息
 */
@interface K18UserDialog : UIControl

@property(strong, nonatomic) UIImageView *iconView;
@property(strong, nonatomic) UILabel *titleL;
@property(strong, nonatomic) UIButton *guanzhuBtn;
@property(strong, nonatomic) UIButton *sixinBtn;
@property(strong, nonatomic) UIButton *dashangBtn;

@property(strong, nonatomic) UIButton *laheiBtn;

@property(strong, nonatomic) TCUserInfoData *model;
@property(assign, nonatomic) BOOL isHost;//是否主播打开

+ (instancetype) shared;

- (void) showWithIsHost:(BOOL)isHost;

- (void) dismiss;

@end

NS_ASSUME_NONNULL_END
