//
//  K18GiftView.m
//  18kcy
//
//  Created by beuady on 2019/8/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GiftView.h"
#import "K18Common.h"
#import "IconButton.h"
#import "TCUserInfoModel.h"
@interface K18GiftView()
@property(nonatomic, strong) UILabel *tipLabel;
@property(strong, nonatomic) IconButton *giftView;
@property(strong, nonatomic) UILabel *leftLabel;
@property(strong, nonatomic) UISegmentedControl *segmentControl;
@property(strong, nonatomic) UIButton *okBtn;


@property(strong, nonatomic) NSArray *countDatas;

@property(strong, nonatomic) UIView *pView;
@property(assign, nonatomic) CGRect originFrame;

@property(strong, nonatomic) NSString *targetId;
@property(strong, nonatomic) NSString *targetName;
@property(strong, nonatomic) NSString *songId;
@end

@implementation K18GiftView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:k18_dashangEvent object:nil];
}

- (instancetype)initWithModel:(TCLiveInfo *)model
{
    self = [super init];
    if (self) {
        self.model = model;
        [self initUI];
    }
    return self;
}

- (instancetype)initWithTargetId:(NSString *)targetId targetName:(nonnull NSString *)targetName
{
    self = [super init];
    if (self) {
        self.targetName = targetName;
        self.targetId = targetId;
        [self initUI];
    }
    return self;
}

- (instancetype)initWithTargetId:(NSString *)targetId targetName:(NSString *)targetName songId:(NSString *)songId
{
    self = [super init];
    if (self) {
        self.targetName = targetName;
        self.targetId = targetId;
        self.songId = songId;
        [self initUI];
    }
    return self;
}

- (NSArray *)countDatas
{
    if (!_countDatas) {
        _countDatas = @[@1, @10, @20, @50, @66, @99];
    }
    return _countDatas;
}

- (void) initUI {
//    [self addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchUpInside)];
    [self addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchDown)];
    self.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dashangeEvent:) name:k18_dashangEvent object:nil];
    
    self.frame = CGRectMake(0, 0, kMainScreenWidth, kMainScreenHeight);
    self.backgroundColor = RGBAOF(0, 0.3);
        
    self.originFrame = CGRectMake(0, kMainScreenHeight - 250, kMainScreenWidth, 250);
    self.pView = [UIView new];
    _pView.frame = self.originFrame;
    _pView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.pView];
    
    _tipLabel = [UILabel new];
    _tipLabel.textColor = RGBOF(0x323232);
    _tipLabel.font = [UIFont systemFontOfSize:18];
    _tipLabel.textAlignment = NSTextAlignmentCenter;
    [self.pView addSubview:_tipLabel];
    
    UIView *line1 = [UIView new];
    line1.backgroundColor = RGBOF(0xcccccc);
    [self.pView addSubview:line1];
    
    _giftView = [IconButton new];
    [_giftView setImage:[UIImage imageNamed:@"icon_flower"] forState:(UIControlStateNormal)];
    [_giftView setTitle:@"拥有0" forState:(UIControlStateNormal)];
    _giftView.imageSize = CGSizeMake(60, 60);
    [_giftView setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
    _giftView.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.pView addSubview:_giftView];
    _giftView.userInteractionEnabled = NO;
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = RGBOF(0xcccccc);
    [self.pView addSubview:line2];

    
    _leftLabel = [UILabel new];
    _leftLabel.text = @"数量:";
    [self.pView addSubview:_leftLabel];
    
    _segmentControl = [[UISegmentedControl alloc]initWithParent:self.pView];
    for (int i=0; i<self.countDatas.count; i++) {
        [_segmentControl insertSegmentWithTitle:[self.countDatas[i] stringValue] atIndex:i animated:NO];
    }
    _segmentControl.tintColor = k18_TopicC;
    _segmentControl.selectedSegmentIndex = 0;
    
    _okBtn = [UIButton new];
    _okBtn.frame = CGRectMake(0, 0, 60, 30);
    [_okBtn setTitle:@"确定" forState:(UIControlStateNormal)];
    [self.pView addSubview:_okBtn];
    [_okBtn k18_setStyle];
    
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pView);
        make.width.equalTo(self.pView);
        make.height.mas_equalTo(60);
    }];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLabel.mas_bottom);
        make.width.equalTo(self.pView);
        make.height.mas_equalTo(0.5);
    }];
    
    [_giftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(75, 82));
        make.center.equalTo(self.pView);
    }];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLabel.mas_bottom);
        make.width.equalTo(self.pView);
        make.height.mas_equalTo(0.5);
    }];

    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.giftView.mas_bottom).offset(15);
        make.width.equalTo(self.pView);
        make.height.mas_equalTo(0.5);
    }];
    
    [_segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line2.mas_bottom);
        make.bottom.equalTo(self.pView.mas_bottomMargin).offset(-10);
        make.left.equalTo(self.leftLabel.mas_right).offset(10);
        make.right.equalTo(self.okBtn.mas_left).offset(-10);
    }];
    
    [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pView).offset(10);
        make.centerY.equalTo(self.segmentControl);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    [_okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.pView).offset(-10);
        make.centerY.equalTo(self.segmentControl);
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
    
   
    [self.okBtn addTarget:self action:@selector(sendFlowerA) forControlEvents:(UIControlEventTouchUpInside)];
}


- (void)setModel:(TCLiveInfo *)model
{
    _model = model;
    self.targetId = [self.model.userid substringFromIndex:2];
    self.targetName = self.model.userinfo.nickname;
}

- (void) updateUI {
    self.tipLabel.text = [NSString stringWithFormat:@"送礼物给%@",self.targetName];
    
    K18User *user = K18UserModel.shared.currentUserDetail;
    
    int num = user.cyFlowerNum.intValue;
    
    [_giftView setTitle:[NSString stringWithFormat:@"拥有%d",num] forState:(UIControlStateNormal)];
}

- (void)show
{
    [self.superview addSubview:self];
    //    [parentView addSubview:self];
    self.hidden = NO;
    [self updateUI];
    CGRect destFrame = self.originFrame;
    CGRect startFrame = self.originFrame;
    startFrame.origin.y += startFrame.size.height;
    self.pView.frame = startFrame;
    [UIView animateWithDuration:0.35 animations:^{
        self.pView.frame = destFrame;
    }];
}

- (void)showWithModel:(TCLiveInfo *)model
{
    self.model = model;
    [self show];
}

- (void)dismiss
{

    CGRect startFrame = self.originFrame;
    CGRect destFrame = self.originFrame;
    destFrame.origin.y += startFrame.size.height;
    [UIView animateWithDuration:0.35 animations:^{
        self.pView.frame = destFrame;
    } completion:^(BOOL finished) {
//        [self removeFromSuperview];
        self.hidden = YES;
        self.pView.frame = startFrame;
    }];
}

- (void) sendFlowerA {
    NSString *sendUserid = k18_loginUserId;
    NSString *recUserid = self.targetId;//[self.model.userid substringFromIndex:2];
    id hud = [[HUDHelper sharedInstance] loading];
    
    int count = 0;
    if (self.segmentControl.selectedSegmentIndex < self.countDatas.count) {
        count = [self.countDatas[self.segmentControl.selectedSegmentIndex] intValue];
    }
    
    __weak typeof(self)wself = self;
    if (self.songId==0) {
        [K18UserModel sendLiveFlowerWithRoomId:self.model.roomId sendUserid:sendUserid receiveUserid:recUserid flowers:count completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            if (error) {
                [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
                return;
            }
            int remainCount = K18UserModel.shared.currentUserDetail.cyFlowerNum.intValue;
            K18UserModel.shared.currentUserDetail.cyFlowerNum = @(remainCount - count).stringValue;
            [[HUDHelper sharedInstance] stopLoading:hud message:@"送花成功" delay:2 completion:nil];
            [wself dismiss];
            
            if(self.giftDelegate){
                [self.giftDelegate onSendFlowerSuccess:count];
            }
            
        }];
    }else{
        
        [K18UserModel sendSongFlowerWithSongId:self.songId recieveId:self.targetId flowers:count completion:^(id  _Nonnull params, NSError * _Nullable error) {
            
            if (error) {
                [[HUDHelper sharedInstance] stopLoading:hud message:error.localizedDescription];
                return;
            }
            int remainCount = K18UserModel.shared.currentUserDetail.cyFlowerNum.intValue;
            K18UserModel.shared.currentUserDetail.cyFlowerNum = @(remainCount - count).stringValue;
            [[HUDHelper sharedInstance] stopLoading:hud message:@"送花成功" delay:2 completion:nil];
            [wself dismiss];
            
            if(self.giftDelegate){
                [self.giftDelegate onSendFlowerSuccess:count];
            }
        }];
        
    }
}

- (void) dashangeEvent:(NSNotification*)notify {
    id m = notify.object;
    if ([m isKindOfClass:[TCLiveInfo class]]) {
        self.model = notify.object;
        [self show];
    }else{
        TCUserInfoData *infoData = notify.object;
        self.targetId = [infoData.identifier substringFromIndex:2];
        [self show];
        self.tipLabel.text = [NSString stringWithFormat:@"送礼物给%@",infoData.nickName];
    }
    
}


@end
