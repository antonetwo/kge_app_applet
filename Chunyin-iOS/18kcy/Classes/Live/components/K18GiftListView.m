//
//  K18GiftListView.m
//  18kcy
//
//  Created by beuady on 2019/8/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18GiftListView.h"
#import "K18Common.h"
#import "K18Live.h"
#import "K18GiftCell.h"

@interface K18GiftListView() <UITableViewDataSource,UITableViewDelegate>
@property(strong, nonatomic) UITableView *tableView;
@property(strong, nonatomic) UILabel *tipLabel;
@property(strong, nonatomic) UIView *pView;

@property(strong, nonatomic) TCLiveInfo *model;
@property(strong, nonatomic) NSMutableArray *dataSource;
@property(strong, nonatomic) UILabel *noTipLabel;

@property(assign, nonatomic) CGRect lastFrame;
@end

@implementation K18GiftListView

- (instancetype)initWithModel:(TCLiveInfo *)model
{
    self = [super init];
    if (self) {
        self.model = model;
        [self initUI];
        [self addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchUpInside)];
        [self addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchDown)];
        self.hidden = YES;

    }
    return self;
}

- (void) initUI {
    self.frame = CGRectMake(0, 0, kMainScreenWidth, kMainScreenHeight);
//    self.backgroundColor = RGBAOF(0xc23132, 1);
    
    self.pView = [UIView new];
    _pView.frame = CGRectMake(0, kMainScreenHeight - 250, kMainScreenWidth, 250);
    _pView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.pView];
    self.lastFrame = self.pView.frame;
    
    _tipLabel = [UILabel new];
    _tipLabel.textColor = RGBOF(0x323232);
    _tipLabel.font = [UIFont systemFontOfSize:18];
    _tipLabel.textAlignment = NSTextAlignmentCenter;
    _tipLabel.text = @"收到的礼物";
    [self.pView addSubview:_tipLabel];
    
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pView);
        make.width.equalTo(self.pView);
        make.height.mas_equalTo(50);
    }];
    
    _tableView = [[UITableView alloc]init];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _tableView.separatorColor = RGBOF(0xcccccc);
    _tableView.layer.borderColor = RGBOF(0xcccccc).CGColor;
    _tableView.layer.borderWidth = 0.5;
    [self.pView addSubview:_tableView];
    [_tableView registerNib:[UINib nibWithNibName:@"K18GiftCell" bundle:nil] forCellReuseIdentifier:@"K18GiftCell"];
    
    _noTipLabel = [UILabel new];
    _noTipLabel.textColor = RGBOF(0x323232);
    _noTipLabel.font = [UIFont systemFontOfSize:14];
    _noTipLabel.textAlignment = NSTextAlignmentCenter;
    _noTipLabel.text = @"暂无礼物";
    [self.pView addSubview:_noTipLabel];
    _noTipLabel.hidden = YES;
    
    [_noTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.tableView);
        make.center.equalTo(self.tableView);
    }];
    
   
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLabel.mas_bottom);
        make.left.equalTo(self.pView);
        make.right.equalTo(self.pView);
        make.bottom.equalTo(self.pView);
    }];
}

- (void)show
{
    [self.superview addSubview:self];
    self.hidden = NO;
    
    [self reqData];
    CGRect destFrame = self.lastFrame;
    CGRect startFrame = self.lastFrame;
    startFrame.origin.y += startFrame.size.height;
    self.pView.frame = startFrame;
    [UIView animateWithDuration:0.35 animations:^{
        self.pView.frame = destFrame;
    } completion:^(BOOL finished) {

    }];
}

- (void)dismiss
{
    CGRect startFrame = self.lastFrame;
    CGRect destFrame = self.lastFrame;
    destFrame.origin.y += startFrame.size.height;
    [UIView animateWithDuration:0.35 animations:^{
        self.pView.frame = destFrame;
    } completion:^(BOOL finished) {
        self.hidden = YES;
//        self.pView.frame = startFrame;
    }];
}

#pragma mark - Request

- (void) reqData {
    __weak typeof(self)wself = self;
    NSDictionary *params = @{@"liveRoomId":notNil(self.model.roomId)};
    [self sendWithPath:kAPI_liveRoomFlowers params:params succ:^(id  _Nonnull data) {
        
        wself.dataSource = [NSArray yy_modelArrayWithClass:[K18Gift class] json:data].mutableCopy;
        
        [wself.tableView reloadData];
        if (wself.dataSource.count==0) {
            wself.noTipLabel.hidden = NO;
        }else{
            wself.noTipLabel.hidden = YES;
        }
        
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] syncStopLoadingMessage:error.localizedDescription];
    }];
}

#pragma mark - UITableView dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18GiftCell *cell = [tableView dequeueReusableCellWithIdentifier:@"K18GiftCell"];
    K18Gift *model = self.dataSource[indexPath.row];
    cell.iconView.image = [UIImage imageNamed:@"songhua -xaunze"];
    cell.countLabel.text = [NSString stringWithFormat:@"+%@",model.cyTotalFlowerNum];
    cell.usernameL.text = model.userName;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

@end
