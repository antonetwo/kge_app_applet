//
//  K18GiftListView.h
//  18kcy
//
//  Created by beuady on 2019/8/5.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCLiveListModel.h"

NS_ASSUME_NONNULL_BEGIN


/**
 收到礼物的列表界面
 */
@interface K18GiftListView : UIControl



- (instancetype) initWithModel:(TCLiveInfo*)model;

- (void) show;

- (void) dismiss;

@end

NS_ASSUME_NONNULL_END
