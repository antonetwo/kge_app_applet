//
//  K18HostDialog.m
//  18kcy
//
//  Created by 唐 on 2019/8/1.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18HostDialog.h"
#import "K18Common.h"
#import "AppDelegate.h"
#import "K18UserModel.h"
#import "K18LiveService.h"
#import "K18ComplainController.h"
#import "TUIConversationCellData.h"
#import "K18ChatController.h"
#import "K18NavigationController.h"

@interface K18HostDialog()
@property(nonatomic) BOOL isHost;
@property(strong, nonatomic) UIView *maskCenterView;
@property(assign, nonatomic) BOOL isFollow;//是否关注
@property(assign, nonatomic) BOOL reqSuc;
@end

@implementation K18HostDialog

+ (instancetype) shared
{
	static dispatch_once_t once;
	static id instance;
	dispatch_once(&once, ^{
		instance = [self new];
	});
	return instance;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self initUI];

//        [self addTarget:self action:@selector(close) forControlEvents:(UIControlEventTouchUpInside)];
        
	}
	return self;
}

- (void) close {
	[self dismiss];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint p = [touch locationInView:self];
    if (!CGRectContainsPoint(self.centerView.frame, p) && p.x!=0) {
        [self close];
    }
}

- (void) tmpAction {
    
}

- (void) initUI {
	self.frame = [UIScreen mainScreen].bounds;
	
	self.backgroundColor = RGBAOF(0, 0.3);
	_centerView = [UIView new];
	_centerView.backgroundColor = RGBAOF(0xffffff, 0.8);
	_centerView.layer.cornerRadius = 8;
	_centerView.layer.masksToBounds = YES;
    [self addSubview:_centerView];
//    _centerView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tmpTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tmpAction)];
//    [_centerView addGestureRecognizer:tmpTap];
	
	_tousuBtn = [UIButton new];
	[_tousuBtn setTitle:@"投诉" forState:(UIControlStateNormal)];
	[_tousuBtn setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
	_tousuBtn.titleLabel.font = [UIFont systemFontOfSize:12];
	[self.centerView addSubview:_tousuBtn];

    
	
	_iconView = [[UIImageView alloc] init];
	[self.centerView addSubview:_iconView];
	_iconView.layer.cornerRadius = 73/2.0;
	_iconView.layer.masksToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetailView)];
    [_iconView addGestureRecognizer:tap];
    _iconView.userInteractionEnabled = YES;
	
	_nameL = [UILabel new];
	_nameL.font = [UIFont systemFontOfSize:18];
	_nameL.textColor = RGBOF(0x8A3CE8);
	_nameL.textAlignment = NSTextAlignmentCenter;
	[self.centerView addSubview:_nameL];
	
	_levelL = [UILabel new];
	_levelL.textColor = [UIColor whiteColor];
	_levelL.backgroundColor = RGBOF(0xfcc10f);
	_levelL.font = [UIFont systemFontOfSize:12];
	_levelL.layer.cornerRadius = 4;
	_levelL.layer.masksToBounds = YES;
	_levelL.textAlignment = NSTextAlignmentCenter;
	[self.centerView addSubview:_levelL];
	
	_roomIdL = [UILabel new];
	_roomIdL.textColor = RGBOF(0x323232);
	_roomIdL.font = [UIFont systemFontOfSize:18];
	_roomIdL.textAlignment = NSTextAlignmentCenter;
	[self.centerView addSubview:_roomIdL];
	
	_roomL = [UILabel new];
	_roomL.textColor = RGBOF(0x323232);
	_roomL.font = [UIFont systemFontOfSize:15];
	_roomL.numberOfLines = 0;
	_roomL.textAlignment = NSTextAlignmentCenter;
	[self.centerView addSubview:_roomL];
	
	_line = [UIView new];
	_line.backgroundColor = RGBOF(0xA0A0A0);
	[self.centerView addSubview:_line];
	_line2 = [UIView new];
	_line2.backgroundColor = RGBOF(0xA0A0A0);
	[self.centerView addSubview:_line2];
	
	_guanzhuBtn = [UIButton new];
	[_guanzhuBtn setImage:[UIImage imageNamed:@"guanzhu"] forState:(UIControlStateNormal)];
	[_guanzhuBtn setImage:[UIImage imageNamed:@"quxiaoguanzhu"] forState:(UIControlStateSelected)];
	[self.centerView addSubview:_guanzhuBtn];
	
	_sixinBtn = [UIButton new];
	[_sixinBtn setImage:[UIImage imageNamed:@"sixin"] forState:(UIControlStateNormal)];
	[self.centerView addSubview:_sixinBtn];
	
	_dashangBtn = [UIButton new];
	[_dashangBtn setImage:[UIImage imageNamed:@"dashaang"] forState:(UIControlStateNormal)];
	[self.centerView addSubview:_dashangBtn];
	
	[_centerView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(329, 358));//299
		make.center.equalTo(self);
	}];

	
	[_tousuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.right.equalTo(self.centerView).offset(-10);
		make.top.equalTo(self.centerView).offset(10);
		make.size.mas_equalTo(CGSizeMake(40, 30));
	}];
	
	[_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.mas_equalTo(25);
		make.centerX.equalTo(self.centerView);
		make.size.mas_equalTo(CGSizeMake(73, 73));
	}];
	
	[_nameL mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(300, 17));
		make.top.equalTo(self.iconView.mas_bottom).offset(10);
		make.centerX.equalTo(self.centerView);
	}];
	
	[_levelL mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(42, 20));
		make.top.equalTo(self.nameL.mas_bottom).offset(10);
		make.centerX.equalTo(self.centerView);
	}];
	
	[_roomIdL mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(260, 18));
		make.top.equalTo(self.levelL.mas_bottom).offset(20);
		make.centerX.equalTo(self.centerView);
	}];
	
	[_line mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(self.centerView).offset(14);
		make.right.equalTo(self.centerView).offset(-14);
		make.top.equalTo(self.roomIdL.mas_bottom).offset(9);
		make.height.mas_equalTo(1);
	}];
	
	[_roomL mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(self.centerView).offset(17);
		make.right.equalTo(self.centerView).offset(-17);
		make.top.equalTo(self.line.mas_bottom).offset(9);
	}];
	
	[self hostViewConstiants];
	
    _maskCenterView = [UIView new];
    _maskCenterView.backgroundColor = [UIColor whiteColor];
    _maskCenterView.layer.cornerRadius = _centerView.layer.cornerRadius;
    _maskCenterView.frame = CGRectMake(0, 0, 329, 358);
    _maskCenterView.userInteractionEnabled = NO;
    [self.centerView addSubview:_maskCenterView];
    self.centerView.maskView = _maskCenterView;
	
	[self addEvents];
}

- (void) addEvents {
	[_tousuBtn addTarget:self action:@selector(tousuA) forControlEvents:(UIControlEventTouchUpInside)];
	[_sixinBtn addTarget:self action:@selector(sixinA) forControlEvents:(UIControlEventTouchUpInside)];
	[_guanzhuBtn addTarget:self action:@selector(guanzhuA) forControlEvents:(UIControlEventTouchUpInside)];
	[_dashangBtn addTarget:self action:@selector(dashangA) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void) hostViewConstiants {
	[_line2 mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(self.centerView).offset(14);
		make.right.equalTo(self.centerView).offset(-14);
		make.top.equalTo(self.roomL.mas_bottom).offset(9);
		make.bottom.equalTo(self.centerView).offset(-77);
		make.height.mas_equalTo(1);
	}];
	
	[_sixinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(76, 26));
		make.centerX.equalTo(self.centerView);
		make.top.equalTo(self.line2.mas_bottom).offset(25);
		make.bottom.equalTo(self.centerView).offset(-25);
	}];
	
	[_guanzhuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(76, 26));
		make.centerY.equalTo(self.sixinBtn);
		make.right.equalTo(self.sixinBtn.mas_left).offset(-20);
	}];
	
	[_dashangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.size.mas_equalTo(CGSizeMake(76, 26));
		make.centerY.equalTo(self.sixinBtn);
		make.left.equalTo(self.sixinBtn.mas_right).offset(20);
	}];
}

- (void)setModel:(TCLiveInfo *)model
{
	_model = model;
//	_titleL.text = model.nickName;
	UIImage *img = [UIImage imageNamed:k18_DefaultIcon];
	_iconView.image = img;
	[_iconView sd_setImageWithURL:[NSURL URLWithString:model.userinfo.headpic] placeholderImage:img];
	_nameL.text = model.userinfo.nickname;
	_levelL.text = @"Lv1";
	_roomIdL.text = [NSString stringWithFormat:@"房间号 %d",model.roomId.intValue];
	_roomL.text = model.title;

    if(!self.reqSuc) {
        [[K18HostDialog shared] reqIsFollow:nil];
    }
	
    [self setNeedsLayout];
	
}

- (void)setIsHost:(BOOL)isHost
{
	_isHost = isHost;
	if (isHost) {//
		_maskCenterView.frame = CGRectMake(0, 0, _maskCenterView.width, 279);
		_line2.hidden = YES;
		self.tousuBtn.hidden = YES;
	}else{
		_maskCenterView.frame = CGRectMake(0, 0, _maskCenterView.width, 358);
		_line2.hidden = NO;
		self.tousuBtn.hidden = NO;
        self.centerView.maskView = nil;
        
	}
}

- (void)showByIsHost:(BOOL)isHost
{
    AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [del.window addSubview:self];
	self.isHost = isHost;

}

- (void)dismiss
{
	[self removeFromSuperview];

}

#pragma mark - Actions

- (void)gotoDetailView {
    [self dismiss];
    NSString *uid = [self.model.userid substringFromIndex:2];
    [K18LiveService gotoPersonDetail:uid parent:nil];
}


- (void) tousuA {
    
    [self dismiss];
    
    K18ComplainController *vc = [K18ComplainController new];
    vc.targetId = [self.model.userid substringFromIndex:2];
    [[AppDelegate sharedAppDelegate].navigationViewController pushViewController:vc animated:YES];
}

- (void) sixinA {
    [self dismiss];
    TUIConversationCellData *data = [TUIConversationCellData new];
    data.convType = TIM_C2C;
    data.title = self.model.userinfo.nickname;
    data.convId = self.model.userid;
    data.avatarUrl = [NSURL URLWithString:self.model.userinfo.headpic];
    K18ChatController *vc = [[K18ChatController alloc]init];
    vc.conversationData = data;
    [[AppDelegate sharedAppDelegate] pushViewController:vc animated:YES];
}

- (void) guanzhuA {
    __weak typeof(self)wself = self;
    void(^reqGuanzhu)(void) = ^(){
        NSString *targetId = [self.model.userid substringFromIndex:2];
        [K18UserModel doFollowForFollowId:targetId isFollow:!wself.isFollow completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
            wself.isFollow = !wself.isFollow;
            if(wself.isFollow==YES){
                [[HUDHelper sharedInstance] syncStopLoadingMessage:@"关注成功" delay:1 completion:nil];
            }else{
                [[HUDHelper sharedInstance] syncStopLoadingMessage:@"取消关注成功" delay:1 completion:nil];
            }
            
            if (wself.isFollow) {
                self.guanzhuBtn.selected = YES;
            }else{
                self.guanzhuBtn.selected = NO;
            }
            
            
            
        }];
    };
    
    if (!self.reqSuc) {
        [self reqIsFollow:^(NSError *err) {
            reqGuanzhu();
        }];
    }else{
        reqGuanzhu();
    }
    
   
}

- (void) dashangA {
    [[NSNotificationCenter defaultCenter] postNotificationName:k18_dashangEvent object:self.model];
    [self dismiss];
}

#pragma mark - Private

- (void) reqIsFollow:(void(^)(NSError*err)) completion {
    static int reqing = NO;
    if(reqing){
        return;
    }
    reqing = YES;
    NSString *myId = k18_loginUserId;
    NSString *targetId = [self.model.userid substringFromIndex:2];
//    [[HUDHelper sharedInstance] syncLoading];
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_getFollow params:@{@"cyUserId":notNil(myId), @"cyOtherUserId":notNil(targetId)} succ:^(id  _Nonnull data) {
        reqing = NO;
//        [[HUDHelper sharedInstance] syncStopLoading];
        wself.reqSuc = YES;
        NSLog(@"是否关注:%@",data);
        BOOL isFollow = [data[@"cyFollow"] boolValue];
        wself.isFollow = isFollow;
        if (wself.isFollow) {
            self.guanzhuBtn.selected = YES;
        }else{
            self.guanzhuBtn.selected = NO;
        }
        
        if(completion){
            completion(nil);
        }
        
    } failed:^(NSError * _Nonnull error) {
        reqing = NO;
        wself.reqSuc = NO;
        [[HUDHelper sharedInstance] syncStopLoadingMessage:error.localizedDescription delay:1 completion:nil];
        if(completion){
            completion(error);
        }
    }];
}


@end
