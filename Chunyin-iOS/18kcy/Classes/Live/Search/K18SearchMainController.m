//
//  K18SearchMainController.m
//  18kcy
//
//  Created by 唐 on 2019/6/27.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SearchMainController.h"
#import "K18SearchController.h"
#import "JXPageListView.h"
#import "K18SearchListView.h"
#import "K18NoneView.h"
#import <MJRefresh.h>
#import "K18SearchLiveView.h"
@interface K18SearchMainController () <UISearchResultsUpdating,UISearchControllerDelegate,JXPageListViewDelegate,UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property(strong, nonatomic) K18SearchController *searchController;
@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) JXPageListView *pageListView;

@property (nonatomic, strong) NSMutableArray *listViewArray;
@property (nonatomic, strong) NSArray *titles;

@end

@implementation K18SearchMainController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.title = @"搜索";
	
	self.searchController = [[K18SearchController alloc]initWithSearchResultsController:nil];
	self.searchController.searchResultsUpdater = self;
	self.searchController.delegate = self;
	self.searchController.searchBar.delegate = self;
	if (@available(iOS 11.0, *)) {
		
		self.navigationItem.searchController = self.searchController;
		self.navigationItem.hidesSearchBarWhenScrolling = YES;
	} else {
		
	}
	
	self.definesPresentationContext = YES;
//	[self.searchController.searchBar becomeFirstResponder];
    
	
	[self initUI_delay];
}

- (void)initUI_delay
{
	//分类
	
	_titles = @[@"歌曲", @"歌手", @"歌房", @"直播", @"用户"];
	
    _listViewArray = [NSMutableArray array];
    for (int i=0; i<_titles.count; i++) {
        if (i==2) {
            K18NoneView *categoryListView = [[K18NoneView alloc] init];
            [_listViewArray addObject:categoryListView];
            continue;
        }else if(i==3){
            K18SearchLiveView *listView = [K18SearchLiveView new];
            listView.rootVC = self;
            [_listViewArray addObject:listView];
        }else{
            K18SearchListView *categoryListView = [[K18SearchListView alloc] init];
            categoryListView.searchType = i;
            categoryListView.rootVC = self;
            [_listViewArray addObject:categoryListView];
        }
	}
	
	
	self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
	self.pageListView.listViewScrollStateSaveEnabled = YES;
	self.pageListView.pinCategoryViewVerticalOffset = 0;
	//Tips:pinCategoryViewHeight要赋值
	self.pageListView.pinCategoryViewHeight = 50;
	//Tips:操作pinCategoryView进行配置
	self.pageListView.pinCategoryView.titles = self.titles;
    self.pageListView.mainTableView.mj_header = nil;//cancel refreshHeader
	
	//添加分割线，这个完全自己配置
	UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, self.view.bounds.size.width, 1)];
	lineView.backgroundColor = [UIColor lightGrayColor];
	[self.pageListView.pinCategoryView addSubview:lineView];
	
	//Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
	self.pageListView.mainTableView.dataSource = self;
	self.pageListView.mainTableView.delegate = self;
	self.pageListView.mainTableView.scrollsToTop = NO;
	[self.pageListView.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
	[self.view addSubview:self.pageListView];
	
	
	[self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.view.mas_topMargin);
		make.bottom.equalTo(self.view);
		make.left.equalTo(self.view);
		make.right.equalTo(self.view);
	}];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.lastNaviHidden = self.navigationController.navigationBarHidden;
    self.navigationController.navigationBarHidden = NO;
    [self presentViewController:self.searchController animated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = self.lastNaviHidden;
    [self.searchController dismissViewControllerAnimated:YES completion:nil];
    self.navigationItem.searchController = nil;
}

- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {
	self.view.hidden = NO;
    [self udpateSearchResult];
}

- (void) udpateSearchResult {
    NSInteger index = self.pageListView.pinCategoryView.selectedIndex;
    if(index<self.listViewArray.count){
        K18SearchListView *listView = self.listViewArray[self.pageListView.pinCategoryView.selectedIndex];
        if([listView isKindOfClass:[K18SearchListView class]]){
            listView.searchKey = self.searchController.searchBar.text;
            [listView requestData:nil];
        }else if([listView isKindOfClass:[K18SearchLiveView class]]){
            K18SearchLiveView *liveView = listView;
            liveView.searchKey = self.searchController.searchBar.text;
            [liveView requestData:nil];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
	[self.searchController.searchBar resignFirstResponder];
}

#pragma mark - UISearchControllerDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - JXPageViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	//Tips:需要传入mainTableView的scrollViewDidScroll事件
	[self.pageListView mainTableViewDidScroll:scrollView];
}

//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
	return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
	self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    
    [self udpateSearchResult];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
		return [self.pageListView listContainerCellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
		return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];

	return nil;
}




@end
