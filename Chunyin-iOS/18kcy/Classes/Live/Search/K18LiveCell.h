//
//  K18LiveCell.h
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class K18Live;
NS_ASSUME_NONNULL_BEGIN

@interface K18LiveCell : UICollectionViewCell

@property (nonatomic, strong) id model;
@property(strong, nonatomic) K18Live *liveM;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *descL;
@property (weak, nonatomic) IBOutlet UILabel *peopleL;
@property (weak, nonatomic) IBOutlet UILabel *levelL;
@property (weak, nonatomic) IBOutlet UILabel *roomidL;


@end

NS_ASSUME_NONNULL_END
