//
//  K18SearchController.m
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SearchController.h"

@interface K18SearchController ()<
UISearchBarDelegate>

@end

@implementation K18SearchController

- (instancetype)initWithSearchResultsController:(UIViewController *)searchResultsController
{
	self = [super initWithSearchResultsController:searchResultsController];
	if (self) {
		[self configSearchBar];
	}
	return self;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self configSearchBar];
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

	self.delegate = self;
		
	self.dimsBackgroundDuringPresentation = NO;
	
	self.definesPresentationContext = YES;
	
	self.hidesNavigationBarDuringPresentation = YES;
	
	
}

- (void) configSearchBar {
	
    self.searchBar.placeholder = @"歌曲/歌手/用户/歌房房号/直播房间号";
	
	self.searchBar.tintColor = [UIColor whiteColor];
	self.searchBar.barStyle = UISearchBarStyleDefault;
	self.searchBar.barTintColor = [UIColor whiteColor];
	
	self.searchBar.showsCancelButton = YES;
	
	//	self.searchBar.delegate = self;
	[self setupCancelButton];
	
	UITextField *textfield = [self findTextView];
	textfield.textColor = self.searchBar.tintColor;
	
	if(@available(iOS 11.0, *)){
		UIView *v = textfield.subviews.firstObject;
		v.backgroundColor = self.searchBar.barTintColor;
		v.layer.cornerRadius = 4;
	}else{
		textfield.backgroundColor = self.searchBar.barTintColor;
	}
	
	[self.searchBar sizeToFit];
	NSLog(@"%@",self.searchBar);
}

#pragma mark -

- (void) setupCancelButton {
	NSArray *list = self.searchBar.subviews.firstObject.subviews;
	for (UIView *view in list) {
		if ([view isKindOfClass:[UIButton class]]) {
			
			UIButton *button = (UIButton *)view;
//			[button setTitleColor:self.searchBar.barTintColor forState:(UIControlStateNormal)];
//			button.backgroundColor = [UIColor blackColor];
//			[button setTitle:@"取消" forState:(UIControlStateNormal)];
//			button.layer.masksToBounds = NO;
//			button.clipsToBounds = NO;
			NSAttributedString *att = [[NSAttributedString alloc]initWithString:@"取消" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12], NSForegroundColorAttributeName:self.searchBar.barTintColor}];
			[button setAttributedTitle:att forState:(UIControlStateNormal)];
			break;
		}
	}
}

- (UITextField *) findTextView {
	NSArray *list = self.searchBar.subviews.firstObject.subviews;
	for (UIView *view in list) {
		if ([view isKindOfClass:[UITextField class]]) {
			
			return (UITextField*)view;

		}
	}
	return nil;
}



@end
