//
//  K18SearchListView.h
//  18kcy
//
//  Created by 唐 on 2019/6/27.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPageListView.h"
#import "K18Live.h"
#import "K18Common.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SearchType) {
	SearchType_1 = 0,
	SearchType_2,
	SearchType_3,
    SearchType_4,
    SearchType_5,
};

@interface K18SearchListView : UIView< JXPageListViewListDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
//@property (nonatomic, assign) BOOL isNeedHeader;
//@property (nonatomic, assign) BOOL isFirstLoaded;

@property(nonatomic) SearchType searchType;

@property(strong, nonatomic) NSString *searchKey;

@property(nonatomic,weak) UIViewController *rootVC;

- (void) setNeedRefresh;
- (void) requestData:(void(^)(void))callback;



@end

NS_ASSUME_NONNULL_END
