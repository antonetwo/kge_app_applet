//
//  K18SearchListView.m
//  18kcy
//
//  Created by 唐 on 2019/6/27.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18SearchListView.h"
#import <MJRefresh/MJRefresh.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "K18SongCell.h"
#import "K18SongService.h"
#import "K18RankCell.h"
#import "K18SearchM.h"
#import "K18LiveService.h"

@interface K18SearchListView () <UITableViewDataSource, UITableViewDelegate,K18SongCellDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) NSIndexPath *lastSelectedIndexPath;
@property(nonatomic) NSInteger pageNumber;
@property(nonatomic) BOOL isPageMax;
@property(assign, nonatomic) BOOL needRefresh;

@property(strong, nonatomic) K18SearchM *searchUserModel;
@end

@implementation K18SearchListView

- (void)dealloc
{
	self.scrollCallback = nil;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
//        self.isFirstLoaded = YES;
		
		_tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
		self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
		self.tableView.backgroundColor = [UIColor whiteColor];
		self.tableView.tableFooterView = [UIView new];
		self.tableView.dataSource = self;
		self.tableView.delegate = self;
		[self.tableView registerNib:[UINib nibWithNibName:@"K18SongCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        [self.tableView registerNib:[UINib nibWithNibName:@"K18RankCell" bundle:nil] forCellReuseIdentifier:@"K18RankCell"];

		[self addSubview:self.tableView];
		
		[_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
			make.top.equalTo(self);
			make.bottom.equalTo(self);
			make.left.equalTo(self);
			make.right.equalTo(self);
		}];
        
        [self setNeedRefresh];
	}
	return self;
}

- (void)loadMoreDataTest {
	//	[self.dataSource addObject:@"加载更多成功"];
	[self.tableView reloadData];
}

- (void)setSearchKey:(NSString *)searchKey
{
    _searchKey = searchKey;
    self.isPageMax = NO;
    self.pageNumber = 0;
    [self.dataSource removeAllObjects];
}

#pragma mark - Data service

- (void) requestData:(void(^)(void))callback {
    if (self.searchKey.length==0) {
        [self.tableView reloadData];
        return;
    }

    __weak typeof(self)wself = self;
	NSDictionary *params = @{@"cyFlag":@((int)self.searchType),
                             @"pageNumber":@(self.pageNumber+1),
                             @"pageSize":@(kPageSize),
                             @"name":notNil(self.searchKey),
                             };
	[self sendWithPath:kAPI_searchforFlag params:params succ:^(id  _Nonnull data) {
		wself.pageNumber ++;
		[wself dealDataSourceWithJson:data];
		[wself.tableView reloadData];
        wself.needRefresh = NO;
        if(callback)callback();
	} failed:^(NSError * _Nonnull error) {
        wself.needRefresh = NO;
		[[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
        if(callback)callback();
	}];
	
}

- (void) dealDataSourceWithJson:(id)json {
    NSArray *list;
    if (self.searchType == 4 ) {
        list = [NSArray yy_modelArrayWithClass:[K18SearchM class] json:json[@"list"]];
    }else{
        list = [NSArray yy_modelArrayWithClass:[K18Song class] json:json[@"list"]];
    }
	K18Song *m1 = list.lastObject;
	K18Song *m2 = self.dataSource.lastObject;
	if (!m1) {
		self.isPageMax = YES;
		[self setRefreshFooter];
		return;
	}
	
	if ([m1.id isEqualToString:m2.id]) {
		self.isPageMax = YES;
		[self setRefreshFooter];
		return;
	}
    
    if (self.dataSource.count==0 && list.count < kPageSize) {
        self.isPageMax = YES;
        [self setRefreshFooter];
    }
	
	if (self.dataSource.count) {
		[self.dataSource addObjectsFromArray:list];
	}else{
		self.dataSource = list.mutableCopy;
	}
}

- (void)  setRefreshFooter {
	if (!self.isPageMax) {
		//模拟加载完数据之后添加footer
		__weak typeof(self)wself = self;
		self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{

            [wself requestData:^{
                [wself.tableView.mj_footer endRefreshing];
            }];
//            NSDictionary *params = @{@"cyType":@(self.searchType),
//                                     @"pageNumber":@(self.pageNumber+1),
//                                     @"pageSize":@(kPageSize),
//                                     @"name":notNil(self.searchKey),
//                                     };
//            [wself sendWithPath:kAPI_searchforFlag params:params succ:^(id  _Nonnull data) {
//                [wself.tableView.mj_footer endRefreshing];
//                wself.pageNumber ++;
//                [wself dealDataSourceWithJson:data];
//                [wself.tableView reloadData];
//                [wself setRefreshFooter];
//            } failed:^(NSError * _Nonnull error) {
//                [wself.tableView.mj_footer endRefreshing];
//                [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
//            }];
			
		}];
	}else {
		self.tableView.mj_footer = nil;
	}
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchType == 4) {
        K18RankCell *cell = (K18RankCell*)[tableView dequeueReusableCellWithIdentifier:@"K18RankCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        K18SearchM *model = self.dataSource[indexPath.row];
        cell.model = model;
        
        //ui
//        @property (weak, nonatomic) IBOutlet UIImageView *iconView;
//        @property (weak, nonatomic) IBOutlet UILabel *titleL;
//        @property (weak, nonatomic) IBOutlet UILabel *subL;
//        @property (weak, nonatomic) IBOutlet UIButton *button;
        [cell.iconView sd_setImageWithURL:[NSURL URLWithString:model.cyImg] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
        cell.titleL.text = model.cyUserName;
        cell.subL.text = [NSString stringWithFormat:@"K歌号:%@ 粉丝:%@人 作品:%@",model.id,model.userFansSize, model.songLivesSize];
        
        if (model.cyFollow.boolValue) {
            [cell.button setTitle:@"取消关注" forState:(UIControlStateNormal)];
        }else{
            [cell.button setTitle:@"关注TA" forState:(UIControlStateNormal)];
        }
        
        __weak typeof(self)wself = self;
        cell.didSend = ^(id  _Nonnull model) {
            [wself concern:model];
        };
        return cell;
    }else{
        K18SongCell *cell = (K18SongCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        K18Song *model = self.dataSource[indexPath.row];
        cell.model = model;
        
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchType == 4) {
        return 60;
    }
	return [K18SongCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchType == 4) {
        K18SearchM *model = self.dataSource[indexPath.row];
        [K18LiveService gotoPersonDetail:model.id parent:self.rootVC];
    }else{
            
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	!self.scrollCallback?:self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate

- (UIScrollView *)listScrollView {
	return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
	self.scrollCallback = callback;
}

- (void)listViewLoadDataIfNeeded {
	if (!self.needRefresh) {
		return;
	}
		
    [self requestData:nil];
			
}

- (void)setNeedRefresh
{
    self.needRefresh = YES;
}

#pragma mark - K18SongCellDelegate

- (void) cell:(K18SongCell *)cell didSelect:(K18Song *)model
{
    [K18SongService gotoRecordingWithModel:model root:self.rootVC];
}

- (void) concern:(K18SearchM *)model {
    self.searchUserModel = model;
    NSString *targetId = model.id;
    BOOL isFollow = !model.cyFollow.boolValue;
    __weak typeof(self)wself = self;
    [K18UserModel doFollowForFollowId:targetId isFollow:isFollow completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
        if (error) {
            [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
            return;
        }
        
        //update ui
        wself.searchUserModel.cyFollow = @(isFollow).stringValue;
        [wself.tableView reloadData];
        
    }];
    
}

@end
