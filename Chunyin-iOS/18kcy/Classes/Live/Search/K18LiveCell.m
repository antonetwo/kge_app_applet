//
//  K18LiveCell.m
//  18kcy
//
//  Created by 唐 on 2019/6/26.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LiveCell.h"
#import "K18Common.h"
#import "K18Live.h"
#import "K18Fund.h"
#import "TCLiveListModel.h"

@implementation K18LiveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(TCLiveInfo *)model
{
	_model = model;
	NSString *url = model.userinfo.frontcover;
	__weak typeof(self)wself = self;
	[self.iconView sd_setImageWithURL:[NSURL URLWithString:notNil(url)] placeholderImage:[UIImage imageNamed:k18_DefaultCover] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//        __strong typeof(wself)sself = wself;
        model.userinfo.frontcoverImage = image;
	}];
	
	self.peopleL.text = @(model.viewercount).stringValue;
	self.levelL.text = [NSString stringWithFormat:@"Lv%d",model.userinfo.level];
	self.descL.text = model.title;
	 self.roomidL.text = [NSString stringWithFormat:@"房间号:%@",model.roomId];
}

- (void)setLiveM:(K18Live *)liveM
{
    _liveM = liveM;
    NSString *url = liveM.cyLiveImgUrl;
//    __weak typeof(self)wself = self;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:notNil(url)] placeholderImage:[UIImage imageNamed:k18_DefaultCover]];
    
    self.peopleL.text = liveM.cyVisitorNum;
    self.levelL.text = [NSString stringWithFormat:@"Lv%d",liveM.cyLevel.intValue];
    self.descL.text = liveM.cyContent;
    self.roomidL.text = [NSString stringWithFormat:@"房间号:%@",liveM.id];
}

@end
