//
//  K18SearchLiveView.h
//  18kcy
//
//  Created by beuady on 2019/8/20.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JXPageListView.h"
#import "K18Live.h"
#import "K18Common.h"

NS_ASSUME_NONNULL_BEGIN

/**
 搜索直播页面
 */
@interface K18SearchLiveView : UIView< JXPageListViewListDelegate>

@property(nonatomic,strong) UIViewController *playVC;

@property(strong, nonatomic) NSString *searchKey;

@property(nonatomic,weak) UIViewController *rootVC;

- (void) setNeedRefresh;
- (void) requestData:(void(^)(void))callback;
@end

NS_ASSUME_NONNULL_END
