//
//  K18LiveRankListView.m
//  18kcy
//
//  Created by beuady on 2019/8/19.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18LiveRankListView.h"
#import "K18Rank.h"
#import "K18SongService.h"
#import "K18LiveService.h"
#import "K18RankCell.h"
#import "K18GiftView.h"
@interface K18LiveRankListView()<UITableViewDataSource, UITableViewDelegate,K18GiftViewDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property(assign, nonatomic) BOOL needRefresh;
@property(strong, nonatomic) UILabel *noTipLabel;

@property(strong, nonatomic) K18GiftView *giftView;

@property(strong, nonatomic) K18ManRank *sendedFlowerModel;
@end

@implementation K18LiveRankListView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = [NSMutableDictionary dictionary];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.tableFooterView = [UIView new];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.tableView registerNib:[UINib nibWithNibName:@"K18RankCell" bundle:nil] forCellReuseIdentifier:@"K18RankCell"];
        
        [self addSubview:self.tableView];
        
        _noTipLabel = [UILabel new];
        _noTipLabel.textColor = RGBOF(0x323232);
        [self addSubview:_noTipLabel];
        _noTipLabel.text = @"暂无内容";
        [_noTipLabel sizeToFit];
        _noTipLabel.hidden = YES;
        
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
        [self setNeedRefresh];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.noTipLabel.center = CGPointMake(self.width/2, self.height/2);
}

- (NSMutableArray *) dataWithInnerTypes {
    NSString *key = [NSString stringWithFormat:@"%d_%d",self.cyRankType,self.cyWeekType];
    NSMutableArray *list = self.dataSource[key];
    if (!list) {
        list = [NSMutableArray array];
        self.dataSource[key] = list;
    }
    return list;
}

- (void) requestData:(void(^)(void))callback {
    static int loading = 0;
    
    id hud = [[HUDHelper sharedInstance] loading];
    
    __weak typeof(self)wself = self;
    NSDictionary *params = @{@"cyRankType":@(self.cyRankType),
                             @"cyWeekType":@(self.cyWeekType)
                             };
    [self sendWithPath:kAPI_liveRank params:params succ:^(id  _Nonnull data) {
        [[HUDHelper sharedInstance] stopLoading:hud message:nil delay:0 completion:nil];
        loading = 0;
        NSMutableArray *originDatas = [wself dataWithInnerTypes];
        [originDatas removeAllObjects];
        NSArray *list;
        if(wself.cyRankType==0){
            list = [NSArray yy_modelArrayWithClass:[K18ManRank class] json:data];
        }else{
            list = [NSArray yy_modelArrayWithClass:[K18MusicRank class] json:data];
        }
        [originDatas addObjectsFromArray:list];
        wself.needRefresh = NO;
        [wself.tableView reloadData];
        if (list.count==0) {
            wself.noTipLabel.hidden= NO;
        }else{
            wself.noTipLabel.hidden= YES;
        }
        if(callback) callback();
    } failed:^(NSError * _Nonnull error) {
        [[HUDHelper sharedInstance] stopLoading:hud];
        loading = 0;
        wself.needRefresh = NO;
        if(callback) callback();
        [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
    }];
    
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self dataWithInnerTypes].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    @property (weak, nonatomic) IBOutlet UIImageView *iconView;
//    @property (weak, nonatomic) IBOutlet UILabel *titleL;
//    @property (weak, nonatomic) IBOutlet UILabel *subL;
//    @property (weak, nonatomic) IBOutlet UIButton *button;

    K18RankCell *cell = (K18RankCell*)[tableView dequeueReusableCellWithIdentifier:@"K18RankCell"];
    if(self.cyRankType==0){
        
        K18ManRank *m = [self dataWithInnerTypes][indexPath.row];
        NSString *url = notNil(m.cyImg);
        [cell.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
        cell.titleL.text = m.cyUserName;
        if (self.cyWeekType==0) {
            cell.subL.text = [NSString stringWithFormat:@"热度:%@ | %@",m.cyTotalFlowerNum, m.lastWeekRankStr];
        }else{
            cell.subL.text = [NSString stringWithFormat:@"热度:%@",m.cyTotalFlowerNum];
        }
        cell.button.hidden = NO;
        cell.model = m;
        __weak typeof(self)wself = self;
        cell.didSend = ^(id  _Nonnull model) {
            [wself doSendFlower:model];
        };
        
    }else{
        K18MusicRank *m = [self dataWithInnerTypes][indexPath.row];
        NSString *url = notNil(m.cyImg);
        [cell.iconView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
        cell.titleL.text = m.cyUserName;
        cell.subL.text = [NSString stringWithFormat:@"贡献:%@",m.cyTotalFlowerNum];
        cell.button.hidden = YES;
    }
    
    return cell;
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.cyRankType==0){
        K18ManRank *model = [self dataWithInnerTypes][indexPath.row];
        [K18LiveService gotoPersonDetail:model.cySendUserId parent:self.rootVC];
    }else{
        K18MusicRank *model = [self dataWithInnerTypes][indexPath.row];
        [K18LiveService gotoPersonDetail:model.cySendUserId parent:self.rootVC];
        
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback?:self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (void)listViewLoadDataIfNeeded {
    if (!self.needRefresh) {
        return;
    }
    
    [self requestData:^{
        
    }];
    
}

- (void)setNeedRefresh
{
    self.needRefresh = YES;
}

- (void)listViewRefreshData:(void (^)(void))callback
{
    [self requestData:callback];
}

#pragma mark - actions

- (void) doSendFlower:(K18ManRank *)model {
    self.sendedFlowerModel = model;
    if(!self.giftView){
        self.giftView = [[K18GiftView alloc]initWithTargetId:model.cyUserId targetName:model.cyUserName];
        self.giftView.giftDelegate = self;
        [self.rootVC.navigationController.view addSubview:self.giftView];
    }
    [self.giftView show];
}

- (void)onSendFlowerSuccess:(int)sendCount
{
    if(self.cyWeekType!=1){//不是上周
        self.sendedFlowerModel.cyTotalFlowerNum = @(self.sendedFlowerModel.cyTotalFlowerNum.integerValue + sendCount).stringValue;
    }
    [self.tableView reloadData];
}

@end
