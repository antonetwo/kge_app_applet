//
//  K18RankCell.m
//  18kcy
//
//  Created by beuady on 2019/8/18.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RankCell.h"
#import "K18Common.h"

@implementation K18RankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    [self.button k18_setStyle];
    [self.button setTitleColor:RGBOF(0x323232) forState:(UIControlStateNormal)];
    self.button.layer.borderColor = k18_TopicC.CGColor;
    self.button.layer.borderWidth = 1;
    self.button.layer.masksToBounds = YES;
    self.button.layer.cornerRadius = self.button.height/2;
    
    self.iconView.layer.masksToBounds = YES;
    self.iconView.layer.cornerRadius = self.iconView.height/2;
    [self.button addTarget:self action:@selector(sendA) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.didSend = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

- (void) sendA {
    if (self.didSend) {
        self.didSend(self.model);
    }
}

@end
