//
//  K18UserDetailController.m
//  18kcy
//
//  Created by beuady on 2019/8/6.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18UserDetailController.h"
#import "JXPageListView.h"
#import "K18SongListView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Masonry/Masonry.h>
#import "K18Cell1.h"
#import "K18Cell2.h"
#import "K18SongListController.h"
#import "K18SongAuthorController.h"
#import "K18PublishController1.h"
#import "K18SongService.h"
#import "K18LiveService.h"
#import "K18NoneView.h"
#import "K18UserModel.h"
#import "User.h"
#import <MJRefresh/MJRefresh.h>
#import "K18WorkListView.h"
#import "TCLoginModel.h"
#import "K18DetailCell1.h"
#import "K18DetailListView.h"
#import "K18ComplainController.h"
#import "TUIConversationCellData.h"
#import "K18ChatController.h"
#import "AppDelegate.h"
/**
 他人详情页面
 */
@interface K18UserDetailController ()<JXPageListViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, strong) JXPageListView *pageListView;
@property (nonatomic, strong) NSMutableArray *listViewArray;

@property(strong, nonatomic) NSArray<K18Category *> *categoryList;

@property(strong, nonatomic) K18User *userInfo;
@property(assign, nonatomic) BOOL lastNaviHiden;
@end

@implementation K18UserDetailController

- (instancetype)initWithUserId:(NSString *)userId
{
    self = [super init];
    if (self) {
        self.userId = userId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self)wself = self;
    self.pageListView.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [wself reqData];
    }];

    [self reqData];
}

- (void) reqData {
    NSString *userid = self.userId;
    __weak typeof(self)wself = self;
    [self sendWithPath:kAPI_getUserInfo params:@{@"cyUserId":userid} succ:^(id  _Nonnull data) {
        wself.userInfo = [K18User yy_modelWithJSON:data];
        [wself.pageListView.mainTableView reloadData];
        [wself.pageListView.mainTableView.mj_header endRefreshing];
    } failed:^(NSError * _Nonnull error) {
        
        [wself.pageListView.mainTableView.mj_header endRefreshing];
    }];
}

- (void) initData {
    
}


- (void)initUI {
    self.title = @"个人详情";
    _titles = @[@"作品", @"伴奏", @"合唱"];

    if([k18_loginUserId isEqualToString:self.userId]){
        
    }else{
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"gengduo"] style:(UIBarButtonItemStylePlain) target:self action:@selector(moreAction)];
        self.navigationItem.rightBarButtonItem = barItem;
    }
    
    _listViewArray = [NSMutableArray array];
    for (int i=0; i<3; i++) {
        if(i==0){
            K18DetailListView *categoryListView = [[K18DetailListView alloc] init];
            categoryListView.isNeedHeader = YES;
            categoryListView.userId = self.userId;
            categoryListView.rootVC = self;
            [_listViewArray addObject:categoryListView];
        }else{
            K18NoneView *v = [K18NoneView new];
            [_listViewArray addObject:v];
        }
    }
    
    self.pageListView = [[JXPageListView alloc] initWithDelegate:self];
    self.pageListView.listViewScrollStateSaveEnabled = YES;
    self.pageListView.pinCategoryViewVerticalOffset = 0;
    self.pageListView.mainTableView.separatorInset = UIEdgeInsetsZero;
    //Tips:pinCategoryViewHeight要赋值
    self.pageListView.pinCategoryViewHeight = 50;
    //Tips:操作pinCategoryView进行配置
    self.pageListView.pinCategoryView.titles = self.titles;
    
    //添加分割线，这个完全自己配置
//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, self.view.bounds.size.width, 1)];
//    lineView.backgroundColor = [UIColor lightGrayColor];
//    [self.pageListView.pinCategoryView addSubview:lineView];
    
    //Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
    self.pageListView.mainTableView.dataSource = self;
    self.pageListView.mainTableView.delegate = self;
    self.pageListView.mainTableView.scrollsToTop = NO;
    [self.pageListView.mainTableView registerNib:[UINib nibWithNibName:@"K18DetailCell1" bundle:nil] forCellReuseIdentifier:@"cell0"];
    [self.view addSubview:self.pageListView];
    
    
    [self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_topMargin);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.lastNaviHiden = self.navigationController.navigationBarHidden;
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = self.lastNaviHiden;
}


#pragma mark - JXPageViewDelegate
//Tips:实现代理方法
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
    return self.listViewArray;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 + 1;//底部的分类滚动视图需要作为最后一个section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        //Tips:最后一个section（即listContainerCell所在的section）返回listContainerCell的高度
        return [self.pageListView listContainerCellHeight];
    }else{
        return 334;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        //Tips:最后一个section（即listContainerCell所在的section）配置listContainerCell
        return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
    }else{
        __weak typeof(self)wself = self;
        
        
        K18DetailCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"cell0"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.userInfo;
        [cell.sixinBtn addTarget:self action:@selector(sixinA) forControlEvents:(UIControlEventTouchUpInside)];
        
        return cell;
        
    }
    return nil;
}

#pragma mark - Actions

- (void) sixinA {
    TUIConversationCellData *data = [TUIConversationCellData new];
    data.convType = TIM_C2C;
    data.title = self.userInfo.cyUserName;
    data.convId = [NSString stringWithFormat:@"cy%@",self.userInfo.id];
    data.avatarUrl = [NSURL URLWithString:self.userInfo.cyImg];
    K18ChatController *vc = [[K18ChatController alloc]init];
    vc.conversationData = data;
    [[AppDelegate sharedAppDelegate] pushViewController:vc animated:YES];
}

- (void) settingAction {
    [[TCLoginModel sharedInstance] logout:^{
        
        [K18UserModel shared].autoLogin = NO;
        [LoginService enterLoginUI];
        DebugLog(@"退出登录成功");
    }];
    
}

//分类菜单
- (void) clickCagatoryWithIndex:(NSInteger)index title:(NSString *)title {
    NSLog(@"%@",@(index));
    [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"];
    
}

- (void) clickMenuWithIndex:(NSInteger)index {
    [[HUDHelper sharedInstance] tipMessage:@"开发中，敬请期待"];
    switch (index) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        case 3:
            
            break;
            
        default:
            break;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Tips:需要传入mainTableView的scrollViewDidScroll事件
    [self.pageListView mainTableViewDidScroll:scrollView];
}

- (void) moreAction {
    UIAlertController *av = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:(UIAlertControllerStyleActionSheet)];
    //gengduo
    UIAlertAction *a1 = [UIAlertAction actionWithTitle:@"投诉" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
        
        [self tousu];
        
    }];
    UIAlertAction *a2 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    
    [av addAction:a1];
    [av addAction:a2];
    
    [self presentViewController:av animated:YES completion:nil];
}

- (void) tousu {
    K18ComplainController *vc = [K18ComplainController new];
    vc.targetId = self.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Datas

- (NSMutableArray*)initialDataWithIndex:(NSInteger)index {
    NSMutableArray *list = [NSMutableArray array];
    
    //TODO test data
    for (int i=0; i<10; i++) {
        K18Song *model = [K18Song testNew];
        [list addObject:model];
    }
    return list;
}
@end
