//
//  K18DetailListView.m
//  18kcy
//
//  Created by beuady on 2019/8/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18DetailListView.h"
#import "K18Work.h"
#import "K18SongService.h"
#import "K18Fund.h"

@implementation K18DetailListView

- (NSString *) requestPath
{
    return kAPI_getMySongLives;
}
- (NSString *) cellNibName
{
    return @"K18WorkCell";
}

- (NSDictionary *)requestExtraParams
{
    return @{@"cyUserId":notNil(self.userId)};
}

- (Class)modelClass
{
    return [K18Fund class];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    K18Fund *model = self.dataSource[indexPath.row];
    [K18SongService gotoSongDetailWithId:model.id root:self.rootVC];
}

- (NSString *)noneDataTip {
    return @"你还没有发布作品哦~";
}

@end
