//
//  K18DetailListView.h
//  18kcy
//
//  Created by beuady on 2019/8/8.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseListView.h"

NS_ASSUME_NONNULL_BEGIN

@interface K18DetailListView : K18BaseListView

@property(strong, nonatomic) NSString *userId;

@end

NS_ASSUME_NONNULL_END
