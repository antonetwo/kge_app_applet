//
//  K18MineCell1.h
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class K18User;

NS_ASSUME_NONNULL_BEGIN

@interface K18DetailCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *kidL;

@property (weak, nonatomic) IBOutlet UILabel *likeNum;
@property (weak, nonatomic) IBOutlet UILabel *followNum;
@property (weak, nonatomic) IBOutlet UILabel *levelNum;
@property (weak, nonatomic) IBOutlet UILabel *albumNum;
@property (weak, nonatomic) IBOutlet UILabel *infoL;
@property (weak, nonatomic) IBOutlet UILabel *signL;

@property(strong, nonatomic) K18User *model;
@property (weak, nonatomic) IBOutlet UIButton *concernBtn;
@property (weak, nonatomic) IBOutlet UIButton *sixinBtn;

- (IBAction)conernA:(id)sender;
- (IBAction)sixinA:(id)sender;

@end

NS_ASSUME_NONNULL_END
