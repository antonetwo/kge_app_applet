//
//  K18MineCell1.m
//  18kcy
//
//  Created by beuady on 2019/6/30.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18DetailCell1.h"
#import "User.h"
#import "K18Common.h"
#import "K18UserModel.h"

@interface K18DetailCell1()
@property(nonatomic,assign) BOOL isFollow;
@end

@implementation K18DetailCell1

- (void)awakeFromNib {
    [super awakeFromNib];
	self.iconView.layer.cornerRadius = self.iconView.width/2;
	self.iconView.layer.masksToBounds = YES;
    
//    self.concernBtn set
    [self.concernBtn k18_setStyle];
}

- (void)setIsFollow:(BOOL)isFollow
{
    _isFollow = isFollow;
    if (isFollow) {
        self.concernBtn.backgroundColor = [UIColor clearColor];
        [self.concernBtn setTitle:@"取消关注" forState:(UIControlStateNormal)];
        [self.concernBtn setTitleColor:RGBOF(0xcccccc) forState:(UIControlStateNormal)];
    }else{
        self.concernBtn.backgroundColor = k18_TopicC;
        [self.concernBtn setTitle:@"关注" forState:(UIControlStateNormal)];
        [self.concernBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//@property (weak, nonatomic) IBOutlet UIImageView *iconView;
//@property (weak, nonatomic) IBOutlet UILabel *nameL;
//@property (weak, nonatomic) IBOutlet UILabel *kidL;
//
//@property (weak, nonatomic) IBOutlet UILabel *likeNum;
//@property (weak, nonatomic) IBOutlet UILabel *followNum;
//@property (weak, nonatomic) IBOutlet UILabel *levelNum;
//@property (weak, nonatomic) IBOutlet UILabel *albumNum;
//@property (weak, nonatomic) IBOutlet UILabel *infoL;
//@property (weak, nonatomic) IBOutlet UILabel *signL;

- (void)setModel:(K18User *)model
{
	_model = model;
	K18User *m = model;
    
    if(!_model){
        return;
    }
    
	[self.iconView sd_setImageWithURL:[NSURL URLWithString:notNil(m.cyImg)] placeholderImage:[UIImage imageNamed:k18_DefaultIcon]];
	self.nameL.text = m.cyUserName;
	self.kidL.text = [NSString stringWithFormat:@"K歌号:%@",m.id];
	self.likeNum.text = m.userFansSize;
	self.followNum.text = m.userFollowsSize;
	self.levelNum.text = [NSString stringWithFormat:@"Lv%d",m.userLevel.intValue];
	self.albumNum.text = m.userPhotosSize;
	
	NSString *gender = m.cySex.intValue == 0 ? @"" : ( m.cySex.intValue == 1 ? @"男" : @"女");
    NSString *age = [NSString stringWithFormat:@"%d岁",m.age.intValue];
	self.infoL.text = [NSString stringWithFormat:@"资料：%@\t%@\t%@\t%@",gender, age, m.cyProvince, m.cyCity];
	
	if(m.cyPersonalSign.length){
		self.signL.text = [NSString stringWithFormat:@"个性签名:%@", m.cyPersonalSign];
	}else{
		self.signL.text = @"个性签名: 暂无签名";
	}
    
    if(![k18_loginUserId isEqualToString:model.id]){
        [K18UserModel getIsFollowWithId:model.id completion:^(id  _Nonnull params, NSError * _Nullable error) {
            
            if (!error) {
                self.isFollow = [params boolValue];
                
            }
            
        }];
        self.concernBtn.hidden = NO;
        self.sixinBtn.hidden = NO;
    }else{
        self.concernBtn.hidden = YES;
        self.sixinBtn.hidden = YES;
    }
}

- (IBAction)conernA:(id)sender {
    
    NSString *targetId = self.model.id;
    BOOL isFollow = !self.isFollow;
    __weak typeof(self)wself = self;
    [K18UserModel doFollowForFollowId:targetId isFollow:isFollow completion:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
        if (error) {
            [[HUDHelper sharedInstance] tipMessage:error.localizedDescription];
            return;
        }

        //update ui
        wself.isFollow = isFollow;
        if (isFollow) {
            [[HUDHelper sharedInstance] tipMessage:@"关注成功"];
        }else{
            [[HUDHelper sharedInstance] tipMessage:@"取消关注成功"];
        }
        
    }];
    
    
}
- (IBAction)sixinA:(id)sender {
    
    
}
@end
