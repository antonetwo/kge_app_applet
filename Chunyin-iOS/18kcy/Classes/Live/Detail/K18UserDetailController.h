//
//  K18UserDetailController.h
//  18kcy
//
//  Created by beuady on 2019/8/6.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18BaseController.h"

NS_ASSUME_NONNULL_BEGIN


/**
 他人详情页面
 */
@interface K18UserDetailController : K18BaseController

@property(strong, nonatomic) NSString *userId;

- (instancetype) initWithUserId:(NSString *)userId;

@end

NS_ASSUME_NONNULL_END
