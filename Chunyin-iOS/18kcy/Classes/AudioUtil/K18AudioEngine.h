//
//  K18AudioEngine.h
//  18kcy
//
//  Created by 唐 on 2019/7/15.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, K18ReverbType) {

	K18ReverbTypeNone = 0,

    K18ReverbTypeKTV,//KTV
    
    K18ReverbTypeCixing, //磁性
    K18ReverbTypeKongling,//空灵
    K18ReverbTypeYouyuan,//悠远
    K18ReverbTypeLuyinpeng,//录音棚
	K18ReverbTypeLaochangpian,//老唱片
	K18ReverbType3Dmihuan,//3d迷幻
};

@class K18AudioEngine;

@protocol K18AudioEngineDelegate <NSObject>
@optional

//录制相关
- (void) record:(K18AudioEngine*_Nonnull)engine currentTime:(NSTimeInterval)curTime volume:(float)volume;

- (void) onRecordStop:(NSURL *_Nullable)exportURL duration:(NSTimeInterval)duration;

- (void) onPlaying:(K18AudioEngine*_Nonnull)engine currentTime:(NSTimeInterval)curTime;

//播放相关
@required
- (void) onPlayStop:(K18AudioEngine*_Nonnull)engine;

@end

NS_ASSUME_NONNULL_BEGIN

@interface K18AudioEngine : NSObject

@property(nonatomic) BOOL useReverb;//Default=NO, 是否需要混响
@property(nonatomic) BOOL useEarReturn;//Default=YES

@property(nonatomic) CGFloat pitch;//升降调参数
@property(nonatomic) float volume;//mixer 音量
@property(nonatomic) K18ReverbType reverbType;//混响类型
//@property(readonly) NSTimeInterval currentRecTime;//当前录音时间
@property(readonly) NSTimeInterval currentPlayTime;//当前播放的时间
@property(readonly) NSTimeInterval playDuration;//当前播放的音频总时长
@property(readonly) BOOL isPausePlay;

@property(nonatomic, weak) id<K18AudioEngineDelegate> delegate;


/**
 耳返开关
 */
@property(nonatomic) BOOL enableEarReturn;

+(instancetype)shared;

+(void) setupAudioSession;//统一设置session catory

/**
 录制相关
 ======
 */
-(void)startRecord:(nullable void(^)(void)) succ
			  fail:(nullable void(^)(NSError *_Nullable err)) fail;
-(void)startRecord:(nullable void(^)(void)) succ
              fail:(nullable void(^)(NSError *_Nullable err)) fail
          isSwitch:(BOOL)isSwitch;//是否耳返切换重录
-(void) stopRecord:(nullable void(^)(NSURL *_Nullable recordedURL, NSError*_Nullable error)) completion;
-(void) stopRecord:(nullable void(^)(NSURL *_Nullable recordedURL, NSError*_Nullable error)) completion immediately:(BOOL)immediately;

-(void) pauseRecord;
-(void) resumeRecord;
-(BOOL) isRecording;


/**
 播放相关
 =========
 */
@property(copy, nonatomic) void(^startPlayCallback)();

- (void) playFromURL:(nonnull NSURL*)url
                succ:(nullable void(^)(void)) succ
                fail:(nullable void(^)(NSError *_Nullable err)) fail;


/**
 脱机渲染导出混响音频

 @param url <#url description#>
 @param succ <#succ description#>
 @param fail <#fail description#>
 */
- (void) playOfflineRenderFromURL:(nonnull NSURL*)url
							 succ:(nullable void(^)(NSURL *exportURL)) succ
							 fail:(nullable void(^)(NSError *_Nullable err)) fail;

- (void)clearOfflineRenderCache;

/**
 跳转播放

 @param position seconds
 */
- (void) seekToPosition:(NSTimeInterval)position;

- (void) stop;
- (void) pause;
- (void) play;
- (BOOL) isPlaying;


/**
 释放资源
 */
- (void) dispose;

@end


/**
 统一管理录音时间、录音数据流
 */
@interface K18RecDataManager : NSObject
{
@public
    //计算录音音量相关
    float averagePowerForChannel0;
    float averagePowerForChannel1;
    float minLevel;
    float maxLevel;
}
@property(assign, nonatomic) NSTimeInterval currentRecTime;
@property(strong, nonatomic) NSMutableData *data;//缓冲区
@property(nonatomic) CGFloat currentVolume;//自己计算的人声音量
@property(strong, nonatomic) NSString * curFilePath;//录音文件路径


- (void)reset;
//- (NSString *)pcm2WAV:(NSString *)pcmFilePath;

@end

NS_ASSUME_NONNULL_END
