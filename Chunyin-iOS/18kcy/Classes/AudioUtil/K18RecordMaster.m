//
//  K18RecordMaster.m
//  18kcy
//
//  Created by beuady on 2019/8/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18RecordMaster.h"
#import <AVFoundation/AVFoundation.h>
#import "lame.h"
#import <Accelerate/Accelerate.h>
#import "K18AudioEngine.h"
#import "XBEchoCancellation/XBEchoCancellation.h"
#import "K18AudioUtil.h"
#import "VVAudioUtil.h"

@interface K18RecordMaster()<AVAudioRecorderDelegate>
@property(nonatomic, strong) AVAudioRecorder *recorder;
@property(copy, nonatomic) void(^stopCallback)(NSURL * _Nullable, NSError * _Nullable);
@end

@implementation K18RecordMaster
{
    BOOL _isRecording;

}

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)dealloc
{
    [self stopRecord:nil];
}

- (void)startRecord:(void (^)(void))succ fail:(void (^)(NSError * _Nonnull))fail
{
    K18Log(@"%@",NSHomeDirectory());
    
    [self disposeRecord];
    [self initRecord];
    
    //
    [_recorder prepareToRecord];
    [_recorder recordAtTime:0];
    
    K18Log(@"开始录音");
    if(succ){
        succ();
    }
}

- (void)stopRecord:(void (^)(NSURL * _Nullable, NSError * _Nullable))completion
{
    [self stopRecord:completion immediately:NO];
}

- (void)stopRecord:(void (^)(NSURL *recordedURL, NSError*error))completion immediately:(BOOL)immediately
{
    self.stopCallback = completion;
    
    [_recorder stop];
    K18Log(@"停止录音");
    
//    NSURL *url = [NSURL fileURLWithPath:self.recData.curFilePath];
//    if (completion) {
//        completion(url, nil);
//    }
    
}

- (void)pauseRecord
{
    self->_isRecording = NO;
    [_recorder pause];
}

- (void)resumeRecord
{
    [_recorder record];
}

- (BOOL)isRecording
{
//    return self->_isRecording;
    return _recorder.isRecording;
}

- (void) initRecord {
    
    NSString *url = self.recData.curFilePath;
    
    NSDictionary *settings = [K18AudioUtil settingWithRate:kSampleRate bitDepth:16 channels:kChannels];
    
    self.recorder = [[AVAudioRecorder alloc]initWithURL:[NSURL URLWithString:url] settings:settings error:nil];
    self.recorder.delegate = self;
    
    self.recorder.meteringEnabled = YES;
    
}

- (void) disposeRecord {
    if (self.recorder) {
        self.recorder.delegate = nil;
        [self.recorder stop];
        [self.recorder deleteRecording];
        K18Log(@"移除上一次的录音器");
    }

    self->_isRecording = NO;
    

    K18Log(@"master:销毁录音器");
}

- (float)updateMeters
{
    if (!self.recorder.isRecording) {
        return 0;
    }
    
    [self.recorder updateMeters];
    
    
    double avgVolume = 0.0;
    
    double volume = 0.0;
    
    avgVolume = [self.recorder averagePowerForChannel:0];
    avgVolume = ABS(avgVolume);
    
    volume = log10(1 + avgVolume) *1.25;//*10
    
//    NSLog(@"%@",@(volume));
    return volume;
    
}

#pragma mark - delegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    K18Log(@"录制完成, flag=%d",flag);
    if(self.stopCallback){
        self.stopCallback([NSURL URLWithString:self.recData.curFilePath], nil);
        self.stopCallback = nil;
    }
}

/* if an error occurs while encoding it will be reported to the delegate. */
- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error
{
    if(self.stopCallback){
        self.stopCallback(nil, error);
    }
}


@end
