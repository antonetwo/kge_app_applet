//
//  K18AudioUtil.h
//  18kcy
//
//  Created by beuady on 2019/8/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "lame.h"
#import <Accelerate/Accelerate.h>
#import <AVFoundation/AVFoundation.h>

#define kSampleRate 16000 //44100
#define kChannels 1
#define kRecordDir @"Records"

static int bufSize = 4096;

#define K18Log(log, ...) NSLog(@"[k18]" log, ## __VA_ARGS__)

NS_ASSUME_NONNULL_BEGIN

@interface K18AudioUtil : NSObject
/**
 耳机是否接入
 */
+ (BOOL)isHeadsetPluggedIn;

+ (NSString *)createRecordFilename;

+ (NSString *)createFileDir:(NSString *)dir;

//pcm录制配置
+ (NSDictionary *) settingWithRate:(CGFloat)samepleRate bitDepth:(int)bitDepth channels:(int)channels;

+ (lame_t) createLame:(CGFloat)inputSampleRate;

+ (void) disposeLame:(lame_t _Nonnull *_Nonnull)alame;

+ (AVAudioFile *) readFileWithUrl:(NSURL *)url;

+ (NSString *)pcm2WAV:(NSString *)pcmFilePath;

@end

NS_ASSUME_NONNULL_END
