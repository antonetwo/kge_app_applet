//
//  K18AudioUtil.m
//  18kcy
//
//  Created by beuady on 2019/8/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AudioUtil.h"
#import <AVFoundation/AVFoundation.h>
#import "lame.h"
#import <Accelerate/Accelerate.h>
#include <errno.h>

@implementation K18AudioUtil

/**
 耳机是否接入
 
 @return <#return value description#>
 */
+ (BOOL)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}

+ (NSString *)createRecordFilename {
    NSDate *date_ = [NSDate date];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
    [dateformater setDateFormat:@"yyyy-MM-dd_HH-mm-ss"];
    NSString *timeFileName = [dateformater stringFromDate:date_];
    return timeFileName;
}

+ (NSString *)createFileDir:(NSString *) dir
{
    NSString *docsDir = NSTemporaryDirectory();
    NSString *savedImagePath = [docsDir
                                stringByAppendingPathComponent:dir];
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:savedImagePath isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:savedImagePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return savedImagePath;
}

+ (NSDictionary *) settingWithRate:(CGFloat)samepleRate bitDepth:(int)bitDepth channels:(int)channels {
    NSDictionary *recordSetting = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSNumber numberWithFloat: samepleRate],AVSampleRateKey, //采样率
                                   [NSNumber numberWithInt: kAudioFormatLinearPCM],AVFormatIDKey,
                                   [NSNumber numberWithInt:bitDepth],AVLinearPCMBitDepthKey,//采样位数 默认 16
                                   [NSNumber numberWithInt: channels], AVNumberOfChannelsKey,//通道的数目,
                                   [NSNumber numberWithInt:AVAudioQualityMax], AVEncoderAudioQualityKey,  //音质
                                   
                                   nil];
    return recordSetting;
}

#pragma mark + Lame

+ (lame_t) createLame:(CGFloat)sampleRate {
    K18Log(@"createLame.sampleRate=%@",@(sampleRate));
//    int32_t sampleRate = (int32_t)((inputSampleRate) /2);
    
    lame_t alame = lame_init();
    lame_set_in_samplerate(alame, sampleRate/2);
    lame_set_VBR_mean_bitrate_kbps(alame, 96);
    lame_set_VBR(alame, vbr_off);
    lame_init_params(alame);
    
    return alame;
}

+ (void) disposeLame:(lame_t*)alame {
    if (alame) {
        lame_close(*alame);
        K18Log(@"disposeLame ok");
    }
}

+ (AVAudioFile *)readFileWithUrl:(NSURL *)url
{
    NSError *readErr;
    AVAudioFile *audioFile;
    if ([url.lastPathComponent.pathExtension isEqualToString:@""] ||
        [url.lastPathComponent.pathExtension isEqualToString:@"pcm"]) {
//        NSDictionary *settings = [self settingWithRate:kSampleRate bitDepth:16 channels:kChannels];
        audioFile = [[AVAudioFile alloc]initForReading:url commonFormat:AVAudioPCMFormatInt16 interleaved:YES error:&readErr];
        K18Log(@"readIsPcm,err=%@",readErr);

    }else{
        audioFile = [[AVAudioFile alloc]initForReading:url error:&readErr];
        K18Log(@"readIsMp3,err=%@",readErr);
    }
    
    return audioFile;
}

+ (NSString *)pcm2WAV:(NSString *)pcmFilePath
{
    NSLog(@"转换wav");
    NSString *pcmPath = pcmFilePath;
    
    NSString *wavPath = [pcmFilePath.stringByDeletingPathExtension stringByAppendingString:@".wav"];
    char *pcmPath_c = (char *)[pcmPath UTF8String];
    char *wavPath_c = (char *)[wavPath UTF8String];
    convertPcm2Wav(pcmPath_c, wavPath_c, kChannels, kSampleRate);
    return wavPath; 
    //进入沙盒找到xbMedia.wav即可
}



// pcm 转wav

//wav头的结构如下所示：

typedef  struct  {
    
    char        fccID[4];
    
    int32_t      dwSize;
    
    char        fccType[4];
    
} HEADER;

typedef  struct  {
    
    char        fccID[4];
    
    int32_t      dwSize;
    
    int16_t      wFormatTag;
    
    int16_t      wChannels;
    
    int32_t      dwSamplesPerSec;
    
    int32_t      dwAvgBytesPerSec;
    
    int16_t      wBlockAlign;
    
    int16_t      uiBitsPerSample;
    
}FMT;

typedef  struct  {
    
    char        fccID[4];
    
    int32_t      dwSize;
    
}DATA;

/*
 int convertPcm2Wav(char *src_file, char *dst_file, int channels, int sample_rate)
 请问这个方法怎么用?参数都是什么意思啊
 
 赞  回复
 code书童： @不吃鸡爪 pcm文件路径，wav文件路径，channels为通道数，手机设备一般是单身道，传1即可，sample_rate为pcm文件的采样率，有44100，16000，8000，具体传什么看你录音时候设置的采样率。
 */

int convertPcm2Wav(char *src_file, char *dst_file, int channels, int sample_rate)

{
    int bits = 16;
    
    //以下是为了建立.wav头而准备的变量
    
    HEADER  pcmHEADER;
    
    FMT  pcmFMT;
    
    DATA  pcmDATA;
    
    unsigned  short  m_pcmData;
    
    FILE  *fp,*fpCpy;
    
    if((fp=fopen(src_file,  "rb"))  ==  NULL) //读取文件
        
    {
        perror("fopen");
        printf("open pcm file %s error\n", src_file);
        
        return -1;
        
    }
    
    if((fpCpy=fopen(dst_file,  "wb+"))  ==  NULL) //为转换建立一个新文件
        
    {
        
        printf("create wav file error\n");
        
        return -1;
        
    }
    
    //以下是创建wav头的HEADER;但.dwsize未定，因为不知道Data的长度。
    
    strncpy(pcmHEADER.fccID,"RIFF",4);
    
    strncpy(pcmHEADER.fccType,"WAVE",4);
    
    fseek(fpCpy,sizeof(HEADER),1); //跳过HEADER的长度，以便下面继续写入wav文件的数据;
    
    //以上是创建wav头的HEADER;
    
    if(ferror(fpCpy))
        
    {
        
        printf("error\n");
        
    }
    
    //以下是创建wav头的FMT;
    
    pcmFMT.dwSamplesPerSec=sample_rate;
    
    pcmFMT.dwAvgBytesPerSec=pcmFMT.dwSamplesPerSec*sizeof(m_pcmData);
    
    pcmFMT.uiBitsPerSample=bits;
    
    strncpy(pcmFMT.fccID,"fmt  ", 4);
    
    pcmFMT.dwSize=16;
    
    pcmFMT.wBlockAlign=2;
    
    pcmFMT.wChannels=channels;
    
    pcmFMT.wFormatTag=1;
    
    //以上是创建wav头的FMT;
    
    fwrite(&pcmFMT,sizeof(FMT),1,fpCpy); //将FMT写入.wav文件;
    
    //以下是创建wav头的DATA;  但由于DATA.dwsize未知所以不能写入.wav文件
    
    strncpy(pcmDATA.fccID,"data", 4);
    
    pcmDATA.dwSize=0; //给pcmDATA.dwsize  0以便于下面给它赋值
    
    fseek(fpCpy,sizeof(DATA),1); //跳过DATA的长度，以便以后再写入wav头的DATA;
    
    fread(&m_pcmData,sizeof(int16_t),1,fp); //从.pcm中读入数据
    
    while(!feof(fp)) //在.pcm文件结束前将他的数据转化并赋给.wav;
        
    {
        
        pcmDATA.dwSize+=2; //计算数据的长度；每读入一个数据，长度就加一；
        
        fwrite(&m_pcmData,sizeof(int16_t),1,fpCpy); //将数据写入.wav文件;
        
        fread(&m_pcmData,sizeof(int16_t),1,fp); //从.pcm中读入数据
        
    }
    
    fclose(fp); //关闭文件
    
    pcmHEADER.dwSize = 0;  //根据pcmDATA.dwsize得出pcmHEADER.dwsize的值
    
    rewind(fpCpy); //将fpCpy变为.wav的头，以便于写入HEADER和DATA;
    
    fwrite(&pcmHEADER,sizeof(HEADER),1,fpCpy); //写入HEADER
    
    fseek(fpCpy,sizeof(FMT),1); //跳过FMT,因为FMT已经写入
    
    fwrite(&pcmDATA,sizeof(DATA),1,fpCpy);  //写入DATA;
    
    fclose(fpCpy);  //关闭文件
    
    return 0;
    
}


@end
