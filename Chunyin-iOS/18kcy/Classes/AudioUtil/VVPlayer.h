//
//  VVPlayer.h
//  SQApp
//
//  Created by 唐 on 2018/11/16.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class VVPlayer;

@protocol VVPlayerDelegate <NSObject>

- (void) vvplayer:(VVPlayer *)player onStart:(NSError *)error;

- (void) vvplayer:(VVPlayer *)player onEnd:(NSError *)error;

- (void) vvplayer:(VVPlayer *)player onCurrentTimeSec:(NSTimeInterval)currentTimeSec total:(NSTimeInterval)total;

@end

@interface VVPlayer : NSObject<VVPlayerDelegate>

@property(nonatomic, weak) id<VVPlayerDelegate> delegate;

@property(strong, nonatomic) AVPlayer *player;

@property(strong, nonatomic) NSURL *url;

- (instancetype)initWithURL:(NSURL *)URL;

- (void) play;
- (void) pause;

- (void) resetToPlay;


@end
