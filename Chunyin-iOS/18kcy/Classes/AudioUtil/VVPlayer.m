//
//  VVPlayer.m
//  SQApp
//
//  Created by 唐 on 2018/11/16.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "VVPlayer.h"

@interface VVPlayer()
@property(weak, nonatomic) id timeObserver;


@property(strong, nonatomic) id observer;
@end

@implementation VVPlayer


- (void)dealloc
{
	[self.player.currentItem removeObserver:self forKeyPath:@"status" context:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_player.currentItem];
	
}

- (instancetype)initWithURL:(NSURL *)url
{
	self = [super init];
	if (self) {
		//http://www.etangka.net/zzptadmin/Upload//BChant/20170228034958.mp3
//		url = [NSURL URLWithString:@"http://www.etangka.net/zzptadmin/Upload//BChant/20170228034958.mp3"];
		_url = url;

		AVPlayerItem *item = [AVPlayerItem playerItemWithURL:url];
		self.player = [[AVPlayer alloc] initWithPlayerItem:item];
		[self.player.currentItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
		if (@available(iOS 10.0, *)) {
			self.player.automaticallyWaitsToMinimizeStalling = NO;
		} else {
			// Fallback on earlier versions
		}
	
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:_player.currentItem];
		
	}
	return self;
}

- (void)setUrl:(NSURL *)url
{
	_url = url;
	AVPlayerItem *item = [AVPlayerItem playerItemWithURL:url];
	[self.player.currentItem removeObserver:self forKeyPath:@"status" context:nil];
	[self.player replaceCurrentItemWithPlayerItem:item];
	[self removeTimeObserver];
	[item addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:_player.currentItem];
}

- (void)play
{
	[self.player play];

	[self startTimeObserver];
}

- (void) resetToPlay {
	
	self.url = _url;
	
	[self play];
}

- (void)pause
{
	[self.player pause];
}

- (void) startTimeObserver {
//	return;//不使用
	if (self.player.status!=AVPlayerStatusReadyToPlay) {
		NSLog(@"player not ready to cal timer");
		return;
	}
	[self removeTimeObserver];
	__weak typeof(self)wself = self;
	
	self.timeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
		NSTimeInterval current = CMTimeGetSeconds(time);
		NSTimeInterval total = CMTimeGetSeconds(wself.player.currentItem.duration);
		
//		CMTime time = sself.player.currentTime;
//		NSTimeInterval currentTimeSec = time.value / time.timescale;
//		NSLog(@"currentTimeSec:%g", current);
		__strong typeof(wself)sself = wself;
		if(sself){
			if ([sself.delegate respondsToSelector:@selector(vvplayer:onCurrentTimeSec:total:)]) {
				[sself.delegate vvplayer:sself onCurrentTimeSec:current total:total];
			}
		}
		//总时间
		NSLog(@"current=%f, total=%f", current, total);
	}];
}

- (void) removeTimeObserver {
	if (self.timeObserver) {
		[self.player removeTimeObserver:self.timeObserver];
		self.timeObserver = nil;
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"status"]) {
		if(self.player.status==AVPlayerStatusReadyToPlay){
			NSLog(@"准备播放");
			[self startTimeObserver];
			if ([self.delegate respondsToSelector:@selector(vvplayer:onStart:)]) {
				[self.delegate vvplayer:self onStart:nil];
			}
						
		}else{
			//call play error
			NSLog(@"播放失败");
			if ([self.delegate respondsToSelector:@selector(vvplayer:onStart:)]) {
				[self.delegate vvplayer:self onStart:[NSError errorWithDomain:nil code:-1 userInfo:nil]];
			}
		}
	}
}

- (void) playFinished:(NSNotification *)notify {
	NSLog(@"播放完成");
	[self removeTimeObserver];
	if ([self.delegate respondsToSelector:@selector(vvplayer:onEnd:)]) {
		[self.delegate vvplayer:self onEnd:nil];
	}
}

@end
