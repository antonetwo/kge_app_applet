//
//  AEncodePCMOperation.h
//  MakeAudioDemo
//
//  Created by 唐 on 2018/11/6.
//  Copyright © 2018 beuady. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AEncodePCMOperation : NSOperation
- (id)initWithTarget:(id)tar action:(SEL)ac audioSrcPath:(NSString *)path;
- (void) getVolumnsWithPath:(NSString *)path completion:(void(^)(NSArray*)) completion;
@end

NS_ASSUME_NONNULL_END
