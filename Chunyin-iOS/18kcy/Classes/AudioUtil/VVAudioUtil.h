//
//  VVAudioUtil.h
//  MakeAudioDemo
//
//  Created by 唐 on 2018/10/29.
//  Copyright © 2018 beuady. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Recorder.h"
#import "AudioConvert.h"
typedef void(^CompletionBlock)(NSError *error);
typedef void(^OutputBlock)(NSError *error,NSString *destPath);
NS_ASSUME_NONNULL_BEGIN


@interface VVAudioUtil : NSObject

@property(strong, nonatomic) Recorder *recorder;

@property(strong, nonatomic) AudioConvert *converter;

@property(assign, nonatomic) AVFileType outputType;//默认AVFileTypeAppleM4A
@property(strong, nonatomic) NSString *presetName;

+(instancetype)shareAudioUtil;

+ (void) requestAccess:(void(^)(BOOL granted)) block;
+(AVAuthorizationStatus) getAudioAccess;

/**
 混音合成录音

 */
- (void) combine:(NSURL *)sourceURL
		srcStart:(int64_t)srcStart
		  srcEnd:(int64_t)srcEnd
		 bgmPath:(NSURL *)bgmURL
		bgmStart:(int64_t)bgmStart
		  bgmEnd:(int64_t)bgmEnd
	   srcVolume:(float)srcVolume
	   bgmVolume:(float)bgmVolume
	  completion:(nonnull OutputBlock)completion;

/**
 音频裁剪
*/
- (void) cut:(NSString *)sourcePath start:(CMTime)startTime end:(CMTime)endTime completion:(OutputBlock) completion;


/**
 编码成Mp3
 */
- (void) encode:(NSString *)sourcePath completion:(OutputBlock) completion;

/**
 音频变声
 */
- (void) changeVoiceWithPath:(NSString *)sourcePath
					   pitch:(int)pitch
						rate:(int)rate
					   tempo:(int)tempo
				  completion:(OutputBlock) completion;

- (double) getDurationWithPath:(NSString *) filePath;
- (void)getMetaInfoWithPath:(NSString *)filePath callback:(void(^)(NSDictionary *))callback;


/**
 删除合成，裁剪等临时过渡音频
 */
- (void) deleteTransitAudioFiles;

@end

NS_ASSUME_NONNULL_END
