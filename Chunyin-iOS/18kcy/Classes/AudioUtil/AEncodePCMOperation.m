//
//  AEncodePCMOperation.m
//  MakeAudioDemo
//
//  Created by 唐 on 2018/11/6.
//  Copyright © 2018 beuady. All rights reserved.
//

#import "AEncodePCMOperation.h"
#import <MAD/mad.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
//#include <sys/soundcard.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <sys/types.h>

//#include <alsa/asoundlib.h>
#define SAMPLE_RATE 44100
#define CHANNELS 2
#define PCM_DEVICE "plughw:0,0"
//static snd_pcm_hw_params_t *hwparams = NULL;
//static snd_pcm_t *pcm_handle = NULL;

struct buffer
{
	unsigned char const *start;
	unsigned long length;
};
static int decode (unsigned char const *, unsigned long);
static int exportToPcm(char const *,char const * );
//static int init_alsa ();
FILE *outFile;

@interface AEncodePCMOperation ()
{
	id target;
	SEL action;

}
@property (strong, nonatomic) NSString *audioSrcPath;
@property (strong, nonatomic) NSString *audioOutPath;
@end

@implementation AEncodePCMOperation


- (id)initWithTarget:(id)tar action:(SEL)ac audioSrcPath:(NSString *)path
{
	self = [super init];
	if (self) {
		target = tar;
		action = ac;
		self.audioSrcPath = path;
		
		NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
		self.audioOutPath = [path stringByAppendingPathComponent:@"export.pcm"];
	}
	return self;
}

- (void)main {
	[self audioEncode_TocPcm];
}

- (void) audioEncode_TocPcm {
	@try {
		int flag = exportToPcm(self.audioSrcPath.UTF8String,self.audioOutPath.UTF8String);
		
		NSMutableArray *volumns = [NSMutableArray array];
		if (flag==0) {
			if(simplest_pcm16le_volumeleft(self.audioSrcPath.UTF8String, volumns)==0){
				NSLog(@"%@", volumns);
			}
		}
		
		if (!self.isCancelled) {
			[target performSelectorOnMainThread:action withObject:(flag==0 ? self.audioOutPath : nil) waitUntilDone:NO];
		}
	} @catch (NSException *exception) {
		NSLog(@"%@",exception);
	} @finally {
		
	}
}

- (void) getVolumnsWithPath:(NSString *)path completion:(void(^)(NSArray*)) completion {
	NSMutableArray *list = [NSMutableArray array];
	@try {
		simplest_pcm16le_volumeleft(path.UTF8String, list);
	} @catch (NSException *exception) {
		NSLog(@"%@",exception);
	} @finally {
		completion(list);
	}
	
}

int simplest_pcm16le_volumeleft(char const*url,NSMutableArray* volumns){
	FILE *fp=fopen(url,"rb+");
	int cnt=0;
	unsigned char *sample=(unsigned char *)malloc(4);
	
	while(!feof(fp)){
		short *samplenum=NULL;
		fread(sample,1,4,fp);
		
		samplenum=(short *)sample;
		int db = getPcmDB(samplenum, 4);
		[volumns addObject:@(db)];
		cnt++;
	}
	printf("Sample Cnt:%d\n",cnt);
	
	free(sample);
	fclose(fp);

	return 0;
}

int getPcmDB(const unsigned char *pcmdata, size_t size) {
	
	int db = 0;
	short int value = 0;
	double sum = 0;
	for(int i = 0; i < size; i += 2)
	{
		memcpy(&value, pcmdata+i, 2); //获取2个字节的大小（值）
		sum += abs(value); //绝对值求和
	}
	sum = sum / (size / 2); //求平均值（2个字节表示一个振幅，所以振幅个数为：size/2个）
	if(sum > 0)
	{
		db = (int)(20.0*log10(sum));
	}
	return db;
}

static int exportToPcm(char const *srcPath,char const * destPath)
{
	struct stat stat;
	void *fdm;
	//	char const *file = destPath;
	int fd;
	
	char const *readFile = srcPath;
	outFile = fopen(destPath, "w+");
	fd = open (srcPath, O_RDONLY);
	if (fstat (fd, &stat) == -1 || stat.st_size == 0)
		return -1;
	fdm = mmap (0, stat.st_size, PROT_READ, MAP_SHARED, fd, 0);//将文件内容拷贝到内存里面
	if (fdm == MAP_FAILED)
		return -1;
	
	decode (fdm, stat.st_size);//进行解码
	fclose(outFile);//关闭文件指针
	
	NSLog(@"%@",NSHomeDirectory());
	if (munmap (fdm, stat.st_size) == -1)
		return -1;
	return 0;
}

//static int init_alsa ()
//{
//    snd_pcm_stream_t stream = SND_PCM_STREAM_PLAYBACK;
//    char *pcm_name;
//    int rate = SAMPLE_RATE;     /* Sample rate */
//    int exact_rate;             /* Sample rate returned by */
//    pcm_name = strdup (PCM_DEVICE);
//    snd_pcm_hw_params_alloca (&hwparams);
//    if (snd_pcm_open (&pcm_handle, pcm_name, stream, 0) < 0)
//   {
//        fprintf (stderr, "Error opening PCM device %s/n", pcm_name);
//        return -1;
//    }
//    if (snd_pcm_hw_params_any (pcm_handle, hwparams) < 0)
//    {
//        fprintf (stderr, "Can not configure this PCM device./n");
//        return -1;
//   }
//    if (snd_pcm_hw_params_set_access (pcm_handle, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED) < 0)
//    {
//        fprintf (stderr, "Error setting access./n");
//        return -1;
//    }
//    if (snd_pcm_hw_params_set_format (pcm_handle, hwparams, SND_PCM_FORMAT_S16_LE) < 0)
//    {
//        fprintf (stderr, "Error setting format./n");
//        return -1;
//    }
//    exact_rate = rate;
//    if (snd_pcm_hw_params_set_rate_near (pcm_handle, hwparams, &exact_rate, 0) < 0)
//    {
//        fprintf (stderr, "Error setting rate./n");
//        return -1;
//    }
//    if (rate != exact_rate)
//    {
//        fprintf (stderr, "The rate %d Hz is not supported by your hardware./n==> Using %d Hz instead./n", rate, exact_rate);
//    }
//    if (snd_pcm_hw_params_set_channels (pcm_handle, hwparams, CHANNELS) < 0)
//    {
//        fprintf (stderr, "Error setting channels./n");
//        return -1;
//    }
//    if (snd_pcm_hw_params (pcm_handle, hwparams) < 0)
//    {
//        fprintf (stderr, "Error setting HW params./n");
//        return -1;
//    }
//    return 0;
//}

static enum mad_flow input (void *data, struct mad_stream *stream)
{
	struct buffer *buffer = data;
	if (!buffer->length)
		return MAD_FLOW_STOP;
	mad_stream_buffer (stream, buffer->start, buffer->length);
	buffer->length = 0;
	return MAD_FLOW_CONTINUE;
}
/*这一段是处理采样后的pcm音频 */
static inline signed int scale (mad_fixed_t sample)
{
	sample += (1L << (MAD_F_FRACBITS - 16));
	if (sample >= MAD_F_ONE)
		sample = MAD_F_ONE - 1;
	else if (sample < -MAD_F_ONE)
		sample = -MAD_F_ONE;
	return sample >> (MAD_F_FRACBITS + 1 - 16);
}
static enum mad_flow output (void *data, struct mad_header const *header, struct mad_pcm *pcm)
{
	unsigned int nchannels, nsamples, n;
	mad_fixed_t const *left_ch, *right_ch;
	unsigned char Output[6912], *OutputPtr;
	int fmt, wrote, speed, exact_rate, err, dir;
	nchannels = pcm->channels;
	n = nsamples = pcm->length;
	left_ch = pcm->samples[0];
	right_ch = pcm->samples[1];
	//    fmt = AFMT_S16_LE;
	//    speed = pcm->samplerate * 2;        /*播放速度是采样率的两倍 */
	OutputPtr = Output;//将OutputPtr指向Output
	while (nsamples--)
	{
		signed int sample;
		sample = scale (*left_ch++);
		*(OutputPtr++) = sample >> 0;
		*(OutputPtr++) = sample >> 8;
		if (nchannels == 2)
		{
			sample = scale (*right_ch++);
			*(OutputPtr++) = sample >> 0;
			*(OutputPtr++) = sample >> 8;
		}
	}
	OutputPtr = Output;//由于之前的操作将OutputPtr的指针指向了最后，这时需要将其指针移动到最前面
	fwrite(OutputPtr, 1, n*2*nchannels, outFile);
	//    snd_pcm_writei (pcm_handle, OutputPtr, n);
	OutputPtr = Output;//写完文件后，OutputPtr的指针也移动到了最后，这时需要将其指针移动到最前面
	return MAD_FLOW_CONTINUE;
}
static enum mad_flow error (void *data, struct mad_stream *stream, struct mad_frame *frame)
{
	return MAD_FLOW_CONTINUE;
}
static int decode (unsigned char const *start, unsigned long length)
{
	struct buffer buffer;
	struct mad_decoder decoder;
	int result;
	buffer.start = start;
	buffer.length = length;
	mad_decoder_init (&decoder, &buffer, input, 0, 0, output, error, 0);
	mad_decoder_options (&decoder, 0);
	result = mad_decoder_run (&decoder, MAD_DECODER_MODE_SYNC);
	mad_decoder_finish (&decoder);
	return result;
	
}

@end
