//
//  K18AudioEngine.m
//  18kcy
//
//  Created by 唐 on 2019/7/15.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "K18AudioEngine.h"
#import <AVFoundation/AVFoundation.h>
#import "lame.h"
#import <Accelerate/Accelerate.h>
#import "K18RecordMaster.h"
#import "K18AudioUtil.h"
#import "MP3Encoder.h"

#define kForceUseNode 1 //0表示强制用node引擎
#define kOfflineCacheDir @"OfflineCache"

//static int bufSize = 4096;

@interface K18AudioEngine()
//录音
@property(nonatomic, strong) AVAudioEngine *engine;

@property(strong, nonatomic) K18RecDataManager *recData;
//@property(strong, nonatomic) NSMutableData *data;//缓冲区
//@property(nonatomic, readwrite) NSTimeInterval currentRecTime;//当前录音时间
@property(readwrite, nonatomic) NSTimeInterval currentPlayTime;//当前播放的时间
//@property(nonatomic) CGFloat currentVolume;//自己计算的人声音量
@property(strong, nonatomic) K18RecordMaster *recorder;

//播放
@property(strong, nonatomic) AVAudioEngine *playEngine;
@property(strong, nonatomic) NSArray *bgmURLs;
@property(strong, nonatomic) AVAudioFile *bgmFile;
@property(strong, nonatomic) CADisplayLink *displayLink;


// 引擎节点
@property(strong, nonatomic) AVAudioPlayerNode *playNode;
@property(strong, nonatomic) AVAudioUnitTimePitch *pitchNode;
@property(strong, nonatomic) AVAudioUnitReverb *reverbNode;//混响
@property(strong, nonatomic) AVAudioUnitDistortion *echoNode;//回声

@property(nonatomic) AVAudioFramePosition lastStartFramePosition;
@property(nonatomic) BOOL hookSchedulePlay;//修改播放进度的时候，会停一下，这时候hook住不回调出去
@property(nonatomic,readwrite) BOOL isPausePlay;

@property(assign, nonatomic) int recordMode;//0=扬声器, 1=耳机
@end

@implementation K18AudioEngine
{
    lame_t _lame;
    unsigned char *mp3Buffer;
	unsigned char *offlineMp3Buffer;
	
	//计算录音音量相关
//    float averagePowerForChannel0;
//    float averagePowerForChannel1;
//    float minLevel;
//    float maxLevel;
}

static K18AudioEngine *instance = nil;

+(instancetype)shared
{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] initOnce];
    });
    return instance;
}

- (void)dealloc
{
    free(mp3Buffer);//单例不清理，无法跑这里
	if(offlineMp3Buffer){
		free(offlineMp3Buffer);
	}
	[[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioEngineConfigurationChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
}

- (instancetype)initOnce
{
    self = [self init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configarationChange:) name:AVAudioEngineConfigurationChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeChange:) name:AVAudioSessionRouteChangeNotification object:nil];//耳机插拨
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
		[K18AudioUtil createFileDir:kRecordDir];
		[K18AudioUtil createFileDir:kOfflineCacheDir];
		
        self.useEarReturn = YES;
        self.useReverb = NO;
        self.recData = [K18RecDataManager new];
//        self.data = [NSMutableData data];
        mp3Buffer = (unsigned char *)malloc(bufSize);

        self.volume = 1.0;
        self.recData->minLevel = 0.1;
        self.recData->maxLevel = 0.1;
        self.recData->averagePowerForChannel0 = 0;
        self.recData->averagePowerForChannel1 = 0;
    }
    return self;
}

+ (void)setupAudioSession
{
    NSError *err;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    if (session.category == AVAudioSessionCategoryPlayAndRecord) {
        K18Log(@"session.category, %@",session.category);
        return;
    }
//    AVAudioSessionCategoryOptions options = AVAudioSessionCategoryOptionDuckOthers | AVAudioSessionCategoryOptionDefaultToSpeaker;
//    if (@available(iOS 11.0,*)) {
//        [session setCategory:AVAudioSessionCategoryPlayAndRecord mode:AVAudioSessionModeDefault options:options error:&err ];
//    }else if (@available(iOS 10.0, *)) {
//        [session setCategory:AVAudioSessionCategoryPlayAndRecord mode:AVAudioSessionModeDefault options:options error:&err];
//    } else {
//        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
//    }
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&err];

    @try {
        
//        [session setPreferredSampleRate:kSampleRate error:&err];
//        [session setPreferredIOBufferDuration:0.1 error:&err];
        [session setActive:YES error:&err];
        K18Log(@"setActive, %@",err);
    } @catch (NSException *exception) {
        K18Log(@"%@",exception);
    } @finally {
        
    }
}

#pragma mark - Public Interface

- (void)startRecord:(void (^)(void))succ fail:(void (^)(NSError * _Nonnull))fail
{
    [self startRecord:succ fail:fail isSwitch:NO];
}

- (void)startRecord:(void (^)(void))succ fail:(void (^)(NSError * _Nonnull))fail isSwitch:(BOOL)isSwitch
{
    NSError *err;
    if(isSwitch==NO){
        self.pitch = 0;
        self.reverbType = 0;
        [self.recData reset];
        if(kForceUseNode && !(self.useEarReturn && [K18AudioUtil isHeadsetPluggedIn])){
            self.recordMode = 0;
        }else{
            self.recordMode = 1;
        }
        K18Log(@"recordMode=%d",self.recordMode);
        
//        [[AVAudioSession sharedInstance] setActive:NO error:&err];
//        NSLog(@"AVAudioSession.active=NO, err=%@",err);
        
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        
    }
    __weak typeof(self)wself = self;
    if (self.recordMode == 0) {//扬声器方案
        [self.recorder startRecord:^{
            [wself startTimer];
            if(succ){
                succ();
            }
        } fail:nil];
        return;//不再运行后面
    }
    
    [self disposeRecord];
    [self initRecord];
    
    if (!self.engine.inputNode) {
        K18Log(@"audioEngine初始化失败");
        if(fail) fail([NSError errorWithDomain:@"" code:-1 userInfo:@{@"msg":@"audioEngine初始化失败"}]);
        return;
    }
	
    AVAudioNode *input = self.engine.inputNode;//TODO 应该是升降调之后的node，进行采样回调
    AVAudioFormat *format = [input inputFormatForBus:0];

    [input installTapOnBus:0 bufferSize:(AVAudioFrameCount)bufSize format:format block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
		__strong typeof(wself)sself = wself;
        float* buf = buffer.floatChannelData[0];
        if (buf!=NULL && sself) {
            int32_t frameLength = (int32_t)(buffer.frameLength/2);
            int bytes = lame_encode_buffer_interleaved_ieee_float(sself->_lame, buf, frameLength, sself->mp3Buffer, bufSize);

            if (bytes >0){
                [sself.recData.data appendBytes:sself->mp3Buffer length:bytes];
            }
            
			
			//计算音量
			CGFloat levelLowpassTrig = 0.5;
			Float32 avgValue = 0;
			vDSP_meamgv(buf, 1, &avgValue, (vDSP_Length)frameLength);
			sself.recData->averagePowerForChannel0 = (levelLowpassTrig * ((avgValue==0) ? -100.0 : 20.0 * log10f(avgValue))) + ((1-levelLowpassTrig) * sself.recData->averagePowerForChannel0);
			float volume = MIN((sself.recData->averagePowerForChannel0 + 50.0)/50.0, 1.0);
			sself.recData->minLevel = MIN(sself.recData->minLevel, volume);
			sself.recData->maxLevel = MAX(sself.recData->maxLevel, volume);
			
			sself.recData.currentVolume = ABS(volume);
            
//            sself.currentRecTime += buffer.frameLength/when.sampleRate;
//			K18Log(@"currentRecTime=%@",@(self.currentRecTime));
        }
		
    }];
	
	[self.engine prepare];
	[self.engine startAndReturnError:&err];
	if (err) {
		if (fail) {
			fail(err);
		}
		K18Log(@"%@",err);
	}else{
		[self startTimer];
		K18Log(@"开始录音");
		if(succ){
			succ();
		}
	}
	
}

- (void)stopRecord:(void (^)(NSURL * _Nullable, NSError * _Nullable))completion
{
	[self stopRecord:completion immediately:NO];
}

- (void)stopRecord:(void (^)(NSURL *recordedURL, NSError*error))completion immediately:(BOOL)immediately
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    dispatch_async(dispatch_queue_create("com.k18.queue", NULL), ^{
        
        if (self.recordMode == 0) {//扬声器方案
            __weak typeof(self)wself = self;
            [self.recorder stopRecord:^(NSURL * _Nullable recordedURL, NSError * _Nullable error) {
                
                NSString *wavPath = [K18AudioUtil pcm2WAV:recordedURL.relativePath];
                
                recordedURL = [NSURL fileURLWithPath:wavPath];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (completion) {
                        completion(recordedURL, error);
                    }
                    
                    if (!immediately) {
                        
                        if ([wself.delegate respondsToSelector:@selector(onRecordStop:duration:)]) {
                            [wself.delegate onRecordStop:recordedURL duration:wself.recData.currentRecTime];
                        }
                    }
                });
//                [self stopTimer];
            } immediately:immediately];
            
        }else{
            
            // detach nodes
            [self.engine.inputNode removeTapOnBus:0];
            
            NSString *recordFilePath = self.recData.curFilePath;
            recordFilePath = [recordFilePath.stringByDeletingPathExtension stringByAppendingString:@".mp3"];

            
            [self.recData.data writeToFile:recordFilePath atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion([NSURL fileURLWithPath:recordFilePath], nil);
                }
                
                
                if (!immediately) {
                    
                    if ([self.delegate respondsToSelector:@selector(onRecordStop:duration:)]) {
                        [self.delegate onRecordStop:[NSURL fileURLWithPath:recordFilePath] duration:self.recData.currentRecTime];
                    }
                }
                
            });
            
            //dispose record
            [self disposeRecord];
//            [self stopTimer];
        }
        
    });

}

- (void)pauseRecord
{
    if (self.recordMode == 0) {//扬声器方案
        [self.recorder pauseRecord];
    }else{
        [self.engine pause];
    }
	[self stopTimer];
}

- (void)resumeRecord
{
    if (self.recordMode == 0) {//扬声器方案
        [self.recorder resumeRecord];
    }else{
        if(!self.engine.isRunning){
            [self.engine startAndReturnError:nil];
        }
    }
}

- (BOOL)isRecording
{
    if (self.recordMode == 0) {//扬声器方案
        return [self.recorder isRecording];
    }else{
        return self.engine.isRunning;
    }
}

- (void)play
{
	if (!self.playEngine.isRunning) {
		NSError *err;
		[self.playEngine startAndReturnError:&err];
		K18Log(@"播放, err=%@",err);
		[self startTimer];
		[self.playNode play];
	}
	self.isPausePlay = NO;
}

- (void)pause
{
	self.isPausePlay = YES;
    [self.playNode pause];
    [self.playEngine pause];
	[self stopTimer];
    K18Log(@"playEngine.pause, isRunning=%d",self.playEngine.isRunning);
}

- (void)stop
{
	self.isPausePlay = NO;
    self.hookSchedulePlay = YES;
    if(self.playNode.playing){
        [self.playNode stop];
    }
    self.hookSchedulePlay = NO;
    if(self.playEngine.isRunning){
        [self.playEngine stop];
    }
    [self stopTimer];
}

- (BOOL) isPlaying{
	return self.playEngine.isRunning;
}

- (void)playFromURL:(NSURL *)url succ:(void (^)(void))succ fail:(void (^)(NSError * _Nonnull))fail
{
    K18Log(@"playFromURL");
	if (url.absoluteString.length==0) {
		if(fail)fail([NSError errorWithDomain:@"com.k18engine" code:-100 userInfo:@{@"msg":@"url为空"}]);
		return;
	}
	self.bgmURLs = @[url];
    [self disposePlay];
	NSInteger flag = [self initPlay];
	
	if(flag!=1){
		if(fail)fail([NSError errorWithDomain:@"com.k18engine" code:flag userInfo:@{@"msg":@"初始化失败"}]);
		return;
	}
	
	NSError *err;
	[self.playEngine prepare];
	[self.playEngine startAndReturnError:&err];
    if (err) {
        K18Log(@"engine start, error=%@",err);
        return;
    }
	//
	__weak typeof(self)wself = self;
	[self.playNode scheduleFile:self.bgmFile atTime:nil completionHandler:^{
		
		if (wself.hookSchedulePlay==NO){
			[wself onPlayCompletion];
		}

	}];
	[self.playNode play];
	[self startTimer];
	K18Log(@"开始播放");
	
	if (self.useReverb) {
		self.reverbType = _reverbType;//refresh
	}
    
    if (self.startPlayCallback) {
        self.startPlayCallback();
        self.startPlayCallback = nil;
    }
}

- (void)playOfflineRenderFromURL:(NSURL *)url succ:(void (^)(NSURL * _Nonnull))succ fail:(void (^)(NSError * _Nullable))fail
{
    K18Log(@"playOfflineRenderFromURL, url=%@",url);
    __weak typeof(self)wself = self;
    dispatch_async(dispatch_queue_create("com.k18.queue", DISPATCH_QUEUE_SERIAL), ^{
    __strong typeof(wself)sself = wself;
    [self stopTimer];
    [self clearOfflineRenderCache];
	if (self.playNode) {
		[self.playNode stop];
	}
//    if(!sself->offlineMp3Buffer){
//        sself->offlineMp3Buffer = (unsigned char *)malloc(bufSize);
//    }
	NSArray *tempUrls = self.bgmURLs;
	self.bgmURLs = @[url];
	[self disposePlay];
	[self initPlay];
	
    if(self.useReverb){
        self.reverbType = sself->_reverbType;//refresh
    }

    self.pitch = sself->_pitch;
    
	//初始化引擎
	__block NSError *err;
	[self.playEngine prepare];
	[self.playEngine startAndReturnError:&err];
	[self.playNode scheduleFile:self.bgmFile atTime:nil completionHandler:nil];
    
    [self.playEngine stop];//stop to offline
	
	AVAudioFormat *format = self.bgmFile.processingFormat;
    MP3Encoder *mp3Encoder = [[MP3Encoder alloc]initWithAVFormat:format];

    K18Log(@"format: %@",format);
	AVAudioFrameCount maxNumberOfFrames = 4096;
	BOOL can = [self.playEngine enableManualRenderingMode:(AVAudioEngineManualRenderingModeOffline) format:format maximumFrameCount:maxNumberOfFrames error:&err];
	K18Log(@"开启离线渲染 can=%d, err=%@",can,err);
    

	
	[self.playEngine startAndReturnError:&err];
	K18Log(@"再次运行引擎, err=%@",err);
	[self.playNode play];
	
	
	AVAudioPCMBuffer *buffer = [[AVAudioPCMBuffer alloc]initWithPCMFormat:self.playEngine.manualRenderingFormat frameCapacity:self.playEngine.manualRenderingMaximumFrameCount];
	NSString *fileName = url.lastPathComponent;
	NSString *destPath = [NSTemporaryDirectory()
						  stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.mp3",kOfflineCacheDir,fileName.stringByDeletingPathExtension]];
	K18Log(@"offline destPath = %@", destPath);
	
    AVAudioFramePosition bgmFileLength = self.bgmFile.length;
    K18Log(@"bgmFileLength=%lld",bgmFileLength);
    K18Log(@"正在转码混响音频");
    __block BOOL isErr = NO;
		while (isErr==NO && self.playEngine.manualRenderingSampleTime < bgmFileLength) {
			
			AVAudioFrameCount framesToRender = MIN(buffer.frameCapacity, bgmFileLength - self.playEngine.manualRenderingSampleTime);
			
			AVAudioEngineManualRenderingStatus status = [self.playEngine renderOffline:framesToRender toBuffer:buffer error:&err];
//            K18Log(@"renderOffline.err=%@, bgmFileLength=%lld",err, bgmFileLength);
			if (status==AVAudioEngineManualRenderingStatusSuccess) {
				
				float* buf = buffer.floatChannelData[0];
				if (buf!=NULL) {
//                    int32_t frameLength = (int32_t)(buffer.frameLength/2);
//                    int bytes;
                    
                    [mp3Encoder encodePCMData:buf len:buffer.frameLength completeBlock:^(unsigned char *encodedData, int len) {
                        if (len >0){
                            [sself.recData.data appendBytes:encodedData length:len];
                        }
                    }];
					
				}
			}else{
				isErr = YES;
				break;
			}
		}
        if(isErr){
            K18Log(@"离线渲染frame错误");
        }
        __block AVAudioFramePosition lastSampleTime = self.playEngine.manualRenderingSampleTime;
        K18Log(@"lastSampleTime=%lld", lastSampleTime);
        
        K18Log(@"转码混响音频结束");
        [self.recData.data writeToFile:destPath atomically:YES];
        
		
		[self.playEngine disableManualRenderingMode];
		[self.playEngine stop];
		self.bgmURLs = tempUrls;
		K18Log(@"关闭离线渲染 flag=%d",self.playEngine.isInManualRenderingMode);
		
		dispatch_async(dispatch_get_main_queue(), ^{
//            [self clearOfflineRenderCache];
            K18Log(@"lastSampleTime=%lld, bgmFile.length=%lld",lastSampleTime,bgmFileLength);
			if(lastSampleTime >= bgmFileLength){
				
				if (succ) {
					succ([NSURL fileURLWithPath:destPath]);
				}
				K18Log(@"渲染导出文件成功");
			}else{
                K18Log(@"合成异常");
				if (fail) {
					fail(nil);
				}
			}
		});
		
    });
}

- (void)seekToPosition:(NSTimeInterval)position
{
	self.currentPlayTime = position;
	BOOL isPlaying = self.playNode.isPlaying;
    K18Log(@"seekToPosition:%@",@(position));
	self.hookSchedulePlay = YES;
	
	if(self.playEngine.isRunning){
		
		[self.playNode stop];//
		self.hookSchedulePlay = NO;
		__weak typeof(self)wself = self;
		AVAudioFramePosition startFrame = position * self.bgmFile.processingFormat.sampleRate;
		AVAudioFrameCount frameCount = (AVAudioFrameCount)self.bgmFile.length;
		if (frameCount>100) {
			_lastStartFramePosition = startFrame;
			[self.playNode scheduleSegment:self.bgmFile startingFrame:startFrame frameCount:frameCount atTime:nil completionHandler:^{
				if (wself.hookSchedulePlay==NO){
					[wself onPlayCompletion];
				}
			}];
		}
		
		if (isPlaying) {
			[self.playNode play];
		}
	}
	
}

- (void)dispose
{
	[self disposeRecord];
	[self disposePlay];
}

- (void)clearOfflineRenderCache
{
	[K18AudioUtil disposeLame:&_lame];
	
	if(offlineMp3Buffer){
		free(offlineMp3Buffer);
		offlineMp3Buffer = NULL;
	}
	
    self.recData.data = [NSMutableData data];
	
	NSFileManager *fManager = [NSFileManager defaultManager];
	NSString *convertDir = [NSTemporaryDirectory() stringByAppendingPathComponent:kOfflineCacheDir];
	BOOL dir = NO;
	if ([fManager fileExistsAtPath:convertDir isDirectory:&dir] && dir) {
        [fManager removeItemAtPath:convertDir error:nil];
        [K18AudioUtil createFileDir:kOfflineCacheDir];
	}
}

#pragma mark - Setters

- (void)setVolume:(float)volume
{
	_volume = volume;
	self.playEngine.mainMixerNode.outputVolume = volume;
	
}

- (void)setPitch:(CGFloat)pitch
{
	_pitch = pitch;
	self.pitchNode.pitch = pitch;
    K18Log(@"setPitch=%f",pitch);
}

- (void)setReverbType:(K18ReverbType)reverbType
{
    _reverbType = reverbType;
    K18Log(@"setReverbType=%d",reverbType);
    
	switch (reverbType) {
		case K18ReverbTypeNone:
		{

			self.echoNode.wetDryMix = 0;
			self.reverbNode.wetDryMix = 0;

		}
			break;
		case K18ReverbTypeKTV:
		{
            self.echoNode.wetDryMix = 10;
			[self.echoNode loadFactoryPreset:(AVAudioUnitDistortionPresetMultiEcho1)];
			[self.reverbNode loadFactoryPreset:(AVAudioUnitReverbPresetCathedral)];
			self.reverbNode.wetDryMix = 50;
		}
			break;
        case K18ReverbTypeCixing:
        {
            [self.echoNode loadFactoryPreset:(AVAudioUnitDistortionPresetMultiEcho1)];
            self.echoNode.wetDryMix = 20;
            [self.reverbNode loadFactoryPreset:1];
            self.reverbNode.wetDryMix = 80;
            //6=空灵
            //9=悠远
            //0=老唱片
            //1=磁性
        }
            break;
        case K18ReverbTypeKongling:
        {
            self.echoNode.wetDryMix = 5;
            [self.reverbNode loadFactoryPreset:6];
            self.reverbNode.wetDryMix = 80;
           
        }
            break;
        case K18ReverbTypeYouyuan:
        {
            self.echoNode.wetDryMix = 5;
            [self.reverbNode loadFactoryPreset:9];
            self.reverbNode.wetDryMix = 80;
          
        }
            break;
		default:
		{
			self.echoNode.wetDryMix = 0;
			self.reverbNode.wetDryMix = 0;
		}
			break;
	}
}


#pragma mark - Actions & Notifications

- (void) updateProgress {
    
    if (kForceUseNode && !(self.useEarReturn && [K18AudioUtil isHeadsetPluggedIn])) {//扬声器方案
        self.recData.currentVolume = [self.recorder updateMeters];
    }    
    
    if (self.playEngine.isRunning && self.playNode && [self.delegate respondsToSelector:@selector(onPlaying:currentTime:)]) {
        AVAudioTime *time = [self.playNode playerTimeForNodeTime:self.playNode.lastRenderTime];
        self.currentPlayTime = (_lastStartFramePosition + time.sampleTime) / time.sampleRate;
        self.currentPlayTime = MAX(0, self.currentPlayTime);
        [self.delegate onPlaying:self currentTime:self.currentPlayTime];
        //        K18Log(@"updateProgress, %@",@(self.currentPlayTime));
    }
    
    if ([self.delegate respondsToSelector:@selector(record:currentTime:volume:)]) {
        self.recData.currentRecTime = self.currentPlayTime;
        [self.delegate record:self currentTime:self.recData.currentRecTime volume:self.recData.currentVolume];
    }
}

- (void) configarationChange:(NSNotification *)notify {
//    K18Log(@"configarationChange:%@", notify.object);
}

- (void) routeChange:(NSNotification *)notify {
    NSDictionary *interuptionDict = notify.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
        {
            K18Log(@"耳机插入, engine.isRunning = %d, playNode.isPlaying=%d",self.engine.isRunning,self.playNode.isPlaying);
            
        }
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
        {

            K18Log(@"耳机拔出, engine.isRunning = %d,playNode.isPlaying=%d",self.engine.isRunning,self.playNode.isPlaying);
        }
    }
}

#pragma mark - Private

- (void) onPlayCompletion {
	K18Log(@"onPlayCompletion");
	dispatch_async(dispatch_get_main_queue(), ^{
		if ([self.delegate respondsToSelector:@selector(onPlayStop:)]) {
			[self.delegate onPlayStop:self];
		}
	});
}

- (NSTimeInterval)playDuration
{
	AVAudioFrameCount frameCount = (AVAudioFrameCount)self.bgmFile.length;
	if (frameCount==0) {
		return 1;
	}
	double sampleRate = self.bgmFile.processingFormat.sampleRate;
	return frameCount / sampleRate;
}

- (void) startTimer
{
//    K18Log(@"startTimer");
	if(!self.displayLink){
		self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateProgress)];
		[self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	}
}

- (void) stopTimer
{
//    K18Log(@"stopTimer");
	if (self.displayLink) {
		[self.displayLink invalidate];
		self.displayLink = nil;
	}
}

- (NSInteger) initPlay {
    K18Log(@"initPlay");
    if(!self.playEngine){
        self.playEngine = [[AVAudioEngine alloc]init];
    }
	
	if (self.bgmURLs.firstObject) {
		NSURL *url = self.bgmURLs.firstObject;
        self.bgmFile = [K18AudioUtil readFileWithUrl:url];
		if(!self.bgmFile){
			K18Log(@"加载播放文件失败");
            return -1;
		}
	}
	
	// attach nodes by sort
	self.playNode = [[AVAudioPlayerNode alloc]init];
	[self.playEngine attachNode:self.playNode];
	
	NSMutableArray *connectNodes = [NSMutableArray array];
	[connectNodes addObject:self.playNode];
	
	self.pitchNode = [[AVAudioUnitTimePitch alloc]init];
	[self.playEngine attachNode:self.pitchNode];
	self.pitchNode.pitch = self.pitch;
    K18Log(@"初始化pitch=%f",self.pitch);
	[connectNodes addObject:self.pitchNode];
	
	if (self.useReverb) {
		self.echoNode = [[AVAudioUnitDistortion alloc]init];
		self.echoNode.wetDryMix = 0;
		[self.playEngine attachNode:self.echoNode];
		[connectNodes addObject:self.echoNode];

		self.reverbNode = [[AVAudioUnitReverb alloc]init];
		[self.playEngine attachNode:self.reverbNode];
		[connectNodes addObject:self.reverbNode];
	}
	
	[connectNodes addObject:self.playEngine.mainMixerNode];
    
    // connect nodes
    [self connectingNodes:connectNodes];
    
    self.playEngine.mainMixerNode.outputVolume = self.volume;
    K18Log(@"initPlay end");
	return 1;
}

- (void) connectingNodes:(NSArray *)nodes {
	// connect nodes
	for (int i=0; i<nodes.count-1; i++) {
		[self.playEngine connect:nodes[i] to:nodes[i+1] format:self.bgmFile.processingFormat];
	}
}

- (void)disposePlay {
	self.hookSchedulePlay = YES;
	[self.playNode stop];
	[self.playNode reset];
	self.hookSchedulePlay = NO;
	
	if (self.playEngine){
		[self.playEngine stop];
		[self.playEngine reset];
	}
	
	if(self.playNode){
		[self.playEngine detachNode:self.playNode];
        self.playNode = nil;
    }
    if(self.pitchNode){
		[self.playEngine detachNode:self.pitchNode];
    }
    if(self.reverbNode){
        [self.playEngine detachNode:self.reverbNode];
    }
    if(self.echoNode){
        [self.playEngine detachNode:self.echoNode];
	}
	
	// reset properties
	self.bgmFile = nil;
	self.lastStartFramePosition = 0;
	self.currentPlayTime = 0;
	self.isPausePlay = NO;
	
//    self.useEarReturn = YES;
	
	K18Log(@"销毁播放器");
}

- (void) initRecord {
    self.engine = [[AVAudioEngine alloc]init];
    
    if (!self.engine) {
        K18Log(@"engine AVAudioEngine 初始化失败");
        return;
    }
	
	
    AVAudioNode *inputNode = self.engine.inputNode;
    AVAudioFormat *format = [inputNode inputFormatForBus:0];
	NSMutableArray *connectNodes = [NSMutableArray array];
	[connectNodes addObject:inputNode];
	
	//last setup
	if (self.useEarReturn && [K18AudioUtil isHeadsetPluggedIn]) {// 耳返效果
		K18Log(@"初始化使用耳返效果");
        [connectNodes addObject:self.engine.outputNode];

//        [connectNodes addObject:self.engine.mainMixerNode];
    }else{
		K18Log(@"初始化没有使用耳返");
		[connectNodes addObject:self.engine.mainMixerNode];
    }
    self.engine.mainMixerNode.outputVolume = 1.0;
    [self.engine connect:inputNode to:connectNodes.lastObject format:format];
    
	
	_lame = [K18AudioUtil createLame:format.sampleRate];
}

- (void) disposeRecord {
    [K18AudioUtil disposeLame:&_lame];

	
	if (self.engine) {
		[self.engine stop];
		[self.engine reset];
	}
	
//    self.data = [NSMutableData data];
	
	K18Log(@"销毁录音器");
}


- (int) bitDeptchByCommontFormat:(AVAudioCommonFormat)commonFormat {
	switch (commonFormat) {
		case AVAudioOtherFormat:
			return 8;
			break;
		case AVAudioPCMFormatFloat32:
		case AVAudioPCMFormatInt16:
			return 16;
		case AVAudioPCMFormatInt32:
			return 32;
		case AVAudioPCMFormatFloat64:
			
		default:
			return 8;
			break;
	}
	return 0;
}


- (K18RecordMaster *)recorder
{
    if (!_recorder) {
        _recorder = [K18RecordMaster new];
        _recorder.recData = self.recData;
    }
    return _recorder;
}


@end

@implementation K18RecDataManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void) reset{
    self.currentRecTime = 0;
    self.data = [NSMutableData  data];
    
    NSString *fileName = [K18AudioUtil createRecordFilename];
    NSString *recordFilePath = [NSTemporaryDirectory()
                                stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.pcm",kRecordDir,fileName]];
    self.curFilePath = recordFilePath;
}


@end
