//
//  AudioView.m
//  MakeAudioDemo
//
//  Created by 唐 on 2018/11/6.
//  Copyright © 2018 beuady. All rights reserved.
//

#import "AudioView.h"

#define SOUND_METER_COUNT       40
#define HUD_SIZE       300
#define kLineWidth 2.f
@implementation AudioView

- (instancetype)init
{
	self = [super init];
	if (self) {

	}
	return self;
}

- (void)setVolumnDatas:(NSArray *)volumnDatas
{
	_volumnDatas = volumnDatas;
	[self setNeedsDisplay];
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = UIGraphicsGetCurrentContext();
	
//	UIColor *strokeColor = [UIColor colorWithRed:0.886 green:0.0 blue:0.0 alpha:0.8];
//	UIColor *fillColor = [UIColor colorWithRed:0.5827 green:0.5827 blue:0.5827 alpha:1.0];
//	UIColor *gradientColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
//	UIColor *color = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
	
	// 绘制波形
	[[UIColor blackColor] set];
	CGContextSetLineWidth(context, 1.0);
	CGContextSetLineJoin(context, kCGLineJoinRound);
	
	// 基准线
	int baseLine = self.bounds.size.height/2;
	// 因数
	int multiplier = 1;
	// 音量最大值
	int maxLengthOfWave = 50;
	// 画出的波形的最大值
	int maxValueOfMeter = baseLine;
	
	NSArray *soundMeters;
	if (self.volumnDatas.count<40) {
		soundMeters = self.volumnDatas;
	}else{
		NSRange range = NSMakeRange(self.volumnDatas.count-40, 40);
		soundMeters = [self.volumnDatas subarrayWithRange:range];
	}
	CGRect hudRect = self.bounds;
	
	// 绘制一个类似波形的折线图
	for(CGFloat x = 0; x < soundMeters.count; x++)
	{
		// 基数位置的音量 设置为 -1
		multiplier = ((int)x % 2) == 0 ? 1 : -1;
		// y 是波形的顶点 （波峰 或者 波谷） = baseLine + 波形的相对长度 * multiplier
		NSInteger each = [soundMeters[(int)x] intValue];
//		CGFloat y = baseLine + ((maxValueOfMeter * labs(each)) / maxLengthOfWave) * multiplier;
		CGFloat y1 = baseLine + ((maxValueOfMeter * (2 + labs(each))) / maxLengthOfWave) * 1;
		CGFloat y2 = baseLine + ((maxValueOfMeter * (2 + labs(each))) / maxLengthOfWave) * -1;
		CGContextMoveToPoint(context, x * (10) + hudRect.origin.x + 4, y1);
		CGContextAddLineToPoint(context, x * (10) + hudRect.origin.x + 4, y2);
		
	}
	CGContextStrokePath(context);
	
}


@end
