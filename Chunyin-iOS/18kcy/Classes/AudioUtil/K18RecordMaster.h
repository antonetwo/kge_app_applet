//
//  K18RecordMaster.h
//  18kcy
//
//  Created by beuady on 2019/8/12.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class K18RecDataManager;

NS_ASSUME_NONNULL_BEGIN

@interface K18RecordMaster : NSObject
{
    
}
@property(strong, nonatomic) K18RecDataManager *recData;

//@property(strong, nonatomic) NSMutableData *data;//缓冲区
//@property(nonatomic, readwrite) NSTimeInterval currentRecTime;//当前录音时间
//@property(nonatomic) CGFloat currentVolume;//自己计算的人声音量

-(void)startRecord:(nullable void(^)(void)) succ
              fail:(nullable void(^)(NSError *_Nullable err)) fail;
-(void) stopRecord:(nullable void(^)(NSURL *_Nullable recordedURL, NSError*_Nullable error)) completion;
-(void) stopRecord:(nullable void(^)(NSURL *_Nullable recordedURL, NSError*_Nullable error)) completion immediately:(BOOL)immediately;
-(void) pauseRecord;
-(void) resumeRecord;
-(BOOL) isRecording;
-(void)disposeRecord;

-(float) updateMeters;

@end

NS_ASSUME_NONNULL_END
