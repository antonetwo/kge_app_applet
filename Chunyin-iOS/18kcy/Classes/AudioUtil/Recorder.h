//
//  RecordInputView.h
//  InPutBoxView
//
//  Created by chuliangliang on 15-1-6.
//  Copyright (c) 2015年 chuliangliang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIKit.h>

#define DefaultSubPath @"Records" //默认 二级目录 

#define SampleRateKey 8000.0 //采样率
#define LinearPCMBitDepth 16 //采样位数 默认 16
#define NumberOfChannels 1  //通道的数目

@protocol RecorderDelegate <NSObject>
/**
 * 录音进行中
 * currentTime 录音时长
 **/
-(void)recorderCurrentTime:(NSTimeInterval)currentTime;

/**
 * 录音完成
 * filePath 录音文件保存路径
 * fileName 录音文件名
 * duration 录音时长
 **/
-(void)recorderStop:(NSString *)filePath voiceName:(NSString *)fileName duration:(NSTimeInterval)duration;

/**
 * 开始录音
 **/
-(void)recorderStart:(NSError *)error;
@end

@interface Recorder : NSObject<AVAudioRecorderDelegate>

@property (assign, nonatomic) id<RecorderDelegate> recorderDelegate;
@property(strong, nonatomic) NSString *filename;//文件名
@property(strong, nonatomic) NSString *filePath;//文件完整路径
@property(assign, nonatomic) NSTimeInterval fileDuration;//录音时长
@property(assign, nonatomic) BOOL isLastRecordMic;//当前录音是否用mic

/**
 * 录音控件 单例对象
 **/
+(Recorder *)shareRecorder;


/**
 * 开始录音
 * //默认的录音存储的文件夹在 "Document/Voice/文件名(文件名示例: 2015-01-06_12:41).wav"
 * 录音的文件名 "2015-01-06_12:41"
 **/
-(void)startRecord;
/**
 * 停止录音
 **/
-(void)stopRecord;

-(void) pauseRecord;
-(void) resumeRecord;
- (BOOL) isRecording;

/**
 * 获得峰值
 **/
-(float)getPeakPower;
-(float)getPeakVolume;

/**
 * 是否可以录音
 **/
- (BOOL)canRecord;

- (BOOL) deleteRecord;

@end
