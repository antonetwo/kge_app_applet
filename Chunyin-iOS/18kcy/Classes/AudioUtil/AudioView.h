//
//  AudioView.h
//  MakeAudioDemo
//
//  Created by 唐 on 2018/11/6.
//  Copyright © 2018 beuady. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface AudioView : UIView
@property(strong, nonatomic) NSArray *volumnDatas;
@property int bias;
@property int wSize;  
@end

NS_ASSUME_NONNULL_END
