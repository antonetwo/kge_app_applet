//
//  ExtAudioFileMixer.h
//  18kcy
//
//  Created by 唐 on 2019/7/24.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExtAudioFileMixer : NSObject
+ (OSStatus)mixAudio:(NSString *)audioPath1
			andAudio:(NSString *)audioPath2
             volume1:(CGFloat)volume1
             volume2:(CGFloat)volume2
			  toFile:(NSString *)outputPath
  preferedSampleRate:(float)sampleRate;

@end

NS_ASSUME_NONNULL_END
