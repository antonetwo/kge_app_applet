//
//  VVAudioUtil.m
//  MakeAudioDemo
//
//  Created by 唐 on 2018/10/29.
//  Copyright © 2018 beuady. All rights reserved.
//

#import "VVAudioUtil.h"
#import <AVFoundation/AVFoundation.h>
#import "K18AudioUtil.h"

@interface VVAudioUtil()<AudioConvertDelegate, AudioConvertDelegate>
@property(nonatomic,copy) OutputBlock changeVoiceBlock;
@property(nonatomic,copy) OutputBlock encodeBlock;
@end

@implementation VVAudioUtil

+(instancetype)shareAudioUtil
{
	static VVAudioUtil *shared = nil;
	static dispatch_once_t predicate;
	dispatch_once(&predicate, ^{
		shared = [[self alloc] init];
	});
	return shared;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.outputType = AVFileTypeAppleM4A;
		self.presetName = AVAssetExportPresetAppleM4A;
	}
	return self;
}

+ (void) requestAccess:(void(^)(BOOL granted)) block {
	if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio]==AVAuthorizationStatusNotDetermined){
		[AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
			block(granted);
		}];
	}else{
		
	}
}

+ (AVAuthorizationStatus)getAudioAccess
{
	return [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
}

- (void) combine:(NSURL *)sourceURL
		srcStart:(int64_t)srcStart
		  srcEnd:(int64_t)srcEnd
		 bgmPath:(NSURL *)bgmURL
		bgmStart:(int64_t)bgmStart
		  bgmEnd:(int64_t)bgmEnd
	   srcVolume:(float)srcVolume
	   bgmVolume:(float)bgmVolume
	  	completion:(nonnull OutputBlock)completion
{
	NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
	NSString *resultPath = [documentDir stringByAppendingPathComponent:@"result.m4a"];
		
	if(!sourceURL){
		if(completion) completion([NSError errorWithDomain:@"sourcePath not exist" code:0 userInfo:nil],nil);
		return;
	}
	if(!bgmURL){
		if(completion) completion([NSError errorWithDomain:@"bgmPath not exist" code:0 userInfo:nil],nil);
		return;
	}
	
	if([[NSFileManager defaultManager] fileExistsAtPath:resultPath]){
		NSError *errr = nil;
		[[NSFileManager defaultManager] removeItemAtPath:resultPath error:&errr];
		K18Log(@"%@",errr);
	}
	
	//AVURLAsset子类的作用则是根据NSURL来初始化AVAsset对象.
	//	AVURLAsset *videoAsset1 = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:sourcePath] options:nil];
	AVAsset *asset = [AVAsset assetWithURL:sourceURL];
	AVAsset *bgmAsset = [AVAsset assetWithURL:bgmURL];
	
	AVAssetTrack *assetTrack = [[asset tracksWithMediaType:AVMediaTypeAudio] firstObject];
	AVMutableComposition *composition = [AVMutableComposition composition];
	
    NSError *err;
	AVMutableCompositionTrack *compositionTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
	CMTimeRange timeRange = CMTimeRangeMake(CMTimeMake(srcStart, 1), CMTimeMake(srcEnd, 1));
	[compositionTrack insertTimeRange:timeRange ofTrack:assetTrack atTime:kCMTimeZero error:&err];
    K18Log(@"-[compositionTrack insertTimeRange], err=%@",err);
	
	AVAssetTrack *bgmTrack = [[bgmAsset tracksWithMediaType:AVMediaTypeAudio]firstObject];
	AVMutableCompositionTrack *compositionTrack2 = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
	CMTimeRange bgmRange = CMTimeRangeMake(CMTimeMake(bgmStart, 1), CMTimeMake(bgmEnd, 1));
	[compositionTrack2 insertTimeRange:bgmRange ofTrack:bgmTrack atTime:kCMTimeZero error:&err];
	K18Log(@"-[compositionTrack2 insertTimeRange], err=%@",err);
	
	AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
	//音量控制
	AVMutableAudioMixInputParameters *inputParams = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:compositionTrack];
    [inputParams setVolume:srcVolume atTime:kCMTimeZero];
    //[inputParams setVolumeRampFromStartVolume:srcVolume toEndVolume:srcVolume timeRange:]
	AVMutableAudioMixInputParameters *bgmInputParams = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:compositionTrack2];
	[bgmInputParams setVolume:bgmVolume atTime:kCMTimeZero];
	audioMix.inputParameters = @[inputParams,bgmInputParams];
	
	
	//输出
	AVAssetExportSession *exporeSession = [AVAssetExportSession exportSessionWithAsset:composition presetName:self.presetName];
	exporeSession.audioMix = audioMix;
	exporeSession.outputFileType = self.outputType;
	exporeSession.outputURL = [NSURL fileURLWithPath:resultPath];
	exporeSession.shouldOptimizeForNetworkUse = NO;
	__weak AVAssetExportSession* wexporeSession = exporeSession;
	[exporeSession exportAsynchronouslyWithCompletionHandler:^{
		//exporeSession.status
		if(wexporeSession.status!=AVAssetExportSessionStatusCompleted){
			if(completion){
				completion([NSError errorWithDomain:@"AVAssetExportSessionStatus" code:wexporeSession.status userInfo:nil],nil);
			}
		}else{
			completion(nil, resultPath);
		}
	}];
}

- (void) cut:(NSString *)sourcePath start:(CMTime)startTime end:(CMTime)endTime completion:(OutputBlock) completion
{
	if(!sourcePath.length){
		if(completion) completion([NSError errorWithDomain:@"" code:-404 userInfo:nil], nil);
		return;
	}
	
	AVURLAsset *videoAsset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:sourcePath]];
	//音频输出会话
	//AVAssetExportPresetAppleM4A: This export option will produce an audio-only .m4a file with appropriate iTunes gapless playback data(输出音频,并且是.m4a格式)
	AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:videoAsset presetName:self.presetName];
	
	//设置输出路径 / 文件类型 / 截取时间段
	NSString *cutOutPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"cut.m4a"];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:cutOutPath]) {
		[[NSFileManager defaultManager] removeItemAtPath:cutOutPath error:nil];
	}
	
	exportSession.outputURL = [NSURL fileURLWithPath:cutOutPath];
	exportSession.outputFileType = self.outputType;
	exportSession.timeRange = CMTimeRangeFromTimeToTime(startTime, endTime);
	__weak AVAssetExportSession* wexporeSession = exportSession;
	[exportSession exportAsynchronouslyWithCompletionHandler:^{
		//exporeSession.status
		if(wexporeSession.status==AVAssetExportSessionStatusCompleted){
			
			if(completion) completion(nil, cutOutPath);
			
		}else{
			
			if(completion)
				completion([NSError errorWithDomain:@"AVAssetExportSessionStatus" code:wexporeSession.status userInfo:nil],nil);
		}
	}];

}

- (void) changeVoiceWithPath:(NSString *)sourcePath
					   pitch:(int)pitch
						rate:(int)rate
					   tempo:(int)tempo
				  completion:(OutputBlock) completion
{
	AudioConvertConfig dconfig;
	dconfig.sourceAuioPath = [sourcePath UTF8String];
	dconfig.outputFormat = AudioConvertOutputFormat_WAV;
	dconfig.outputChannelsPerFrame = 1;
	dconfig.outputSampleRate = 22050;
	dconfig.soundTouchPitch = pitch;
	dconfig.soundTouchRate = rate;
	dconfig.soundTouchTempoChange = tempo;
	[[AudioConvert shareAudioConvert] audioConvertBegin:dconfig withCallBackDelegate:self];
	self.changeVoiceBlock = completion;
}

- (void) encode:(NSString *)sourcePath completion:(OutputBlock)completion
{
	self.encodeBlock = completion;
	[[AudioConvert shareAudioConvert] audioConvertBeginEncode:sourcePath withCallBackDelegate:self audioOutputSampleRate:8000 audioOutputFormat:(AudioConvertOutputFormat_MP3) audioOutputChannelsPerFrame:2];
	
}

- (double)getDurationWithPath:(NSString *)filePath
{
	if(!filePath) return 0;
	AVURLAsset * asset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:filePath] options:nil];
	return CMTimeGetSeconds(asset.duration);
}

- (void)getMetaInfoWithPath:(NSString *)filePath callback:(void(^)(NSDictionary *))callback
{
	if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		callback(nil);
		return;
	}
	AVURLAsset * asset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:filePath] options:nil];
	[asset loadValuesAsynchronouslyForKeys:@[@"availableMetadataFormats"] completionHandler:^{
		NSMutableDictionary *dic = [NSMutableDictionary dictionary];
		dic[@"duration"] = @(CMTimeGetSeconds(asset.duration));
//		AVMetadataFormat format = asset.availableMetadataFormats.firstObject;
		for (AVMetadataItem *item in asset.commonMetadata) {
			NSLog(@"%@, %@", item.commonKey, item.value);
			if ([item.commonKey isEqualToString:AVMetadataCommonKeyArtist]) {
				dic[@"artist"] = item.value;
			}
			if ([item.commonKey isEqualToString:AVMetadataCommonKeyTitle]) {
				dic[@"title"] = item.value;
			}
		}
		callback(dic);
	}];
}

- (void)deleteTransitAudioFiles
{
	NSString *cutOutPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"cut_tmp.m4a"];
	NSFileManager *fManager = [NSFileManager defaultManager];
	if([fManager fileExistsAtPath:cutOutPath]){
		[fManager removeItemAtPath:cutOutPath error:nil];
	}
	
	NSString *combineOutPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"combine.m4a"];
	if([fManager fileExistsAtPath:combineOutPath]){
		[fManager removeItemAtPath:combineOutPath error:nil];
	}
	
//	NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
//	NSString *resultPath = [documentDir stringByAppendingPathComponent:@"result.m4a"];
//	if([fManager fileExistsAtPath:resultPath]){
//		[fManager removeItemAtPath:resultPath error:nil];
//	}
	NSString *document = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *convertDir = [document stringByAppendingPathComponent:@"AudioConvert"];
	BOOL dir = NO;
	if ([fManager fileExistsAtPath:convertDir isDirectory:&dir] && dir) {
		[fManager removeItemAtPath:convertDir error:nil];
	}
	
}

#pragma mark - getter

- (AudioConvert *)converter
{
	if (!_converter) {
		_converter = [AudioConvert shareAudioConvert];
	}
	return _converter;
}

- (Recorder *)recorder
{
	if (!_recorder) {
		_recorder = [Recorder shareRecorder];
	}
	return _recorder;
}

#pragma mark - AudioConvertDelegate
- (BOOL)audioConvertOnlyDecode
{
	return  NO;
}
- (BOOL)audioConvertHasEnecode
{
	return YES;
}

/**
 * 对音频变声动作的回调
 **/
- (void)audioConvertSoundTouchSuccess:(NSString *)audioPath
{
	[[NSFileManager defaultManager] removeItemAtPath:OUTPUT_DECODEPATH error:nil];
	
	//编码完成
	if(self.changeVoiceBlock){
		self.changeVoiceBlock(nil, audioPath);
	}
	self.changeVoiceBlock = nil;
}

- (void)audioConvertSoundTouchFail
{
	//编码失败
	if(self.changeVoiceBlock){
		self.changeVoiceBlock([NSError errorWithDomain:@"audioConvertEncodeFaild" code:100 userInfo:nil],nil);
	}
	self.changeVoiceBlock = nil;
}

/**
 * 对音频编码动作的回调
 **/

- (void)audioConvertEncodeSuccess:(NSString *)audioPath
{
	//编码完成
	if(self.changeVoiceBlock){
		self.changeVoiceBlock(nil, audioPath);
		self.changeVoiceBlock = nil;
	}
	
	if (self.encodeBlock) {
		self.encodeBlock(nil, audioPath);
		self.encodeBlock = nil;
	}
}

- (void)audioConvertEncodeFaild
{
	//编码失败
	//编码失败
	if(self.changeVoiceBlock){
		self.changeVoiceBlock([NSError errorWithDomain:@"audioConvertEncodeFaild" code:100 userInfo:nil],nil);
		self.changeVoiceBlock = nil;
	}
	
	if (self.encodeBlock) {
		self.encodeBlock([NSError errorWithDomain:@"audioConvertEncodeFaild" code:100 userInfo:nil], nil);
		self.encodeBlock = nil;
	}
}

@end
