//
//  DES3EncryptUtil.h
//  18kcy
//
//  Created by 唐 on 2019/6/6.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DES3EncryptUtil : NSObject

+ (NSString*)encrypt:(NSString*)plainText;

// 解密方法
+ (NSString*)decrypt:(NSString*)encryptText;
@end

NS_ASSUME_NONNULL_END
