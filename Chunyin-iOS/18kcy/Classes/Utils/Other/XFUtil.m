//
//  XFUtil.m
//  XFile
//
//  Created by SunYi on 12-8-29.
//  Copyright (c) 2012年 深圳元度科技有限公司. All rights reserved.
//

#import "XFUtil.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@implementation XFUtil

//拨打电话
+ (void)dailWithPhone:(NSString *)phone{
    NSString * deviceType = [UIDevice currentDevice].model;
    if ([deviceType rangeOfString:@"iPhone" options:NSCaseInsensitiveSearch].length > 0){
        NSString *phoneCallURL = [NSString stringWithFormat:@"telprompt://%@",phone] ;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallURL]];
    }
}

//自动布局子View
+ (void)scrollView:(UIScrollView *)scrollView addSubView:(UIView *(^)(NSInteger index))viewBlock count:(NSInteger)count size:(CGSize)size{
    int colCount=(int)(scrollView.bounds.size.width)/(int)(size.width);
    int colRemain=(int)(scrollView.bounds.size.width)%(int)(size.width);
    if (colRemain<colCount*8) {
        colCount -= 1;
    }
    long rowCount = count%colCount?count/colCount+1:count/colCount;
    
    float totalSpace = scrollView.bounds.size.width-colCount*size.width;
    float space = totalSpace/(colCount+1);
    
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    for (int i=0; i<count; i++) {
        NSInteger row = i / colCount;
        NSInteger col = i % colCount;
        
        frame.origin.x = col * (frame.size.width+space) + space;
        frame.origin.y = row * (frame.size.height+space) +space;
        
        UIView *view = viewBlock(i);
        if (view) {
            [view setFrame:frame];
            [scrollView addSubview:view];
        }
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, rowCount *(frame.size.height+space) +space);
}

+ (void)saveSession:(NSDictionary *)data{
    NSString *sessionString = [data objectForKey:@"Set-Cookie"];
    if (sessionString.length > 0) {
        NSRange rangeBegin = [sessionString rangeOfString:@"SESSION="];
        NSString *tempOne = [sessionString substringFromIndex:(rangeBegin.location + rangeBegin.length)];
        NSRange rangeEnd = [tempOne rangeOfString:@";"];
        NSString *tempTwo = [tempOne substringToIndex:(rangeEnd.location)];
//        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        [userDefault setSession:tempTwo];
    }
}

+ (NSMutableArray *)parseString:(NSString *)imageString{
    if(imageString.length == 0)return nil;
    NSArray *array = [imageString componentsSeparatedByString:@"|"];
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    for (NSString *string in array) {
        [imageArray addObject:string];
    }
    return imageArray;
}

+ (NSNumber *)calculateResultWithAmount:(NSNumber *)amount month:(NSNumber *)month rate:(NSNumber *)rate{
    double businessTemp = pow(1+rate.doubleValue, month.integerValue);
    
    //月均还款
    double businessMonthAmount = amount.floatValue*rate.floatValue*businessTemp/(businessTemp-1);
    
    return @(businessMonthAmount);
}


@end
