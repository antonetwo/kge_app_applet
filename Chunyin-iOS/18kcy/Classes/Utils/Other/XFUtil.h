//
//  XFUtil.h
//  XFile
//
//  Created by SunYi on 12-8-29.
//  Copyright (c) 2012年 深圳元度科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface XFUtil : NSObject

//拨打电话
+ (void)dailWithPhone:(NSString *)phone;

//自动布局子View
+ (void)scrollView:(UIScrollView *)scrollView addSubView:(UIView *(^)(NSInteger index))viewBlock count:(NSInteger)count size:(CGSize)size;

+ (void)saveSession:(NSDictionary *)data;

+ (NSMutableArray *)parseString:(NSString *)imageString;


//房贷计算
+ (NSNumber *)calculateResultWithAmount:(NSNumber *)amount month:(NSNumber *)month rate:(NSNumber *)rate;

@end
