//
//  NSString+XTExtension.m
//  XTFramework
//
//  Created by Qing Xiubin on 13-8-15.
//  Copyright (c) 2013年 XT. All rights reserved.
//

#import "NSString+XTExtension.h"
#import <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

@implementation NSString (XTString)

+ (NSString *)UUID{
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    
    CFStringRef stringRef = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
    CFRelease(uuidRef);
    
    NSString *uuidString = [(__bridge NSString *)stringRef lowercaseString];
    CFRelease(stringRef);
    
    return uuidString;
}

+ (NSString *)mechineID{
    NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
    
    NSString *mechineID = [userInfo objectForKey:@"mechineID"];
    if (mechineID == nil) {
        mechineID = [NSString UUID];
        [userInfo setObject:mechineID forKey:@"mechineID"];
        [userInfo synchronize];
    }
    
    return mechineID;
}

+ (NSString *)uniqueID{
    NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
    
    NSString *uniqueID = [userInfo objectForKey:@"uniqueID"];
    if (uniqueID == nil) {
        uniqueID = [@"IOS" stringByAppendingString:[[self UUID] md5Digest]];
        [userInfo setObject:uniqueID forKey:@"uniqueID"];
        [userInfo synchronize];
    }
    
    return uniqueID;
}

+ (NSString *)randomString:(NSUInteger)length{
    char data[length];
    for (int x=0;x<length;data[x++] = (char)('a' + (arc4random_uniform(26))));
    return [[NSString alloc] initWithBytes:data length:length encoding:NSUTF8StringEncoding];
}

- (NSString *)URLEncodedString
{
    NSString * outputStr = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                             NULL,
                                                                             (__bridge CFStringRef)self,
                                                                             NULL,
                                                                             (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                             kCFStringEncodingUTF8);
    return outputStr;
}

@end


@implementation NSString (XTDigest)

- (NSString *)md5Digest{
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++){
        [hash appendFormat:@"%02x", result[i]];
    }
    return hash;
}


- (NSString *)sha1
{
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i=0; i<CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

- (NSString *)sha1Digest{
    const char *cStr = [self UTF8String];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(cStr, (CC_LONG)strlen(cStr), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++){
        [hash appendFormat:@"%02x", result[i]];
    }
    
    return hash;
}


- (NSString *)hmacSha1Digest:(NSString *)key{
    const char *cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cStr = [self UTF8String];
    
    uint8_t cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cStr, strlen(cStr), cHMAC);
    
    NSString *hash;
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++){
        [output appendFormat:@"%02x", cHMAC[i]];
    }
    
    hash = output;
    
    return hash;
}

- (NSString*)hmacSha256Digest:(NSString *)key{
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [self UTF8String];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    NSString *hashString = [hash description];
    
    hashString = [hashString stringByReplacingOccurrencesOfString:@" " withString:@""];
    hashString = [hashString stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hashString = [hashString stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    return hashString;
}

- (NSString *)hmacMD5Digest:(NSString *)key{
    const char *cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [self cStringUsingEncoding:NSUTF8StringEncoding];
    const unsigned int blockSize = 64;
    char ipad[blockSize];
    char opad[blockSize];
    char keypad[blockSize];
    
    unsigned long keyLen = strlen(cKey);
    CC_MD5_CTX ctxt;
    if (keyLen > blockSize) {
        CC_MD5_Init(&ctxt);
        CC_MD5_Update(&ctxt, cKey, (CC_LONG)keyLen);
        CC_MD5_Final((unsigned char *)keypad, &ctxt);
        keyLen = CC_MD5_DIGEST_LENGTH;
    }
    else {
        memcpy(keypad, cKey, keyLen);
    }
    
    memset(ipad, 0x36, blockSize);
    memset(opad, 0x5c, blockSize);
    
    int i;
    for (i = 0; i < keyLen; i++) {
        ipad[i] ^= keypad[i];
        opad[i] ^= keypad[i];
    }
    
    CC_MD5_Init(&ctxt);
    CC_MD5_Update(&ctxt, ipad, blockSize);
    CC_MD5_Update(&ctxt, cData, (CC_LONG)strlen(cData));
    unsigned char md5[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(md5, &ctxt);
    
    CC_MD5_Init(&ctxt);
    CC_MD5_Update(&ctxt, opad, blockSize);
    CC_MD5_Update(&ctxt, md5, CC_MD5_DIGEST_LENGTH);
    CC_MD5_Final(md5, &ctxt);
    
    const unsigned int hex_len = CC_MD5_DIGEST_LENGTH*2+2;
    char hex[hex_len];
    for(i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        snprintf(&hex[i*2], hex_len-i*2, "%02x", md5[i]);
    }
    
    NSData *HMAC = [[NSData alloc] initWithBytes:hex length:strlen(hex)];
    NSString *hash = [[NSString alloc] initWithData:HMAC encoding:NSUTF8StringEncoding];

    return hash;
}

- (NSString *)cutCom{
    
    if(self.length > 0){
        NSArray *urlArray =  [self componentsSeparatedByString:@"|"];
        
        NSString *finalUrl = nil;
        
        NSRegularExpression *regularExpretion=[NSRegularExpression regularExpressionWithPattern:@".*(.com)[^/]*"
                                                                                        options:0
                                                                                          error:nil];
        for(NSString *url in urlArray){
            NSArray* match = [regularExpretion matchesInString:url options:NSMatchingReportCompletion range:NSMakeRange(0, [url length])];
            
            if (match.count != 0)
            {
                for (NSTextCheckingResult *matc in match)
                {
                    NSRange range = [matc range];
                    
                    if(finalUrl == nil){
                        finalUrl = [url substringFromIndex:range.length];
                    }else{
                        finalUrl = [finalUrl stringByAppendingFormat:@"|%@",[url substringFromIndex:range.length]];
                    }
                    
                }
            }
        }
        
        return finalUrl;
    }
    
    return nil;
}

- (NSString *)addCom{
    if(self.length > 0){
        
        
        NSString *finalUrl = nil;
        
       
        
        return finalUrl;
    }

    return nil;
}

@end





@implementation NSString (XTDate)

static NSDateFormatter *dateFormatter = nil;
- (NSDate *)dateWithFormate:(NSString *)formate{
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
    }
    [dateFormatter setDateFormat:formate];
    return [dateFormatter dateFromString:self];
}

+ (NSString *)getDateStringWithTimeStr:(long)str formatter:(NSString *)formatter
{
    NSTimeInterval time = str;//传入的时间戳str如果是精确到毫秒的记得要/1000
    NSDate *detailDate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; //实例化一个NSDateFormatter对象
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:formatter];
    NSString *currentDateStr = [dateFormatter stringFromDate: detailDate];
    return currentDateStr;
}

+ (NSInteger )getbetweenTime:(NSTimeInterval)time1 time2:(NSTimeInterval)time2
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd-HH:MM:ss"];
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:time1];
    NSDate* date2 = [NSDate dateWithTimeIntervalSince1970:time2];
    NSTimeInterval seconds = [date2 timeIntervalSinceDate:date];
    
    return seconds;
}

+ (NSString *)seconsToMinHourWithTime:(NSInteger)seconds
{
    //format of hour
    NSString *str_hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@时%@分%@秒",str_hour,str_minute,str_second];
    
    return format_time;
}

+ (NSString *)getDateDisplayString:(long long) miliSeconds
{
    NSLog(@"-时间戳---%lld_----",miliSeconds);
    
    NSTimeInterval tempMilli = miliSeconds;
    NSTimeInterval seconds = tempMilli;
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    
    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:myDate];
    
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc ] init ];
    
    //2. 指定日历对象,要去取日期对象的那些部分.
    NSDateComponents *comp =  [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday fromDate:myDate];
    
    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy-MM-dd hh:mm";
    } else {
        if (nowCmps.day==myCmps.day) {
            dateFmt.AMSymbol = @"上午";
            dateFmt.PMSymbol = @"下午";
            dateFmt.dateFormat = @"aaa hh:mm";
            
        } else if((nowCmps.day-myCmps.day)==1) {
            dateFmt.AMSymbol = @"上午";
            dateFmt.PMSymbol = @"下午";
            dateFmt.dateFormat = @"昨天 aaahh:mm";
            
        } else {
            if ((nowCmps.day-myCmps.day) <=7) {
                
                dateFmt.AMSymbol = @"上午";
                dateFmt.PMSymbol = @"下午";
                
                switch (comp.weekday) {
                    case 1:
                        dateFmt.dateFormat = @"星期日 aaahh:mm";
                        break;
                    case 2:
                        dateFmt.dateFormat = @"星期一 aaahh:mm";
                        break;
                    case 3:
                        dateFmt.dateFormat = @"星期二 aaahh:mm";
                        break;
                    case 4:
                        dateFmt.dateFormat = @"星期三 aaahh:mm";
                        break;
                    case 5:
                        dateFmt.dateFormat = @"星期四 aaahh:mm";
                        break;
                    case 6:
                        dateFmt.dateFormat = @"星期五 aaahh:mm";
                        break;
                    case 7:
                        dateFmt.dateFormat = @"星期六 aaahh:mm";
                        break;
                    default:
                        break;
                }
            }else {
                dateFmt.dateFormat = @"yyyy-MM-dd hh:mm";
            }
        }
    }
    return [dateFmt stringFromDate:myDate];
}

@end


@implementation NSString (XTRegex)

- (BOOL)match:(NSString *)expression{
	NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
	if ( nil == regex ){
		return NO;
    }
	
	NSUInteger numberOfMatches = [regex numberOfMatchesInString:self
                                                        options:0
                                                          range:NSMakeRange(0, self.length)];
	if ( 0 == numberOfMatches ){
		return NO;
    }
    
	return YES;
}

- (BOOL)matchAnyOf:(NSArray *)array{
	for ( NSString * str in array ){
		if ( NSOrderedSame == [self compare:str options:NSCaseInsensitiveSearch] ){
			return YES;
		}
	}
	return NO;
}

- (BOOL)isNumberOfDecimal:(NSUInteger)length{
    NSString *tempString = self;
    if (![tempString match:@"^[.0-9]+$"]) {
        return NO;
    }
    if ([tempString hasPrefix:@"00"]) {
        return NO;
    }
    NSRange tempRange = [tempString rangeOfString:@"."];
    if (tempRange.location!=NSNotFound) {
        tempString = [tempString substringFromIndex:tempRange.location+1];
        if (tempString.length>length) {
            return NO;
        }
        tempRange = [tempString rangeOfString:@"."];
        if (tempRange.location!=NSNotFound) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)isTelephone{
	NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_Telephone];
	return [pred evaluateWithObject:self];
}

- (BOOL)isIdCard{
    NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_IDCard];
    return [pred evaluateWithObject:self];
}

- (BOOL)isMobilphone{
	NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_Mobilephone];
	return [pred evaluateWithObject:self];
}

- (BOOL)isMobilphoneCode{
    NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_MobilephoneCoede];
    return [pred evaluateWithObject:self];
}

- (BOOL)isUserName{
	NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_UserName];
	return [pred evaluateWithObject:self];
}

- (BOOL)isPassword{
	NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_Password];
	return [pred evaluateWithObject:self];
}

- (BOOL)isEmail{
	NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_Email];
	return [pred evaluateWithObject:self];
}

- (BOOL)isUrl{
	NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex_Url];
	return [pred evaluateWithObject:self];
}

@end


@implementation NSString (XTSize)

- (CGSize)sizeWithFont:(UIFont *)font byWidth:(CGFloat)width{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize size = [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil].size;
	return CGSizeMake((int)size.width+1,(int)(size.height+1));
}

- (CGSize)sizeWithFont:(UIFont *)font byWidth:(CGFloat)width textLineSpacing:(CGFloat)textHeight{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:textHeight];//调整行间距
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize size = [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil].size;
    return CGSizeMake((int)size.width+1,(int)(size.height+1));
}

- (CGSize)sizeWithFont:(UIFont *)font byHeight:(CGFloat)height textLineSpacing:(CGFloat)textHeight{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    [paragraphStyle setLineSpacing:textHeight];//调整行间距
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize size = [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height)
                                     options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil].size;
    return CGSizeMake((int)size.width+1,(int)size.height);
}

- (CGSize)sizeWithFont:(UIFont *)font byHeight:(CGFloat)height{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize size = [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height)
                                     options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil].size;
    return CGSizeMake((int)size.width+1,(int)size.height);
}


@end

@implementation  NSString (XTAttribute)

- (NSMutableAttributedString *)attributedWithColor:(UIColor *)color font:(UIFont *)font{
    
    NSMutableParagraphStyle * style=[NSMutableParagraphStyle new];
    style.alignment = NSTextAlignmentLeft;
    style.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,
                                color,NSForegroundColorAttributeName,
                                style.copy,NSParagraphStyleAttributeName,
                                nil];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc]initWithString:self
                                                                                       attributes:attributes];
    return attributedString;
}

- (NSMutableAttributedString *)attributedWithColor:(UIColor *)color font:(UIFont *)font lineSpacing:(CGFloat)textHeight
{
    NSMutableParagraphStyle * style=[NSMutableParagraphStyle new];
    style.alignment = NSTextAlignmentLeft;
    style.lineBreakMode = NSLineBreakByCharWrapping;
    style.lineSpacing = textHeight;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,
                                color,NSForegroundColorAttributeName,
                                style.copy,NSParagraphStyleAttributeName,
                                nil];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc]initWithString:self
                                                                                       attributes:attributes];
    return attributedString;
}


- (NSMutableAttributedString *)attributedWithColor:(UIColor *)color font:(UIFont *)font lineSpacing:(CGFloat)textHeight lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    NSMutableParagraphStyle * style=[NSMutableParagraphStyle new];
    style.alignment = NSTextAlignmentLeft;
    style.lineBreakMode = lineBreakMode;
    style.lineSpacing = textHeight;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,
                                color,NSForegroundColorAttributeName,
                                style.copy,NSParagraphStyleAttributeName,
                                nil];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc]initWithString:self
                                                                                       attributes:attributes];
    return attributedString;
}

- (NSMutableAttributedString *)AttributedString:(NSString *)name content:(NSString *)content
{
    // 富文本技术：
    // 1.图文混排
    // 2.随意修改文字样式
    
    //拿到整体的字符串
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",name,content]];

    
    // 设置 name 颜色，也可以设置字体大小等
//    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#576b95"] range:NSMakeRange(0, name.length)];
    
    // 创建图片图片附件
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"detail_icon_caption_black_normal_14x14_"];
    attach.bounds = CGRectMake(0, 0, 14, 14);
    NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
    
    //将图片插入到合适的位置
    [string insertAttributedString:attachString atIndex:0];
    
    return string;
    
}


@end

