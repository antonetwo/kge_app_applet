//
//  AppDelegate.m
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import "AppDelegate.h"
#import "ColorMacro.h"
#import "K18LoginController.h"
#import "LoginService.h"
#import "K18UserModel.h"
#import "UMService.h"
#import <UMShare/UMShare.h>
#import "TCLog.h"
#import "K18Common.h"

@import TXLiteAVSDK_Smart;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	NSLog(@"%@",NSHomeDirectory());
    
    // 请参考 https://cloud.tencent.com/document/product/454/34750 获取License, 小直播配置
    [TXLiveBase setLicenceURL:@"http://license.vod2.myqcloud.com/license/v1/c89d23ea387bd0998333e9d803cd0883/TXLiveSDK.licence" key:@"1518a3b13cd08702bfdd4e4f98fe7544"];
    
    //初始化log模块
    [TXLiveBase sharedInstance].delegate = [TCLog shareInstance];
    
	[[UINavigationBar appearance] setBarTintColor:k18_TopicC];
	[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
	
	[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	self.window.backgroundColor = [UIColor whiteColor];
    
    //友盟初始化
    [UMService initUM];
    
    //界面初始化
    if ([K18UserModel.shared autoLogin] && NEED_LOGIN_EVERYTIMES==0) {
        [LoginService enterMainUI];
    }else{
        [LoginService enterLoginUI];
    }
    
    //接口初始化
    [K18UserModel init:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
        if(!error){
            NSString *blackStr = K18UserModel.shared.blackStr;
            if(blackStr.length){
                //TODO 拉黑处理
                [HUDHelper alert:blackStr action:^{
                    [LoginService enterLoginUI];
                }];
                return;
            }
            
            //
            if ([K18UserModel.shared autoLogin]) {
                [LoginService autoLogin:^{
                    
                }];
            }
        }else{
            
        }
    }];
	
    [self setCommonStyle];
#if DEBUG==NO
    [NSThread sleepForTimeInterval:2];//启动页停留3s
#endif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:k18_LoginSuccessEvent object:nil];

	return YES;
}

- (void) setCommonStyle {
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:0.1], NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateNormal];

//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(NSIntegerMin, NSIntegerMin) forBarMetrics:UIBarMetricsDefault];
    //设置返回图片，防止图片被渲染变蓝，以原图显示
//    [UINavigationBar appearance].backIndicatorTransitionMaskImage = [[UIImage imageNamed:@"back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    [UINavigationBar appearance].backIndicatorImage = [[UIImage imageNamed:@"back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// 支持所有iOS系统
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}


#pragma mark - public

- (void)enterMainUI
{
    [LoginService enterMainUI];
}

- (void)enterLoginUI
{
    [LoginService enterLoginUI];
}

- (void) loginSuccess:(NSNotification *) notify {
        NSString *myId = notNil(k18_loginUserId);
        [self sendWithPath:kAPI_getUserInfo params:@{@"cyUserId":myId} succ:^(id  _Nonnull data) {
            NSLog(@"k18:更新个人信息详情成功");
            K18UserModel.shared.currentUserDetail = [K18User yy_modelWithJSON:data];
            NSLog(@"k18:currentUserDetail=%@",[K18UserModel.shared.currentUserDetail yy_modelToJSONString]);
            
            if (K18UserModel.shared.currentUserDetail.cyImg.length==0) {
                K18UserModel.shared.currentUserDetail.cyImg = K18UserModel.shared.currentUser.cyImg;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:k18IMSyncUserInfoEvent object:nil];
            
        } failed:^(NSError * _Nonnull error) {
            NSLog(@"k18:更新个人信息详情失败, %@",error);
        }];
    
}

#pragma mark - public end

@end
