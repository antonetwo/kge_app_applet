//
//  AppDelegate.h
//  18kcy
//
//  Created by 唐 on 2019/6/3.
//  Copyright © 2019 502papapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseAppDelegate.h"
#import "K18TabBarController.h"

@interface AppDelegate : TCBaseAppDelegate

@property(strong, nonatomic) K18TabBarController *mainTabController;

- (void)enterMainUI;
- (void)enterLoginUI;

@end

