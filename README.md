# K歌APP 和 小程序

#### 介绍

K歌APP（Android和iOS版）：集成了歌词播放功能、录歌、原唱伴奏切换、升降调、简单的混音等功能，此外集成了腾讯直播和聊天消息功能

K歌小程序：集成歌词播放、原唱伴奏切换、录歌等功能

#### 使用说明

1.  下载源码，在相应的开发环境中打开，编译运行
2.  App下载链接：https://wulinger.oss-cn-shenzhen.aliyuncs.com/apk/app-debug.apk
3.  代码仅供学习交流使用，如需商用，请联系QQ（835695733）

#### 界面截图

APP界面截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214203_d3576999_95365.jpeg "01.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214229_1ba9a1c6_95365.jpeg "02.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214247_7cbfd3d9_95365.jpeg "03.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214256_53716936_95365.jpeg "04.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214406_43665234_95365.jpeg "05.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214421_1a20da17_95365.jpeg "06.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214436_a284a542_95365.jpeg "07.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214454_803b757d_95365.jpeg "08.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214506_8bcc6ce9_95365.jpeg "09.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214800_6f200771_95365.jpeg "10.jpeg") 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/214822_1025a897_95365.jpeg "直播-观众端.jpg")     


小程序界面截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215503_a3deb91c_95365.jpeg "x01.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215519_54fe24e1_95365.jpeg "02.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215535_91273f9c_95365.jpeg "x03.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215547_5c97fb2f_95365.jpeg "x04.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215600_88e97888_95365.jpeg "x05.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215611_5d5f61e9_95365.jpeg "x06.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215622_cfafe615_95365.jpeg "x07.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215635_62f1a69b_95365.jpeg "x08.jpeg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215650_01302e9f_95365.jpeg "x09.jpeg")     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0226/215706_3836efc6_95365.jpeg "x10.jpeg")


